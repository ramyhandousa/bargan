<?php

namespace App\Console\Commands;

use App\Libraries\oneSignal;
use App\Models\Order;
use App\Models\Setting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class NotifyUserAboutInterestingCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offer_users:NotifyUserAboutInterestingCategories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'NotifyUserAboutInterestingCategories';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $time_now = Carbon::now()->toDateTimeString();

        $system_interesting_categories = Setting::getBody('notify_users_about_interesting_categories');

        $end_time_order =  Carbon::now()->subHours($system_interesting_categories)->toDateTimeString();

        Order::whereNull('offer_id')->whereHas('offers')->whereStatus('pending')
            ->whereBetween('end_bidding_duration',[$end_time_order , $time_now ])
            ->select(['id','category_id','user_id'])
            ->chunk(100, function ($orders) {

                foreach ($orders as $order){
                    // collect id to not send offer because there send before
                    $user_offers = $order->offers_pending->where('notify_before','=',0)->where('is_pay','=',0)
                        ->pluck('user_id')->toArray();

                    $user_offers_unique  =  array_unique( $user_offers);

                    $users = User::where([
                        ['id','!=',$order->user_id],
                        ['is_active_phone','=',1],
                        ['disable_notifications','=',0],
                        ['is_suspend','=',0],
                    ])->whereNotIn('id',$user_offers_unique)
                        ->whereHas('categories',function ($category) use ($order){

                            $category->where('category_id',$order->category_id)
                                ->orWhereHas('parent',function ($parent)  use ($order){

                                    $parent->where('id',$order->category_id);
                                })->orWhereHas('children',function ($category_child) use ($order){

                                    $category_child->where('id',$order->category_id );
                                });
                        })->select('id')->with('devices')->get();

                    // Collect Devices
                    $devices = $users?
                        $users->pluck('devices')->flatten() ?
                            $users->pluck('devices')->flatten()->pluck('device')
                            :null
                        :null;

                    $content_user_en    = Lang::get('order.notify_users_about_interesting_categories',["order_name" => $order->title ],'en') ;
                    $content_user_ar    = Lang::get('order.notify_users_about_interesting_categories',["order_name" => $order->title],'ar') ;
                    if (count($devices) > 0) {
                        $this->push->sendMessage($devices, [
                            'order_id' => $order->id, 'title' => "orders",
                        ], $content_user_en, $content_user_ar, "orders", "orders");
                    }

                    // To Stop Send More to make offers because end before
                    $order->offers_pending->where('notify_before','=',0)
                        ->where('is_pay','=',0)->each->update(['notify_before' => 1]);

                }
            });
    }
}

<?php

namespace App\Console\Commands;

use App\Libraries\oneSignal;
use App\Models\Order;
use App\Models\Setting;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Lang;

class NotifyUserOrderHaveOffers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Order_Man:NotifyUserOrderHaveOffers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = ' Notify User Order Have Offers pls take any action';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
        parent::__construct();
    }


    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $time_system_notify_before_about_offers = Setting::getBody('owner_order_notify_before_about_offers');

        $time_now = \Carbon\Carbon::now()->toDateTimeString();

        $end_time_order = \Carbon\Carbon::now()->subHours($time_system_notify_before_about_offers)->toDateTimeString();

        Order::whereHas('user',function ($user){
            $user->where('disable_notifications', 0)->where('is_suspend',0);
        })->whereNull('offer_id')->whereNotifyBefore(0)
            ->whereHas('offers')->whereStatus('pending')
            ->whereBetween('end_bidding_duration',[$end_time_order , $time_now ])
            ->with(['filter_user.devices'])
            ->select(['id','user_id','title','notify_before'])
            ->chunk(100, function ($orders) use ($time_system_notify_before_about_offers) {
                foreach ($orders as $order){
                    // update notify_before to make sure not send again
                        $order->update(['notify_before' => 1 ]);

                    $devices = $order->filter_user->devices?->pluck('device');
                    $title          = "orders";

                    $content_user_en    = Lang::get('order.owner_order_notify_before_about_offers',["time_system" => $time_system_notify_before_about_offers ],'en') ;
                    $content_user_ar    = Lang::get('order.owner_order_notify_before_about_offers',["time_system" => $time_system_notify_before_about_offers],'ar') ;
                    if (count($devices) > 0) {
                        $this->push->sendMessage($devices, [
                            'order_id' => $order->id, 'title' => $title,
                        ], $content_user_en, $content_user_ar, "orders", "orders");
                    }
                }
            });


    }
}

<?php

namespace App\Console\Commands;

use App\Libraries\InsertNotification;
use App\Libraries\oneSignal;
use App\Libraries\PushNotification;
use App\Models\Offer;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class OfferEndBiddingDuration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'endBiddingDuration:OfferEndBiddingDuration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'finish Offer where time out false';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public $push;
    public  $notify;

    public function __construct(InsertNotification $notification ,oneSignal $push)
    {

        $this->push = $push;
        $this->notify = $notification;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = Carbon::now()->format('Y-m-d H:i');

        $offers = Offer::where([
            ['end_bidding_duration' ,'!=',null],
            ['end_bidding_duration' ,'<',$now],
            ['time_out','=',0],
            ['is_pay','=',0],
        ])->with('order.user.devices')->get();

        if (count($offers) > 0):

            foreach ($offers as $offer):

                $all_devices = $offer['order']['user']['devices'] ? collect($offer['order']['user']['devices'])->pluck('device')->toArray() : null;

                $offer->update([ 'time_out' => 1  ]);

                $notify = $this->notify->NotificationDbType(13,$offer->order->user_id,null, null,$offer->order,$offer);

                if (count($all_devices) > 0){

//                    $this->push->sendPushNotification($all_devices, null, $notify['title'], $notify['body'],
//                        [
//                            'id'                => $notify['id'],
//                            'title_key'         => $notify['title'],
//                            'body_key'          => $notify['translation'],
//                            'order_id'          =>  $notify['order_id'],
//                            'offer_id'          =>  $notify['offer_id'],
//                            'body_arguments'    => [
//                                "order_name"     => $offer->order->title,
//                                "user_name"     => $offer->user->name,
//                            ]
//                        ]
//                    );

                    $order_name = $offer->order->title;
//                    $user_name =  $offer->user->name;
                    $user_name =  $offer->rand_user_id;
                    $title_ar      = Lang::get('order.'.$notify['title'],[],'ar') ;
                    $title_en      =  Lang::get('order.'.$notify['title'],[],'en') ;
                    $content_en    = Lang::get('order.'.$notify['translation'],["order_name" => $order_name ,"user_name" => $user_name ],'en') ;
                    $content_ar    = Lang::get('order.'.$notify['translation'],["order_name" => $order_name ,"user_name" => $user_name],'ar') ;


                    $this->push->sendMessage($all_devices, [
                        'id'                => $notify['id'],
                        'title_key'         => $notify['title'],
                        'body_key'          => $notify['translation'],
                        'order_id'          =>  $notify['order_id'],
                        'offer_id'          =>  $notify['offer_id'],
                        'body_arguments'    => [
                            "order_name"     => $order_name,
                            "user_name"     =>   $user_name,
                        ]
                    ],$content_en,$content_ar,$title_en,$title_ar);
                }


            endforeach;

        endif;

    }
}

<?php

namespace App\Console\Commands;

use App\Libraries\InsertNotification;
use App\Libraries\oneSignal;
use App\Libraries\PushNotification;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class OrderEndBiddingDuration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'endBiddingDuration:OrderEndBiddingDuration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'finish order where time out false';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public $push;
    public  $notify;


    public function __construct(InsertNotification $notification ,oneSignal $push)
    {

        $this->push = $push;
        $this->notify = $notification;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = Carbon::now()->format('Y-m-d H:i');

        $orders = Order::where([
            ['end_bidding_duration' ,'!=',null],
            ['offer_id' ,'=',null],
            ['end_bidding_duration' ,'<',$now],
            ['time_out','=',0],
            ['is_accepted','=',1],
        ])->with('user.devices','offers_pending.user.devices')->get();

        if (count($orders) > 0):

            foreach ($orders as $order):

                $offer_users = $order['offers_pending'] ? collect($order['offers_pending'])->pluck('user.devices') : null;

                $offer_devices = $offer_users ? $offer_users->flatten()->pluck('device')->toArray() : null;

                $user_devices = $order['user']['devices'] ? collect($order['user']['devices'])->pluck('device')->toArray() : null;

//                $all_devices = collect($user_devices)->merge($offer_devices);

                    $order->update([ 'time_out' => 1  ]);
                    $notify = $this->notify->NotificationDbType(9,$order->user_id,null, null,$order);


                        $title_ar      = Lang::get('order.'.$notify['title'],[],'ar') ;
                        $title_en      =  Lang::get('order.'.$notify['title'],[],'en') ;
                        $content_en    = Lang::get('order.'.$notify['translation'],['order_name' => $order->title ],'en') ;
                        $content_ar    = Lang::get('order.'.$notify['translation'],['order_name' => $order->title ],'ar') ;


                        $this->push->sendMessage($user_devices, [
                                    'id'                => $notify['id'],
                                    'title_key'         => $notify['title'],
                                    'body_key'          => $notify['translation'],
                                    'order_id'          =>  $notify['order_id'],
                                    'body_arguments'    => [
                                        "order_name"     => $order->title
                                    ]
                        ],$content_en,$content_ar,$title_en,$title_ar);


                        $this->push->sendMessage($offer_devices, [
                            'id'                => $notify['id'],
                            'title_key'         => $notify['title'],
                            'body_key'          => "offer_end_bidding_duration",
                            'order_id'          =>  $notify['order_id'],
                            'body_arguments'    => [
                                "order_name"     => $order->title
                            ]
                        ],$content_en,$content_ar,$title_en,$title_ar);


            endforeach;

        endif;

    }
}

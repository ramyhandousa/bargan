<?php

namespace App\Console\Commands;

use App\Libraries\InsertNotification;
use App\Libraries\oneSignal;
use App\Models\Order;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class OrderRate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'OrderRate:OrderRateUserAndOffer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send Notification To User And offer man To rate Order';

    public $push;
    public  $notify;

    public function __construct(InsertNotification $notification ,oneSignal $push)
    {

        $this->push = $push;
        $this->notify = $notification;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $orders = Order::with('user.devices','offer_man.devices')->whereCanRate(0)->whereStatus('finish')->get();

        $time_rate_system = Setting::getBody('time_order_rate');

        if (count($orders) > 0){
            foreach ($orders as $order):


                $now = Carbon::now()->format('Y-m-d H:i');

                if ($now >= Carbon::parse($order->updated_at)->addMinutes($time_rate_system)->format('Y-m-d H:i')){

                    $order->update(['updated_at' => Carbon::now()->addMinutes(Setting::getBody('resend_rate_time')) , 'can_rate' => 1]);


                    $user_devices = $order['user']['devices'] ? collect($order['user']['devices'])->pluck('device')->toArray() : null;

                    $offer_devices = $order['offer_man'] ? collect($order['offer_man']['devices'])->pluck('device')->toArray() : null;

                    $this->send_notification(14,$order,$offer_devices);

                    $this->send_notification(15,$order,$user_devices);

                }

            endforeach;
        }

    }


    public function send_notification($type , $order , $devices){

        if ($type == 14){

            $user_name = $order->offer_man->name;
            $notify = $this->notify->NotificationDbType(14,$order->offer_man,$order->user,null,$order,$order->my_offer);

        }else{

            $user_name = $order->user->name;

            $notify = $this->notify->NotificationDbType(15,$order->user,$order->offer_man,null,$order,$order->my_offer);
        }

            $title_ar      = Lang::get('order.'.$notify['title'],[],'ar') ;
            $title_en      =  Lang::get('order.'.$notify['title'],[],'en') ;
            $content_en    = Lang::get('order.'.$notify['translation'],['order_name' => $order->title ,'user_name' => $user_name ],'en') ;
            $content_ar    = Lang::get('order.'.$notify['translation'],['order_name' => $order->title ,'user_name' => $user_name ],'ar') ;

            $this->push->sendMessage($devices, [
                'id'                => $notify['id'],
                'title_key'         => $notify['title'],
                'body_key'          => $notify['translation'],
                'order_id'          =>  $notify['order_id'],
                'body_arguments'    => [
                    "order_name"     => $order->title
                ]
            ],$content_en,$content_ar,$title_en,$title_ar);
    }
}

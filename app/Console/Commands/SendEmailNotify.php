<?php

namespace App\Console\Commands;

use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendEmailNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendEmail:sendEmailnotify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command sendEmailnotify';

    public $push;

    public function __construct( PushNotification $push)
    {

        $this->push = $push;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $users = User::where('is_active_email',0)->where('send_notification',0)->with('devices')->get();

        if (count($users) > 0):


//            foreach ($users as $user):
//
//                $all_devices = $user['devices'] ? collect($user['devices'])->pluck('device')->toArray() : null;
//
//                $user->update([ 'is_active_email' => 1  , 'send_notification' => 1 ]);
//
//
//                if (count($all_devices) > 0){
//
//                    $this->push->sendPushNotification($all_devices, null, "email_activation", "your_email_has_been_activated",
//                        [
//                            'type'              => 4,
//                            'title_key'         => "email_activation",
//                            'body_key'          => "your_email_has_been_activated",
//                            'body_arguments'    => [
//                                "user_id" => $user->id ,
//                            ],
//                        ]
//                    );
//                }
//
//
//            endforeach;

        endif;
    }
}

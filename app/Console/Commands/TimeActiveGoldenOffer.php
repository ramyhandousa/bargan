<?php

namespace App\Console\Commands;

use App\Libraries\oneSignal;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class TimeActiveGoldenOffer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'TimeActiveGoldenOffer:TimeActiveGoldenOffer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'finish golden offer make golden offer or complete order ';


    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $now = Carbon::now()->format('Y-m-d H:i');

        $orders = Order::where([
            ['is_accepted','=',1],['is_suspend','=',0],
            ['golden_offer','=',1], ['active_golden_offer','=',1],
            ['end_bidding_duration' ,'<=',$now], ['status' ,'=','pending' ],
            ['time_out','=',0]
        ])->whereHas('my_offer',function ($offer){
            $offer->where('is_pay',1);
        })->with('user.devices')->get();

        if (count($orders) > 0){

            foreach ($orders as $order){
                // في وقت علشان ينهي الطلب
                $time_active_golden = Setting::getBody('time_active_golden_offer');
                $time_to_end_order = Carbon::parse($order->end_bidding_duration)->addMinutes($time_active_golden);

                $title          = "orders";
                $title_ar      = Lang::get('order.'.$title,[],'ar') ;
                $title_en      =  Lang::get('order.'.$title,[],'en') ;
                $body_user      = "end_bidding_duration_golden_offer";
                $body_argument  = [ "order_name"    => $order->title ];
                $offer = $order->my_offer;
                $order->update([ 'time_out' => 1 ,'end_bidding_duration' => $time_to_end_order ]);
                $this->notification_database($order ,$offer , $title,$body_argument, $body_user );

                $user_devices = $order['user']['devices'] ? collect($order['user']['devices'])->pluck('device')->toArray() : null;

                if (count($user_devices) > 0){
                    $this->send_public_notifications($user_devices , $order , $offer , $title, $title_ar ,$title_en ,$body_user);
                }

            }
        }


    }


    public function notification_database($order ,$offer , $title,$body_argument, $body_user  ){
        $data = [
            'order_id'     => $order->id,'offer_id'  => $offer->id, 'title'        => $title,
            'body_arguments' => json_encode($body_argument) , 'created_at'   => now()
        ];
        Notification::create(array_merge($data,[
            'user_id' => $order->user_id,'sender_id' => $order->user_offer_id ,"translation" => $body_user,"body" => $body_user
        ]));

    }


    public function send_public_notifications($devices , $order , $offer , $title, $title_ar ,$title_en ,$body_text){

        $content_user_en    = Lang::get('order.'.$body_text,["order_name" => $order->title ],'en') ;
        $content_user_ar    = Lang::get('order.'.$body_text,["order_name" => $order->title],'ar') ;

        $this->push->sendMessage($devices,[
            'order_id'      => $order->id,
            'offer_id'      => $offer->id,
            'title'         => $title,
            'body'          => $body_text,
        ],$content_user_en,$content_user_ar,$title_en,$title_ar);

    }
}

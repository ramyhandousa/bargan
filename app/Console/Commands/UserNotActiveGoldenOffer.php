<?php

namespace App\Console\Commands;

use App\Events\RefuseOtherOffersWithOutAcceptedOffer;
use App\Libraries\oneSignal;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class UserNotActiveGoldenOffer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:UserNotActiveGoldenOffer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'order golden offer equal 1 and user not active golden offer after time system
                                we should send notification';

    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // Order golden_offer == 1
        // User Should Active Golden Offer but he don`t active
        // Or User Active Golden Offer but Not Take any action
        // System make Time To active Golden offer
        // We Should Send Notification To User To Active Golden Offer
        // If We Send Notification To User We Should Time limit before completing the Order
        // If User Not Active After Send Notification We Should Complete Order
        //---------------------------------------------------------------------
        // ------> make  before any thing user not active golden offer or active but now > end_bidding_duration
        // We Have column => ( time_active_golden_offer ) time to send notification to user to active golden offer
        // We Have column => (time_complete_order ) to complete order

        $orders = Order::where([
            ['is_accepted','=',1], ['is_suspend','=',0], ['golden_offer','=',1], ['status' ,'=','pending' ],
        ])->whereHas('my_offer',function ($offer){
            $offer->where('is_pay',1);
        })->with('user.devices','offer_man.devices')->get();

        if (count($orders) > 0){

            foreach ($orders as $order){

                $now = Carbon::now()->format('Y-m-d H:i');
                $title          = "orders";
                $title_ar       = Lang::get('order.'.$title,[],'ar') ;
                $title_en       =  Lang::get('order.'.$title,[],'en') ;
                $body_argument  = [ "order_name"    => $order->title ];
                $offer = $order->my_offer;

                $user_devices = $order['user']['devices'] ? collect($order['user']['devices'])->pluck('device')->toArray() : null;
                $offer_devices = $order['offer_man']['devices'] ? collect($order['offer_man']['devices'])->pluck('device')->toArray() : null;

                $data = [
                    'order_id' => $order->id,'offer_id' => $offer->id, 'title' => $title,
                    'body_arguments' => json_encode($body_argument) , 'created_at' => now()
                ];

                if (!$order->time_complete_order && $now >= Carbon::parse($order->time_active_golden_offer)  ){
                    // 1- Send Notification  To active Golden Offer
                    // 2- update column => time_complete_order  by Time in setting dashboard called =>  time_limit_active_golden

                         $this->timeToActiveGoldenOffer($data, $order,$user_devices, $offer , $title, $title_ar ,$title_en);

                }else{
                    // make Sure column now() >=   time_complete_order
                    // User Not active golden offer Or active but Time out  we should now complete order

                        $not_active_golden_offer_paid = $order->active_golden_offer == 0 && $order->is_pay == 1;
                        $active_golden_offer_time_out = $order->active_golden_offer == 1 && $now >= Carbon::parse($order->end_bidding_duration);

                            if ($order->time_complete_order &&  ( $not_active_golden_offer_paid ||$active_golden_offer_time_out) ){

                                if($now >= Carbon::parse($order->time_complete_order)){
                                      $this->completeOrder($data,$order,$offer , $user_devices , $offer_devices ,$title, $title_ar ,$title_en);
                                }
                            }

                }

            }
        }
    }


    private function timeToActiveGoldenOffer($data, $order,$user_devices, $offer , $title, $title_ar ,$title_en)
    {
        $body_user      = "end_bidding_duration_golden_offer";
        $time_limit_active_golden = Setting::getBody('time_limit_active_golden');

        $order->update(['time_complete_order' => Carbon::now()->addMinutes($time_limit_active_golden) ]);

        if ( $order->active_golden_offer == 1 ){
            $this->notification_database_user($data, $order , $body_user );

            if (count($user_devices) > 0){
                $this->send_public_notifications($user_devices , $order , $offer , $title, $title_ar ,$title_en ,$body_user);
            }
        }

    }


    private function completeOrder($data,$order,$offer , $user_devices , $offer_devices ,$title, $title_ar ,$title_en)
    {
        $body_user      = "complete_order_user";
        $body_offer     = "complete_order_offer";

        // Send Notification To Tell Other Order Is Finish
             event(new RefuseOtherOffersWithOutAcceptedOffer($order->user,$order,$offer));

        $order->update(['status' => 'finish']);

        $offer->update(['status' => 'finish']);

        $order->offers_pending->where('id','!=', $offer->id)->each->update(['status' => 'refuse']);

        $this->notification_database_user($data, $order , $body_user );

        $this->notification_database_offer($data, $order , $body_offer );

        if (count($user_devices) > 0){
            $this->send_public_notifications($user_devices , $order , $offer , $title, $title_ar ,$title_en ,$body_user);
        }

        if (count($offer_devices) > 0){
            $this->send_public_notifications($offer_devices , $order , $offer , $title, $title_ar ,$title_en ,$body_offer);
        }

    }

    public function notification_database_user($data,$order , $body_user  ){
        Notification::create(array_merge($data,[
            'user_id' => $order->user_id,'sender_id' => $order->user_offer_id ,
            "translation" => $body_user,"body" => $body_user
        ]));
    }

    public function notification_database_offer($data ,$order, $body_offer ){
        Notification::create(array_merge($data,[
            'user_id' => $order->user_offer_id,'sender_id' => $order->user_id ,
            "translation" => $body_offer,"body" => $body_offer
        ]));
    }

    public function send_public_notifications($devices , $order , $offer , $title, $title_ar ,$title_en ,$body_text){

        $content_user_en    = Lang::get('order.'.$body_text,["order_name" => $order->title ],'en') ;
        $content_user_ar    = Lang::get('order.'.$body_text,["order_name" => $order->title],'ar') ;

        $this->push->sendMessage($devices,[
            'order_id'      => $order->id, 'offer_id' => $offer->id,
            'title'         => $title, 'body' => $body_text,
        ],$content_user_en,$content_user_ar,$title_en,$title_ar);
    }
}

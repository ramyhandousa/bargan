<?php

namespace App\Console\Commands;

use App\Events\RefuseOtherOffersWithOutAcceptedOffer;
use App\Libraries\oneSignal;
use App\Models\Device;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class activeGoldenTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'timeActiveGolden:orderTimeActiveGolden';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'after user pay and offer man payed wait time for active golden offer ';


    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $now = Carbon::now()->format('Y-m-d H:i');

        $orders = Order::where([
            ['is_accepted','=',1], ['is_suspend','=',0], ['golden_offer','=',1],
            ['end_bidding_duration' ,'<=',$now], ['status' ,'=','pending' ],  ['time_out','=',1]
        ])->whereHas('my_offer',function ($offer){
            $offer->where('is_pay',1);
        })->with('user.devices','offer_man.devices')->get();

        if (count($orders) > 0){

            foreach ($orders as $order){

                $title          = "orders";
                $title_ar      = Lang::get('order.'.$title,[],'ar') ;
                $title_en      =  Lang::get('order.'.$title,[],'en') ;
                $body_user      = "complete_order_user";
                $body_offer     = "complete_order_offer";
                $body_argument  = [ "order_name"    => $order->title ];
                $offer = $order->my_offer;

                // Send Notification To Tell Other Order Is Finish
                event(new RefuseOtherOffersWithOutAcceptedOffer($order->user,$order,$offer));

                $order->update(['status' => 'finish']);

                $offer->update(['status' => 'finish']);

                $order->offers_pending->where('id','!=', $offer->id)->each->update(['status' => 'refuse']);

                $this->notification_database($order ,$offer , $title,$body_argument, $body_user , $body_offer);

                $user_devices = $order['user']['devices'] ? collect($order['user']['devices'])->pluck('device')->toArray() : null;

                if (count($user_devices) > 0){
                    $this->send_public_notifications($user_devices , $order , $offer , $title, $title_ar ,$title_en ,$body_user);
                }

                $offer_devices = $order['offer_man']['devices'] ? collect($order['offer_man']['devices'])->pluck('device')->toArray() : null;

                if (count($user_devices) > 0){
                    $this->send_public_notifications($offer_devices , $order , $offer , $title, $title_ar ,$title_en ,$body_offer);
                }
            }
        }


    }


    public function notification_database($order ,$offer , $title,$body_argument, $body_user , $body_offer ){
        $data = [
            'order_id'     => $order->id,'offer_id'  => $offer->id, 'title'        => $title,
            'body_arguments' => json_encode($body_argument) , 'created_at'   => now()
        ];
        Notification::create(array_merge($data,[
            'user_id' => $order->user_id,'sender_id' => $order->user_offer_id ,"translation" => $body_user,"body" => $body_user
        ]));

        Notification::create(array_merge($data,[
            'user_id' => $order->user_offer_id,'sender_id' => $order->user_id ,"translation" => $body_offer,"body" => $body_offer
        ]));
    }


    public function send_public_notifications($devices , $order , $offer , $title, $title_ar ,$title_en ,$body_text){

        $content_user_en    = Lang::get('order.'.$body_text,["order_name" => $order->title ],'en') ;
        $content_user_ar    = Lang::get('order.'.$body_text,["order_name" => $order->title],'ar') ;

        $this->push->sendMessage($devices,[
            'order_id'      => $order->id,
            'offer_id'      => $offer->id,
            'title'         => $title,
            'body'          => $body_text,
        ],$content_user_en,$content_user_ar,$title_en,$title_ar);

    }
}

<?php

namespace App\Console;

use App\Console\Commands\activeGoldenTime;
use App\Console\Commands\NotifyUserAboutInterestingCategories;
use App\Console\Commands\NotifyUserOrderHaveOffers;
use App\Console\Commands\OfferEndBiddingDuration;
use App\Console\Commands\OrderEndBiddingDuration;
use App\Console\Commands\OrderRate;
use App\Console\Commands\SendEmailNotify;
use App\Console\Commands\TimeActiveGoldenOffer;
use App\Console\Commands\UserNotActiveGoldenOffer;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        OrderEndBiddingDuration::class,
        OfferEndBiddingDuration::class,
        OrderRate::class,
//        SendEmailNotify::class,
//        activeGoldenTime::class,
//        TimeActiveGoldenOffer::class,
        UserNotActiveGoldenOffer::class,
        NotifyUserOrderHaveOffers::class,
        NotifyUserAboutInterestingCategories::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('endBiddingDuration:OrderEndBiddingDuration')->everyMinute();
        $schedule->command('endBiddingDuration:OfferEndBiddingDuration')->everyMinute();
        $schedule->command('command:UserNotActiveGoldenOffer')->everyMinute();
        $schedule->command('OrderRate:OrderRateUserAndOffer')->everyMinute();
        $schedule->command('Order_Man:NotifyUserOrderHaveOffers')->everyMinute();
        $schedule->command('offer_users:NotifyUserAboutInterestingCategories')->everyMinute();

//        $schedule->command('timeActiveGolden:orderTimeActiveGolden')->everyMinute();
//        $schedule->command('TimeActiveGoldenOffer:TimeActiveGoldenOffer')->everyMinute();
//        $schedule->command('sendEmail:sendEmailnotify')->everyMinute();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

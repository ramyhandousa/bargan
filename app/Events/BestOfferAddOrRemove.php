<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BestOfferAddOrRemove
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $title;
    public $body;
    public $order;
    public $request;
    public $topic;
    public $old_offer_price;
    public function __construct($title  , $body,$order  ,$request,$topic,$old_offer_price =null)
    {
        $this->title  =  $title  ;
        $this->body =  $body   ;
        $this->order = $order  ;
        $this->request = $request  ;
        $this->topic =  $topic  ;
        $this->old_offer_price =  $old_offer_price  ;
    }

}

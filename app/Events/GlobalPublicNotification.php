<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class GlobalPublicNotification
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $title;
    public $body;
    public $topic;
    public $type;
    public $order;
    public $user;
    public function __construct($title,$body,$topic,$type,$order = null,$user = null  )
    {
        $this->title = $title;
        $this->body = $body;
        $this->topic = $topic;
        $this->type = $type;
        $this->order = $order;
        $this->user = $user;
    }

}

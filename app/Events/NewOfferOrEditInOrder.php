<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewOfferOrEditInOrder
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $sender;
    public $order;
    public $offer;
    public $request;
    public $type;
    public function __construct($sender,$order ,$offer , $request,$type = 'add')
    {
        $this->sender = $sender;
        $this->order = $order;
        $this->offer = $offer;
        $this->request = $request;
        $this->type = $type;
    }


}

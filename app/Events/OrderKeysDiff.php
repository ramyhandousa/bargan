<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderKeysDiff
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $sender;
    public $order;
    public $request;
    public $title_key;
    public $body_key;
    public $offer;

    public function __construct($user,$sender, $order,$request,$title_key, $body_key,$offer = null)
    {
        $this->user = $user;
        $this->sender = $sender;
        $this->order = $order;
        $this->request = $request;
        $this->title_key = $title_key;
        $this->body_key = $body_key;
        $this->offer = $offer;
    }

}

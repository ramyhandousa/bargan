<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderUpdateGoldenOffer
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $request;
    public $order;

    public function __construct($request  , $order)
    {
        $this->request = $request;
        $this->order = $order;
    }
}

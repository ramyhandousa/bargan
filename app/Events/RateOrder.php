<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RateOrder
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $request;
    public $order;
    public $type;

    public function __construct($user , $request  , $order , $type)
    {
        $this->user = $user;
        $this->request = $request;
        $this->order = $order;
        $this->type = $type;
    }

}

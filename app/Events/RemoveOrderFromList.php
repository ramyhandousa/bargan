<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class RemoveOrderFromList
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $title;
    public $body;
    public $order;
    public $topic;
    public function __construct($title  , $body,$order  ,$topic)
    {
        $this->title  =  $title  ;
        $this->body =  $body   ;
        $this->order = $order  ;
        $this->topic =  $topic  ;
    }

}

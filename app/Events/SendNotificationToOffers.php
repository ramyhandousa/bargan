<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendNotificationToOffers
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $order;
    public $title_key;
    public $body_key;
    public $request;
    public function __construct( $order,$title_key, $body_key, $request = null)
    {
        $this->order = $order;
        $this->title_key = $title_key;
        $this->body_key = $body_key;
        $this->request = $request;
    }

}

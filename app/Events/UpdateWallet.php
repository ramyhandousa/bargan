<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UpdateWallet
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $request;
    public $order;
    public $offer;
    public $progress;
    public $payment;
    public $paymentId;

    public function __construct($user,  $request,$order = null,$offer = null,$progress = 'pull',$payment = "wallet",$paymentId = null)
    {
        $this->user = $user;
        $this->request = $request;
        $this->order = $order;
        $this->offer = $offer;
        $this->progress = $progress;
        $this->payment = $payment;
        $this->paymentId = $paymentId;
    }
}

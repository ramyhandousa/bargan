<?php

namespace App\Events;

use App\Models\Order;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class finishOrder
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $order;

    public $send_to_order_man;
    public $send_to_offer_man;

    public function __construct(Order $order ,$send_to_order_man  = true,$send_to_offer_man  = true)
    {

        $this->order = $order;
        $this->send_to_order_man = $send_to_order_man;
        $this->send_to_offer_man = $send_to_offer_man;
    }
}

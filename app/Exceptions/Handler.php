<?php

namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {

        });
    }


    public function render($request, Throwable $exception)
    {

        if ($request->wantsJson()) {

            if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
                return response()->json([   'status' => 401,  'error' => (array) trans('global.user_not_found')   ],200);
            }
            if ($exception instanceof ModelNotFoundException) {
                return response()->json([   'status' => 400,  'error' => (array) 'تاكد من وجود هذا العنصر'   ],200);
            }

            if($exception instanceof AuthorizationException  ){
                return response()->json(['status' => 400 , 'error' => (array) $exception->getMessage()]);
            }
        }
        return parent::render($request, $exception);
    }



}



<?php

namespace App\Exports;

use App\Http\Resources\Admin\Order\OrderExport;
use App\Models\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithHeadings;

class OrdersExport implements FromCollection  , WithHeadings , ShouldAutoSize , WithColumnWidths
{
    /**
    * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function collection()
    {
       $query = Order::with('offer_man')->latest()->get();

        return OrderExport::collection($query);
    }

    public function headings(): array
    {
        return [
            ' # ',
            ' إسم المستخدم ',
            '    رقم هاتف المستخدم   ',
            '    البريد الإلكتروني للمستخدم',
            ' العنوان  ',
            ' السعر المطلوب   ',
            ' شخصي / تجاري   ',
            ' نوع الطلب   ',
            ' نوع الخدمة   ',
            ' القسم   ',
            ' المدينة ',
            ' فرصة ذهبية ',
            '  التوصيل',
            '  الصناعة',
            '  الضمان',
            '  الدفع',
            '  مدفوع ام لا',

            ' إسم  صاحب العرض ',
            '    رقم هاتف صاحب العرض ',
            '    البريد الإلكتروني  لصاحب العرض ',
            ' السعر المقدم من صاحب العرض  ',
            ' وقت العرض المقدم ',

            'وصف الطلب '
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 5,
            'B' => 25,
            'C' => 20,
            'D' => 25,
            'E' => 30,
            'F' => 15,
            'J' => 20,
            'W' => 50
        ];
    }
}

<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
class UsersExport implements FromCollection , WithHeadings , ShouldAutoSize ,WithStyles , WithColumnWidths
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
//        return  \App\Models\User::whereHas('wallet')->take(10)->limit(10)
//            ->select('id','name','phone','email','desc_category')
//            ->withSum('wallet','price')
//            ->get();

          $data =  \App\Models\User::select('id','name','phone','email','desc_category')
            ->withSum('wallet','price')
              ->get();

//          collect($data)->map(function ($q){
//              $q->desc_category =  implode(', ', $q->desc_category);
//          });


//         return  $data->dd();
         return  $data;
    }


    public function headings(): array
    {
        return [
            ' # ',
            ' الإسم ',
            '    الجوال   ',
            '    البريد الإلكتروني ',
            ' شراء  ',
            ' الرصيد ',
        ];
    }

    public function columnWidths(): array
    {
        return [
            'A' => 5,
            'B' => 40,
            'C' => 25,
            'D' => 40,
            'e' => 50,
            'F' => 10,
        ];
    }

    public function styles(Worksheet $sheet)
    {
//        $sheet->getcell('A1:A5', function($cells) {
//
//
//        });
//        return [
//            // Style the first row as bold text.
//            1    => ['font' => ['bold' => true , 'size' => 15 ]],
//
//            // Styling a specific cell by coordinate.
//            'B2' => ['font' => ['italic' => true] ],
//            // Styling an entire column.
////            'C'  => ['font' => ['size' => 16]],
//        ];
    }
}

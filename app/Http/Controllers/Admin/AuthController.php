<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Auth\checkCodeWithPassword;
use App\Http\Requests\Admin\Auth\forgetPassVaild;
use App\Http\Requests\Admin\Auth\LoginVaild;
use App\Http\Requests\Admin\Auth\resendCodeVaild;
use App\Http\Requests\api\Auth\changePassRequest;
use App\Http\Requests\api\Auth\editUser;
use App\Http\Resources\User\UserResource;
use App\Mail\codeActivation;
use App\Mail\codeActive;
use App\Models\User;
use App\Models\VerifyUser;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    use RespondsWithHttpStatus;

    public function __construct()
    {

        $this->middleware('auth:api')
            ->only(['changPassword' ,'editProfile','logOut']);
    }

    public function login(LoginVaild $request){

        $user = User::whereEmail($request->email)->first();

        return $this->success("بيانات الأدمن", new UserResource($user));
    }


    public function forgetPassword(forgetPassVaild $request){

        $user = User::whereEmail($request->email)->first();

        $action_code = $this->random_code_active();

        $this->createVerfiy($request,$user,$action_code);

        Mail::to($user->email)->send(new codeActivation($user , $action_code));

        $message =   trans('global.activation_code_sent_email') ;

        return $this->success($message, ["code" => $action_code]);
    }

    public function check_code(checkCodeWithPassword $request){

        $verifyUser = VerifyUser::whereEmail($request->email)->with('user')->first();

        $user = $verifyUser['user'];

         if ($request->password){

             $verifyUser['user']->update([ 'password' => $request->password]);
         }

        $verifyUser->delete();

        return $this->success("بيانات الأدمن", new UserResource($user));
    }

    public function resendCode(resendCodeVaild $request){
        $verifyUser = VerifyUser::whereEmail($request->email)->with('user')->first();

        $user = $verifyUser['user'];

        $action_code = $this->random_code_active();

        Mail::to($request->email)->send(new codeActivation($user , $action_code));

        $message =   trans('global.activation_code_sent_email') ;

        return $this->success($message, ["code" => $action_code]);
    }


    public function editProfile(editUser $request){

        $user = $request->user();
        if ($request->newPassword){
            $user->password = $request->newPassword;
        }
        $user->fill($request->validated());
        $user->save();

        return $this->success("بيانات الأدمن", new UserResource($user));
    }

    public function changPassword(changePassRequest  $request){

        $user = $request->user();

        if ($request->newPassword){

            $user->update( [ 'password' => $request->newPassword] );

            $message =  trans('global.password_was_edited_successfully') ;

        }else{

            $message = trans('global.password_not_edited');
        }

        return $this->success($message, new UserResource($user));
    }


    public function createVerfiy($request , $user , $action_code){

        $data = [ 'phone' => $request->phone,'email' => $request->email , 'action_code'  => $action_code];

        VerifyUser::updateOrCreate(['user_id' => $user->id], $data);
    }



    public function logOut(Request  $request){

        return $this->success(  trans('global.logged_out_successfully'));
    }

}

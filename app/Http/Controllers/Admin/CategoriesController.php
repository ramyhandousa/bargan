<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\CategoryStoreVaild;
use App\Http\Resources\Admin\CategoryList;
use App\Models\Category;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class CategoriesController extends Controller
{
    use RespondsWithHttpStatus;

    public function index(Request  $request){

        $categories = Category::query();

        if ($request->has('subCategories')){
            $categories->where('is_active','=',1)->where('parent_id','!=',0);

        }elseif($request->has('newCategories')){

            $categories->where('is_active','=',0);

        }else{
            $categories->where('is_active','=',1)->where('parent_id','=',0);
        }

        return $this->success("الأقسام", CategoryList::collection($categories->withCount('orders','offers')->get()));
    }


    public function show(Category $category){

        return $this->success("بيانات القسم" , new CategoryList($category));
    }

    public function store(CategoryStoreVaild  $request){

        $category = new Category();
        $this->modelCategory($request,$category);

        return $this->success("تم إضافة القسم" , new CategoryList($category));
    }

    public function update(CategoryStoreVaild $request , Category $category){

        $this->modelCategory($request,$category);

        return $this->success("تم تعديل القسم" , new CategoryList($category));
    }

    public function suspendBoolean(Request $request, Category  $category){

        if ($request->suspend == "true"){
            $category->update(['is_suspend' => 1]);
        }else{
            $category->update(['is_suspend' => 0]);
        }

        return $this->success("بيانات القسم" , new CategoryList($category));
    }

    public function activeBoolean(Request $request, Category  $category){

        if ($request->active == "true"){
            $category->update(['is_suspend' => 1]);
        }else{
            $category->update(['is_active' => -1]);
        }

        return $this->success("بيانات القسم" , new CategoryList($category));
    }

    function modelCategory($request, $category){
        if ($request->parent_id){
            $category->parent_id = $request->parent_id;
        }
        $category->is_active = 1;
        $category->{'name:ar'} = $request->name_ar;
        $category->{'name:en'} = $request->name_en;
        $category->{'product_title:ar'} = $request->product_title_ar;
        $category->{'product_title:en'} = $request->product_title_en;
        $category->{'service_title:ar'} = $request->service_title_ar;
        $category->{'service_title:en'} = $request->service_title_en;
        $category->{'description:ar'} = $request->description_ar;
        $category->{'description:en'} = $request->description_en;
        if ($request->image){
            if ($category->image){
                File::delete(public_path().'/'.$category->image);
            }
            $category->image = $request->image;
        }
        $category->save();
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\City\CityStoreValid;
use App\Http\Resources\Admin\CityResource;
use App\Models\City;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class CityController extends Controller
{
    use RespondsWithHttpStatus;

    public function index()
    {
        $cities = City::all();

        return $this->success("الأقسام", CityResource::collection($cities));
    }

    public function store(CityStoreValid $request)
    {
        $city = new City();
        $this->modelCity($request,$city);

        return $this->success("تم إضافة المدينة" , new CityResource($city));
    }


    public function show(City $city)
    {
      return $this->success("بيانات المدينة", new  CityResource($city));
    }

    public function update(CityStoreValid $request , City $city){

        $this->modelCity($request,$city);

        return $this->success("تم تعديل المدينة" , new CityResource($city));
    }


    public function destroy($id)
    {
        //
    }

    public function suspendBoolean(Request $request, City  $city){

        if ($request->suspend == "true"){
            $city->update(['is_suspend' => 1]);
        }else{
            $city->update(['is_suspend' => 0]);
        }

        return $this->success("بيانات المدينة" , new CityResource($city));
    }


    function modelCity($request, $city){
        $city->{'name:ar'} = $request->name_ar;
        $city->{'name:en'} = $request->name_en;
        $city->save();
    }

}

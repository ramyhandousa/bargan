<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\message\replyEmailVaild;
use App\Http\Resources\Admin\ContactUsList;
use App\Libraries\oneSignal;
use App\Mail\ContactUsReply;
use App\Models\Device;
use App\Models\Notification;
use App\Models\Support;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    use RespondsWithHttpStatus ,paginationTrait ;

    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }

    public function index(Request  $request){
        $admin_id = 47;

        $query = Support::with('reply_message');

        if ($request->un_read){
            $query->where('parent_id','=',0)->where('is_read','=',0);
        }else{
            $query->where('user_id','=',$admin_id)->where('parent_id','=',0);
        }

        $total_count =  $query->count();

        $this->pagination_query($request,$query);

        $data = ContactUsList::collection($query->latest()->get());

        return $this->successWithPagination("رسائل التواصل",$total_count, $data);
    }

    public function update(Request  $request , $id){

        $admin_id = 47;

        $support = Support::findOrFail($id);

        $new_support = new Support();
        $new_support->user_id       = $support->sender_id;
        $new_support->sender_id     = $admin_id;
        $new_support->type_id       = $support->type_id;
        $new_support->parent_id     = $support->id;
        $new_support->message       = $request->reply;
        $new_support->replied_at    = now();
        $new_support->save();

        if ($support->sender){
            if ($support->sender->email){
                Mail::to($support->sender->email)->send(new ContactUsReply($support,$new_support));
            }

            $body_argument = [
                "reply" =>  $request->reply,
                "message" =>  $support->message,
            ];

            $notify = Notification::create(['user_id' => $support->sender_id,'sender_id' =>$admin_id,
                'title' => 'contact_us','translation' => 'contact_us_body','body' => 'contact_us_body', 'body_arguments' =>json_encode($body_argument)  ]);

            $title_ar      = Lang::get('order.contact_us',[],'ar') ;
            $title_en      =  Lang::get('order.contact_us',[],'en') ;
            $content_en    = Lang::get('order.contact_us_body',['reply' => $request->reply ,"message" => $support->message ],'en') ;
            $content_ar    = Lang::get('order.contact_us_body',['reply' => $request->reply ,"message" => $support->message ],'ar') ;

        }else{
            Mail::to($support->email)->send(new ContactUsReply($support,$new_support));

        }

        $support->update(['is_read' => 1]);


//        $devices = Device::whereUserId($support->sender_id)->pluck('device');
//
//        $this->push->sendMessage($devices,[
//            'title_key'         => "contact_us",
//            'body_key'          => "contact_us_body",
//            'body_arguments'    => [
//                "reply" =>  $request->reply,
//                "message" =>  $support->message,
//            ],
//        ],$content_en,$content_ar,$title_en,$title_ar);

        return $this->success("تم الرد بنجاح", new ContactUsList($support));
    }

    public function reply_to_email(replyEmailVaild $request,$id){
        $support = Support::findOrFail($id);

        $new_support = [
            'message' => $request->message
        ];

        Mail::to($support->email)->send(new ContactUsReply($support,$new_support));

        return $this->success("تم الرد بنجاح", new ContactUsList($support));
    }


    public function destroy(Support $support){

        $support->delete();

        return $this->success("تم المسح بنجاح");
    }
}

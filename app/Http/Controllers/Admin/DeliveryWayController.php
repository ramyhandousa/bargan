<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Delivery\DeliveryWayStoreVaild;
use App\Http\Resources\Admin\DeliveryWaysList;
use App\Models\DeliveryWay;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class DeliveryWayController extends Controller
{
    use RespondsWithHttpStatus;

    public function index(Request  $request){

        $delivery_ways = DeliveryWay::latest()->get();

        return $this->success("انواع التوصيل", DeliveryWaysList::collection($delivery_ways));
    }


    public function show(DeliveryWay $deliveryWay){

        return $this->success("بيانات التوصيل" , new DeliveryWaysList($deliveryWay));
    }

    public function store(DeliveryWayStoreVaild  $request){

        $deliveryWay = new DeliveryWay();

        $this->modelDeliveryWay($request,$deliveryWay);

        return $this->success("تم إضافة التوصيل" , new DeliveryWaysList($deliveryWay));
    }

    public function update(DeliveryWayStoreVaild $request , DeliveryWay $deliveryWay){

        $this->modelDeliveryWay($request,$deliveryWay);

        return $this->success("تم تعديل التوصيل" , new DeliveryWaysList($deliveryWay));
    }

    public function suspendBoolean(Request $request, DeliveryWay  $deliveryWay){

        if ($request->suspend == "true"){
            $deliveryWay->update(['is_suspend' => 1]);
        }else{
            $deliveryWay->update(['is_suspend' => 0]);
        }

        return $this->success("بيانات التوصيل" , new DeliveryWaysList($deliveryWay));
    }

    function modelDeliveryWay($request, $deliveryWay){

        $deliveryWay->{'name:ar'} = $request->name_ar;
        $deliveryWay->{'name:en'} = $request->name_en;
        $deliveryWay->save();
    }
}

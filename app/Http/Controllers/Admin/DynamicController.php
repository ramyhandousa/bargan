<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Dynamic\DynamicStoreRequest;
use App\Http\Resources\Admin\DynamicFilterList;
use App\Http\Resources\Admin\DynamicList;
use App\Models\Label;
use App\Models\LabelValue;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class DynamicController extends Controller
{
    use RespondsWithHttpStatus;


    public function index(Request $request){

        $definition_type = $request->definition_type ? : "order";

        $labels = Label::whereDeleted(0)
            ->whereHas('label_values')->with('label_values')->get();

        $data = DynamicFilterList::collection($labels);

        return $this->success('بيانات الطلب الإضافي ', $data);
    }

    public function show( $id){

        $label = Label::findOrFail($id);

        return $this->success("بيانات الطلب", new DynamicList($label));
    }

    public function store(DynamicStoreRequest $request){

        $label = new Label();
        $label->{'name:ar'}       = $request->name_ar;
        $label->{'name:en'}       = $request->name_en;
        $label->definition_type   = $request->definition_type;
        $label->order_type        = $request->order_type;
        $label->type              = $request->type;
        $label->save();

        foreach ($request->values as $my_value){
            $label_values = new LabelValue();
            $label_values->label_id  = $label->id;
            $label_values->{'value:ar'} = $my_value['name_ar'];
            $label_values->{'value:en'} = $my_value['name_en'] ;
            $label_values->save();
        }

        return $this->success("تم الإضافة بنجاح",new DynamicList($label));
    }


    public function update(DynamicStoreRequest $request, $id){

        $label = Label::findOrFail($id);
        $label->{'name:ar'}       = $request->name_ar;
        $label->{'name:en'}       = $request->name_en;
        $label->definition_type   = $request->definition_type;
        $label->order_type        = $request->order_type;
        $label->type              = $request->type;
        $label->save();

        $ids =  collect($request->values)->pluck('id')->filter();
        if (count($ids) > 0){
            LabelValue::where('label_id',$label->id)->whereNotIn('id',$ids)->delete();
        }
        foreach ($request->values as $my_value){

            if (isset($my_value['id']) && LabelValue::find($my_value['id'])){
                $label_values =   LabelValue::find($my_value['id']);
            }else{
                $label_values = new LabelValue();
            }
            $label_values->label_id  = $label->id;
            $label_values->{'value:ar'} = $my_value['name_ar'];
            $label_values->{'value:en'} = $my_value['name_en'] ;
            $label_values->save();
        }
        return $this->success("تم التعديل بنجاح  ", new DynamicList($label));
    }

    public function destroy($id){
        $label = Label::whereDeleted(0)->findOrFail($id);

        $label->update(['deleted' => 1]);

        return $this->success("تم المسح بنجاح  ");
    }


}

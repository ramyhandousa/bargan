<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Auth\HelperAdminVaild;
use App\Http\Requests\Admin\Auth\UpdateHelperAdminVaild;
use App\Http\Resources\Admin\AdminList;
use App\Models\User;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class HelperAdminController extends Controller
{
    use RespondsWithHttpStatus  ,paginationTrait;

    public function index(Request $request){

        $users = User::where("defined_user","helper_admin");

        $total_count =  $users->count();

        $this->pagination_query($request,$users);

        $data =   AdminList::collection($users->get());

        return $this->successWithPagination("بيانات مساعدين الأدمن", $total_count , $data);

    }

    public function show($id){

        $user = User::findOrFail($id);

        return $this->success("بيانات مساعد الأدمن",new AdminList($user));
    }

    public function store(HelperAdminVaild  $request){

        $data  = array_merge($request->validated(),[ "defined_user"  => "helper_admin","is_active_email" => true ,"is_active_phone" => true]);

        $user = User::create($data);

        return $this->success("تم إضافة مساعد الأدمن",new AdminList($user));
    }

    public function update(UpdateHelperAdminVaild  $request, $id){

        $user = User::findOrFail($id);
        $user->fill($request->validated());
        $user->save();

        return $this->success("تم تعديل بيانات مساعد الأدمن",new AdminList($user));
    }

    public function my_permission(Request  $request){

        $user = User::whereApiToken($request->bearerToken())->firstOrFail();

        $data = [
            'is_admin'      =>  $user->defined_user == "admin",
            "permission" => $user->permission
        ];
        return $this->success(" بيانات مساعد الأدمن",$data);
    }


    public function suspendBoolean(Request $request, User  $user){

        if ($request->suspend == "true"){
            $user->update(['is_suspend' => 1]);
        }else{
            $user->update(['is_suspend' => 0]);
        }

        return $this->success("بيانات مساعد الأدمن" , new AdminList($user));
    }

}

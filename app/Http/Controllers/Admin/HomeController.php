<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Inqueries;
use App\Models\Offer;
use App\Models\Order;
use App\Models\PromoCode;
use App\Models\Support;
use App\Models\Transaction;
use App\Models\User;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use RespondsWithHttpStatus ;

    public function index(){



        $users_count = ["id" => "users" ,'name_ar' => "عدد المستخدمين" , "name_en" => "total_users" ,
                        "value" => User::where('is_active_phone',1)->where('defined_user','user')->count()];

        $order_finish = ["id" => "orders" ,'query' => ["status" => "finish"] ,'name_ar' => "عدد الطلبات المكتملة" , "name_en" => "Number of finish applications",
                            "value" => Order::query()->where('status','=','finish')->count()];

        $order_pending = ["id" => "pending_orders" ,'name_ar' => "عدد الطلبات المعلقه" , "name_en" => "Number of pending applications",
                            "value" => Order::query()->where('is_accepted','=','0')->count()];

        $order_type_personal_auction = ["id" => "orders" ,'query' => ["account_type" => "personal", "order_type" => "auction"],'name_ar' => "  عدد الطلبات المزادات الشخصية" , "name_en" => "The number of personal auctions requests" ,
                                        "value" => Order::query()->where('is_accepted','=','1')->where('account_type','=','personal')
                                                                ->where('order_type','=','auction')->count()];

        $order_type_commercial_auction = ["id" => "orders" ,'query' => ["account_type" => "commercial", "order_type" => "auction"],'name_ar' => "  عدد الطلبات المزادات التجارية", "name_en" => "The number of commercial auctions requests" ,
                                        "value" => Order::query()->where('is_accepted','=','1')->where('account_type','=','commercial')
                                                                ->where('order_type','=','auction')->count()];

        $order_type_personal_tender = ["id" => "orders" ,'query' => ["account_type" => "personal", "order_type" => "tender"],'name_ar' => "  عدد الطلبات المناقصات الشخصية" , "name_en" => "The number of requests for personal tenders" ,
                                        "value" => Order::query()->where('is_accepted','=','1')->where('account_type','=','personal')
                                                                ->where('order_type','=','tender')->count()];

        $order_type_commercial_tender = ["id" => "orders" ,'query' => ["account_type" => "commercial", "order_type" => "tender"] ,'name_ar' => "عدد الطلبات المناقصات التجارية" , "name_en" => "The number of commercial tenders requests" ,
                                        "value" => Order::query()->where('is_accepted','=','1')->where('account_type','=','commercial')
                                                                ->where('order_type','=','tender')->count()];

        $transaction_all = ['name_ar' => "قيمة تعاملات التطبيق" , "name_en" => "The value of the application transactions",
                                        "value" => Transaction::all()->sum('price') ];

        $transaction_refund_requested = ["id" => "refunds" ,'name_ar' => "طلبات الإسترداد" , "name_en" => "Refund requests" ,
                                        "value" => Transaction::whereRefundRequested('request')->count() ];

        $promo_code = ["id" => "promo_codes" ,'name_ar' => "عدد الأكواد التروجية", "name_en" => "Number of promotional codes"  ,
                                        "value" => PromoCode::count() ];

        $un_support_message = ["id" => "contact_messages",'query' => ['un_read' => 'un_read'] ,'name_ar' => "عدد الرسائل الغير مقروءة  ", "name_en" => "Number of un read message "  ,
                                        "value" => Support::whereParentIdAndIsRead(0,0)->count() ];

        $transaction_by_promo_code = [
            "id" => "transactions" ,'name_ar' => "قيمة شحن المحفظة من خلال الكود الترويجي" ,
            "name_en" => "The value of charging the wallet through the promotional code",
            'query' => ['payment' => 'wallet' ,'progress' => 'deposit_by_promo_code'],
            "value" => Transaction::whereProgress('deposit_by_promo_code')->sum('price')
        ];

        $transaction_by_payment_online_pull = [
            "id" => "transactions" ,'name_ar' => "قيمة المدفوعات الأونلاين" , "name_en" => "Online payments value",
            'query' => ['payment' => 'online' ,'progress' => 'pull'],
            "value" => Transaction::whereProgress('pull')->wherePayment('online')->sum('price')
        ];

        $transaction_by_payment_online_deposit = [
            "id" => "transactions" ,'name_ar' => "قيمة شحن المحفظة من خلال الدفع الإلكتروني" ,
            "name_en" => "The value of charging the wallet through electronic payment",
            'query' => ['payment' => 'online' ,'progress' => 'deposit'],
            "value" => Transaction::whereProgress('deposit')->wherePayment('online')->sum('price')
        ];

        $offers_pending = [
            "id" => "pending_offers" , 'name_ar' => "عدد العروض المعلقة" ,"name_en" => 'offers pending',
            "value" => Offer::whereIsAccepted(0)->count()
        ];

        $inquerie_offers = [
            "id" =>  "pending_inqueries", 'name_ar' => "عدد الإستفسارات المعلقة" ,"name_en" => 'inqueries pending',
            "value" => Inqueries::whereIsAccepted(0)->count()
        ];



        $data = [];
        array_push($data ,
            $order_pending,
            $offers_pending,
            $inquerie_offers,
            $transaction_refund_requested,
            $un_support_message,
            $users_count,
            $order_finish,
            $order_type_personal_auction,
            $order_type_commercial_auction,
            $order_type_personal_tender,
            $order_type_commercial_tender,
            $transaction_all,
            $promo_code,
            $transaction_by_promo_code,
            $transaction_by_payment_online_pull,
            $transaction_by_payment_online_deposit,
        );

        return $this->success('home' , $data);
    }
}

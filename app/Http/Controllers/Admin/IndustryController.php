<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Industry\IndustryStoreVaild;
use App\Http\Resources\Admin\IndustryList;
use App\Models\Industry;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class IndustryController extends Controller
{
    use RespondsWithHttpStatus;

    public function index(Request  $request){

        $delivery_ways = Industry::latest()->get();

        return $this->success("انواع الصناعة", IndustryList::collection($delivery_ways));
    }


    public function show(Industry $industry){

        return $this->success("بيانات الصناعة" , new IndustryList($industry));
    }

    public function store(IndustryStoreVaild  $request){

        $industry = new Industry();

        $this->modelDeliveryWay($request,$industry);

        return $this->success("تم إضافة الصناعة" , new IndustryList($industry));
    }

    public function update(IndustryStoreVaild $request , Industry $industry){

        $this->modelDeliveryWay($request,$industry);

        return $this->success("تم تعديل الصناعة" , new IndustryList($industry));
    }

    public function suspendBoolean(Request $request, Industry  $industry){

        if ($request->suspend == "true"){
            $industry->update(['is_suspend' => 1]);
        }else{
            $industry->update(['is_suspend' => 0]);
        }

        return $this->success("بيانات الصناعة" , new IndustryList($industry));
    }

    function modelDeliveryWay($request, $industry){

        $industry->{'name:ar'} = $request->name_ar;
        $industry->{'name:en'} = $request->name_en;
        $industry->save();
    }
}

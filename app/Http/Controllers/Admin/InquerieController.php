<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\InquerieResource;
use App\Http\Resources\User\UserFilterResource;
use App\Libraries\oneSignal;
use App\Models\Device;
use App\Models\Inqueries;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class InquerieController extends Controller
{
    use RespondsWithHttpStatus , paginationTrait;

    public  $lang;
    public $push;
    public function __construct(oneSignal $push)
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);
        $this->push = $push;
    }

    public function index(Request $request)
    {
        $inqueries = Inqueries::whereIsAccepted(0);

        $count_inqueries  = $inqueries->count();

        $this->pagination_query($request, $inqueries);

        $data = InquerieResource::collection($inqueries->get());

        return $this->successWithPagination('إستفسارات العروض' ,$count_inqueries ,$data);
    }



    public function update(Request $request, $id)
    {
        $inqueries = Inqueries::findOrFail($id);

        $inqueries->update(['is_accepted' => 1 ,'created_at' => now() , 'message' => $request->message]);

        $this->send_notification($inqueries);

        $data = new InquerieResource($inqueries);

        return $this->success("تم التعديل بنجاح" , $data);
    }

    public function acceptedInquerie(Request $request, $id){

        $inqueries = Inqueries::findOrFail($id);

        $inqueries->update(['is_accepted' => 1 ,'created_at' => now() ]);

        $this->send_notification($inqueries);

        return $this->success('  تم قبول الرسالة  بنجاح ' );
    }

    public function send_notification($inqueries)
    {
        $order = $inqueries->order;

        $user_offer_id = $inqueries->user_offer_id;

        if ($inqueries->is_accepted == 1){

            $user_device_id = $inqueries->sender_id != $order->user_id ?  $order->user_id : $inqueries->user_offer_id ;
        }else{

            $user_device_id = $inqueries->sender_id  ;
        }

        $devices = Device::whereUserId($user_device_id)->pluck('device');

        $title = "inqueries";
        $body = "inqueries_offers";

        $content_en    = Lang::get('order.inqueries_offers',["order_name" => $order->title],'en') ;
        $content_ar    = Lang::get('order.inqueries_offers',["order_name" => $order->title],'ar') ;

        $my_data = [
            'id'            => $inqueries->id,
            'order_id'      => $inqueries->order_id,
            'title'         => optional($inqueries->order)->title,
            'sender'        => new UserFilterResource($inqueries->sender),
            'receiver'     => new UserFilterResource($inqueries->user_offer),
            'message'       => $inqueries->message,
            'created_at'    => $inqueries->created_at
        ];

        $this->push->sendMessage($devices,[
            'order_id'          => $order->id,
            'user_offer_id'     =>  $user_offer_id,
            'title_key'         => $title,
            'body_key'          => $body,
            'message'           => $inqueries->message,
            'my_data'           => $my_data,
            'created_at'    => $inqueries->created_at,
        ],$content_en,$content_ar,"inqueries","الإستفسارات");
    }

    public function refuseInquerie(Request $request,$id){

        $inqueries = Inqueries::findOrFail($id);

        $inqueries->update(['is_accepted' => -1 ]);

        $this->send_notification($inqueries);

        return $this->success('  تم رفض الرسالة  بنجاح ' );
    }

}

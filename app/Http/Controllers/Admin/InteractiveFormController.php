<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\InteractiveFormList;
use App\Models\InteractiveForm;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class InteractiveFormController extends Controller
{

    use RespondsWithHttpStatus ,paginationTrait;

    public function index(Request $request){
        $interactive_forms =  InteractiveForm::query();

        if ($request->type){
            $interactive_forms->where("type","=",$request->type);
        }
        $total_count = $interactive_forms->count();

        $this->pagination_query($request , $interactive_forms);

        $data = InteractiveFormList::collection($interactive_forms->with(['user',"reason_rejection"])->get());

        return $this->successWithPagination("الأسباب او الشكاوي",$total_count,$data);
    }
}

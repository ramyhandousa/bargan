<?php

namespace App\Http\Controllers\Admin;

use App\Events\NewOfferOrEditInOrder;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\OfferListResource;
use App\Libraries\oneSignal;
use App\Models\Device;
use App\Models\Notification;
use App\Models\Offer;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class OfferController extends Controller
{
    use RespondsWithHttpStatus , paginationTrait;

    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }

    public function index(Request $request)
    {
       $offers = Offer::whereIsAcceptedAndStatus(0,'pending');

        $count_offers  = $offers->count();

        $this->pagination_query($request, $offers);

        $data = OfferListResource::collection($offers->get());

        return $this->successWithPagination('العروض' ,$count_offers ,$data);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }



    public function acceptedOffer(Request $request,Offer $offer){

        $offer->update(['is_accepted' => 1 ]);

//        $order = $offer->order;

//        $user = $offer->user;

//        event(new NewOfferOrEditInOrder($user,$order,$offer,$request) );

        $key = $offer->is_edit == 1 ? 'edit_new_offer' :  'add_new_offer';

        $this->send_push_notify_to_user($key,$offer,$key);

        return $this->success('  تم قبول العرض  بنجاح ' );
    }

    public function refuseOffer(Request $request,Offer $offer){

        $offer->update(['is_accepted' => -1 ,'status' => 'refuse' ,'time_out' => 1 ]);

        $this->send_push_notify_to_user('offer_refused',$offer,'offer_refused_by_admin',null,'offer');

        return $this->success('  تم رفض العرض  بنجاح ' );
    }

    private function send_push_notify_to_user(  $key , $offer , $body_key ,$request  = null,$to = 'order'){

        $order = $offer->order;

        $data_user = $to == 'order' ? $order->user : $offer->user ;

        $devices        = Device::whereUserId($data_user->id)->pluck('device');

        $body_argument = $this->content_notification($order,$devices,$key,$body_key,$request,$offer);

        Notification::create(['user_id' => $data_user->id,'order_id' => $order->id,'offer_id' => $offer->id,
            'title' => 'orders' ,'translation' => $key,'body' => $key ,
            'body_arguments' => json_encode($body_argument) ,
        ]);
    }


    private function content_notification($order,$devices,$key,$body_key,$request, $offer){
        $order_title    = $order->title;

        $title_ar      = Lang::get('order.orders',[],'ar') ;
        $title_en      =  Lang::get('order.orders',[],'en') ;

        $content_en    = Lang::get('order.'.$key,['user_name' => $offer->rand_user_id,'order_name' => $order_title ,'message_en' => $request ? $request['message_en'] :null ],'en') ;
        $content_ar    = Lang::get('order.'.$key,['user_name' => $offer->rand_user_id,'order_name' => $order_title ,'message_ar' => $request ? $request['message_ar'] :null ],'ar') ;

        $this->push->sendMessage($devices,
            [
                'order_id'      => $order->id,
                'title_key'     => 'orders',
                'body_key'      => $body_key,
                'body_arguments'    => [
                    "order_name"    => $order_title
                ],
            ],$content_en,$content_ar,$title_en,$title_ar
        );

        $body_argument = [
            "order_name" => $order->title ,
            'user_name' => $offer->rand_user_id
        ];

        if ($request){
            $body_argument['message_ar'] = $request['message_ar'];
            $body_argument['message_en'] = $request['message_en'];
        }

        return $body_argument;
    }

}

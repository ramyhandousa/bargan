<?php

namespace App\Http\Controllers\Admin;

use App\Events\NewCategoryInOrder;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Order\UpdateOrder;
use App\Http\Requests\api\validImage;
use App\Http\Resources\Admin\Order\InteractiveOrder;
use App\Http\Resources\Admin\Order\OfferOrder;
use App\Http\Resources\Admin\Order\ShowDetails;
use App\Http\Resources\Admin\OrderList;
use App\Http\Resources\Admin\showOrderResource;
use App\Http\Resources\Order\OrderShowResource;
use App\Libraries\oneSignal;
use App\Libraries\PushNotification;
use App\Models\Device;
use App\Models\Notification;
use App\Models\Order;
use App\Models\Setting;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class OrderController extends Controller
{

    use RespondsWithHttpStatus , paginationTrait;


    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }

    public function index(Request $request){

        $orders =  Order::query();
        if ($request->has('is_accepted') && $request->is_accepted == "false"){

            $orders->where('is_accepted','=',0);
        }else{
            $orders->whereIn('is_accepted', ['1', '-1']);
        }

        if ($request->has("order_type")){
            $orders->where("order_type","=",$request->order_type);
        }

        if ($request->has("account_type")){
            $orders->where("account_type","=",$request->account_type);
        }

        if ($request->has("status")){
            if ($request->status == 'red_alert'){
                $system_time = now()->subMinutes(Setting::getBody('system_alert_minutes'));
                $orders->whereBetween('end_bidding_duration',[now(), $system_time]);
            }else{
                $orders->where("status","=",$request->status);
            }
        }

        if ($request->query("query") != "null"){

            $orders->where('title', 'LIKE', "%{$request->query("query")}%");
        }

        $count_orders = $orders->count();

        $this->pagination_query($request, $orders);

        $data = OrderList::collection($orders->get());

        return $this->successWithPagination('الطلبات' ,$count_orders ,$data);
    }


    public function show(Order $order){

        return $this->success('بيانات الطلب', new showOrderResource($order));
    }

    public function details(Request  $request , Order $order){

        $data = $request->has("offers") ?
                    OfferOrder::collection($order->offers) :
                        InteractiveOrder::collection($order->interactive_forms) ;

//        return $this->success('العروض او الشكاوي للطلب', new ShowDetails($order));
        return $this->success('العروض او الشكاوي للطلب', $data);
    }


    public function update(UpdateOrder  $request,Order $order){

        $data = array_merge($request->validated() ,[ 'created_at' => now() , 'is_accepted' => 1,
            'end_bidding_duration' =>  Carbon::parse(now())->addMinutes($order->bidding_duration)]);

        $order->update($data);

        $this->send_push_notify_to_user('order_updated_by_admin',$order,'order_updated_by_admin');

        event(new NewCategoryInOrder( $request,$order));

        return $this->success('تم تعديل الطلب بنجاح', new OrderShowResource($order));
    }


    public function acceptedOrder(Request $request,Order $order){

        $order->update(['is_accepted' => 1 ,   'end_bidding_duration' =>  Carbon::parse(now())->addMinutes($order->bidding_duration)]);

        $this->send_push_notify_to_user('order_accepted',$order,'order_updated_by_admin');

        event(new NewCategoryInOrder( $request,$order));

        return $this->success('  تم قبول الطلب  بنجاح ' );
    }

    public function refuseOrder(Request $request,Order $order){

        $message = json_encode(['message_ar' => $request->message_ar ,'message_en' => $request->message_en ,]) ;

        $order->update(['is_accepted' => -1 ,'message'=>$message ,  'status' =>  'refuse_system' , 'time_out' => 1 ]);

        $this->send_push_notify_to_user('order_refuse',$order,'order_refuse_by_admin' , $request);

        return $this->success('  تم رفض الطلب  بنجاح ' );
    }

    public function suspendOrder(Request  $request, Order $order){

        $order->update(['is_suspend' =>  $request->suspend == "true" ? 1 : 0]);

        $message =  [
            'message_ar' => ' برجاء التواصل مع الإدارة '.$order->title." تم حظر الطلب ",
            'message_en' => ' Your Order '. $order->title .' Has Been Suspend Please contact the administration '
        ];

        $this->send_push_notify_to_user('suspend_order',$order,'suspend_order_message' , $message);

        return $this->success('  تم العملية  بنجاح ' );
    }


    private function send_push_notify_to_user(  $key , $order , $body_key ,$request  = null){

        $devices        = Device::whereUserId($order->user_id)->whereHas('user',function ($user){
            $user->where('disable_notifications',0);
        })->pluck('device');

        $order_title    = $order->title;

        $title_ar      = Lang::get('order.orders',[],'ar') ;
        $title_en      =  Lang::get('order.orders',[],'en') ;

        $content_en    = Lang::get('order.'.$key,['order_name' => $order_title ,'message_en' => $request ? $request['message_en'] :null ],'en') ;
        $content_ar    = Lang::get('order.'.$key,['order_name' => $order_title ,'message_ar' => $request ? $request['message_ar'] :null ],'ar') ;

        $this->push->sendMessage($devices,
            [
                'order_id'      => $order->id,
                'title_key'     => 'orders',
                'body_key'      => $body_key,
                'body_arguments'    => [
                    "order_name"    => $order_title
                ],
            ],$content_en,$content_ar,$title_en,$title_ar
        );

        $body_argument = [
            "order_name" => $order->title ,
        ];

        if ($request){
            $body_argument['message_ar'] = $request['message_ar'];
            $body_argument['message_en'] = $request['message_en'];
        }

        Notification::create(['user_id' => $order->user_id,'order_id' => $order->id,
            'title' => 'orders' ,'translation' => $key,'body' => $key ,
            'body_arguments' =>json_encode($body_argument) ,
        ]);
    }


    public function upload(validImage $request){

        if ($request->file('image')    ){
            $file = $request->image ;

            $imageName  =  Str::random(20) .$file->getClientOriginalName();
            $file->move(public_path('files'), $imageName);

            $filename = 'files/'.$imageName;

            return $this->success(' الملف', ['file' => $filename]);
        }

        return  $this->failure("يوجد خطا ما");
    }
}

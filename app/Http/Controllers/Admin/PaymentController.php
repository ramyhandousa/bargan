<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Delivery\DeliveryWayStoreVaild;
use App\Http\Requests\Admin\ReasonReject\ReasonStoreVaild;
use App\Http\Resources\Admin\ModelGlobalFilter;
use App\Models\PaymentMethod;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    use RespondsWithHttpStatus;

    public function index(Request $request)
    {
        $payment_methods =  PaymentMethod::latest()->get();

        $data = ModelGlobalFilter::collection($payment_methods);

        return $this->success("البيانات",$data);
    }



    public function store(DeliveryWayStoreVaild $request)
    {

        $reasonRejection = new PaymentMethod();

        $this->modelCategory($request,$reasonRejection);

        return $this->success("تم الإضافة " , new ModelGlobalFilter($reasonRejection));
    }


    public function show(PaymentMethod  $paymentMethod)
    {
        return $this->success("البيانات" , new ModelGlobalFilter($paymentMethod));
    }



    public function update(DeliveryWayStoreVaild $request, PaymentMethod  $paymentMethod)
    {
        $this->modelCategory($request,$paymentMethod);

        return $this->success("تم التعديل " , new ModelGlobalFilter($paymentMethod));
    }


    public function suspendBoolean(Request $request, PaymentMethod  $paymentMethod){

        $paymentMethod->update(['is_suspend' => $request->suspend == "true" ? 1 : 0]);

        return $this->success("البيانات" , new ModelGlobalFilter($paymentMethod));
    }

    function modelCategory($request, $paymentMethod){
        $paymentMethod->{'name:ar'} = $request->name_ar;
        $paymentMethod->{'name:en'} = $request->name_en;
        $paymentMethod->save();
    }
}

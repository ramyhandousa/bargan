<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PromoCodeVaild;
use App\Http\Resources\Admin\PromoCodeFilter;
use App\Http\Resources\Admin\UsersPromoCodeResource;
use App\Models\PromoCode;
use App\Models\Transaction;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class PromoCodesController extends Controller
{

    use RespondsWithHttpStatus;

    public function index(){
        $codes = PromoCode::where('is_deleted',0)->withCount('users_count_code')->get();

        return $this->success("إدارة أكواد الخصم", PromoCodeFilter::collection($codes));
    }

    public function users_code(Request $request) // User Have Used Code
    {
        if (!$request->code){
            return  $this->failure("برجاء الكود لسترجع البيانات");
        }
        $transaction  = Transaction::wherePromoCode($request->code)->with('user')
            ->get(['id','user_id','promo_code','created_at']);

        return $this->success("عدد مستخدمي الكود", UsersPromoCodeResource::collection($transaction));
    }

    public function create(){
        $pageName = 'إضافة كواد الخصم';
        return view('admin.promo_codes.create',compact('pageName'));
    }

    public function store(PromoCodeVaild  $request){

        $promoCode =  PromoCode::create($request->validated());

        return $this->success("تم إضافة الكود" , new PromoCodeFilter($promoCode));
    }


    public function edit($id){

        $promo_code = PromoCode::findOrFail($id);

        $pageName = 'تعديل  كود الخصم';

        return view('admin.promo_codes.edit',compact('pageName','promo_code'));
    }

    public function update(PromoCodeVaild  $request , $id){
        $promo_code = PromoCode::findOrFail($id);

        $promo_code->update($request->validated());

        return $this->success("تم تعديل الكود" ,  new PromoCodeFilter($promo_code));
    }

    public function destroy($id){
        $promo_code = PromoCode::findOrFail($id);

        $promo_code->update(['is_deleted' => 1]);

        return $this->success("تم مسح كود الخصم");
    }


    public function delete(Request  $request){

        $promo_code = PromoCode::findOrFail($request->id);

        $promo_code->update(['is_deleted' => 1]);

        return response()->json([
            'status' => true,
            'data' => $promo_code->id
        ]);
    }

    public function suspend(Request $request , PromoCode $promoCode)
    {

        if ($request->suspend == "true"){
            $promoCode->update(['is_suspend' => 1]);
        }else{
            $promoCode->update(['is_suspend' => 0]);
        }

        $promoCode->loadCount('users_count_code');

        return $this->success("بيانات الكود" ,  new PromoCodeFilter($promoCode));
    }

}

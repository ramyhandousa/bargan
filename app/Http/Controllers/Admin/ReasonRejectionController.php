<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ReasonReject\ReasonStoreVaild;
use App\Http\Resources\Admin\ModelGlobalFilter;
use App\Models\ReasonRejection;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class ReasonRejectionController extends Controller
{
    use RespondsWithHttpStatus;

    public function index(Request $request)
    {
        $reasonRejection = ReasonRejection::query();

        $type = in_array($request->type,['admin_rejection','offer','cancel_order','complaint','Interact_order']);

        if ($type){
            $reasonRejection->where('type','=',$request->type);
        }

        $data = ModelGlobalFilter::collection($reasonRejection->latest()->get());

        return $this->success("البيانات",$data);
    }



    public function store(ReasonStoreVaild $request)
    {

        $reasonRejection = new ReasonRejection();

        $this->modelCategory($request,$reasonRejection);

        return $this->success("تم الإضافة " , new ModelGlobalFilter($reasonRejection));
    }


    public function show(ReasonRejection  $reasonRejection)
    {
        return $this->success("البيانات" , new ModelGlobalFilter($reasonRejection));
    }



    public function update(ReasonStoreVaild $request, ReasonRejection  $reasonRejection)
    {
        $this->modelCategory($request,$reasonRejection);

        return $this->success("تم التعديل " , new ModelGlobalFilter($reasonRejection));
    }


    public function suspendBoolean(Request $request, ReasonRejection  $reasonRejection){

        if ($request->suspend == "true"){
            $reasonRejection->update(['is_suspend' => 1]);
        }else{
            $reasonRejection->update(['is_suspend' => 0]);
        }

        return $this->success("البيانات" , new ModelGlobalFilter($reasonRejection));
    }

    function modelCategory($request, $reasonRejection){
        $reasonRejection->type = $request->type;
        $reasonRejection->{'name:ar'} = $request->name_ar;
        $reasonRejection->{'name:en'} = $request->name_en;
        $reasonRejection->save();
    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Setting\SettingVaildRequest;
use App\Libraries\oneSignal;
use App\Models\Device;
use App\Models\Setting;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    use RespondsWithHttpStatus;

    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }

    public function index(){

        return $this->success("الإعادات",Setting::all(['key','body']));
    }

    public function store(SettingVaildRequest $request){

        foreach ($request->validated() as $key => $value) {

            Setting::updateOrCreate(['key' => $key], ['body' => $value]);
        }

        $data = Setting::whereKey($request->key)->first();

        return $this->success("الإعادات",$data);
    }

    public function send_public_notification(Request  $request){

        $title_ar = 'بارجين';
        $title_en = 'bargain';
        $message_ar = $request->message_ar;
        $message_en = $request->message_en;
        $data = [
            'title_key'     => $title_ar,
            'body_key'      => $message_ar,
            'body_arguments'    => [],
        ];

        if ($request->user_id){

            $devices = Device::whereUserId($request->user_id)->pluck('device');

            try {
                $this->push->sendMessage($devices,$data,$message_en,$message_ar,$title_en,$title_ar);

                return $this->success("تم الإرسال بنجاح");
            }catch (\Exception $exception){
                return $this->failure("حدث خطأ ما  " . $exception);
            }

        }else{
            try {

                $this->push->send_public_data($data,$message_ar,$title_ar);
                $this->push->send_public_data($data,$message_en,$title_en,'en');

                return $this->success("تم الإرسال بنجاح");
            }catch (\Exception $exception){
                return $this->failure("حدث خطأ ما  ");
            }
        }


    }

}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StaticData\StaticDataStoreRequest;
use App\Http\Resources\Admin\StaticDataList;
use App\Models\StaticData;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class StaticDataController extends Controller
{
    use RespondsWithHttpStatus;

    public function index(Request $request){

        $type = $request->type ? : "intro_page";

        $labels = StaticData::where('type',$type)->latest()->get();

        $data = StaticDataList::collection($labels);

        return $this->success('البيانات ', $data);
    }

    public function show( $id){

        $label = StaticData::findOrFail($id);

        return $this->success("التفاصيل", new StaticDataList($label));
    }

    public function store(StaticDataStoreRequest $request){

        $label = new StaticData();

        $this->modelStaticData($request,$label);

        return $this->success("تم الإضافة بنجاح", new StaticDataList($label));
    }


    public function update(StaticDataStoreRequest $request, $id){

        $label = StaticData::findOrFail($id);

        $this->modelStaticData($request,$label);

        return $this->success("تم التعديل بنجاح  ", new StaticDataList($label));
    }

    public function destroy($id){
        $label = StaticData::findOrFail($id);

        $label->delete();

        return $this->success("تم المسح بنجاح  ");
    }


    function modelStaticData($request, $label){

        $label->{'name:ar'}       = $request->name_ar;
        $label->{'name:en'}       = $request->name_en;
        $label->{'title:ar'}       = $request->title_ar;
        $label->{'title:en'}       = $request->title_en;
        $label->type              = $request->type;
        $label->image             = $request->image;
        $label->color             = $request->color;
        $label->save();

    }

}

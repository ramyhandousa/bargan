<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Terms\TermStoreRequest;
use App\Http\Resources\Admin\TermsList;
use App\Models\Term;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class TermController extends Controller
{
    use RespondsWithHttpStatus;

    public function index(Request $request){

        $labels = Term::latest()->get();

        $data = TermsList::collection($labels);

        return $this->success('البيانات ', $data);
    }

    public function show( $id){

        $label = Term::findOrFail($id);

        return $this->success("التفاصيل", new TermsList($label));
    }

    public function store(TermStoreRequest $request){

        $label = new Term();

        $this->modelStaticData($request,$label);

        return $this->success("تم الإضافة بنجاح", new TermsList($label));
    }


    public function update(TermStoreRequest $request, $id){

        $label = Term::findOrFail($id);

        $this->modelStaticData($request,$label);

        return $this->success("تم التعديل بنجاح  ", new TermsList($label));
    }

    public function destroy($id){
        $label = Term::findOrFail($id);

        $label->delete();

        return $this->success("تم المسح بنجاح  ");
    }


    function modelStaticData($request, $label){

        $label->{'name:ar'}       = $request->name_ar;
        $label->{'name:en'}       = $request->name_en;
        $label->save();

    }
}

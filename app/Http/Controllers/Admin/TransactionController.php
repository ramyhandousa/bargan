<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\TransactionResource;
use App\Models\Transaction;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class TransactionController extends Controller
{

    use RespondsWithHttpStatus , paginationTrait;

    public function __invoke(Request $request)
    {
       $payment = $request->payment;

       $progress = $request->progress;

       $transaction = Transaction::wherePaymentAndProgress($payment,$progress);

        $total_count =  $transaction->count();

        $this->pagination_query($request,$transaction);

        $data =   TransactionResource::collection($transaction->get());

        return $this->successWithPagination("  نتائج البحث", $total_count , $data);
    }

}

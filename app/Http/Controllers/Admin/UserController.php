<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OrdersExport;
use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Http\Resources\Admin\showUserFilter;
use App\Http\Resources\Admin\TransactionResource;
use App\Http\Resources\Admin\UserList;
use App\Libraries\oneSignal;
use App\Models\Notification;
use App\Models\Transaction;
use App\Models\User;
use App\Models\VerifyUser;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    use RespondsWithHttpStatus ,paginationTrait;

    public $push;

    public function __construct(oneSignal $push)
    {
        $this->push = $push;
    }

    public function index(Request  $request){
        $users = User::where('defined_user','user');
//        where('is_active_phone',1)->
        if ($request->skip_user){
            $users->where('skip_user',1);
        }
        $q = $request->q;
        if ($q){
            $users->where('name', 'LIKE', "%$q%")
                ->orWhere('phone', 'LIKE', "%$q%")
                ->orWhere('email', 'LIKE', "%$q%");
        }
        $total_count =  $users->count();

        $this->pagination_query($request,$users);

        $data =   UserList::collection($users->get());

        return $this->successWithPagination("بيانات المستخدمين", $total_count , $data);
    }


    public function search_users(Request $request)
    {
        $q = $request->q;

        $users = User::where('name', 'LIKE', "%$q%")
            ->orWhere('phone', 'LIKE', "%$q%")
            ->orWhere('email', 'LIKE', "%$q%");

        $total_count =  $users->count();

        $this->pagination_query($request,$users);

        $data =   UserList::collection($users->get());

        return $this->successWithPagination("  نتائج البحث", $total_count , $data);
    }

    public function show(User $user){
        return $this->success("بيانات المستخدمين", new showUserFilter($user));
    }


    public function suspendBoolean(Request $request, User  $user){

        if ($request->suspend == "true"){
            $user->update(['is_suspend' => 1]);
        }else{
            $user->update(['is_suspend' => 0]);
        }

        return $this->success("بيانات المستخدم" , new UserList($user));
    }


    public function refund_requested(Request  $request){

        $wallets = Transaction::where('refund_requested','!=',"pending")->with(['order','user']);

        $this->pagination_query($request,$wallets);

        $data = TransactionResource::collection($wallets->get());

        return $this->success("طلبات الإسترجاع", $data);
    }

    public function accepted_refund_requested(Transaction  $transaction){

        $price = $transaction->price;
        $order = $transaction->order;
        $offer = $transaction->offer;
        $user = $transaction->user;
        $key = "order_refund_requested_accepted";
        $message = json_encode(['message_ar' => 'تم رفض الطلب بسبب إسترداد المبلغ المدفوع' ,
            'message_en' => 'The request was rejected due to the refund of the amount paid']) ;

        // Order Belongs To User
        if ($order->user_id == $user->id){

            $transaction_user = $user;
            $this->send_push_notify_to_user($transaction_user,$key,$order);
            $order->update(['is_pay' => 0,'status' => 'refuse' ,'message' => $message]);
        }else{
            // This Mean Transaction Belongs To Offer Man

            if (!$offer){
                return  $this->failure('لأسف .. لا يوجد صاحب عرض  لإسترداد المبلغ ');
            }
            $transaction_user = $offer->user;
            $this->send_push_notify_to_user($transaction_user,$key,$order);
            $offer->update(['is_pay' => 0,'status' => 'refuse' ,'message' => $message]);
            if ($order->offer_id = $offer->id){
                $order->update(['offer_id' => null ,'user_offer_id' => null]);
            }
        }

        Transaction::create(['user_id' => $transaction_user->id,'order_id' => $order->id ,'progress' => 'deposit_by_admin',
                             'payment' => 'wallet' ,'price' => abs($price)]);

        $transaction->update(['refund_requested' => "accepted"]);

        return $this->success("تم قبول العملية بنجاح", new TransactionResource($transaction));
    }

    public function refuse_refund_requested(Request $request,Transaction  $transaction){

        $user = $transaction->user;
        $order = $transaction->order;
        $key = "order_refund_requested_refuse";

        $message = json_encode(['message_ar' => $request->message_ar ,
            'message_en' => $request->message_en]) ;

        $this->send_push_notify_to_user($user,$key,$order, $request);

        $transaction->update(['refund_requested' => "refuse" ,'message' => $message]);

        return $this->success("تم رفض العملية بنجاح", new TransactionResource($transaction));
    }


    private function send_push_notify_to_user($user , $key , $order , $request = null){

        $devices = $user->devices->pluck('device');

        if (count($devices) > 0){
            $title_ar      = Lang::get('order.orders',[],'ar') ;
            $title_en      =  Lang::get('order.orders',[],'en') ;

            $content_en    = Lang::get('order.'.$key,['order_name' => $order->title ,'message_en' => $request?$request->message_en:null ],'en') ;
            $content_ar    = Lang::get('order.'.$key,['order_name' => $order->title ,'message_ar' => $request?$request->message_ar:null],'ar') ;

            $this->push->sendMessage( $devices,[
//            'order_id'          =>  $order->id ,
                'wallet'            => true,
                'body_arguments'    => [
                    "order_name" =>  $order->title,
                ],
            ],$content_en,$content_ar,$title_en,$title_ar);
        }

        $body_argument = [
            'order_name' => $order->title,
            'message_ar' => $request?$request->message_ar:null,
            'message_en' => $request?$request->message_en:null,
        ];

        Notification::create(['user_id' => $user->id,'order_id' => $order->id,
                              'title' => 'orders' ,'translation' => $key,'body' => $key ,
                            'body_arguments' =>  json_encode($body_argument)
        ]);
    }


    public function ExportUsers(Request $request)
    {
        return Excel::download(new UsersExport(), 'users-data.xlsx');
    }

    public function ExportOrders(Request $request)
    {
        return Excel::download(new OrdersExport(), 'orders-data.xlsx');
    }


    public function getActivationCode(Request $request)
    {
        if (!$request->phone){
            return $this->failure("برجاء هات التليفون");
        }

        $verfiy = VerifyUser::wherePhone($request->phone)->first();

        if (!$verfiy){
            return  $this->failure('لا يوجد كود لهذا التليفون');
        }

        return    $verfiy->action_code;
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Industry\WarrantyStoreVaild;
use App\Http\Resources\Admin\WarrantyList;
use App\Models\Warranty;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class WarrantyController extends Controller
{
    use RespondsWithHttpStatus;

    public function index(Request  $request){

        $warranty = Warranty::latest()->get();

        return $this->success("انواع الضمان", WarrantyList::collection($warranty));
    }


    public function show(Warranty $warranty){

        return $this->success("بيانات الضمان" , new WarrantyList($warranty));
    }

    public function store(WarrantyStoreVaild  $request){

        $warranty = new Warranty();

        $this->modelDeliveryWay($request,$warranty);

        return $this->success("تم إضافة الضمان" , new WarrantyList($warranty));
    }

    public function update(WarrantyStoreVaild $request , Warranty $warranty){

        $this->modelDeliveryWay($request,$warranty);

        return $this->success("تم تعديل الضمان" , new WarrantyList($warranty));
    }

    public function suspendBoolean(Request $request, Warranty  $warranty){

        if ($request->suspend == "true"){
            $warranty->update(['is_suspend' => 1]);
        }else{
            $warranty->update(['is_suspend' => 0]);
        }

        return $this->success("بيانات الضمان" , new WarrantyList($warranty));
    }

    function modelDeliveryWay($request, $warranty){

        $warranty->{'name:ar'} = $request->name_ar;
        $warranty->{'name:en'} = $request->name_en;
        $warranty->save();
    }
}

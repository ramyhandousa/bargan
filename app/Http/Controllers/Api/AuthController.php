<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\Auth\changePassRequest;
use App\Http\Requests\api\Auth\checkActivation;
use App\Http\Requests\api\Auth\checkPhoneOrEmailExist;
use App\Http\Requests\api\Auth\editUser;
use App\Http\Requests\api\Auth\emaiRequestVaild;
use App\Http\Requests\api\Auth\login;
use App\Http\Requests\api\Auth\register_user;
use App\Http\Requests\api\Auth\resendCodeValid;
use App\Models\VerifyUser;
use App\Repositories\AuthRepository;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use RespondsWithHttpStatus;
    private $authRepository;

    public function __construct(AuthRepository $authRepository)
    {
        $this->authRepository = $authRepository;

        $this->middleware('auth:api')
            ->only(['changPassword' ,'user_info','editProfile','logOut','refreshToken']);
    }


    public function registerUser(register_user  $request){

        $data = $this->authRepository->register($request);

        return $this->success(  "تم تسجيل الدخول بنجاح",$data);
    }

    public function login(login  $request){
        $data =   $this->authRepository->login($request);

        return $this->success(trans('global.logged_in_successfully'),$data);
    }

    public function get_test_code(){
        $verfiy_code = VerifyUser::wherePhone('0599999999')->first();

        $data = ['phone' => $verfiy_code->phone ,'code' => $verfiy_code->action_code];

        return $this->success(  "الكود ",$data);
    }

    public function user_info(Request  $request){
        $data =   $this->authRepository->user_info($request->user());

        return $this->success('user_info',$data);
    }

    public function forgetPassword(checkPhoneOrEmailExist $request){

        $data =    $this->authRepository->forgetPassword($request);

        return $this->success($data['message'],['code' => $data['code'] ]);
    }

    public function resetPassword(checkPhoneOrEmailExist $request){

        $data =    $this->authRepository->resetPassword($request);

        return $this->success( $data['message'],$data['data']);
    }

    public function resendCode(resendCodeValid $request){

        $data =    $this->authRepository->resendCode($request);

        return $this->success(  $data['message'], ['code' => $data['code'] ] );
    }

    public function checkCodeActivation(checkActivation $request){

        $data =    $this->authRepository->checkCode($request);

        return $this->success(  trans('global.your_account_was_activated'),$data);
    }

    public function checkCodeCorrect(checkActivation $request){

        return $this->success('الكود صحيح');
    }

    public function changPassword ( changePassRequest  $request )
    {
        $data =    $this->authRepository->changPassword($request);

        return $this->success(  $data['message']);
    }

    public function editProfile (editUser $request )
    {
        $data =  $this->authRepository->editProfile($request);

        return $this->success(  trans('global.profile_edit_success') ,$data);
    }


    public function logOut(Request  $request){
        $this->authRepository->logOut($request);

        return $this->success(  trans('global.logged_out_successfully'));
    }

    public function refreshToken(Request  $request){
        $this->authRepository->refreshToken($request);

        return $this->success(  'ok');
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Dynamic\orderDynamic;
use App\Models\Label;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class DynamicController extends Controller
{

    use RespondsWithHttpStatus;

    public  $lang;
    public function __construct( )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);
    }

    public function orders(){

        $labels = Label::where('definition_type','order')->whereDeleted(0)
                    ->whereHas('label_values')->with('label_values')->get();

        $data = orderDynamic::collection($labels);

        return $this->success('بيانات الطلب الإضافي ', $data);

    }

    public function offers(){

        $labels = Label::where('definition_type','offer')->whereDeleted(0)
                        ->whereHas('label_values')->with('label_values')->get();

        $data = orderDynamic::collection($labels);

        return $this->success('بيانات العرض الإضافي ', $data);

    }



}

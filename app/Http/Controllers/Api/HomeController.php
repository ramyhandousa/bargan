<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Order\OrderIndex;
use App\Models\Order;
use App\Models\User;
use App\Scoping\Scopes\NameScope;
use App\Scoping\Scopes\OrderAccountType;
use App\Scoping\Scopes\OrderCategory;
use App\Scoping\Scopes\OrderType;
use App\Scoping\Scopes\ServiceType;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    use RespondsWithHttpStatus,paginationTrait;

    public  $lang;
    public function __construct( )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);
    }

    public function suggest_for_you(Request  $request){

        $user = User::whereApiToken($request->bearerToken())->with('categories.children')->first();

        $query = Order::query();

        if ($user){
            $categories = $user['categories']->pluck('id')->toArray();
            $children = $user['categories']->pluck('children')
                        ? $user['categories']->pluck('children')->flatten()->pluck('id')->toArray()
                        : null;

            $idsCategories = array_merge($categories, $children);
            $query->where('user_id','=',$user->id)->where('is_accepted','=',0)->orWhere('is_accepted','=',1);

        }else{
            $idsCategories = null;
            $query->where('is_accepted','=',1);
        }

        $query->where([
            ['user_offer_id' ,'=',null],
            ['end_bidding_duration' ,'>=',Carbon::now()->format('Y-m-d H:i:s')],
            ['status','=',"pending"],
            ['is_pay','=',0],
            ['is_suspend','=',0],
        ])->withScopes($this->filterQuery());

        if ($idsCategories){
            $query->whereHas('category',function ($category) use ($idsCategories ){
                    $category->whereIn('id',$idsCategories);
            });
        }

        $query->orderByRaw("case when order_type = 'auction' then expected_price end desc")
           ->orderByRaw("case when order_type = 'tender' then expected_price end asc");

        $this->pagination_query_withOutOrderBy($request,$query);

        $orders = $query->get();

        return $this->success('المقترح لديك',OrderIndex::collection($orders));
    }

    public function all_orders( Request  $request)
    {
        $user = User::whereApiToken($request->bearerToken())->first();

        $query = Order::query();

        if ($user){

            $query->where('user_id','=',$user->id)->where('is_accepted','=',0)->orWhere('is_accepted','=',1);
        }else{
            $query->where('is_accepted','=',1);
        }

         $query->where([
            ['user_offer_id' ,'=',null],
            ['end_bidding_duration' ,'>=',Carbon::now()->format('Y-m-d H:i:s')],
            ['status','=',"pending"],
            ['is_suspend','=',0],
//            ['is_pay','=',0],

        ])->withScopes($this->filterQuery());

        $this->pagination_query($request,$query);

        $orders = $query->get();

        return $this->success('  كل الطلبات',OrderIndex::collection($orders));
    }


    protected function filterQuery(){
        return [
            'account_type' => new OrderAccountType(),
            'category_id' => new OrderCategory(),
            'order_type' => new OrderType(),
            'service_type' => new ServiceType(),
            'query'       => new NameScope()
        ];
    }


}

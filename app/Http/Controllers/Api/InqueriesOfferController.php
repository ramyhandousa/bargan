<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\offer\InquerieValidStore;
use App\Http\Resources\Offer\InquerieResource;
use App\Libraries\oneSignal;
use App\Mail\newInquerire;
use App\Models\Device;
use App\Models\Inqueries;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Setting;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;

class InqueriesOfferController extends Controller
{
    use RespondsWithHttpStatus,paginationTrait;

    public  $lang;
    public $push;
    public function __construct(oneSignal $push)
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);
        $this->push = $push;
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        if (!$request->order_id){
            return $this->failure("برجاء order_id");
        }

        $user = $request->user();

        if ($request->user_offer_id){
            $master_user_id = $request->user_offer_id;
        }else{

            $offer = Offer::find($request->offer_id);
            $master_user_id = $offer ? $offer->user_id: $user->id;
        }

        $inqueries = Inqueries::whereOrderIdAndUserOfferId($request->order_id,$master_user_id)
            ->where(function($query) use ($user) {
                $query->where('sender_id', '=', $user->id)->whereIn('is_accepted',[-1,1,0])
                    ->orWhere('is_accepted', '=', 1);
            });

        $count_inqueries = $inqueries->count();

        $this->pagination_query($request, $inqueries);

        $data = InquerieResource::collection($inqueries->get());

        $inqueries->get()->each->update(['is_read' => 1]);

        return $this->successWithPagination('الإستفسارات' ,$count_inqueries ,$data);
    }

    public function store(InquerieValidStore $request)
    {
        $user = $request->user();

        $offer = Offer::with('order')->find($request->offer_id);

        $order = $offer ?   $offer['order'] : Order::find($request->order_id);

        if ($request->user_offer_id){
            $user_offer_id = $request->user_offer_id;
        }else{
            $user_offer_id = $offer ? $offer->user_id: $user->id;
        }

        $inquerie = Inqueries::create([
            'order_id' => $request->order_id ,'sender_id' => $user->id,
            'user_offer_id' =>  $user_offer_id,'message' => $request->message
        ]);

        $email = Setting::getBody("email_bargain");

        if ($email){
            Mail::to($email)->send(new newInquerire($user , $inquerie));
        }

//        $user_device_id = $user->id == $order->user_id ? $user_offer_id : $order->user_id ;
//
//        $devices = Device::whereUserId($user_device_id)->pluck('device');
//
//        $title = "inqueries";
//        $body = "inqueries_offers";
//
//        $content_en    = Lang::get('order.inqueries_offers',["order_name" => $order->title],'en') ;
//        $content_ar    = Lang::get('order.inqueries_offers',["order_name" => $order->title],'ar') ;
//        $this->push->sendMessage($devices,[
//            'order_id'          => $order->id,
//            'user_offer_id'     =>  $user_offer_id,
//            'title_key'         => $title,
//            'body_key'          => $body,
//            'body_arguments'    => [
//                "order_id"          => $order->id,
//                "user_offer_id"     => $user_offer_id,
//                "order_name"        => $order->title
//            ],
//            'created_at'    => $inquerie->created_at,
//        ],$content_en,$content_ar,"inqueries","الإستفسارات");

        return $this->success("تم إضافة الرسالة", new InquerieResource($inquerie));
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\setting\ReasonRejectRequest;
use App\Http\Resources\Admin\CityResource;
use App\Http\Resources\CategoryWithChildrenResource;
use App\Http\Resources\User\UserCategoryResource;
use App\Models\Category;
use App\Models\City;
use App\Models\DeliveryWay;
use App\Models\Industry;
use App\Models\PaymentMethod;
use App\Models\ReasonRejection;
use App\Models\Warranty;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;

class ListController extends Controller
{
    use RespondsWithHttpStatus;

    public  $lang;
    public function __construct( )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);
    }

    public function categories(Request  $request){

        $query = Category::whereIsSuspend(0)->whereIsActive(1)->translatedIn($this->lang)->select('id','image');

        $request->categoryId ? $query->whereParentId($request->categoryId) : $query->whereParentId(0);

        $data = UserCategoryResource::collection($query->get());

        return $this->success('الأقسام',$data);
    }

    public function cities(Request  $request){

        $query = City::whereIsSuspend(0);

        $data = CityResource::collection($query->get());

        return $this->success('المدن',$data);
    }

    public function search_categories(Request  $request){

        $query = Category::whereIsSuspend(0)->whereIsActive(1)->whereParentId(0)->translatedIn($this->lang)->select('id','image');

        $data = CategoryWithChildrenResource::collection($query->get());

        return $this->success('الأقسام',$data);
    }


    public function delivery_ways(Request  $request){

        $data = DeliveryWay::whereIsSuspend(0)->translatedIn($this->lang)->select('id')->get();

        return $this->success('طرق الإستلام', $data);
    }


    public function industries(Request  $request){

        $data = Industry::whereIsSuspend(0)->translatedIn($this->lang)->select('id')->get();

        return $this->success(' الصناعة', $data);
    }

    public function warranties(Request  $request){

        $data = Warranty::whereIsSuspend(0)->translatedIn($this->lang)->select('id')->get();

        return $this->success(' الضمان', $data);
    }

    public function payment_methods(Request  $request){

        $data = PaymentMethod::whereIsSuspend(0)->translatedIn($this->lang)->select('id')->get();

        return $this->success(' طرق الدفع', $data);
    }


    public function reason_rejections(ReasonRejectRequest  $request){

        $data = ReasonRejection::whereIsSuspend(0)->whereType($request->type)
                                ->translatedIn($this->lang)->select('id')->get();

        return $this->success(' الأسباب', $data);
    }

}

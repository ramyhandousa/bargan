<?php

namespace App\Http\Controllers\Api;

use App\Events\RateOrder;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\offer\acceptedOfferVaild;
use App\Http\Requests\api\offer\cancelOfferVaild;
use App\Http\Requests\api\offer\OfferOrderUpdate;
use App\Http\Requests\api\offer\OfferOrderVaild;
use App\Http\Resources\Offer\OrderOfferResource;
use App\Models\Offer;
use App\Models\Order;
use App\Repositories\OfferRepository;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class OfferController extends Controller
{
    use RespondsWithHttpStatus;

    private $offerRepository;
    public  $lang;
    public function __construct(OfferRepository $offerRepository)
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);

        $this->offerRepository = $offerRepository;

//        $this->middleware('auth:api')->except(['show']);
        $this->middleware('auth:api');
        $this->middleware('check_all_status_active');
    }

    public function index(Request  $request){

        $data = $this->offerRepository->list($request);

        return $this->successWithPagination('العروض' ,$data['total_count'] ,$data['data']);
    }

    public function show(Offer $offer){

        $data = $this->offerRepository->show_offer($offer);

        return $this->success('تفاصيل العرض',$data);
    }

    public function showOrderOffer(Order $order){

        $data = $this->offerRepository->showOrderOffer($order);

        return $this->success('تفاصيل الطلب',$data);
    }

    public function store(OfferOrderVaild  $request){

        $data = $this->offerRepository->make_offer($request);

        return  $this->success('تم إضافة العرض بنجاح',$data);
    }

    public function update(OfferOrderUpdate $request, Offer $offer){

        $data = $this->offerRepository->edit_offer($request,$offer);

        return  $this->success('تم تعديل العرض بنجاح',$data);
    }

    public function cancel_offer(cancelOfferVaild $request, Offer $offer){

       $data =  $this->offerRepository->cancel_offer($request,$offer);

        return $this->success('تم سحب العرض بنجاح' , $data);
    }

    public function accepted_offer_by_wallet(acceptedOfferVaild $request, Offer $offer){

        // Offer Man  Pay order by Wallet
       $data =  $this->offerRepository->pay_by_wallet($request,$offer);

       if ($data['status'] == 200){

           return $this->success($data['message'] ,$data['data']);

       }else{

           return  $this->failure($data['message']);
       }
    }

    public function rate_order(Request  $request ,Order $order){

        $order->update(['offer_rate' => $request->rate]);

        event(new RateOrder($order->user,$request,$order,16));

        return $this->success(trans('order.order_rating') ,  new OrderOfferResource($order));
    }

}

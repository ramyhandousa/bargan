<?php

namespace App\Http\Controllers\Api;

use App\Events\RateOrder;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\orders\acceptOfferOrderVaild;
use App\Http\Requests\api\orders\cancelOfferVaild;
use App\Http\Requests\api\orders\cancelOrderVaild;
use App\Http\Requests\api\orders\storeOrder;
use App\Http\Requests\api\orders\VaildChooseAnotherOffer;
use App\Http\Requests\api\orders\VaildGoldenOffer;
use App\Http\Requests\api\removeImage;
use App\Http\Requests\api\validImage;
use App\Http\Resources\Order\OrderShowResource;
use App\Models\Offer;
use App\Models\Order;
use App\Repositories\OrderRepository;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    use RespondsWithHttpStatus;

    private $orderRepository;
    public  $lang;
    public  $path;
    public function __construct(OrderRepository $orderRepository)
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);

        $this->path = 'files/';

        $this->orderRepository = $orderRepository;

        $this->middleware('auth:api')->except(['show','upload','removeImage']);
        $this->middleware('check_all_status_active');
    }

    public function index(Request  $request){

        $data = $this->orderRepository->list($request);

        return $this->successWithPagination('الطلبات' ,$data['total_count'] ,$data['data']);
    }

    public function show(Order $order){

        $data = $this->orderRepository->show($order);

        return $this->success('   تفاصيل الطلب',$data);
    }

    public function store(storeOrder  $request){

        $data = $this->orderRepository->makeOrder($request);

        return $this->success('تم الطلب بنجاح',$data);
    }

    public function update(storeOrder  $request,Order $order){

        $data = $this->orderRepository->updateOrder($order,$request);

        return $this->success('تم إعادة طرح الطلب بنجاح',$data);
    }

    public function update_bidding_duration(Request  $request,Order $order){

        $data = $this->orderRepository->update_bidding_duration($order,$request);

        return $this->success('تم إعادة طرح الطلب بنجاح',$data);
    }

    public function bidding_duration_golden_offer(Request  $request,Order $order){

        $data = $this->orderRepository->update_bidding_duration_golden_offer($order,$request);

        return $this->success(trans('order.reintroduced_golden_offer'),$data);
    }

    public function cancel_order(cancelOrderVaild $request, Order $order){

        $data = $this->orderRepository->cancel_order($order,$request);

        return $this->success('تم سحب الطلب بنجاح',$data);
    }

    public function cancel_offer(cancelOfferVaild $request, Offer $offer){

        $data =  $this->orderRepository->cancel_offer($request,$offer);

        return $this->success(trans('order.cancel_offer') , $data);
    }


    public function accepted_offer_by_wallet(acceptOfferOrderVaild $request, Offer $offer){

        $data =  $this->orderRepository->pay_by_wallet($request,$offer);

        if ($data['status'] == 200){

            return $this->success($data['message'] ,$data['data']);
        }else{

            return  $this->failure($data['message']);
        }
    }

    public function choose_another_offer(VaildChooseAnotherOffer  $request , Offer $offer) {

        $data =  $this->orderRepository->choose_another_offer($request,$offer);

        return $this->success('تم تغير العرض بنجاح' ,$data);
    }

    public function active_golden_offer(VaildGoldenOffer  $request , Order $order){

        $data =  $this->orderRepository->golden_offer($request , $order);

        return $this->success(trans('order.add_golden_offer'),$data);
    }


    public function list_previous_orders( Order $order){

        $data = $this->orderRepository->list_previous_orders($order);

        return $this->success('العروض السابقة  ' , $data);
    }

    public function accepted_order(Order $order){

        $order->update(['is_accepted' => 1]);

        return $this->success('  تم قبول الطلب تقدر تشوفيه يادود  ' );
    }

    public function finish(Request $request,Order $order){
        $data =   $this->orderRepository->finish_order($order,$request);

        return $this->success(trans('order.complete_order_user') , $data);
    }

    public function rate_order(Request  $request ,Order $order){

        $order->update(['order_rate' => $request->rate]);

        event(new RateOrder($order->offer_man,$request,$order,17));

        return $this->success(trans('order.order_rating') ,  new OrderShowResource($order));
    }


    public function upload(validImage $request){

        if ($request->file('image')    ){
            $file = $request->image ;

            $imageName  =  Str::random(20) .$file->getClientOriginalName();
            $file->move(public_path('files'), $imageName);

            $filename = $this->path.$imageName;

            return $this->success(' الملف', ['file' => $filename]);
        }

        return  $this->failure("يوجد خطا ما");
    }

    public function removeImage(Request  $request){

        if (count($request->images) > 0){
            foreach ($request->images as $image){
                 File::delete(public_path().'/'.$image);
            }
            return $this->success('تم مسح الملف بنجاح');
        }

        return $this->failure('غالبا مسار الملف غير صحيح');
    }



}

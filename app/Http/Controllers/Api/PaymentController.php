<?php

namespace App\Http\Controllers\Api;

use App\Events\finishOrder;
use App\Events\GlobalPublicNotification;
use App\Events\OfferManAcceptedOrder;
use App\Events\OrderGoldenOfferActive;
use App\Events\OrderGoldenOfferNotActive;
use App\Events\RefuseOtherOffersWithOutAcceptedOffer;
use App\Events\SendAcceptedOffer;
use App\Events\SendWaitingAcceptedOffer;
use App\Events\UpdateWallet;
use App\Http\Controllers\Controller;
use App\Libraries\myfatoorah;
use App\Models\Offer;
use App\Models\Order;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public $token;

    public $basURL;
    public $initiatePayment;
    public $executePayment;

    public $getPaymentStatus;
    public $callBackUrl;
    public $errorUrl;

    public function __construct( )
    {
        $this->token = 'fo63OGIG1gNdRu96e_ZeaQkGzsA_lUoQWBJKNMVDj-MR3oVtied8mPf9_iEJkVjtnZgrF0Whhlm1pHql4vtkZJax_z09Lv4bHFATzT_IVg5Jaewr9_4a1r8TdcEqtZfEpKQuA9JXVKFu1Spzw2QRFPmzfOyBFDSNjIrDso_GCqE7atJMkSeNrSXHZc1pBO80u2neg76Fu2p5ao6Xxs0z7J-BtgAVN0baogS5PT3Hekp_NS6RFaiIIO0wRHulDsgBKMK0_rZj-QsM1dNcp5ZecqcI7D-A1RLUdN4utOxiNtPDV8nuGsOo1OLS5zC0LaOzF8RgKEob1fch8q2TrIsFR7LpW4h737RBajURmx11zAK9FXlcm763as3QOQDX39xYERO9exchl5BsAmHf9El6-dWu5LjfxyQt-3sM3ttJgEMU6ARRM0J1yidhj-oslyz05iOacipPXFkhIfTMdD8SE5UTF8DqkCDRKent3yjbg_K_t7CUB6mJZVxHMqok8ihGHUeWdyM0oXbIp2cqWzIgoPVe-DnZcJa7RNIaUgOqbzOMY_fsPcQdEx6uYF1Lzs9Y9fYhccrkxYKXo8kRLJwZj3GTm1GgQCOfKYPkTqanUPdQROHoPL0nQQvgumpxo9lDy8-1fMIK8_n2qNBW7Ik70MWnBeJwK9cbqfmLQ4etahJtJgeC-8paDTvtxbjNhtBD7bCKaA';
//        $this->token = 'rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL';

        $this->basURL           = 'https://api.myfatoorah.com';
//        $this->basURL           = 'https://apitest.myfatoorah.com';
        $this->getPaymentStatus  = $this->basURL . '/v2/GetPaymentStatus';
        $this->callBackUrl      = request()->root() . '/payments/success'  ;
        $this->errorUrl         =  request()->root() . '/payments/errors';
    }



    public function orders_success(Request $request,Offer $offer){

        $order = $offer->order;

        $user = User::find($order->user_id);

        $commission =  $order->active_golden_offer == 0 ? Setting::getBody('commission_order') : Setting::getBody('commission_golden_order') ;

        $order->update(['offer_id' => $offer->id  ,'is_pay' => 1 , 'user_offer_id' => $offer->user_id  ]);

        if ($order->active_golden_offer == 0){
            $order->update([ 'commission' => $commission ]);
        }else{
            $order->update([ 'golden_commission' => $commission ]);

            if ($order->old_offer_id == null){
                $order->update(['old_offer_id' => $order->offer_id]);
            }
        }

        $check_to_pay_commission =  $order->active_golden_offer == 0 ? Setting::getBody('commission_offer') : Setting::getBody('commission_golden_offer') ;

        if ($check_to_pay_commission == 0){ // that`s mean want admin not pay the offer man by system

            $offer->update(['is_pay' => 1 , 'status' => 'finish' ]);

            if ($order->golden_offer == 0){
                $order->update(['status' => 'finish']);
            }

            if ($order->golden_offer == 0 || $order->active_golden_offer == 1){

                if($order->active_golden_offer != 1) {
                    event(new finishOrder($order,true,false));
                }

                event(new RefuseOtherOffersWithOutAcceptedOffer($user,$order,$offer,$request));
                event(new OrderGoldenOfferNotActive($user,$order,$offer,$request));
            }else{

                event(new OrderGoldenOfferActive($user,$order,$offer,$request));
            }

        }else{
            $offer->update([ 'status' => 'pending',
                'end_bidding_duration' =>  Carbon::now()->addMinutes(Setting::getBody('waitting_to_accept_offer_minutes')) ]);
        }


        event(new UpdateWallet($user,$commission,$order,$offer,'pull',"online",$request->paymentId));

        event(new SendAcceptedOffer($user,$order,$offer,$request));

//        event(new SendWaitingAcceptedOffer($user,$order,$offer,$request));

        event(new GlobalPublicNotification("GlobalPublic", "GlobalPublic","new_offer_or_edit_in_order","offer_accepted",$order,$offer->user));

        return view("payment");
    }


    public function orders_error(){

        return view("errorPayment");
    }


    public function offers_success(Request $request,Offer $offer){

        $order = $offer->order;

        $user = User::find($offer->user_id);

        if ($order->active_golden_offer == 0) {

            $commission = Setting::getBody('commission_offer');
            $offer->update(['commission' => $commission]);

        }else{

            $user_offer = $offer->where('user_id',$user->id)->where('is_pay',1)->first();

            $commission_golden_offer = Setting::getBody('commission_golden_offer');
            if ($user_offer){

                $commission =   $commission_golden_offer - $user_offer->commission;
            }else{

                $commission =  $commission_golden_offer;
            }

            $offer->update(['golden_commission' => $commission]);
        }

        $offer->update(['is_pay' => 1 ]);

        event(new UpdateWallet($user,$commission,$order,$offer,'pull','online',$request->paymentId));

        if ($order->golden_offer == 0 || $order->active_golden_offer == 1){

            event(new OfferManAcceptedOrder($user,$order,$offer,$request));
            event(new RefuseOtherOffersWithOutAcceptedOffer($user,$order,$offer,$request));
            event(new OrderGoldenOfferNotActive($user,$order,$offer,$request));

        }else{
            event(new OrderGoldenOfferActive($user,$order,$offer,$request));
        }

        return view("payment");
    }


    public function offers_error(){

        return view("errorPayment");
    }


    public function wallet_charge(Request  $request, User $user){

        $transactions = Transaction::where('paymentId' , $request->paymentId)->first();

        if ($transactions){
            $message =  "تم شحن المحفظة مسبقا يبدو لديك مشكلة يرجي الرجوع إلي الإدارة";

            return view("errorPayment",compact("message"));
        }
        $status =  $this->myfatoorahResponse($this->getPaymentStatus,$request);

        if ($status['IsSuccess']){

            $price =  $status['Data']['InvoiceValue'];

            event(new UpdateWallet($user,$price,null,null,'deposit',"wallet",$request->paymentId));

            $message =  "   تم شحن المحفظة بنجاح";

            return view("payment",compact("message"));
        }else{
            $message =  "أثناء عملية شحن المحفظة";

            return view("errorPayment",compact("message"));
        }

    }

    public function wallet_error( ){

        return view("errorPayment");
    }


    public function myfatoorahResponse($curlUrl , $request ){

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $curlUrl,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{  \"keyType\":  \"PaymentId\"  ,\"key\": \"$request->paymentId\"}",
            CURLOPT_HTTPHEADER => array("Authorization: Bearer $this->token", "Content-Type: application/json"),

        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return  json_decode($response, true);
    }

}

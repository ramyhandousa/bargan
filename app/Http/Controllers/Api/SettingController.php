<?php

namespace App\Http\Controllers\Api;

use App\Events\ContactUs;
use App\Http\Controllers\Controller;
use App\Http\Requests\api\setting\Contact_Us_Vaild;
use App\Http\Requests\api\setting\interactive_form_vaild;
use App\Http\Resources\Setting\IntroPageResource;
use App\Mail\emailMessage;
use App\Models\InteractiveForm;
use App\Models\Order;
use App\Models\ReasonRejection;
use App\Models\Setting;
use App\Models\StaticData;
use App\Models\Support;
use App\Models\TypesSupport;
use App\Models\User;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SettingController extends Controller
{
    use RespondsWithHttpStatus;

    public  $lang;
    public function __construct( )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);

        $this->middleware('auth:api')->only(['interactive_form']);
    }

    public function aboutUs(){

        $about_us =  Setting::where('key','about_us_' .$this->lang)->first();

        $data =  [
            'about_us' => $about_us ? $about_us->body : '',
            "twitter" => Setting::getBody('twitter'),
            "facebook" => Setting::getBody('facebook'),
            "snapchat" => Setting::getBody('snapchat'),
            "linkedin" => Setting::getBody('linkedin'),
            "instagram" => Setting::getBody('instagram'),
            "youtube" => Setting::getBody('youtube'),
        ] ;

        return $this->success('عن التطبيق ', $data);
    }

    public function terms_user(){
        $setting = Setting::where('key','terms_user_'.$this->lang)->first();

        $data =  $setting ? $setting->body : '' ;

        return $this->success( 'الشروط والأحكام', $data);
    }

    public function getTypesSupport(){

        $data = TypesSupport::select('id')->where('is_suspend',0)->get();

        return $this->success( 'أنواع التواصل', $data);
    }

    public function contact_us(Contact_Us_Vaild $request ){

        $token = $request->bearerToken();

        $user =  User::whereApiToken($token)->first();
        if ($token){
            if (!$user){
                return  $this->failure(trans('order.session_user_time_out'));
            }
        }
        $support = new Support();
        $support->user_id = 47;
        if ($token && $user){
            $support->sender_id =  $user->id;
        }else{
            $support->skip_user = $request->email;
        }
        $support->type_id = (int) $request->type;
        $support->message = $request->message;
        $support->save();

        $message = [

            'title'     => 'تواصل معنا',
            'type'      => TypesSupport::find($request->type)->name,
            'user'      => $user ? '  من  المستخدم  ' . $user->name :  '   من إيميل المستخدم  ' . $request->email,
            'message'   =>       $request->message
        ] ;

        try {
            Mail::to(Setting::getBody('email_bargain'))->send(new emailMessage($message));
        }catch (\Exception $exception){

        }

        return $this->success( trans('global.message_was_sent_successfully'));
    }

    public function interactive_form(interactive_form_vaild $request){
        $user = Auth::user();

        $reason_rejection = ReasonRejection::find($request->reason_rejection_id);


        $message = [
            'title'     => 'حالات الطلب الطلب',
            'order'     => Order::find($request->order_id),
            'type'      =>  Lang::get('order.'. $reason_rejection->type,[],'ar') ,
            'user'      =>  '   من  المستخدم  ' . $user->name,
            'message'   =>      $reason_rejection->name
        ] ;

        InteractiveForm::create([
            'user_id'               =>  $user->id,
            'type'                  =>  $reason_rejection->type,
            'order_id'              =>  $request->order_id,
            'reason_rejection_id'   =>  $request->reason_rejection_id,
//            'message'               =>  $request->message
        ]);
        try {
            Mail::to(Setting::getBody('email_bargain'))->send(new emailMessage($message));
        }catch (\Exception $exception){

        }

        return $this->success( trans('global.message_was_sent_successfully'));
    }


    public function intro_static_data (){

        $static_data = StaticData::whereType("intro_page")->get();

        $data = IntroPageResource::collection($static_data);

        return $this->success( 'بيانات التطبيق', $data);
    }

    public function intro_golden_offer (){

        $static_data = StaticData::whereType("golden_offer")->get();

        $data = IntroPageResource::collection($static_data);

        return $this->success( 'بيانات العرض الذهبي', $data);
    }

    public function common_questions (){

        $static_data = StaticData::whereType("common_questions")->get();

        $data = IntroPageResource::collection($static_data);

        return $this->success( 'بيانات  الأسئلة الشائعة', $data);
    }
}

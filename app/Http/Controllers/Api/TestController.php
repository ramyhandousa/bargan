<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Libraries\oneSignal;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public $notification;

    public function __construct(oneSignal $notification)
    {
        $this->notification = $notification;
    }


    public function test_notification(){

        $devices = [  "cdf347c6-4a70-4ff7-8511-3748a6d9e94b"];
        $data = [
            "order_id" => 1000,
            "title" => "bola"
        ];

        $headings = [
          'ar' =>  "رامي",
            "en" => "ramy"
        ];

        $content_ar =  'بولة';
        $content_en = 'English Message';

        $buttons = [];

        array_push($buttons,[
            'id' => 'details',
            'text' => 'تفاصيل الطلب'
        ]);
        array_push($buttons,[
            'id' => 'cart',
            'text' => '   السلة'
        ]);

       return $this->notification->sendMessage($devices,$data,$content_en,$content_ar,"السلام عليكم ",Carbon::now());
    }
}

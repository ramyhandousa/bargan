<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\User\NotifaictionCollectionResource;
use App\Http\Resources\User\UserCategoryResource;
use App\Http\Resources\User\UserNotifiactionResource;
use App\Libraries\PushNotification;
use App\Models\Category;
use App\Models\CategoryUser;
use App\Models\Notification;
use App\Models\Setting;
use App\Models\User;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    use RespondsWithHttpStatus , paginationTrait;

    public  $lang;
    public $push;

    public function __construct(PushNotification $push )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        $this->push = $push;
        app()->setLocale($this->lang);

        $this->middleware('auth:api')->except('active_mail_user');
        $this->middleware('check_all_status_active')->except('active_mail_user');
    }

    public function active_mail_user(Request $request){
        $token = substr($request->_token,40);

        $user = \App\Models\User::whereApiToken($token)->with('devices')->firstOrFail();

        $user->update(['is_active_email' => 1]);
        // Collect Devices
        $devices = $user['devices']? $user['devices']->pluck('device') :null;

        if(count($devices ) > 0  ) {

            $this->push->sendPushNotification($devices, null, "email_activation", "your_email_has_been_activated",
                [
                    'type'              => 4,
                    'title_key'         => "email_activation",
                    'body_key'          => "your_email_has_been_activated",
                    'body_arguments'    => [
                        "user_id" => $user->id ,
                    ],
                ]
            );
        }

        return redirect()->route('wasActiveted');
    }
    public function list_notifications(Request $request){

        $user= $request->user();

        $query = Notification::whereUserId($user->id);

        $total_count = $query->count();

        $this->pagination_query($request, $query);

        $check_to_pay_commission =   (int) Setting::getBody('commission_offer') ;

        $data = new NotifaictionCollectionResource($query->get(),$check_to_pay_commission);

        $query->get()->each->update(['is_read' => 1]);

        return $this->successWithPagination('الإشعارات',$total_count,$data);
    }

    public function category_follow(Category $category){

        $user = Auth::user();

        $categoryUser = CategoryUser::whereUserIdAndCategoryId($user->id,$category->id)->first();

        $categoryUser ? $categoryUser->delete()
                      : CategoryUser::create(['user_id' => $user->id,'category_id' => $category->id]);

        return $this->success('القسم' , new UserCategoryResource($category));
    }

}

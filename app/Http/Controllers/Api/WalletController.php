<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\api\cart\VaildEmailCart;
use App\Http\Requests\api\Wallet\PromoCodeVaild;
use App\Http\Resources\Order\OrderCartResource;
use App\Http\Resources\User\MyWalletResource;
use App\Mail\CartEmail;
use App\Mail\emailMessage;
use App\Models\Order;
use App\Models\PromoCode;
use App\Models\ReasonRejection;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use App\Traits\paginationTrait;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class WalletController extends Controller
{
    use RespondsWithHttpStatus ,paginationTrait;

    public  $lang;

    public function __construct( )
    {
        $this->lang = request()->headers->get('Accept-Language') ?  : 'ar';

        app()->setLocale($this->lang);

        $this->middleware('auth:api');
        $this->middleware('check_all_status_active');
    }

    public function my_wallet(){

       $user =  Auth::user();

       $filter = function ($query){
         $query->select('id','title','uuid');
       };

       //where("payment","wallet")->
       $wallet =  Transaction::whereUserId($user->id)
           ->select('id','order_id','progress','price','refund_requested','message','created_at')->latest()->with(['order' => $filter])->get();

       $total_money  = $wallet->sum('price');

       $data = MyWalletResource::collection($wallet);
        return response([
            'status' => 200,
            'message' => "المحفظة",
            'total_money' => $total_money,
            'data' => $data,
        ], 200);
    }

    public function refund_requested(Request  $request, Transaction $transaction){

        $user = $request->user();

        if ($transaction->refund_requested == "request"){
            return  $this->failure("تم إبلاغ الإدارة مسبقا");
        }

        $reason_rejection = ReasonRejection::find($request->reason_id);

        $message = [
            'title'     => 'طلبات الإسترداد',
            'order'     => Order::find($transaction->order_id),
            'type'      =>  Lang::get('order.'. $reason_rejection->type,[],'ar') ,
            'user'      =>  '   من  المستخدم  ' . $user->name,
            'message'   =>      $reason_rejection->name
        ] ;

        try {
            Mail::to(Setting::getBody('email_bargain'))->send(new emailMessage($message));
        }catch (\Exception $exception){

        }

        $transaction->update(['refund_requested' => "request",'reason_rejection_id' => $request->reason_id]);

        return $this->success("سوف يتم إبلاغ الإدارة ");
    }

    public function my_cart(Request  $request){

        $user =  Auth::user();

        $query = DB::table('orders')
            ->where([
                ['orders.user_id','=',$user->id],
                ["orders.is_pay",'=',1],
                ['offers.is_pay','=',1],
            ])
            ->orWhere([
                ['orders.user_id','=',$user->id],
                ['orders.active_golden_offer','=',1],
                ['offers.is_pay','=',1],
            ])
            ->orWhere([
                ['offers.user_id','=',$user->id],
                ['offers.is_pay','=',1],
            ])
            ->orWhere([
                ['orders.user_offer_id','=',$user->id],
                ['offers.user_id','=',$user->id],
                ["orders.is_pay",'=',1],
                ['offers.is_pay','=',1],
            ])
            ->select('orders.id','orders.uuid','orders.title',
                        'orders.order_type','orders.category_id','offers.user_id as user_offer_id','offers.id as offer_id','orders.user_id')
            ->join('offers','orders.id','=','offers.order_id');
////        ;
//            ->get();
//        return $query;
//        $query = Order::whereUserId($user->id)->where("is_pay",1)
//            ->whereHas("my_offer",function ($my_offer){
//                $my_offer->where("is_pay",1);
//            })->orWhere('active_golden_offer',1)
//            ->whereHas("my_offer",function ($my_offer){
//                $my_offer->where("is_pay",1);
//            })
//            ->orWhere('user_offer_id',$user->id)->whereHas("my_offer",function ($my_offer){
//                    $my_offer->where("is_pay",1);
//        });

        $total_count = $query->count();

        $this->pagination_query_withOutOrderBy($request,$query);

        $data = OrderCartResource::collection($query->get());

        return $this->successWithPagination('السلة',$total_count,$data);
    }


    public function send_email_by_cart(VaildEmailCart  $request ){

        $enter_user = $request->user();

        $order = Order::find($request->order_id);

        $user = $order->user_id == $enter_user->id ? $order->offer_man : $order->user;

        Mail::to($enter_user->email)->send(new CartEmail($user , $order));

        return $this->success("تم إرسال الإيميل بنجاح");
    }


    public function promo_code_wallet(PromoCodeVaild $request){

        $user = $request->user();

        if (Transaction::whereUserId($user->id)->wherePromoCode($request->code)->exists()){
            return  $this->failure(trans('order.promo_code_used'));
        }

        $promo_code  = PromoCode::whereCode($request->code)->first();

        $transaction = Transaction::create(['user_id' => $user->id ,'progress' => 'deposit_by_promo_code',
            'payment' => 'wallet' ,'price' => $promo_code->money ,'promo_code' => $request->code ]);

        return $this->success("تم إضافة رصيد في المحفظة", new MyWalletResource($transaction));
    }


}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\vaildRegisterData;
use App\Libraries\PushNotification;
use App\Models\Device;
use App\Models\User;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    public $push;

    public function __construct( PushNotification $push)
    {
        $this->push = $push;
    }


    public function register_data(vaildRegisterData  $request){

        User::create($request->validated());
        return redirect()->back()->with('success', 'تم تسجيل بياناتك بنجاح..');
    }

    public function data_users(){
        $users = User::latest()->get();

        return view('social_media.table_users',compact('users'));
    }


    public function accepted_order(Request  $request){

        $order = \App\Models\Order::find($request->id);

        if (!$order){
            return response()->json(['message' => 'برجاء إعادة تحميل الصفحة' ,'status' => false]);
        }

        if ($order->is_accepted == 1){
            return response()->json(['message' => 'تم قبول الطلب مسبقا' ,'status' => false]);
        }

        $order->update(['is_accepted' => 1]);

        $devices = Device::whereUserId($order->user_id)->pluck('device');

        if (count($devices) > 0){
            $this->push->sendPushNotification($devices, null, 'orders', 'order_accepted',
                [
                    'order_id'      => $order->id,
                    'title_key'     => 'orders',
                    'body_key'      => 'order_accepted',
                    'body_arguments'    => [
                        "order_name"    => $order->title
                    ],
                ]
            );
        }



        return response()->json(['data' => $request->all() ,'status' => true]);
    }
}

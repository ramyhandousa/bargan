<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class checkSuspendAndActiveUsers
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $token = $request->bearerToken();
        if ($token){
            $user = User::whereApiToken($token)->first();

            if ($user){

                if ($user->is_suspend == 1){
                    return  $this->failure('للاسف تم حظرك من الإدارة');
                }

//                if ($user->is_active_email == 0){
//
//                    return  $this->failure('يرجي تفعيل إيميلك الشخصي من فضلك');
//                }

                if ($user->is_active_phone == 0){

                    return  $this->failure('يرجي تفعيل هاتفك الشخصي من فضلك');
                }

            }
        }
        return $next($request);
    }


    protected function failure($error = [], $status = 400)
    {
        return response([
            'status' => $status,
            'errors' => (array) $error,
        ], 200);
    }
}

<?php

namespace App\Http\Requests\Admin\Auth;

use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class LoginVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'email' => 'required|exists:users,email',
            'password' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.exists' => "برجاء التأكد من أن هذا الإيميل موجود",
            'email.required' => trans('global.required'),
            'password.required' => trans('global.required'),
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator){


            $credentials = ['email' => $this->email,'password' => $this->password,'defined_user' => ['admin' , 'helper_admin']];

            if (!$token = auth()->attempt($credentials)) {

                $validator->errors()->add('unavailable', trans('global.username_password_notcorrect'));
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

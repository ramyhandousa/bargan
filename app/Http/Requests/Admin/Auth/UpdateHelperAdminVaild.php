<?php

namespace App\Http\Requests\Admin\Auth;

use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateHelperAdminVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = User::find($this->helper_admin);
        return [
            'name'          => 'max:75',
            'phone'         => 'nullable|digits_between:9,15|unique:users,phone,' . $user->id ,
            'email'         => 'email|unique:users,email,' . $user->id ,
            "password"      => "max:255",
            "permission"    => "array",
            'image'         => 'image|mimes:jpeg,png,jpg|max:30240'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

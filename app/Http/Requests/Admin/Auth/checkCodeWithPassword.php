<?php

namespace App\Http\Requests\Admin\Auth;

use App\Models\VerifyUser;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class checkCodeWithPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'       => 'required',
            'code'        => ['required' ],
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => trans('global.required'),
            'email.email' => trans('validation.email'),
            'phone.exists' =>  trans('global.user_not_found'),
            'code.required' => 'إدخل الكود من فضلك',
            'code.exists' =>  trans('global.activation_code_not_correct'),
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            if ($this->email){
                $user = VerifyUser::where("email" ,  $this->email )->first();

                if (! $user) {
                    $validator->errors()->add('unavailable', trans('global.username_password_notcorrect'));
                }


                $code = VerifyUser::whereEmail($this->email)->where('action_code',$this->code)->exists();
                if (!$code){
                    $validator->errors()->add('unavailable', trans('global.activation_code_not_correct'));
                    return;
                }
            }


        });
    }

    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

<?php

namespace App\Http\Requests\Admin\Auth;

use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class forgetPassVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'provider' => 'required',
            'email' => 'required',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

//            $field = filter_var($this->provider, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
            if ($this->email){
                $user =  User::where("email" ,  $this->email )->whereIn('defined_user' , ['admin' , 'helper_admin'])->first();
                if (! $user) {
                    $validator->errors()->add('unavailable', trans('global.username_password_notcorrect'));
                }
            }

        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

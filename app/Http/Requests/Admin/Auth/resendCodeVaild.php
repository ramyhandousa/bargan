<?php

namespace App\Http\Requests\Admin\Auth;

use App\Models\VerifyUser;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class resendCodeVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'email'  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => trans('global.required'),
            'email.email' => trans('validation.email'),
            'phone.exists' =>  trans('global.user_not_found'),
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            if ($this->email){
                $user = VerifyUser::where("email" ,  $this->email )->first();
                if (! $user) {
                    $validator->errors()->add('unavailable', trans('global.username_password_notcorrect'));
                }
            }

        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

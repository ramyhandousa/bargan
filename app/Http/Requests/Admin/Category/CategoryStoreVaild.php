<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CategoryStoreVaild extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name_ar'           => 'required',
            'name_en'           => 'required',
            'description_ar'    => 'nullable',
            'product_title_ar'   => 'nullable',
            'product_title_en'    => 'nullable',
            'service_title_ar'   => 'nullable',
            'service_title_en'   => 'nullable',
            'description_en'    => 'nullable',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }

}

<?php

namespace App\Http\Requests\Admin\Dynamic;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class DynamicStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'definition_type'   => 'required|in:order,offer',
            'order_type'        => 'required|in:auction,tender',
            'type'              => 'required|in:checkbox,select',
            'name_ar'           => 'required|max:225',
            'name_en'           => 'required|max:225',
            'values'            => 'required|array',
            'values.*.name_ar'  => 'required|max:225',
            'values.*.name_en'  => 'required|max:225',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }

}

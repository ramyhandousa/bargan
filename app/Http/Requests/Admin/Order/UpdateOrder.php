<?php

namespace App\Http\Requests\Admin\Order;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class UpdateOrder extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'title'                => 'required|max:255',
            'images'                => 'nullable',
            'description'          => 'required|max:2000',
            'category_id'          => 'required|exists:categories,id',
            'expected_price'       => 'required|numeric|min:1|max:1000000000',
            'account_type'         => 'nullable|in:personal,commercial',
            'order_type'           => 'nullable|in:auction,tender',
            'service_type'         => 'nullable|in:product,service',
        ];
    }

    public function messages()
    {
        return [
            'title.required'                => 'إسم المنتج مطلوب',
            'description.required'          => '  الوصف مطلوب',
        ];
    }




    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

<?php

namespace App\Http\Requests\Admin\ReasonReject;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ReasonStoreVaild extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'type'              => 'required|in:admin_rejection,offer,cancel_order,complaint,Interact_order',
            'name_ar'           => 'required',
            'name_en'           => 'required',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

<?php

namespace App\Http\Requests\Admin\Setting;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class SettingVaildRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'commission_order' => 'nullable',
           'commission_offer' => 'nullable',
           'waitting_to_accept_offer_minutes' => 'nullable',
           'commission_golden_order' => 'nullable',
           'commission_golden_offer' => 'nullable',
           'time_active_golden_offer' => 'nullable',
           'about_us_ar' => 'nullable',
           'about_us_en' => 'nullable',
           'terms_user_ar' => 'nullable',
           'terms_user_en' => 'nullable',
           'linkedin' => 'nullable',
           'twitter' => 'nullable',
           'facebook' => 'nullable',
           'instagram' => 'nullable',
           'snapchat' => 'nullable',
           'youtube' => 'nullable',
           'time_order_rate' => 'nullable',
           'resend_rate_time' => 'nullable',
           'email_bargain' => 'nullable',
           'system_alert_minutes' => 'nullable',
           'time_limit_active_golden' => 'nullable',
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

<?php

namespace App\Http\Requests\Admin\StaticData;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StaticDataStoreRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'type'              => 'required|in:intro_page,golden_offer,common_questions',
            'name_ar'           => 'required|max:225',
            'name_en'           => 'required|max:225',
            'title_ar'           => 'nullable|max:225',
            'title_en'           => 'nullable|max:225',
            'image'              => 'nullable|max:1000',
            "color"              => 'nullable|max:255'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

<?php

namespace App\Http\Requests\api\Auth;

use App\Models\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class checkPhoneOrEmailExist extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider' => 'required',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $field = filter_var($this->provider, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

            $user =  User::where($field ,  $this->provider )->first();
            if (! $user) {
                $validator->errors()->add('unavailable', trans('global.username_password_notcorrect'));
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

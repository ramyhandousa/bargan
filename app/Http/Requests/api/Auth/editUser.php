<?php

namespace App\Http\Requests\api\Auth;

use App\Rules\checkOldPass;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class editUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        return [
            'name'    => 'max:75',
            'phone'         => 'numeric|digits_between:9,15|unique:users,phone,' . $user->id ,
            'email'         => 'email|unique:users,email,' . $user->id ,
            'oldPassword' => [   new checkOldPass() ],
//            'category'      => 'required|in:personal,commercial',
            'categories'        => 'nullable|exists:categories,id',
            'new_categories'   => 'nullable',
            'image'         => 'image|mimes:jpeg,png,jpg|max:30240'
        ];
    }


    public function messages()
    {
        return [
            'email.unique' => trans('global.unique_email'),
            'email.email' => trans('validation.email'),
            'phone.unique' => trans('global.unique_phone'),
        ];
    }

    public function validated()
    {
        $data =  parent::validated();

        return collect($data)->except("oldPassword","categories","new_categories")->toArray();
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

<?php

namespace App\Http\Requests\api\Auth;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class login extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'provider' => 'required',
        ];
    }

   public function messages()
   {
    return [
      'provider.required' => trans('global.required'),
    ];
   }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $field = filter_var($this->provider, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

            if (! $user = User::where($field,$this->provider)->first()) {

                $validator->errors()->add('unavailable', trans('global.username_password_notcorrect'));
            }
        });
    }

      protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

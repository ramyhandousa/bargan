<?php

namespace App\Http\Requests\api\Auth;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class register_user extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'          => 'required|max:75',
            'email'         => 'required|email|unique:users',
            'phone'         => 'required|unique:users|numeric',
//            'password'      => 'required|min:6',
//            'category'      => 'required|in:personal,commercial',
            'categories'        => 'nullable|exists:categories,id',
            'new_categories'   => 'nullable'
        ];
    }

    public function validated()
    {
        $data =  parent::validated();

        return collect($data)->only(['name','email','phone'])->toArray();
    }

    protected function failedValidation(Validator $validator) {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

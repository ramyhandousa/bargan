<?php

namespace App\Http\Requests\api\cart;

use App\Models\Order;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class VaildEmailCart extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'order_id' => 'required|exists:orders,id',
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order =  Order::findOrFail($this->order_id);


            if ($order->user_offer_id  == null){
                $validator->errors()->add('unavailable', 'يجب التأكد من أنك تمتلك صاحب عرض لإرسال الإيميل .. ');
                return;
            }

        });
    }


    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }

}

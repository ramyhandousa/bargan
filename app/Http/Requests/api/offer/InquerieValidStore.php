<?php

namespace App\Http\Requests\api\offer;

use App\Models\Order;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class InquerieValidStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'order_id' => 'required|exists:orders,id',
            'offer_id'  => 'nullable|exists:offers,id',
            'message'  => 'required'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order = Order::with('offers')->findOrFail($this->order_id);

            if ( !in_array($order->status,['pending','accepted']) ) {
                $validator->errors()->add('unavailable', 'تأكد من ان هذا الطلب  يمكنك المحاثة عليه');
            }

            if ($order->offers->count() <= 0){
                $validator->errors()->add('unavailable', 'تأكد من ان هذا الطلب  لديه عروض حتي يكمنك المحاثة عليه');
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

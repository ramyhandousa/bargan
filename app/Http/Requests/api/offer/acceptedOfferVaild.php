<?php

namespace App\Http\Requests\api\offer;

use App\Models\Order;
use App\Models\Setting;
use App\Models\Transaction;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class acceptedOfferVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $offer =  $this->route('offer');

        if ($offer->user_id == Auth::id()){

            return true;
        }else{
            return  false;
        }

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $offer =  $this->route('offer');

            $user = Auth::user();



            if ($offer->status != 'pending'){
                $validator->errors()->add('unavailable', 'للأسف هذا العرض لم يعد متاح حاليا للقبول..');
                return;
            }

            $order = $offer->order;

            if ( $order->status != 'pending' ) {
                $validator->errors()->add('unavailable', 'تأكد من ان هذا الطلب جاري لقبول العرض ');
                return;
            }

            if ($order->user_offer_id  != $user->id){
                $validator->errors()->add('unavailable', 'يجب التأكد من أنك صاحب العرض الموافق عليه .. ');
                return;
            }

            if ($offer->is_pay == 1){
                $validator->errors()->add('unavailable', 'تم دفع هذا الطلب مسبقا..');
                return;
            }

            $total_wallet = Transaction::whereUserId($user->id)->sum('price');

            $commission =  Setting::getBody('commission_offer') ;

            if ( $commission > $total_wallet  ){
                $validator->errors()->add('unavailable', 'لأسف رصيدك في المحفظة لا يكفي.. ');
                return;
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

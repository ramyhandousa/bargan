<?php

namespace App\Http\Requests\api\offer;

use App\Models\Order;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class cancelOfferVaild extends FormRequest
{
    public function authorize()
    {
        $offer = $this->route('offer');

        if ($offer->user_id == Auth::id()){
            return  true;
        }else{
            return false;
        }

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reason_rejection_id' => 'required|exists:reason_rejections,id',
//                Rule::exists('reason_rejections')->where(function ($query) {
//                    return $query->where('type', 'cancel_order');
//                })

        ];
    }


    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $offer =  $this->route('offer');

            if ($offer->status != 'pending'){
                $validator->errors()->add('unavailable', 'تم سحب عرضك مسبقا برجاء التأكد');
                return;
            }

            $order = Order::findOrFail($offer->order_id);

            if ( $order->status != 'pending' ) {
                $validator->errors()->add('unavailable', 'تأكد من ان هذا الطلب جاري لسحب عرضك');
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

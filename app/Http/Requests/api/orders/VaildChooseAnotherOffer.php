<?php

namespace App\Http\Requests\api\orders;

use App\Models\Order;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class VaildChooseAnotherOffer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $offer =  $this->route('offer');

        $order = $offer->order;

        if ($order->user_id == Auth::id()){
            return  true;
        }else{
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $offer =  $this->route('offer');

            $order = $offer->order;


            if ( $order->status != 'pending' ) {
                $validator->errors()->add('unavailable', 'تأكد من ان هذا الطلب جاري ');
                return;
            }

            if ( $order->is_pay != 1 ) {
                $validator->errors()->add('unavailable', 'تأكد من ان هذا الطلب مدفوع مسبقا  ');
                return;
            }


        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>  400 ,'error'=> $values], 200));
    }
}

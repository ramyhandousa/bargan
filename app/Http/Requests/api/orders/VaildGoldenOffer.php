<?php

namespace App\Http\Requests\api\orders;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class VaildGoldenOffer extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $order = $this->route('order');

        if ($order->user_id == Auth::id()){
            return  true;
        }else{
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }


    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order = $this->route('order');

            if ( $order->status != 'pending' ) {
                $validator->errors()->add('unavailable', 'تأكد من ان هذا الطلب جاري ');
                return;
            }

            $now = Carbon::now()->format('Y-m-d H:i');

            if ( $order->active_golden_offer != 0 && Carbon::parse($order->end_bidding_duration) >= $now  ) {
                $validator->errors()->add('unavailable', 'العرض الذهبي موجود حاليا ');
                return;
            }

//           $offers = $order->offers_pending->count();
//
//            if ($offers == 0){
//                $validator->errors()->add('unavailable', 'للأسف هذا الطلب لا يوجد لديه عروض لإستقبال عرض أقل . ');
//            }
        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>  400 ,'error'=> $values], 200));
    }

}

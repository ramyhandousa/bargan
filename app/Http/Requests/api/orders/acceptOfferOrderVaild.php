<?php

namespace App\Http\Requests\api\orders;

use App\Models\Setting;
use App\Models\Transaction;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;

class acceptOfferOrderVaild extends FormRequest
{
    public function authorize()
    {
        $offer =  $this->route('offer');

        $order = $offer->order;

        if ($order->user_id == Auth::id()){
            return true;
        }else{
            return  false;
        }

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            'price'     => 'required|min:1|max:10'
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $offer =  $this->route('offer');

//            if ($offer->status != 'pending'){
//                $validator->errors()->add('unavailable', 'للأسف هذا العرض لم يعد متاح حاليا للقبول..');
//                return;
//            }

            $order = $offer->order;

            if ( $order->status != 'pending' ) {
                $validator->errors()->add('unavailable', 'تأكد من ان هذا الطلب جاري لقبول العرض ');
                return;
            }

//            if ($order->user_offer_id  != null){
//                $validator->errors()->add('unavailable', 'تم تحديد عرض لهذا الطلب مسبقا برجاء الإنتظار.. ');
//                return;
//            }


            $total_wallet = Transaction::whereUserId(Auth::id())->sum('price');
            $commission =  Setting::getBody('commission_order') ;


            if ( $commission > $total_wallet  ){
                $validator->errors()->add('unavailable', 'لأسف رصيدك في المحفظة لا يكفي.. ');
                return;
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }
}

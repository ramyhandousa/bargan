<?php

namespace App\Http\Requests\api\orders;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class cancelOrderVaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $order = $this->route('order');

         if ($order->user_id == Auth::id()){
             return  true;
        }else{
             return false;
        }

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'reason_rejection_id' => 'required|exists:reason_rejections,id',
//                Rule::exists('reason_rejections')->where(function ($query) {
//                    return $query->where('type', 'cancel_order');
//                })

        ];
    }


    public function withValidator($validator)
    {
        $validator->after(function ($validator){

            $order = $this->route('order');

            if ( $order->status != 'pending' ) {
                $validator->errors()->add('unavailable', 'تأكد من ان هذا الطلب جاري ');
            }
        });
    }

    protected function failedValidation(Validator $validator)
    {
        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }

}

<?php

namespace App\Http\Requests\api\setting;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class Contact_Us_Vaild extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'    => 'required|exists:types_supports,id',
            'message'    => 'required',
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'برجاء اختيار قسم التواصل',
            'message.required' => 'مطلوب الرسالة الخاصه بك',
        ];
    }



    protected function failedValidation(Validator $validator)
    {

        $values = $validator->errors()->all();

        throw new HttpResponseException(response()->json(['status'=>400 ,'error'=> $values], 200));
    }


}

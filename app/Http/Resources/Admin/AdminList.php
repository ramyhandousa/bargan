<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            =>  $this->id,
            'name'          =>  $this->name,
            'phone'         =>  $this->when($this->phone,$this->phone),
            'email'         =>  $this->email,
            'is_suspend'    =>  (boolean)$this->is_suspend,
            'message'       =>  $this->when($this->message ,$this->message),
            'permission'    => $this->permission
        ];
    }
}

<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name_ar'           => optional($this->translate('ar'))->name,
            'name_en'           => optional($this->translate('en'))->name,
            'product_title_ar'           => optional($this->translate('ar'))->product_title,
            'product_title_en'           => optional($this->translate('en'))->product_title,
            'service_title_ar'           => optional($this->translate('ar'))->service_title,
            'service_title_en'           => optional($this->translate('en'))->service_title,
            'description_ar'    => $this->when(optional($this->translate('ar'))->description,optional($this->translate('ar'))->description),
            'description_en'    => $this->when(optional($this->translate('en'))->description,optional($this->translate('en'))->description),
            'parent'            => $this->when($this->parent , new CategoryList($this->parent)),
            'is_suspend'        => (boolean) $this->is_suspend,
            'image'             => $this->when($this->image,$this->image),
            'orders_count'      => $this->orders_count,
            'offers_count'      => $this->offers_count,
        ];
    }
}

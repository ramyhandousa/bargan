<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\User\UserFilterResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ContactUsList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
//            $this->mergeWhen(request('un_read'),[
//                'user'      =>  new UserFilterResource(optional($this->parent)->sender ? : $this->parent)  ,
//            ]),
            'user'      =>  new UserFilterResource($this->sender ?: $this) ,
            'message'   => $this->message,
            'reply'   =>  optional($this->reply_message)->message,
//            'replied_at' => $this->sender ?  optional($this->reply_message)->replied_at : $this->created_at,
            'replied_at' =>  optional($this->reply_message)->replied_at ? Carbon::parse(optional($this->reply_message)->replied_at )->setTimezone('UTC') : null,
            'created_at' => $this->created_at,
            'type'  =>  new ModelGlobalFilter($this->type)
        ];
    }
}

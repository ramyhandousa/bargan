<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class DynamicList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'definition_type'    => $this->definition_type,
            'order_type'    => $this->order_type,
            'type'          => $this->type,
            'name_ar'       => optional($this->translate('ar'))->name,
            'name_en'       => optional($this->translate('en'))->name,
            'values'        => DynamicValuesList::collection($this->label_values)
        ];
    }
}

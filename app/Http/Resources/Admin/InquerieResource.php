<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\User\UserFilterResource;
use Illuminate\Http\Resources\Json\JsonResource;

class InquerieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'order_id'      => $this->order_id,
            'title'         => optional($this->order)->title,
            'sender'        => new UserFilterResource($this->sender),
            'receiver'     => new UserFilterResource($this->user_offer),
            'message'       => $this->message,
            'created_at'    => $this->created_at
        ];
    }
}

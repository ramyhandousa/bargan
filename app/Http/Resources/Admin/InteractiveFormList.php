<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\User\UserFilterResource;
use App\Models\Order;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class InteractiveFormList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
//            "type" => $this->type,
            "order_id" => optional($this->order)->id,
            "order_title" => optional($this->order)->title,
            "message" => optional($this->reason_rejection)->name,
            "created_at" => $this->created_at,
            "user" => new UserFilterResource($this->user),
        ];
    }
}

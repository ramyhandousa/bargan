<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class ModelGlobalFilter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'name_ar'           => optional($this->translate('ar'))->name,
            'name_en'           => optional($this->translate('en'))->name,
            'is_suspend'        => (boolean) $this->is_suspend
        ];
    }
}

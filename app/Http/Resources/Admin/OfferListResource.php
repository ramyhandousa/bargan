<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\GlobalResourceIndex;
use App\Http\Resources\Order\OrderDetailsResource;
use App\Http\Resources\User\UserFilterResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OfferListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'uuid'                  => $this->rand_user_id,
            'order_id'              => $this->order_id,
            'description'           => $this->description,
            'order_title'           => optional($this->order)->title,
            'is_pay'                => (boolean) $this->is_pay,
            'time_out'              => (boolean) $this->time_out,
            'golden_offer'          => (boolean)$this->golden_offer,
            'price_offer'           =>  $this->expected_price,
            'is_accepted'           => (boolean) $this->is_accepted,
            'created_at'            => $this->created_at,
            'status'                => $this->when($this->status,$this->status),
            'payment_method'        => $this->when($this->payment_method,new GlobalResourceIndex($this->payment_method)) ,
            'industry'              => $this->when($this->industry,new GlobalResourceIndex($this->industry)) ,
            'warranty'              => $this->when($this->warranty,new GlobalResourceIndex($this->warranty)) ,
            'delivery_way'          => $this->when($this->delivery_way,new GlobalResourceIndex($this->delivery_way))  ,
            'reason_rejection'      => $this->when($this->reason_rejection,new GlobalResourceIndex($this->reason_rejection)),
            'user'                  => new UserFilterResource($this->user),
            'offer_details'         => $this->when($this->offer_details, OrderDetailsResource::collection($this->offer_details)),
        ];
    }
}

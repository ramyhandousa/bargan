<?php

namespace App\Http\Resources\Admin\Order;

use App\Http\Resources\User\UserFilterResource;
use Illuminate\Http\Resources\Json\JsonResource;

class InteractiveOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "message" => optional($this->reason_rejection)->name,
            "created_at" => $this->created_at,
            "user" => new UserFilterResource($this->user),
        ];
    }
}

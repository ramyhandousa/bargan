<?php

namespace App\Http\Resources\Admin\Order;

use App\Http\Resources\GlobalResourceIndex;
use App\Http\Resources\User\UserFilterResource;
use App\Models\Setting;
use Illuminate\Http\Resources\Json\JsonResource;

class OfferOrder extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'uuid'                  => $this->rand_user_id,
            'order_identity'        => $this->when($this->order_identity,$this->order_identity),
            'description'           => $this->when($this->description,$this->description),
            'price_offer'           => $this->when($this->expected_price,$this->expected_price),
            'delivery_duration'     => $this->when($this->delivery_duration,$this->delivery_duration),
            'commission'            => Setting::getBody('commission_offer'),
            'waiting_duration'      => Setting::getBody('waitting_to_accept_offer_minutes'),
            'status'                => $this->when($this->status,$this->status),
            'golden_offer'          => (boolean)$this->golden_offer,
            'is_pay'                => (boolean) $this->is_pay,
            'time_out'              => (boolean) $this->time_out,
            'created_at'              =>  $this->created_at,
            'is_accepted_offer'     => $this->order->offer_id == $this->id,

            'payment_method'        => $this->when($this->payment_method,new GlobalResourceIndex($this->payment_method)) ,
            'industry'              => $this->when($this->industry,new GlobalResourceIndex($this->industry)) ,
            'warranty'              => $this->when($this->warranty,new GlobalResourceIndex($this->warranty)) ,
            'delivery_way'          => $this->when($this->delivery_way,new GlobalResourceIndex($this->delivery_way))  ,
            'reason_rejection'      => $this->when($this->reason_rejection,new GlobalResourceIndex($this->reason_rejection)),
            'user'                  => new UserFilterResource($this->user)
        ];
    }
}

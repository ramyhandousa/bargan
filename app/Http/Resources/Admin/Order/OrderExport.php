<?php

namespace App\Http\Resources\Admin\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderExport extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $this->user;
        $offer_man = optional($this->offer_man);

        return [
            'id'                => $this->id,
            'name'              => $user->name,
            'phone'             => $user->phone,
            'email'             => $user->email,
            'title'             => $this->title,
            'price'             => number_format($this->expected_price,2),
            'account_type'      => $this->golden_offer == "personal" ? "شخصي" : "تجاري",
            'order_type'        => $this->golden_offer == "auction" ? "مزاد ": "مناقصة",
            'service_type'      => $this->service_type == "product" ? "منتج" : "خدمة",
            'category'          => optional($this->category)->name,
            'city'              => optional($this->city)->name,
            'golden_offer'      => $this->golden_offer == 0 ? "✔" : "",
            'delivery'          => optional($this->delivery_way)->name,
            'industry'          => optional($this->industry)->name,
            'warranty'          => optional($this->warranty)->name,
            'payment_method'    => optional($this->payment_method)->name,
            'payed'             => $this->is_pay == 0 ? "✔" : "",

            'offer_man_name'    => $offer_man->name,
            'offer_man_phone'   => $offer_man->phone,
            'offer_man_email'   => $offer_man->email,
            'offer_man_price'   => number_format($offer_man->expected_price,2),
            'offer_man_created' => optional($offer_man->created_at)->diffForHumans(),

            'description'       => $this->description,

        ];
    }
}

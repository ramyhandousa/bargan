<?php

namespace App\Http\Resources\Admin\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class ShowDetails extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            $this->mergeWhen(!$request->has("offers"),[
                'interactive_order' => InteractiveOrder::collection($this->interactive_forms)
            ]),
            $this->mergeWhen($request->has("offers"),[
                'offers' => OfferOrder::collection($this->offers)
            ])
        ];
    }
}

<?php

namespace App\Http\Resources\Admin;

use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'title'                 => $this->title,
            'expected_price'        => $this->expected_price,
            'category'              => new CategoryFilter($this->category) ,
            'golden_offer'          => (boolean) $this->golden_offer,
            'bidding_duration'      => $this->bidding_duration,
            'status'                => $this->status,
            'system_alert_minutes'  => Setting::getBody("system_alert_minutes"),
            $this->mergeWhen(!Carbon::parse($this->end_bidding_duration)->isPast(),[
                'duration'              => Carbon::parse($this->end_bidding_duration)->diffInSeconds(date('Y-m-d H:i:s')),
            ]),
            'offers' => count($this->offers) > 0,
            'interactive_forms' => count($this->interactive_forms) > 0
        ];
    }
}

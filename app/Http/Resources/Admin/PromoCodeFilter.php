<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class PromoCodeFilter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'code'      => $this->code,
            'money'     => $this->money,
            'start_at'  => $this->start_at,
            'end_at'    => $this->end_at,
            'is_suspend'    => (boolean) $this->is_suspend,
            'times_used'    => $this->users_count_code_count,
        ];
    }
}

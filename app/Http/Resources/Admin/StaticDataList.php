<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class StaticDataList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'type'            => $this->type,
            'image'            => $this->image,
            'color'            => $this->color,
            'name_ar'       => optional($this->translate('ar'))->name,
            'name_en'       => optional($this->translate('en'))->name,
            'title_ar'       => optional($this->translate('ar'))->title,
            'title_en'       => optional($this->translate('en'))->title,
        ];
    }
}

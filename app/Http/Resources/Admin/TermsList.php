<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class TermsList extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name_ar'       => optional($this->translate('ar'))->name,
            'name_en'       => optional($this->translate('en'))->name,
        ];
    }
}

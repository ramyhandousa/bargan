<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\User\UserFilterResource;
use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'order_id'              => $this->order_id,
            'progress'              => $this->progress,
            'price'                 => $this->price,
            'created_at'            => $this->created_at,
            'allow_refund'          =>   $this->progress == "pull",
            'refund_requested'      =>   $this->refund_requested != "pending",
            'status'                =>   $this->refund_requested,
            'title'                 => optional($this->order)->title,
            'uuid'                  =>  optional($this->order)->uuid,
            'reason'               => new ModelGlobalFilter( $this->reason_rejection),
            'user'                  => new UserFilterResource($this->user)
        ];
    }
}

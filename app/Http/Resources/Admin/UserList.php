<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\CategoryFilter;
use App\Http\Resources\GlobalResourceIndex;
use App\Models\Transaction;
use Illuminate\Http\Resources\Json\JsonResource;

class UserList extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            =>  $this->id,
            'name'          =>  $this->name,
            'phone'         =>  $this->phone,
            'email'         =>  $this->email,
            'is_suspend'    =>  (boolean)$this->is_suspend,
            'message'       =>  $this->when($this->message ,$this->message),
            'categories'    => $this->skip_user == 1 ? $this->desc_category : CategoryFilter::collection($this->categories),
            'dreams'        => $this->desc_dream,
//            'wallet'        => $this->my_wallet(),
            'wallet'        => Transaction::whereUserId($this->id)->sum('price'),
            'created_at'    => $this->created_at
//            'categories'  =>  GlobalResourceIndex::collection($this->categories)
        ];
    }
}

<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class UsersPromoCodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             =>  optional($this->user)->id,
            'name'           =>  optional($this->user)->name,
            'phone'          =>  optional($this->user)->phone,
            'email'          =>  optional($this->user)->email,
            'time_used_code' =>  $this->created_at
        ];
    }
}

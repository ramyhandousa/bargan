<?php

namespace App\Http\Resources\Admin;

use App\Http\Resources\CategoryResourceIndex;
use App\Http\Resources\GlobalResourceIndex;
use App\Http\Resources\User\UserFilterResource;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class showOrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->id,
            'uuid'              => $this->uuid,
            'account_type'      => $this->account_type,
            'order_type'        => $this->order_type,
            'service_type'      => $this->service_type,
            'title'             => $this->title,
            'description'       => $this->description,
            'best_offer'        => $this->min_price_offers(),
            'expected_price'    => $this->expected_price,
            'delivery_duration' => $this->delivery_duration,
            'bidding_duration'  => $this->bidding_duration,
            'system_alert_minutes'  => Setting::getBody("system_alert_minutes"),
            'created_at'        => $this->created_at,
            'status'            => $this->when($this->status,$this->status),
            'golden_offer'      => (boolean) $this->golden_offer,
            'active_golden_offer'   => (boolean) $this->active_golden_offer,
            'category'          => new CategoryResourceIndex($this->category),
            'payment_method'    => new GlobalResourceIndex($this->payment_method),
            'industry'          => new GlobalResourceIndex($this->industry),
            'warranty'          => new GlobalResourceIndex($this->warranty),
            'delivery_way'      => new GlobalResourceIndex($this->delivery_way),
            'user'              => new UserFilterResource($this->user),
            'images'            => $this->when($this->images ,$this->images  ) ,
            'offers'            => count($this->offers) > 0,
            'reason_rejection'      => $this->when($this->reason_rejection,new GlobalResourceIndex($this->reason_rejection)),

            $this->mergeWhen(!Carbon::parse($this->end_bidding_duration)->isPast(),[
                'duration'              => Carbon::parse($this->end_bidding_duration)->diffInSeconds(date('Y-m-d H:i:s')),
            ]),
        ];
    }
}

<?php

namespace App\Http\Resources\Admin;

use App\Models\Transaction;
use Illuminate\Http\Resources\Json\JsonResource;

class showUserFilter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            =>  $this->id,
            'name'          =>  $this->name,
            'phone'         =>  $this->phone,
            'email'         =>  $this->email,
            'is_suspend'    =>  (boolean)$this->is_suspend,
            'message'       =>  $this->when($this->message ,$this->message),
            'transaction_deposit_by_promo_code' => $this->transaction_deposit_by_promo_code(),
            'transaction_deposit_by_admin' => $this->transaction_deposit_by_admin(),
            'transaction_deposit_by_user' => $this->transaction_deposit_by_user(),
            'transaction_pull' => $this->transaction_pull(),
            'wallet' => Transaction::whereUserId($this->id)->sum('price'),
            'categories'  =>  CategoryFilter::collection($this->categories)
        ];
    }
}

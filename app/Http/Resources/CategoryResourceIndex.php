<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResourceIndex extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name_ar'           => optional($this->translate('ar'))->name,
            'name_en'           => optional($this->translate('en'))->name,
            'description'   => $this->when($this->description,$this->description),
            'parent'        => $this->when($this->parent , new CategoryResourceIndex($this->parent))
        ];
    }
}

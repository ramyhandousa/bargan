<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryWithChildrenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name_ar'           => optional($this->translate('ar'))->name,
            'name_en'           => optional($this->translate('en'))->name,
            'image'             => $this->when($this->image,$this->image),
            'has_children'  => (boolean) $this->children->count() > 0,
            'children'       => $this->when($this->children ,  CategoryWithChildrenResource::collection($this->children) )
        ];
    }
}

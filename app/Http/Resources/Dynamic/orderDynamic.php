<?php

namespace App\Http\Resources\Dynamic;

use Illuminate\Http\Resources\Json\JsonResource;

class orderDynamic extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_type' => $this->order_type,
            'type' => $this->type,
            'name' => $this->name,
            'values' => valueDynamic::collection($this->label_values)
        ];
    }
}

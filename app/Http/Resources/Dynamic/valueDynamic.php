<?php

namespace App\Http\Resources\Dynamic;

use Illuminate\Http\Resources\Json\JsonResource;

class valueDynamic extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'  => $this->id,
            'value' => $this->value
        ];
    }
}

<?php

namespace App\Http\Resources\Offer;

use App\Http\Resources\User\UserFilterResource;
use Illuminate\Http\Resources\Json\JsonResource;

class InquerieResource extends JsonResource
{

    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'order_id'      => $this->order_id,
            'sender_id'     => $this->sender_id,
            'user_offer_id' => $this->user_offer_id,
            'message'       => $this->message,
            'sender'        => new UserFilterResource($this->sender),
            'user_offer'    => new UserFilterResource($this->user_offer),
            'created_at'    => $this->created_at
        ];
    }
}

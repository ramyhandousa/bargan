<?php

namespace App\Http\Resources\Offer;

use App\Http\Resources\CategoryResourceIndex;
use Illuminate\Http\Resources\Json\JsonResource;

class OfferIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'order'            => new OfferOrderFilter($this->order),
            'price_offer'      => $this->expected_price,

        ];
    }
}

<?php

namespace App\Http\Resources\Offer;

use App\Http\Resources\CategoryResourceIndex;
use Illuminate\Http\Resources\Json\JsonResource;

class OfferOrderFilter extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'title'            => $this->when($this->title , $this->title),
            'category'         => $this->when($this->category , new CategoryResourceIndex($this->category)),
        ];
    }
}

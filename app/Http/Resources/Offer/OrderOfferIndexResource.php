<?php

namespace App\Http\Resources\Offer;

use App\Http\Resources\CategoryResourceIndex;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderOfferIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::whereApiToken($request->bearerToken())->first();
        return [
            'id'                => $this->id,
            'title'             => $this->title,
            'order_type'        => $this->order_type  ,
            'category'          => new CategoryResourceIndex($this->category),
            'current_offer'     =>   $this->when($this->user_current_offer($user->id,$request->offer_status ), new OfferShowResource($this->user_current_offer($user->id,$request->offer_status)))
        ];
    }
}

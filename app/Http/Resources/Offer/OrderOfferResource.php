<?php

namespace App\Http\Resources\Offer;

use App\Http\Resources\CategoryResourceIndex;
use App\Http\Resources\GlobalResourceIndex;
use App\Http\Resources\Order\OrderDetailsResource;
use App\Models\Inqueries;
use App\Models\Offer;
use App\Models\Setting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderOfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $commission = $this->get_commission($this);

        $user = $request->user();

        $offers = $user->id ==  $this->user_id ? $this->offers_still_not_accepted : $this->offers_still_not_accepted->where('user_id',$user->id);

        if ($this->is_accepted == 0){
            $is_accepted = null;
        }elseif ($this->is_accepted == 1){
            $is_accepted = true;
        }else{
            $is_accepted = false;
        }

        // make sure
        // user enter == order->user_id
        // user offer == order->user_offer_id
        $can_rate = ($this->user_id == $user->id || $this->user_offer_id == $user->id) && $this->can_rate ==1;

        return [
            'id'                    => $this->id,
            'user_id'               => $this->user_id,
            'offer_id'              => $this->offer_id,
            'old_offer_id'          => $this->old_offer_id,
            'uuid'                  => $this->uuid,
            'account_type'          => $this->account_type,
            'order_type'            => $this->order_type,
            'service_type'          => $this->service_type,
            'title'                 => $this->title,
            'images'                => $this->when($this->images ,$this->images  ) ,
            'description'           => $this->description,
            'best_offer'            => $this->min_price_offers(),
            'commission'            => $commission,
            'waiting_duration'      => Setting::getBody('waitting_to_accept_offer_minutes'),
            'expected_price'        => $this->expected_price,
            'delivery_duration'     => $this->delivery_duration,
            'bidding_duration'      => $this->bidding_duration,
            'system_alert_minutes'  => Setting::getBody("system_alert_minutes"),
            'created_at'            => $this->created_at,
            'status'                => $this->when($this->status, $this->status),
            'order_rate'            => $this->when($this->order_rate,$this->order_rate),
            'offer_rate'            => $this->when($this->offer_rate , $this->offer_rate),
            'can_rate'              =>   $can_rate	,
            'user_rate'             => $this->user->total_rate(),
            'is_pay'                => (boolean)$this->is_pay,
            'golden_offer'          => (boolean)$this->golden_offer,
            'is_accepted'           => $is_accepted ,
            'active_golden_offer'   => (boolean)$this->active_golden_offer,
            'category'              => new CategoryResourceIndex($this->category),
            'payment_method'        => new GlobalResourceIndex($this->payment_method),
            'industry'              => new GlobalResourceIndex($this->industry),
            'warranty'              => new GlobalResourceIndex($this->warranty),
            'delivery_way'          => new GlobalResourceIndex($this->delivery_way),
            'reason_rejection'      => $this->when($this->reason_rejection, new GlobalResourceIndex($this->reason_rejection)),
            $this->mergeWhen(!Carbon::parse($this->end_bidding_duration)->isPast(),[
                'duration'          => Carbon::parse($this->end_bidding_duration)->diffInSeconds(date('Y-m-d H:i:s')),
            ]),
            $this->mergeWhen($this->active_golden_offer,[
                'golden_commission'     => Setting::getBody('commission_golden_offer'),
                'accepted_offer_price'  => optional($this->my_offer)->expected_price,
                'best_golden_offer'     => $this->min_price_golden_offers(),
            ]),
            $this->mergeWhen(!Carbon::parse($this->end_bidding_duration)->isPast(),[
                'duration'              => Carbon::parse($this->end_bidding_duration)->diffInSeconds(date('Y-m-d H:i:s')),
            ]),
            'current_offer'             =>   $this->when($this->user_current_offer($user->id,$request->offer_status ), new OfferShowResource($this->user_current_offer($user->id,$request->offer_status))),
            'offers'                    => OfferShowResource::collection($offers),

            'count_inqueries_unread'    =>  $this->get_count_un_read_inqueries($user,$this)

//            $this->mergeWhen($request->has('previous'),[
//                'previous_offer'        =>   $this->when($this->user_pervious_offer($user->id), new OfferShowResource($this->user_pervious_offer($user->id))),
//            ]),
//
//            'current_offer'        =>   $this->when($this->user_current_offer($user->id,$request->offer_status ), new OfferShowResource($this->user_current_offer($user->id,$request->offer_status)))

        ];
    }


    public function get_commission($order){
        if ($order->active_golden_offer == 0) {

            $commission = Setting::getBody('commission_offer');

        }else{
            $user_offer = Offer::where('user_id' ,$order->user_offer_id)->where('is_pay',1)->first();

            $commission_golden_offer = Setting::getBody('commission_golden_offer');
            if ($user_offer){

                $commission =   $commission_golden_offer - $user_offer->commission;
            }else{

                $commission =  $commission_golden_offer;
            }
        }

        return $commission;

    }

    public function get_count_un_read_inqueries($user,$order)
    {
        if ($user){
            $count = Inqueries::where([
                ['order_id','=',$order->id],
                ['user_offer_id','=',$user->id],
                ['is_accepted','=',1],
                ['is_read','=',0],
            ])->count();
        }else{
            $count = 0;
        }

        return  $count;
    }
}

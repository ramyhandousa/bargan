<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\CategoryResourceIndex;
use App\Models\Category;
use App\Models\Offer;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderCartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
//        $enter_user = $request->user()->id == $this->user_id ? $this->offer_man : $this->user;
        $enter_user = $request->user()->id == $this->user_id
            ? User::find($this->user_offer_id)
            : User::find($this->user_id);

//        $offer = Offer::find($this->offer_id);

        return [
            'id'                 => $this->id,
            'uuid'              => $this->uuid,
            'user_id'           => $this->user_id,
            'title'             => $this->title,
            'order_type'        => $this->order_type,
            'category'          => new CategoryResourceIndex(Category::find($this->category_id)),
//            'category'          => new CategoryResourceIndex($this->category),
//            'user'              => new UserFilterResource($enter_user,$this->user_id,$this->uuid,$offer),
            'user'              => new UserCartFilter($enter_user),
        ];
    }
}

<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $label = $this->label;
        $label_value  = $this->label_value;
        return [
            'id'   => $this->when($label,$label->id),
            'order_type'   => $this->when($label,$label->order_type),
            'type' => $this->when($label,$label->type),
            'name' => $this->when($label,$label->name) ,
            'value' => [
                'id'    =>     $this->when($label_value,$label_value->id)  ,
                'value' =>  $this->when($label_value,$label_value->value),
            ]
        ];
    }
}

<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\CategoryResourceIndex;
use App\Http\Resources\GlobalResourceIndex;
use App\Http\Resources\Offer\OfferShowResource;
use App\Models\Setting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderIndex extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::whereApiToken($request->bearerToken())->first();

        if ($this->is_accepted == 0){
            $is_accepted = null;
        }elseif ($this->is_accepted == 1){
            $is_accepted = true;
        }else{
            $is_accepted = false;
        }

        return [
            'id'                    => $this->id,
            'user_id'               => $this->user_id,
            'account_type'          => $this->account_type,
            'order_type'            => $this->order_type,
            'service_type'          => $this->service_type,
            'title'                 => $this->title,
            'expected_price'    => $this->expected_price,
            'active_golden_offer'   => (boolean) $this->active_golden_offer,
            'golden_offer'          => (boolean) $this->golden_offer,
            'is_accepted'            =>  $is_accepted,
            'category'              => new CategoryResourceIndex($this->category),
            'best_offer'            => $this->min_price_offers(),
            'bidding_duration'      => $this->bidding_duration,
            'system_alert_minutes'  => Setting::getBody("system_alert_minutes"),
            'status'                => $this->status,
            'created_at'            => $this->created_at,
            $this->mergeWhen(!Carbon::parse($this->end_bidding_duration)->isPast() ,[
                'duration'              => Carbon::parse($this->end_bidding_duration)->diffInSeconds(date('Y-m-d H:i:s')),
            ]),
            $this->mergeWhen( $user,[

                // The User Enter See his Offer
                'user_offer'        =>   $this->when($user&&$this->user_offer($user->id), $user ? new OfferShowResource($this->user_offer($user->id)) : null)
            ])

        ];
    }
}

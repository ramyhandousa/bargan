<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\GlobalResourceIndex;
use App\Models\Inqueries;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderOfferShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'                    => $this->id,
            'price_offer'           => $this->expected_price,
            'created_at'            => $this->created_at,
            'uuid'                  => $this->rand_user_id,
            'description'           => $this->when($this->description,$this->description),
            'order_identity'        => $this->when($this->order_identity,$this->order_identity),
            'payment_method'        => $this->when($this->payment_method,new GlobalResourceIndex($this->payment_method)) ,
            'industry'              => $this->when($this->industry,new GlobalResourceIndex($this->industry)) ,
            'warranty'              => $this->when($this->warranty,new GlobalResourceIndex($this->warranty)) ,
            'delivery_way'          => $this->when($this->delivery_way,new GlobalResourceIndex($this->delivery_way))  ,
            'status'                => $this->status,
            'golden_offer'      => (boolean) $this->golden_offer,
            'is_pay'                => (boolean)$this->is_pay,
            'user_rate'         => $this->user->total_rate('offer_rate'),
            'count_inqueries_unread' =>  $this->get_count_un_read_inqueries($this->order,$this),

        ];
    }


    public function get_count_un_read_inqueries($order,$offer)
    {
           return Inqueries::where([
                ['order_id','=',$order->id],
//                ['sender_id','!=',$offer->user_id],
                ['user_offer_id','=',$offer->user_id],
                ['is_accepted','=',1],
                ['is_read','=',0],
            ])->count();

    }
}

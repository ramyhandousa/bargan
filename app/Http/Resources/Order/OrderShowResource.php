<?php

namespace App\Http\Resources\Order;

use App\Http\Resources\Admin\CityResource;
use App\Http\Resources\CategoryResourceIndex;
use App\Http\Resources\GlobalResourceIndex;
use App\Http\Resources\Offer\OfferShowResource;
use App\Http\Resources\User\UserFilterResource;
use App\Models\Inqueries;
use App\Models\Setting;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Log;

class OrderShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = request()->headers->get('Accept-Language') ?  : 'ar';
        $user = User::whereApiToken($request->bearerToken())->first();

        $offers = $this->active_golden_offer == 1 ? $this->golden_offer_status :
                            $this->offers->where('created_at','>=',$this->created_at)->where('status','!=','cancel');

        if ($this->is_accepted == 0){
            $is_accepted = null;
        }elseif ($this->is_accepted == 1){
            $is_accepted = true;
        }else{
            $is_accepted = false;
        }
        $message = json_decode($this->message);

        return [
            'id'                => $this->id,
            'user_id'           => $this->user_id,
            'uuid'              => $this->uuid,
            'offer_id'          => $this->offer_id,
            'account_type'      => $this->account_type,
            'order_type'        => $this->order_type,
            'service_type'      => $this->service_type,
            'title'             => $this->title,
            'images'            => $this->when($this->images ,$this->images  ) ,
            'description'       => $this->description,
            'best_offer'        => $this->min_price_offers(),
            'commission'        => Setting::getBody('commission_order'),
            'waiting_duration'  => Setting::getBody('waitting_to_accept_offer_minutes'),
            'offer_commission'  =>  Setting::getBody('commission_offer'),
            'offer_golden_commission'  => Setting::getBody('commission_golden_offer') ,
            'expected_price'    => $this->expected_price,
            'delivery_duration' => $this->delivery_duration,
            'bidding_duration'  => $this->bidding_duration,
            'system_alert_minutes'  => Setting::getBody("system_alert_minutes"),
            'created_at'        => $this->created_at,
            'status'            => $this->when($this->status,$this->status),
//            'rate'              => $this->when($this->rate_order,optional($this->rate_order)->rate),
            'message'           => $this->when($message, $lang == "ar" ? $message->message_ar ?? null : $message->message_en ?? null),
            'order_rate'        => $this->when($this->order_rate,$this->order_rate),
            'offer_rate'        => $this->when($this->offer_rate , $this->offer_rate),
            'can_rate'          => (boolean) $this->can_rate	,
            'user_rate'         => $this->user->total_rate(),
            'is_pay'            => (boolean) $this->is_pay,
            'golden_offer'      => (boolean) $this->golden_offer,
            'is_accepted'            => $is_accepted ,
            'active_golden_offer'   => (boolean) $this->active_golden_offer,
            'city'              => $this->when($this->city_id,new CityResource($this->city)),
            'category'          => new CategoryResourceIndex($this->category),
            'payment_method'    => new GlobalResourceIndex($this->payment_method),
            'industry'          => new GlobalResourceIndex($this->industry),
            'warranty'          => new GlobalResourceIndex($this->warranty),
            'delivery_way'      => new GlobalResourceIndex($this->delivery_way),
            'order_details'     => $this->when($this->order_details, OrderDetailsResource::collection($this->order_details)),
            'reason_rejection'      => $this->when($this->reason_rejection,new GlobalResourceIndex($this->reason_rejection)),
            'offers_count'      => $this->offers_pending->count(),
            $this->mergeWhen($this->active_golden_offer,[
                'golden_commission'              => Setting::getBody('commission_golden_order'),
            ]),
            $this->mergeWhen(!Carbon::parse($this->end_bidding_duration)->isPast(),[
                'duration'              => Carbon::parse($this->end_bidding_duration)->diffInSeconds(date('Y-m-d H:i:s')),
            ]),
            $this->mergeWhen(request()->has('offers'),[
                'offers'        =>      OrderOfferShowResource::collection($offers)
            ]),
            $this->mergeWhen(request()->has('accepted_offer'),[


                // The Offer Man Owner Order Accepted Him
                'accepted_offer'     =>  new OfferShowResource($this->accepted_offer($this->offer_id))
            ]),
            $this->mergeWhen(request()->has('user_offer') && $user,[

                // The User Enter See his Offer
                'user_offer'        =>   $this->when($user&&$this->user_offer($user->id), $user ? new OfferShowResource($this->user_offer($user->id)) : null)
            ]),
        ];
    }


}

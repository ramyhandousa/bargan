<?php

namespace App\Http\Resources\Order;

use Illuminate\Http\Resources\Json\JsonResource;

class UserFilterResource extends JsonResource
{
    public $user_id ;
    public $uuid ;
    public $offer;

    public function __construct( $resource ,$user_id, $uuid,$offer)
    {
        parent::__construct($resource);
        $this->user_id = $user_id;
        $this->uuid = $uuid;
        $this->offer = $offer;
    }



    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = $request->user();

        $uuid = $user->id == $this->user_id ? $this->uuid : $this->offer->rand_user_id;

        return [
            'id'    => $this->id,
            'uuid'  => $uuid,
            'name'  => $this->name,
            'phone' => $this->phone,
            'email' => $this->email,
        ];
    }
}

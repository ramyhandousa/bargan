<?php

namespace App\Http\Resources\Setting;

use Illuminate\Http\Resources\Json\JsonResource;

class IntroPageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->when($this->name,$this->name),
            'title' => $this->when($this->title,$this->title),
            'image' => $this->when($this->image,$this->image),
            'color' => $this->when($this->color,$this->color),
        ];
    }
}

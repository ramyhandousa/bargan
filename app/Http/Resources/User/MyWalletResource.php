<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;

class MyWalletResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = request()->headers->get('Accept-Language') ?  : 'ar';
        $message = json_decode($this->message);
        return [
            'id'            => $this->id,
            'progress'      => $this->progress,
            'price'         => $this->price,
            'created_at'    => $this->created_at,
            'allow_refund' =>   $this->progress == "pull",
            'refund_requested' =>   $this->refund_requested != "pending",
            'message'           => $this->when($message, $lang == "ar" ? $message->message_ar ?? null : $message->message_en ?? null),
            $this->mergeWhen($this->order,[
                'order_id'     =>  $this->order_id,
                'uuid'         =>  optional($this->order)->uuid,
                'title'        =>   optional($this->order)->title ,
            ])

        ];
    }
}

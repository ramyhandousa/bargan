<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Lang;

class NotifaictionCollectionResource extends ResourceCollection
{

    public $my_additional_data;

    public function __construct($resource,$my_additional_data)
    {
        parent::__construct($resource);
        $this->my_additional_data  = $my_additional_data;
    }



    public function toArray($request){
        $my_additional_data =  $this->my_additional_data;

        return $this->collection->map(function( $my_res) use($request ,$my_additional_data ){

        $lang = request()->headers->get('Accept-Language') ?  : 'ar';

         $message      = Lang::get('order.' .$my_res->translation,[],$lang) ;
               return [
            'id'                    => $my_res->id,
            'message'               =>  $message,
            'title_key'             => $my_res->title,
            'body_key'              => $my_res->translation,
            'order_id'              => $my_res->order_id ,
            'offer_id'              =>  $my_res->offer_id ,
            'body_arguments'        =>  $my_res->body_arguments ,
            'is_read'               => (boolean) $my_res->is_read,
            'disable_offer_payment' => $my_additional_data > 0
        ];
        })->all();
    }
}

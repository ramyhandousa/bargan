<?php

namespace App\Http\Resources\User;

use App\Http\Resources\CategoryResourceIndex;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class UserCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::whereApiToken($request->bearerToken())->first();
        return [
            'id'            => $this->id,
            'name_ar'           => optional($this->translate('ar'))->name,
            'name_en'           => optional($this->translate('en'))->name,
            'product_title_ar'           => optional($this->translate('ar'))->product_title,
            'product_title_en'           => optional($this->translate('en'))->product_title,
            'service_title_ar'           => optional($this->translate('ar'))->service_title,
            'service_title_en'           => optional($this->translate('en'))->service_title,
            'description'   => $this->when($this->description,$this->description),
            'image'             => $this->when($this->image,$this->image),
            'has_children'  => (boolean) $this->children->count() > 0,
            'is_follow'     => (boolean) $user ? $user->categories->contains($this->id) : false,
            'parent'        => $this->when($this->parent , new CategoryResourceIndex($this->parent)),
        ];
    }
}

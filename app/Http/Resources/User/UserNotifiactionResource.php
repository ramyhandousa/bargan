<?php

namespace App\Http\Resources\User;

use App\Models\Setting;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Lang;

class UserNotifiactionResource extends JsonResource
{



    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $lang = request()->headers->get('Accept-Language') ?  : 'ar';

        $message      = Lang::get('order.' .$this->translation,[],$lang) ;


        return [
            'id'                    => $this->id,
            'message'               =>  $message,
            'title_key'             => $this->title,
            'body_key'              => $this->translation,
            'order_id'              => $this->when($this->order_id,$this->order_id),
            'offer_id'              => $this->when($this->offer_id,$this->offer_id),
            'body_arguments'        => $this->when($this->body_arguments,$this->body_arguments),
            'is_read'               => (boolean) $this->is_read,
        ];
    }
}

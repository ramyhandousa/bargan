<?php

namespace App\Http\Resources\User;

use App\Http\Resources\CategoryFilter;
use App\Http\Resources\GlobalResourceIndex;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            =>  $this->id,
            'name'          =>  $this->name,
            'phone'         =>  $this->when($this->phone,$this->phone),
            'email'         =>  $this->email,
            'api_token'     =>  $this->api_token,
            'is_admin'      =>  $this->defined_user == "admin",
            'email_active'  =>  (boolean)$this->is_active_email,
            'phone_active'  =>  (boolean)$this->is_active_phone,
            'is_suspend'    =>  (boolean)$this->is_suspend,
            'disable_notifications'    =>  (boolean)$this->disable_notifications,
            'message'       =>  $this->when($this->message ,$this->message),
            'order_rate'          =>  $this->total_rate()  ,
            'offer_rate'          =>   $this->total_rate('offer_rate') ,
            $this->mergeWhen( $request->path() ==  'api/Auth/editProfile',[
                'code'     =>   $this->when($this->code, optional($this->code)->action_code),
            ]),
            $this->mergeWhen(count($this->categories) > 0,[
                'categories'  =>  CategoryFilter::collection($this->categories)
            ])

        ];
    }
}

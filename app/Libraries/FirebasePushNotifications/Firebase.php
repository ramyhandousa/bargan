<?php

namespace App\Libraries\FirebasePushNotifications;


class Firebase
{


    // sending push message to single user by firebase reg id
    public function send($to, $message, $pushData)
    {
        $fields = array(
            'to' => $to,
            'data' => $message,
            'notification' => $pushData
        );
        return $this->sendPushNotification($fields);
    }

    // Sending message to a topic by topic name
    public function sendToTopic($to, $data, $pushData)
    {
        $fields = array(
            'to' => '/topics/' . $to,
            'data' => $data,
//            'notification' => $pushData
        );

        return $this->sendPushNotification($fields);
    }

    // sending push message to multiple users by firebase registration ids
    public function sendMultipleAndroid($registration_ids, $message)
    {
        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $message,

            'content_available' => true
            //'notification' => $pushData // ios

        );

        return $this->sendPushNotification($fields);
    }


        public function sendMultipleIos($registration_ids, $pushData)
    {
        $fields = array(
            'registration_ids' => $registration_ids,
            // 'data' => $pushData, // android
            'notification' => $pushData ,// ios
            'content_available' => true

        );

        return $this->sendPushNotification($fields);
    }

        public function sendMultipleNotificationAndData($registration_ids, $notification ,$pushData )
    {
        $fields = array(
            'registration_ids' => $registration_ids,
            'data' => $pushData, // android
            'notification' => $notification, // ios
            'content_available' => true

        );

        return $this->sendPushNotification($fields);
    }

    // function makes curl request to firebase servers
    private function sendPushNotification($fields)
    {

//        include './config.php';
        // Set POST variables
        $url = 'https://fcm.googleapis.com/fcm/send';

        $headers = array(
            'Authorization: key=AAAA-wWA0f4:APA91bETIkov9PxRV6Cwk_Cp666uqXJNpvdFSQq0tGGESPSbLrVmRYcHXcddww6XMEw_lTXmy7-UJctVxP-qo2wNm_iE3YkbR1O_EIKBOtKqeV_lVR9oktLbEGxhWrTQhOgFNXfpjrnL',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);

        return $result;
    }


}

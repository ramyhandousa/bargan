<?php


namespace App\Libraries\FirebasePushNotifications;


use Carbon\Carbon;

class Push
{
    private $id;
    private $userId;
    private $userImage;
    private $titleKey;
    private $bodyKey;
    private $body_arguments;
    private $fileName;
    private $url;
    private $orderId;
    private $offerId;
    private $orderType;
    private $orderAction;
    private $best_offer;
    private $type;
    private $is_read;
    private $title;
    private $message;
    private $image;
    private $data;
    private $created_at;


    function __construct()
    {

    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }
    public function setUserImage($userImage)
    {
        $this->userImage = $userImage;
    }
    public function setTitleKey($titleKey)
    {
        $this->titleKey = $titleKey;
    }
    public function setBodyKey($bodyKey)
    {
        $this->bodyKey = $bodyKey;
    }
    public function setBodyArguments($body_arguments)
    {
        $this->body_arguments = $body_arguments;
    }
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }
    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setOrderType($orderType)
    {
        $this->orderType = $orderType;
    }
    public function setOrderAction($orderAction)
    {
        $this->orderAction = $orderAction;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }
    public function setOfferId($offerId)
    {
        $this->offerId = $offerId;
    }

    public function setBestOffer($best_offer)
    {
        $this->best_offer = $best_offer;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function setIsRead($read)
    {
        $this->is_read = $read;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setImage($imageUrl)
    {
        $this->image = $imageUrl;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function setIsBackground($is_background)
    {
        $this->is_background = $is_background;
    }
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;
    }

    public function getPushData()
    {
        $res = array();
        $res['id']                   = $this->id;
        $res['user_id']              = $this->userId;
        $res['user_image']           = $this->userImage;
        $res['title_key']            = $this->titleKey;
        $res['body_key']             = $this->bodyKey;
        $res['body_arguments']       = $this->body_arguments;
        $res['best_offer']           = $this->best_offer;
        $res['message']              = $this->message;
        $res['fileName']             = $this->fileName;
        $res['url']                  = $this->url;
        $res['order_id']             = $this->orderId;
        $res['offer_id']             = $this->offerId;
        $res['order_type']           = $this->orderType;
        $res['order_action']           = $this->orderAction;
        $res['type']                 = $this->type;
        $res['is_read']              = $this->is_read;
        $res['title']                = $this->title;
        $res['body']                 = $this->message;
        $res['created_at']           = Carbon::parse($this->created_at)->format('Y-m-d H:i');
        $res['timestamp']            = date('Y-m-d G:i:s');

        $res['content_available']        = true;
        return $res;
    }


    public function getPushNotification()
    {

        $res = array();
        $res['title']       = $this->title;
        $res['body']        = $this->message;
        $res['icon']        = $this->image ?:"default";
        $res['sound']        = "default";
        $res['additional']  = $this->data;
        $res['timestamp']   = date('Y-m-d G:i:s');
        $res['click_action'] = $this->data['href'] ?: "FLUTTER_NOTIFICATION_CLICK";
        $res['link']        = $this->data['href'] ?: " ";
        $res['content_available']        = true;

        return $res;
    }

}

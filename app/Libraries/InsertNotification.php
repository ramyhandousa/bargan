<?php
namespace App\Libraries;

use App\Libraries\PushNotification;
use App\Models\Device;
use  App\Models\Notification;

class InsertNotification {


    public  function NotificationDbType($type  ,$user,  $sender = null ,  $request = null  , $order = null ,$additional = null){

        switch($type){

            case $type == 1:

                return Notification::create([
                    'user_id' => $user ,
                    'sender_id' => $sender,
                    'title' =>   trans('global.connect_us')  ,
                    'body' => $request ,
                    'type' => 1,
                ]);

                break;

            case $type == 2:

                // contactUs Form Users
                return Notification::create([
                    'user_id' => 1 ,
                    'sender_id' => $sender ,
                    'title' => trans('global.connect_us'),
                    'body' => $request ,
                    'type' => 2,
                ]);

                break;

            case $type == 3:
                $body_argument = [
                    "order_name" => $order->title ,
                ];
                return Notification::create([
                    'user_id'       =>  $user ,
                    'sender_id'     =>  $sender->id,
                    'order_id'      =>  $order->id,
                    'offer_id'      =>  $additional,
                    'title'         =>  'accepted_offer'  ,
                    'translation'   =>  'owner_accepted_offer' ,
                    'body'          =>  $sender->name ,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'          =>  3,
                ]);

                break;

            case $type == 4:

                return Notification::create([
                    'user_id'       =>  $user ,
                    'sender_id'     =>  $sender->id,
                    'order_id'      =>  $order,
                    'offer_id'      =>  $additional,
                    'title'         =>  'waiting_accepted_offer'  ,
                    'translation'   =>  'review_accepted_offer' ,
                    'body'          =>  $sender->name ,
                    'type'          =>  4,
                ]);

                break;

            // Type => 5 reserve For new category in order

            case $type == 6:
                $body_argument = [
//                    "user_name"     => $sender->name,
                    "user_name"     => $additional->rand_user_id,
                    "order_name"    => $order->title
                ];
                return Notification::create([
                    'user_id'       => $user ,
                    'sender_id'     => $sender->id,
                    'order_id'      => $order->id,
                    'offer_id'      => $additional->id,
                    'title'         =>  'offers'  ,
                    'translation'   =>  'add_new_offer' ,
                    'body'          =>  $sender->name    ,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'          => 6,
                ]);

                break;
            case $type == 7:
                $body_argument = [
//                    "user_name"     => $sender->name,
                    "user_name"     => $additional->rand_user_id,
                    "order_name"    => $order->title
                ];
                return Notification::create([
                    'user_id'       => $user ,
                    'sender_id'     => $sender->id,
                    'order_id'      => $order->id,
                    'offer_id'      => $additional->id,
                    'title'         =>  'offers'  ,
                    'translation'   =>  'edit_new_offer' ,
                    'body'          =>  $sender->name    ,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'          => 7,
                ]);

                break;

            case $type == 8:
                $body_argument = [
                    "order_name"    => $order->title ,
//                    "user_name"     => $sender->name,
                    "user_name"     => $order->uuid,
                ];
                return Notification::create([
                    'user_id'       =>  $user ,
                    'sender_id'     =>  $sender->id,
                    'order_id'      =>  $order->id,
                    'offer_id'      =>  $additional,
                    'title'         =>  'orders'  ,
                    'translation'   =>  'offer_payed_and_finish_order' ,
                    'body'          =>  $sender->name ,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'          =>  8,
                ]);

                break;

            case $type == 9:
                $body_argument = [
                    "order_name"     => $order->title
                ];
                return Notification::create([
                    'user_id'       =>   $user ,
                    'sender_id'     =>   null,
                    'order_id'      =>   $order->id,
                    'topic'         =>  'system',
                    'title'         =>  'bidding_duration'  ,
                    'translation'   =>  'end_bidding_duration' ,
                    'body'          =>  'end_bidding_duration' ,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'          =>  9,
                ]);

                break;

            case $type == 10:
                return Notification::create([
                    'user_id'      =>   $user->id,
                    'sender_id'    =>   $sender->id,
                    'order_id'     =>   $order->id,
                    'title'        =>  'new_golden_offer'  ,
                    'body'         =>  'new_golden_offer_with_price' ,
                    'translation'  =>  'new_golden_offer_with_price' ,
                    'type'         =>   10,
                ]);

                break;

                  //type 11 محجوز


            case $type == 12:

                $body_argument = [
                    "order_name"    => $order->title ,
//                    "user_name"     => $sender->name,
                    "user_name"     => $order->uuid,
                ];
                return Notification::create([
                    'user_id'       =>  $user ,
                    'sender_id'     =>  $sender->id,
                    'order_id'      =>  $order->id,
                    'offer_id'      =>  $additional,
                    'title'         =>  'orders'  ,
                    'translation'   =>  'offer_payed_and_waiting_golden_activation' ,
                    'body'          =>  $sender->name ,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'          =>  12,
                ]);

                break;


            case $type == 13:

                $body_argument = [
                    "order_name"     => $additional->order->title,
//                    "user_name"     => $additional->user->name,
                    "user_name"     => $additional->order->uuid,
                ];
                return Notification::create([
                    'user_id'       =>   $user ,
                    'sender_id'     =>   null,
                    'order_id'      =>   $order->id,
                    'offer_id'      =>   $additional->id,
                    'topic'         =>  'system',
                    'title'         =>  'waiting_duration'  ,
                    'translation'   =>  'waiting_duration_ended' ,
                    'body'          =>  'waiting_duration_ended' ,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'          =>  13,
                ]);

                break;


            case $type == 14:

                $body_argument = [
                    "order_name"     => $order->title,
                    "user_name"     => $order->offer_man->name,
                ];
                return Notification::create([
                    'user_id'       =>   $user->id ,
                    'sender_id'     =>   $sender->id,
                    'order_id'      =>   $order->id,
                    'offer_id'      =>   $additional->id,
                    'title'         =>  'rating'  ,
                    'translation'   =>  'rate_order_offer_man' ,
                    'body'          =>  'rate_order_offer_man' ,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'          =>  14,
                ]);

                break;

            case $type == 15:

                $body_argument = [
                    "order_name"     => $order->title,
                    "user_name"     => $order->user->name,
                ];
                return Notification::create([
                    'user_id'       =>   $user->id ,
                    'sender_id'     =>   $sender->id,
                    'order_id'      =>   $order->id,
                    'offer_id'      =>   $additional->id,
                    'title'         =>  'rating'  ,
                    'translation'   =>  'rate_order_user' ,
                    'body'          =>  'rate_order_user' ,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'          =>  15,
                ]);

                break;

            case $type == 16:

                $body_argument = [
                    "order_name"     => $order->title,
                    "user_name"     => $order->uuid,
                ];
                return Notification::create([
                    'user_id'       =>   $user->id ,
                    'sender_id'     =>   $sender->id,
                    'order_id'      =>   $order->id,
                    'offer_id'      =>   $additional->id,
                    'title'         =>  'rating'  ,
                    'translation'   =>  'order_rate_by_offer_man' ,
                    'body'          =>  'order_rate_by_offer_man' ,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'          =>  16,
                ]);

                break;

            case $type == 17:

                $body_argument = [
                    "order_name"     => $order->title,
                    "user_name"     => $order->uuid,
                ];
                return Notification::create([
                    'user_id'       =>   $user->id ,
                    'sender_id'     =>   $sender->id,
                    'order_id'      =>   $order->id,
                    'offer_id'      =>   $additional->id,
                    'title'         =>  'rating'  ,
                    'translation'   =>  'order_rate_by_user' ,
                    'body'          =>  'order_rate_by_user' ,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'          =>  17,
                ]);

                break;

            case $type == 24:
                $data = Notification::create([
                    'user_id'      => $user->id,
                    'sender_id'    => 1,
                    'invitation_id'     => $order->id,
                    'title'        =>   'التحويلات'  ,
                    'body'         =>  ' تم رفض التحويل علي طلبك    رقم ' .    $order->id  .' بسبب  ' . $request,
                    'type'         => 24,
                ]);
                return  $data;
                break;

            case $type == 25:
                $data = Notification::create([
                    'user_id'      => $user->id,
                    'sender_id'    => 1,
                    'invitation_id'     => $order->id,
                    'title'        =>   'التحويلات'  ,
                    'body'         =>  ' تم رفض التحويل علي طلبك من المشروع  رقم ' .    $order->id  .' بسبب  ' . $request,
                    'type'         => 25,
                ]);
                return  $data;
                break;

            default:

        }


    }


    private function insertData($data)
    {
        if (count($data) > 0) {

            $time = ['created_at' => now(),'updated_at' => now()];
            Notification::insert($data   + $time);

        }
    }


}


<?php


namespace App\Libraries;



use App\Models\Device;
use App\Models\Notification;
use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class oneSignal
{

    function sendMessage( $devices,$data,$content_en,$content_ar, $heading_en = null,$heading_ar = null , $buttons_ar = null, $buttons_en = null , $ios_badgeCount = null){

        $user_devices = Device::whereIn('device',$devices)->pluck('user_id')->toArray();

        $users = count(array_unique($user_devices));

        $check_users = $users == 1 ;
        if ($check_users){
            $ios_badgeCount = Notification::whereUserIdAndIsRead($user_devices[0],0)->count();
        }

        $data['disable_offer_payment'] =   Setting::getBody('commission_offer')  > 0;

        $fields = array(
            'app_id' => "8f1d88ca-34ae-4dfa-b0f0-850f3610748e",
            'include_player_ids' => $devices,
            'channel_for_external_user_ids' => 'push',
            'data' => $data,
            'contents' => [
                "en" => $content_en,
                "ar" => $content_ar
            ],
            'headings' => [
                "en" => $heading_en,
                "ar" => $heading_ar
            ],
            'buttons' => $buttons_ar,
            'content_available' => true,
//            'ios_badgeType' => $check_users ? 'SetTo' : 'Increase' ,
//            'ios_badgeCount' => $ios_badgeCount ?? 1,
            'priority' => 'high',
//            'send_after' => Carbon::now()->addSeconds(10)
        );

        if (isset($ios_badgeCount)){
            $fields['ios_badgeType'] = 'SetTo';
            $fields['ios_badgeCount'] = $ios_badgeCount;
        }

       return $this->end_point_notification($fields);
    }

//

//    function sendMessage($devices,$data,$content_en,$content_ar, $heading_en = null,$heading_ar = null , $buttons_ar = null, $buttons_en = null ){
//
//
//        $en =   $this->_sendMessage('en',$devices,$data,$content_en,$heading_en ,$buttons_en);
//
//        $ar =   $this->_sendMessage('ar',$devices,$data,$content_ar,$heading_ar,$buttons_ar);
//
//        return [$en , $ar];
//    }

//    function _sendMessage($lang , $devices,$data,$content,$headings =null, $buttons= null){
//
//        $filters = [ ];
//
//        foreach ($devices as $device){
//
//           array_push($filters,[
//               "field"=> "tag",
//               "key"=> "locale",
//               "relation"=> "=",
//               "value"=> $device. '_'.$lang
//           ]);
//
//            array_push($filters,[
//                    'operator' => 'OR'
//            ]);
//
//        }
//        array_pop($filters);
//
//        $fields = array(
//            'app_id' => "8f1d88ca-34ae-4dfa-b0f0-850f3610748e",
////            'include_player_ids' => $devices,
//            'channel_for_external_user_ids' => 'push',
//            'data' => $data,
//            'contents' => [
//                "en" => $content
//            ],
//            'headings' => [
//                "en" => $headings
//            ],
//            'filters' => $filters,
//            'buttons' => $buttons,
//            'content_available' => true,
//            'ios_badgeType' => 'Increase',
//            'ios_badgeCount' => 1,
//            'priority' => 'high',
//            'send_after' => Carbon::now()->addSeconds(10)
//        );
//
//       return $this->end_point_notification($fields);
//    }
//

    public function send_topic_data($data){

        $fields = array(
            'app_id' => "8f1d88ca-34ae-4dfa-b0f0-850f3610748e",
            'included_segments' => array(
                'Subscribed Users'
            ),
            'data' => $data,
            'content_available' => true,
            'priority' => 'high'
        );

       return $this->end_point_notification($fields);
    }

    public function send_public_data($data , $content,$headings, $lang = 'ar'){

        $filters = [ ];
        array_push($filters,[
            "field"=> "tag",
            "key"=> "localeCode",
            "relation"=> "=",
            "value"=> $lang
        ]);

        $fields = array(
            'app_id' => "8f1d88ca-34ae-4dfa-b0f0-850f3610748e",
            'channel_for_external_user_ids' => 'push',
            'data' => $data,
            'contents' => [
                "en" => $content
            ],
            'headings' => [
                "en" => $headings
            ],
            'filters' => $filters,
            'content_available' => true,
            'ios_badgeType' => 'Increase',
            'ios_badgeCount' => 1,
            'priority' => 'high'
        );

       return $this->end_point_notification($fields);
    }


    private function end_point_notification($fields){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
            'Authorization: Basic NWMwM2ZkM2MtN2Q0Yy00Nzk5LWFmMzItYWQ2YThjNzM1MjMz'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}

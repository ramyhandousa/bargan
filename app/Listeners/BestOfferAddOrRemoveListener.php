<?php

namespace App\Listeners;

use App\Events\BestOfferAddOrRemove;
use App\Libraries\oneSignal;
use App\Libraries\PushNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class BestOfferAddOrRemoveListener
{
    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  BestOfferAddOrRemove  $event
     * @return void
     */
    public function handle(BestOfferAddOrRemove $event)
    {
        $title = $event->title;
        $body = $event->body;
        $order = $event->order;
        $request = $event->request;
        $topic = $event->topic;
        $old_offer_price = $event->old_offer_price;

        $best_offer = $order->min_price_offers();

        if ($request->expected_price){

            $expected_price = (boolean) ($request->expected_price == $best_offer);
        }else{

            $expected_price = $best_offer ? (boolean) ($old_offer_price != $best_offer) : true;
        }

        if ($expected_price ){
//            $this->push->sendPushNotification(null, null, $title, $body,[
//                'order_id'      => $order->id,
//                'best_offer'    => $best_offer ? : 0
//            ],$topic);

            $data = [
                'order_id'      => $order->id,
                'best_offer'    => $best_offer ? : 0
            ];

            $this->push->send_topic_data($data);
        }


    }
}

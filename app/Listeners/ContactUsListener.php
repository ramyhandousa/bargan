<?php

namespace App\Listeners;

use App\Events\ContactUs;
use App\Libraries\InsertNotification;
use App\Libraries\PushNotification;
use App\Mail\emailMessage;
use App\Models\Device;
use App\Models\Setting;
use Illuminate\Support\Facades\Mail;

class ContactUsListener
{
    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,PushNotification $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  ContactUs  $event
     * @return void
     */
    public function handle(ContactUs $event)
    {
        $devices = Device::where('user_id', $event->user )->pluck('device');

        $notify =    $this->notify->NotificationDbType(2,$event->user,$event->sender,$event->request->message);

        try {
            Mail::to(Setting::getBody('email_bargain'))->send(new emailMessage($event->request->message));
        }catch (\Exception $exception){

        }
        if(count($devices ) > 0  ) {

            $this->push->sendPushNotification($devices, null, $notify['title'], $notify['body'],
                [
                    'id'            => $notify['id'],
                    'type'          => $notify['type'],
                    'is_read'       => $notify['is_read'],
                    'title'         => $notify['title'],
                    'body'          => $notify['body'],
                    'href'          => route('contact_us_inbox.index'),
                    'created_at'    => $notify['created_at'],
                ]
            );
        }
    }
}

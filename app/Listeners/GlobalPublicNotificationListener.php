<?php

namespace App\Listeners;

use App\Events\GlobalPublicNotification;
use App\Libraries\oneSignal;
use App\Libraries\PushNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class GlobalPublicNotificationListener
{

    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  GlobalPublicNotification  $event
     * @return void
     */
    public function handle(GlobalPublicNotification $event)
    {
        $title = $event->title;
        $body = $event->body;
        $order = $event->order;
        $topic = $event->topic;
        $type = $event->type;
        $user = $event->user;

//        $this->push->sendPushNotification(null, null, $title, $body,[
//            'user_id'      => $user ? $user->id : null,
//            'order_id'      => $order->id,
//            "order_action"  => $type
//        ],$topic);

        $data = [
            'user_id'      => $user ? $user->id : null,
            'order_id'      => $order->id,
            "order_action"  => $type
        ];

        $this->push->send_topic_data($data);

    }
}

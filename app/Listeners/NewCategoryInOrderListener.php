<?php

namespace App\Listeners;

use App\Events\NewCategoryInOrder;
use App\Libraries\oneSignal;
use App\Libraries\PushNotification;
use App\Models\Category;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class NewCategoryInOrderListener
{
    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  NewCategoryInOrder  $event
     * @return void
     */
    public function handle(NewCategoryInOrder $event)
    {
        $sender     = $event->order->user;
        $order      = $event->order;
        $request    = $event->request;
        $title      = "follow_categories";
        $body       = "new_category_of_order";

        $category_id = $request->category_id ? $request->category_id : $order->category_id;

        $my_category = Category::find($category_id);

        // Send Notification To Users Who Follow This Category Of New Order Or Parent Category
             $users = User::select('id')->where('id','!=',$order->user->id)->whereHas('categories',function ($category) use ($category_id){

                $category->where('category_id',$category_id)->orWhereHas('parent',function ($parent)  use ($category_id){
                            $parent->where('id',$category_id);
                        })->orWhereHas('children',function ($parent)  use ($category_id){
                            $parent->where('id',$category_id);
                        });
             })->with('devices')->get();

        $this->insert_notification($users,$sender,$order, $title,$body,$my_category);

        // Collect Devices
        $devices = $users?
                        $users->pluck('devices')->flatten() ?
                            $users->pluck('devices')->flatten()->pluck('device')
                            :null
                        :null;

        if(count($devices ) > 0  ) {

            $category_name_ar = $my_category->translate('ar')->name;
            $category_name_en = $my_category->translate('en')->name;

            $title_ar      = Lang::get('order.follow_categories',[],'ar') ;
            $title_en      =  Lang::get('order.follow_categories',[],'en') ;
            $content_en    = Lang::get('order.new_category_of_order',['order_name' => $order->title , 'category_name_en' => $category_name_en],'en') ;
            $content_ar    = Lang::get('order.new_category_of_order',['order_name' => $order->title , 'category_name_ar' => $category_name_ar],'ar') ;

            $this->push->sendMessage($devices,[
                'type'              => 4,
                'title_key'         => $title,
                'body_key'          => $body,
                'order_id'          =>  $order->id ,
                'body_arguments'    => [
                    "category_name_ar" =>  $category_name_ar,
                    "category_name_en" =>  $category_name_en,
                    'order_name'        => $order->title
                ],
            ],$content_en,$content_ar,$title_en,$title_ar);

        }

    }

    public function insert_notification($users, $sender , $order , $title, $body,$my_category){

        if (count($users) > 0) {

            $data  =  collect($users)->map(function ($value) use ($sender , $order , $title, $body,$my_category) {
                $body_argument = [
                    "category_name_ar" => $my_category->translate('ar')->name ,
                    "category_name_en" => $my_category->translate('en')->name ,
                    'order_name' => $order->title
                ];
                return [
                    'user_id'       => $value->id,
                    'sender_id'    => $sender->id,
                    'order_id'     => $order->id,
                    'title'        => $title,
                    'translation'  => $body,
                    'body'         => $body,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'         => 5,
                    'created_at'   => now()
                ];
            })->toArray();

            Notification::insert($data);
        }
    }
}

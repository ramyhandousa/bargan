<?php

namespace App\Listeners;

use App\Events\NewGoldenOffer;
use App\Libraries\oneSignal;
use App\Libraries\PushNotification;
use App\Models\Notification;
use App\Models\Offer;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class NewGoldenOfferListener
{

    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }


    /**
     * Handle the event.
     *
     * @param  NewGoldenOffer  $event
     * @return void
     */
    public function handle(NewGoldenOffer $event)
    {

        $order  = $event->order;
        $sender = $order->user_id;
//        $title  = "new_golden_offer";
        $title  = "golden_offer";
//        $body   = "new_golden_offer_with_price";
        $body   = "golden_offer_activated";

//        $offers = $order->offers->where('created_at','>=',$order->created_at)->where('status','!=','cancel')
//                    ->where('is_pay','=',0)->pluck('user_id')->toArray();
        $offers = $order->offers_pending
//            ->where('created_at','>=',$order->created_at)
                            ->where('is_pay','=',0)->pluck('user_id')->toArray();


        $ids = array_unique($offers);

        if (count($ids) > 0){
            $users = User::whereIn('id' , $ids)->with('devices')->get();

            $order_price = Offer::find($order->offer_id)->expected_price;
            $body_arguments = [
                "order_name"    => $order->title ,
                "order_price"   => $order_price
            ];

            $this->insert_notification($ids,$sender,$order, $title,$body , $body_arguments);

            // Collect Devices
            $devices = $users ?
                $users->pluck('devices')->flatten() ?
                    $users->pluck('devices')->flatten()->pluck('device')
                    :null
                :null;

            if(count($devices ) > 0  ) {

                $title_ar      = Lang::get('order.golden_offer',[],'ar') ;
                $title_en      =  Lang::get('order.golden_offer',[],'en') ;

                $data_content = ['order_name' => $order->title ,'order_price' => $order_price ];
                $content_en    = Lang::get('order.golden_offer_activated',$data_content,'en') ;
                $content_ar    = Lang::get('order.golden_offer_activated',$data_content,'ar') ;

                $this->push->sendMessage($devices,[
                    'type'              => 10,
                    'order_id'          => $order->id,
                    'title_key'         => $title,
                    'body_key'          => $body,
                    'body_arguments'    => $body_arguments,
                ],$content_en,$content_ar,$title_en,$title_ar);
            }
        }



    }

    public function insert_notification($offers, $sender , $order , $title, $body , $body_arguments){

        if (count($offers) > 0) {

            $data  =  collect($offers)->map(function ($offer) use ($sender , $order , $title, $body , $body_arguments) {
                return [
                    'user_id'      => $offer,
                    'sender_id'    => $sender,
                    'order_id'     => $order->id,
                    'title'        => $title,
                    'translation'  => $body,
                    'body'         => $body,
                    'body_arguments' =>json_encode($body_arguments) ,
                    'type'         => 10,
                    'created_at'   => now()
                ];
            })->toArray();

            Notification::insert($data);
        }
    }
}

<?php

namespace App\Listeners;

use App\Events\BestOfferAddOrRemove;
use App\Events\NewOfferOrEditInOrder;
use App\Libraries\InsertNotification;
use App\Libraries\oneSignal;
use App\Libraries\PushNotification;
use App\Models\Device;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class NewOfferOrEditInOrderListener
{
    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,oneSignal $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  NewOfferOrEditInOrder  $event
     * @return void
     */
    public function handle(NewOfferOrEditInOrder $event)
    {
        $sender     = $event->sender;
        $order      = $event->order;
        $user       = $event->order->user_id;
        $offer      = $event->offer;
        $request    = $event->request;
        $type       = $event->type;


        if ($type == 'add'){

            $notify = $this->notify->NotificationDbType(6,$user,$sender,$request,$order,$offer);

        }else{
            $notify = $this->notify->NotificationDbType(7,$user,$sender,$request,$order,$offer);
        }


        event(new BestOfferAddOrRemove($notify['title'], $notify['body'],$order,$request,'new_offer_or_edit_in_order'));


        $devices = Device::whereUserId($user)->pluck('device');

        if(count($devices ) > 0  ) {

            $title_ar      = Lang::get('order.offers',[],'ar') ;
            $title_en      =  Lang::get('order.offers',[],'en') ;

            $data_content = ['order_name' => $order->title ,'user_name' => $offer->rand_user_id ];
            if ($type == 'add'){

                $content_en    = Lang::get('order.add_new_offer',$data_content,'en') ;
                $content_ar    = Lang::get('order.add_new_offer',$data_content,'ar') ;

            }else{

                $content_en    = Lang::get('order.edit_new_offer',$data_content,'en') ;
                $content_ar    = Lang::get('order.edit_new_offer',$data_content,'ar') ;
            }

            $this->push->sendMessage($devices,[
                'id'            => $notify['id'],
                'type'          => $notify['type'],
                'order_id'      => $notify['order_id'],
                'offer_id'      => $notify['offer_id'],
                'title_key'     => $notify['title'],
                'body_key'      => $notify['translation'],
                'body_arguments'    => [
                    "user_name"     =>  $order->uuid,
                    "order_name"    => $order->title
                ],
                'created_at'    => $notify['created_at'],
            ],$content_en,$content_ar,$title_en,$title_ar);
        }
    }
}

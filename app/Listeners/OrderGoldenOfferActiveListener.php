<?php

namespace App\Listeners;

use App\Events\OrderGoldenOfferActive;
use App\Libraries\InsertNotification;
use App\Libraries\oneSignal;
use App\Libraries\PushNotification;
use App\Models\Device;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Lang;

class OrderGoldenOfferActiveListener
{
    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,oneSignal $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  OrderGoldenOfferActive  $event
     * @return void
     */
    public function handle(OrderGoldenOfferActive $event)
    {
        $sender = $event->user;
        $order = $event->order;
        $offer = $event->offer;
        $request = $event->request;

        $notify = $this->notify->NotificationDbType(12,$order->user_id,$sender,$request,$order,$offer->id);

        $devices = Device::whereUserId($order->user_id)->pluck('device');

        if(count($devices ) > 0  ) {

            $data_content = ['order_name' => $order->title ,'user_name' => $order->uuid ];
            $title_ar      = Lang::get('order.orders',[],'ar') ;
            $title_en      =  Lang::get('order.orders',[],'en') ;
            $content_en    = Lang::get('order.offer_payed_and_waiting_golden_activation',$data_content,'en') ;
            $content_ar    = Lang::get('order.offer_payed_and_waiting_golden_activation',$data_content,'ar') ;
            $buttons_ar = [];
            $buttons_en = [];
            array_push($buttons_ar,['id' => 'details', 'text' =>   'تفاصيل الطلب'  ]);
            array_push($buttons_ar,['id' => 'cart', 'text' =>  'السلة' ]);
            array_push($buttons_en,['id' => 'details', 'text' =>   'order details']);
            array_push($buttons_en,['id' => 'cart', 'text' =>   'cart']);

            $this->push->sendMessage($devices,[
                'id'            => $notify['id'],
                'type'          => $notify['type'],
                'order_id'      => $notify['order_id'],
                'offer_id'      => $notify['offer_id'],
                'title_key'     => $notify['title'],
                'body_key'      => $notify['translation'],
                'body_arguments'    => [
                    "user_name"     => $sender->id,
                    "order_name"    => $order->title
                ],
                'created_at'    => $notify['created_at'],
            ],$content_en,$content_ar,$title_en,$title_ar,$buttons_ar,$buttons_en);
        }
    }
}

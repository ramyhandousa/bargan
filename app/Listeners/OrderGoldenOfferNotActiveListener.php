<?php

namespace App\Listeners;

use App\Events\OrderGoldenOfferNotActive;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class OrderGoldenOfferNotActiveListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderGoldenOfferNotActive  $event
     * @return void
     */
    public function handle(OrderGoldenOfferNotActive $event)
    {

        $offer = $event->offer;

        $order = $event->order;

        $order->update(['status' => 'finish']);

        $offer->update(['status' => 'finish']);

        $order->offers_pending->where('id','!=', $offer->id)->each->update(['status' => 'refuse']);


    }
}

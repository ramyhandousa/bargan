<?php

namespace App\Listeners;

use App\Events\OrderKeysDiff;
use App\Libraries\oneSignal;
use App\Models\Device;
use App\Models\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Lang;

class OrderKeysDiffListener
{
    public $push;

    public function __construct(oneSignal $push)
    {
        $this->push = $push;
    }
    /**
     * Handle the event.
     *
     * @param  OrderKeysDiff  $event
     * @return void
     */
    public function handle(OrderKeysDiff $event)
    {
        $user       = $event->user;
        $sender     = $event->sender;
        $order      = $event->order;
        $offer      = $event->offer;
        $order_offer   = $order->my_offer;
        $request    = $event->request;
        $title_key  = $event->title_key;
        $body_key   = $event->body_key;

        if ($offer && $body_key == 'cancel_offer_with_user_name'){
            $user_name = $offer->rand_user_id;
            $expected_price = $offer->expected_price;
        }else{
            $user_name = $order->uuid;
            $expected_price = $order->expected_price;
        }
        $data_content = ['order_name' => $order->title ,'user_name' => $user_name ,'price' => number_format($expected_price,2) ];

        Notification::create([
            'user_id'       => $user->id , 'sender_id'     => $sender->id,
            'order_id'      => $order->id, 'title'         =>  $title_key  ,
            'translation'   =>  $body_key , 'body_arguments' =>json_encode($data_content) ,
        ]);

        $title_ar      = Lang::get('order.'.$title_key,[],'ar') ;
        $title_en      =  Lang::get('order.'.$title_key,[],'en') ;
        $content_en    = Lang::get('order.'.$body_key,$data_content,'en') ;
        $content_ar    = Lang::get('order.'.$body_key,$data_content,'ar') ;

        $devices = Device::whereUserId($user->id)->pluck('device');

        if(count($devices ) > 0  ) {
            $this->push->sendMessage($devices,[
                'order_id'      => $order->id,
                'title_key'     => $title_key,
                'body_key'      => $body_key,
                'body_arguments'    => [
                    "user_name"     => $user_name,
                    "order_name"    => $order->title,
                    'price'         => number_format($expected_price,2)
                ],
            ],$content_en,$content_ar,$title_en,$title_ar);

        }
    }
}

<?php

namespace App\Listeners;

use App\Events\RateOrder;
use App\Libraries\InsertNotification;
use App\Libraries\oneSignal;
use App\Models\Device;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class RateOrderListener
{

    public $push;
    public  $notify;

    public function __construct(InsertNotification $notification , oneSignal $push)
    {
        $this->push = $push;
        $this->notify = $notification;
    }

    /**
     * Handle the event.
     *
     * @param  RateOrder  $event
     * @return void
     */
    public function handle(RateOrder $event)
    {
        $user      = $event->user;
        $order      = $event->order;
        $request    = $event->request;
        $type        = $event->type;


        $devices = Device::whereUserId($user->id)->pluck('device');

        if ($type == 17){

            $user_name = $order->offer_man->name;
            $notify = $this->notify->NotificationDbType($type,$order->offer_man,$order->user,null,$order,$order->my_offer);

        }else{

            $user_name = $order->user->name;

            $notify = $this->notify->NotificationDbType($type,$order->user,$order->offer_man,null,$order,$order->my_offer);
        }

        $title_ar      = Lang::get('order.'.$notify['title'],[],'ar') ;
        $title_en      =  Lang::get('order.'.$notify['title'],[],'en') ;
        $content_en    = Lang::get('order.'.$notify['translation'],['order_name' => $order->title ,'user_name' => $user_name ],'en') ;
        $content_ar    = Lang::get('order.'.$notify['translation'],['order_name' => $order->title ,'user_name' => $user_name ],'ar') ;

        $this->push->sendMessage($devices, [
            'id'                => $notify['id'],
            'title_key'         => $notify['title'],
            'body_key'          => $notify['translation'],
            'order_id'          =>  $notify['order_id'],
            'body_arguments'    => [
                "order_name"     => $order->title
            ]
        ],$content_en,$content_ar,$title_en,$title_ar);

    }
}

<?php

namespace App\Listeners;

use App\Events\RefuseOtherOffersWithOutAcceptedOffer;
use App\Libraries\oneSignal;
use App\Models\Notification;
use Illuminate\Support\Facades\Lang;

class RefuseOtherOffersWithOutAcceptedOfferListener
{
    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }
    /**
     * Handle the event.
     *
     * @param  RefuseOtherOffersWithOutAcceptedOffer  $event
     * @return void
     */
    public function handle(RefuseOtherOffersWithOutAcceptedOffer $event)
    {
        $sender = $event->user;
        $order = $event->order;
        $offer = $event->offer;
        $title = "offers";
        $body =  "offer_refused";

        $user_offer_pending = $order->offers_pending->where('user_id','!=', $sender->id)->pluck('user_id')->toArray();
        $user_ids = array_unique($user_offer_pending);

        $master_offer_query = $order->offers_pending->whereIn('user_id' , $user_ids)->whereNotIn('id',$offer->id);

        if ($order->golden_offer == 0){
             $master_offer_query->where('golden_offer', 0);
        }else{
             $master_offer_query->where('golden_offer', 1);
        }
        $offer_pending = $master_offer_query->values();

        // User Who Owner Order Not Accepted Offer
        $users_waiting =  $offer_pending ? $offer_pending->load('user')
                                            ? $offer_pending->load('user')->pluck('user') :null
                                            :null;
        $this->insert_notification($users_waiting,$sender,$order,$offer, $title,$body);

        // Collect Devices

        $devices = $users_waiting
            ?$users_waiting->pluck('devices')->flatten()
                ?$users_waiting->pluck('devices')->flatten()->pluck('device')
                :null
            :null;

        if(count($devices ) > 0  ) {

            $title_ar      = Lang::get('order.'.$title,[],'ar') ;
            $title_en      =  Lang::get('order.'.$title,[],'en') ;
            $content_en    = Lang::get('order.'.$body,["order_name" => $order->title ],'en') ;
            $content_ar    = Lang::get('order.'.$body,["order_name" => $order->title],'ar') ;


            $this->push->sendMessage($devices,
                [
                    'type'          => 4,
                    'order_id'      => $order->id,
                    'offer_id'      => $offer->id,
                    'title'         => $title,
                    'body'          => $body,
                ],$content_en,$content_ar,$title_en,$title_ar);
        }

    }


    public function insert_notification($users_waiting, $sender , $order , $offer, $title, $body){

        if (count($users_waiting) > 0) {
                $body_argument = [
                    "order_name"    => $order->title
                ];
            $data  =  collect($users_waiting)->map(function ($value) use ($sender , $order , $offer, $title, $body , $body_argument) {
                return [
                    'user_id'      => $value->id,
                    'sender_id'    => $sender->id,
                    'order_id'     => $order->id,
                    'offer_id'     => $offer->id,
                    'title'        => $title,
                    'translation'   => $body,
                    'body'         => $sender->name,
                    'body_arguments' =>json_encode($body_argument) ,
                    'type'         => 4,
                    'created_at'   => now()
                ];
            })->toArray();

            Notification::insert($data);
        }
    }
}

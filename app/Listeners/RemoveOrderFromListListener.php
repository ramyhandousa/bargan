<?php

namespace App\Listeners;

use App\Events\GlobalPublicNotification;
use App\Events\RemoveOrderFromList;
use App\Libraries\oneSignal;
use App\Libraries\PushNotification;
use App\Models\Notification;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class RemoveOrderFromListListener
{
    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  RemoveOrderFromList  $event
     * @return void
     */
    public function handle(RemoveOrderFromList $event)
    {
        $title = $event->title;
        $body = $event->body;
        $order = $event->order;
        $topic = $event->topic;

        event(new GlobalPublicNotification($title, $body,$topic,"cancel",$order));

        $offers = $order->offers_pending->where('user_id','!=',$order->user_offer_id)->pluck('user_id')->toArray();

        $ids = array_unique($offers);

        // User Who Owner Order Not Accepted Offer
        $users_waiting =  User::whereIn('id' , $ids)->with('devices')->get();

        $this->insert_notification($users_waiting,$order,$order, $title,$body);

        // Collect Devices
        $devices = $users_waiting ?
            $users_waiting->pluck('devices')->flatten() ?
                $users_waiting->pluck('devices')->flatten()->pluck('device')
                :null
            :null;

        if(count($devices ) > 0  ) {

            $title_ar      = Lang::get('order.'.$title,[],'ar') ;
            $title_en      =  Lang::get('order.'.$title,[],'en') ;
            $content_en    = Lang::get('order.'.$body,[ "order_name"    => $order->title ],'en') ;
            $content_ar    = Lang::get('order.'.$body,[ "order_name"    => $order->title],'ar') ;


            $this->push->sendMessage($devices,[
                'type'              => 11,
                'order_id'          => $order->id,
                'title_key'         => $title,
                'body_key'          => $body,
                'body_arguments'    => [
                    "order_name"    => $order->title ,
                ],
            ],$content_en,$content_ar,$title_en,$title_ar);
        }

    }

    public function insert_notification($users_waiting, $sender , $order , $title, $body){

        if (count($users_waiting) > 0) {

            $data  =  collect($users_waiting)->map(function ($value) use ($sender , $order , $title, $body) {
                $body_argument = [
                    "order_name" => $order->title ,
                ];
                return [
                    'user_id'      => $value->id,
                    'sender_id'    => $sender->user_id,
                    'order_id'     => $order->id,
                    'title'        => $title,
                    'translation'   => $body,
                    'body'         => $sender->name,
                    'type'         => 11,
                    'body_arguments' =>json_encode($body_argument) ,
                    'created_at'   => now()
                ];
            })->toArray();

            Notification::insert($data);
        }
    }
}

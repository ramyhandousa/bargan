<?php

namespace App\Listeners;

use App\Events\SendAcceptedOffer;
use App\Libraries\InsertNotification;
use App\Libraries\oneSignal;
use App\Libraries\PushNotification;
use App\Models\Device;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Lang;

class SendAcceptedOfferListener
{
    public $notify;
    public $push;

    public function __construct(InsertNotification $notification,oneSignal $push)
    {
        $this->notify = $notification;
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  SendAcceptedOffer  $event
     * @return void
     */
    public function handle(SendAcceptedOffer $event)
    {
        $sender = $event->user;
        $order = $event->order;
        $offer = $event->offer;
        $request = $event->request;

        $notify = $this->notify->NotificationDbType(3,$offer->user_id,$sender,$request,$order,$offer->id);

        $devices = Device::whereUserId($offer->user_id)->pluck('device');

        if(count($devices ) > 0  ) {

            $data_content = ['order_name' => $order->title ,'user_name' => $order->uuid ];
            $title_ar      = Lang::get('order.'.$notify['title'],[],'ar') ;
            $title_en      =  Lang::get('order.'.$notify['title'],[],'en') ;
            $content_en    = Lang::get('order.'.$notify['translation'],$data_content,'en') ;
            $content_ar    = Lang::get('order.'.$notify['translation'],$data_content,'ar') ;


            $this->push->sendMessage($devices,[
                'id'            => $notify['id'],
                'type'              => $notify['type'],
                'title_key'         => $notify['title'],
                'body_key'          => $notify['translation'],
                'order_id'          =>  $notify['order_id'],
                'offer_id'          => $notify['offer_id'],
                'body_arguments'    => [
                    "order_name"    => $order->title ,
                ],
            ],$content_en,$content_ar,$title_en,$title_ar);
        }

    }
}

<?php

namespace App\Listeners;

use App\Events\SendNotificationToOffers;
use App\Libraries\oneSignal;
use App\Models\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Lang;

class SendNotificationToOffersListener
{
    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }


    public function handle(SendNotificationToOffers $event)
    {

        $order = $event->order;
        $sender = $event->order->user;
        $title = $event->title_key;
        $body = $event->body_key;

        $users_waiting =   $order->offers_pending->values()->load('user')->pluck('user');

        $this->insert_notification($users_waiting,$sender,$order, $title,$body);

        // Collect Devices
        $devices = $users_waiting ? $users_waiting->pluck('devices')->flatten()
            ?$users_waiting->pluck('devices')->flatten()->pluck('device')
            :null
            :null;

        if (count($devices) > 0){
            $this->sender_notification_by_devices($devices,$order,$title,$body);
        }

    }

    public function sender_notification_by_devices($devices,$order, $title , $body){
        $title_ar      = Lang::get('order.'.$title,[],'ar') ;
        $title_en      =  Lang::get('order.'.$title,[],'en') ;
        $content_en    = Lang::get('order.'.$body,[ ],'en') ;
        $content_ar    = Lang::get('order.'.$body,[],'ar') ;

        $this->push->sendMessage($devices,
            [
                'order_id'      => $order->id,
                'title_key'     => $title,
                'body_key'      => $body,
                'body_arguments'    => [
                    "user_name"     => $order->uuid,
                    "order_name"    => $order->title
                ],
            ],$content_en,$content_ar,$title_en,$title_ar);
    }

    public function insert_notification($users_waiting, $sender , $order , $title, $body){

        if (count($users_waiting) > 0) {

            $data  =  collect($users_waiting)->map(function ($value) use ($sender , $order , $title, $body) {
                $data_content = ['order_name' => $order->title ,'user_name' => $order->uuid ];
                return [
                    'user_id'      => $value->id,
                    'sender_id'    => $sender->id,
                    'order_id'     => $order->id,
                    'title'        => $title,
                    'translation'   => $body,
                    'body_arguments' =>json_encode($data_content),
                    'created_at'   => now()
                ];
            })->toArray();

            Notification::insert($data);
        }
    }
}

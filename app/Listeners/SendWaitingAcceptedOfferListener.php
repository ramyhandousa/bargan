<?php

namespace App\Listeners;

use App\Events\SendWaitingAcceptedOffer;
use App\Libraries\InsertNotification;
use App\Libraries\oneSignal;
use App\Libraries\PushNotification;
use App\Models\Device;
use App\Models\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Log;

class SendWaitingAcceptedOfferListener
{

    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }


    /**
     * Handle the event.
     *
     * @param  SendWaitingAcceptedOffer  $event
     * @return void
     */
    public function handle(SendWaitingAcceptedOffer $event)
    {
        $sender = $event->user;
        $order = $event->order;
        $offer = $event->offer;
        $title = "waiting_accepted_offer";
        $body =  "review_accepted_offer";

        $order->offers->where('id','!=' , $offer->id)->each->update(['status' => 'refuse']);

        // User Who Owner Order Not Accepted Offer
            $users_waiting =  $order->offers_pending->whereNotIn('id',$offer->id)->values()
                ? $order->offers_pending->whereNotIn('id',$offer->id)->values()->load('user')
                    ? $order->offers_pending->whereNotIn('id',$offer->id)->values()->load('user')->pluck('user')
                :null
                    :null;

            $this->insert_notification($users_waiting,$sender,$order,$offer, $title,$body);

        // Collect Devices
            $devices = $users_waiting
                            ?$users_waiting->pluck('devices')->flatten()
                                    ?$users_waiting->pluck('devices')->flatten()->pluck('device')
                            :null
                                    :null;

        if(count($devices ) > 0  ) {

//            $this->push->sendPushNotification($devices, null, $title, $body,
//                [
//                    'type'          => 4,
//                    'order_id'      => $order->id,
//                    'offer_id'      => $offer->id,
//                    'title'         => $title,
//                    'body'          => $body,
//                ]
//            );

            $title_ar      = Lang::get('order.'.$title,[],'ar') ;
            $title_en      =  Lang::get('order.'.$title,[],'en') ;
            $content_en    = Lang::get('order.'.$body,[ ],'en') ;
            $content_ar    = Lang::get('order.'.$body,[],'ar') ;


            $this->push->sendMessage($devices,
                [
                    'type'          => 4,
                    'order_id'      => $order->id,
                    'offer_id'      => $offer->id,
                    'title'         => $title,
                    'body'          => $body,
            ],$content_en,$content_ar,$title_en,$title_ar);
        }
    }


    public function insert_notification($users_waiting, $sender , $order , $offer, $title, $body){

        if (count($users_waiting) > 0) {

            $data  =  collect($users_waiting)->map(function ($value) use ($sender , $order , $offer, $title, $body) {
                return [
                    'user_id'      => $value->id,
                    'sender_id'    => $sender->id,
                    'order_id'     => $order->id,
                    'offer_id'     => $offer->id,
                    'title'        => $title,
                    'translation'   => $body,
                    'body'         => $sender->name,
                    'type'         => 4,
                    'created_at'   => now()
                ];
            })->toArray();

            Notification::insert($data);
        }
    }
}

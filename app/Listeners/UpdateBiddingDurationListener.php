<?php

namespace App\Listeners;

use App\Events\SendNotificationToOffers;
use App\Events\UpdateBiddingDuration;
use App\Libraries\oneSignal;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Lang;

class UpdateBiddingDurationListener
{

    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  UpdateBiddingDuration  $event
     * @return void
     */
    public function handle(UpdateBiddingDuration $event)
    {
        $order = $event->order;
        $sender = $event->order->user;
        $title = 'orders';
        $body = 'update_bidding_duration';

        event(new SendNotificationToOffers($order,$title,$body));

        $order->offers_pending->each->update(['status' => 'refuse']);

        $order->update(['user_offer_id' => null, 'offer_id' => null ,'time_out' => 0
                        ,'end_bidding_duration' => Carbon::parse(now())->addMinutes($order->bidding_duration)]);

    }


}

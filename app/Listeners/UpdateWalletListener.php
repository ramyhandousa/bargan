<?php

namespace App\Listeners;

use App\Events\UpdateWallet;
use App\Models\Transaction;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateWalletListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UpdateWallet  $event
     * @return void
     */
    public function handle(UpdateWallet $event)
    {
        $user = $event->user;

        $order = $event->order ? $event->order->id : null ;

        $offer = $event->offer ? $event->offer->id : null ;

        $paymentId = $event->paymentId ? $event->paymentId : null ;

        $progress = $event->progress;

        $payment = $event->payment;

        if ($progress == 'pull'){

            $price = - $event->request;

        }else{

            $price = $event->request;
        }

        Transaction::create(['user_id' => $user->id ,'order_id' => $order ,'offer_id' => $offer ,
                    'progress' => $progress ,'price' => $price ,'payment' => $payment , 'paymentId' => $paymentId]);
    }
}

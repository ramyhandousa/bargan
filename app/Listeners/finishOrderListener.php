<?php

namespace App\Listeners;

use App\Events\finishOrder;
use App\Libraries\oneSignal;
use App\Models\Device;
use App\Models\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Lang;

class finishOrderListener
{
    public $push;

    public function __construct( oneSignal $push)
    {
        $this->push = $push;
    }

    /**
     * Handle the event.
     *
     * @param  finishOrder  $event
     * @return void
     */
    public function handle(finishOrder $event)
    {
        $order          = $event->order;
        $offer          = $order->my_offer;
        $title          = "orders";
        $body_user      = "complete_order_user";
        $body_offer     = "complete_order_offer";
        $body_argument  = [ "order_name"    => $order->title ];

        $title_ar      = Lang::get('order.'.$title,[],'ar') ;
        $title_en      =  Lang::get('order.'.$title,[],'en') ;

        $notification = Notification::whereUserIdAndOrderIdAndOfferIdAndTranslation($order->user_offer_id,$order->id,$offer->id,$body_offer)->first();

        if (!$notification){

            $this->notification_database($event,$order,$offer,$title,$body_argument,$body_user,$body_offer);

            if ($event->send_to_order_man){
                $this->push_notification_to_user_order($order,$offer,$title,$title_ar,$title_en,$body_user);
            }

            if ($event->send_to_offer_man){
                $this->push_notification_to_user_offer($order,$offer,$title,$title_ar,$title_en,$body_offer);
            }

        }
    }

    public function notification_database($event , $order ,$offer , $title,$body_argument, $body_user , $body_offer ){
        $data = [
            'order_id'     => $order->id,'offer_id'  => $offer->id, 'title'        => $title,
            'body_arguments' => json_encode($body_argument) , 'created_at'   => now()
        ];

        if ($event->send_to_order_man){
            Notification::create(array_merge($data,[
                'user_id' => $order->user_id,'sender_id' => $order->user_offer_id ,"translation" => $body_user,"body" => $body_user
            ]));
        }

        if ($event->send_to_offer_man){
            Notification::create(array_merge($data,[
                'user_id' => $order->user_offer_id,'sender_id' => $order->user_id ,"translation" => $body_offer,"body" => $body_offer
            ]));
        }

    }


    public function push_notification_to_user_order($order , $offer , $title, $title_ar ,$title_en ,$body_user){

        $user_devices = Device::where('user_id',$order->user_id)->pluck('device');

        $content_user_en    = Lang::get('order.'.$body_user,["order_name" => $order->title ],'en') ;
        $content_user_ar    = Lang::get('order.'.$body_user,["order_name" => $order->title],'ar') ;

        $this->push->sendMessage($user_devices,[
            'order_id'      => $order->id,
            'offer_id'      => $offer->id,
            'title'         => $title,
            'body'          => $body_user,
        ],$content_user_en,$content_user_ar,$title_en,$title_ar);

    }


    public function push_notification_to_user_offer($order , $offer , $title, $title_ar ,$title_en ,$body_offer){

        $offer_devices = Device::where('user_id',$order->user_offer_id)->pluck('device');

        $content_offer_en    = Lang::get('order.'.$body_offer,["order_name" => $order->title ],'en') ;
        $content_offer_ar    = Lang::get('order.'.$body_offer,["order_name" => $order->title],'ar') ;

        $this->push->sendMessage($offer_devices,[
            'order_id'      => $order->id,
            'offer_id'      => $offer->id,
            'title'         => $title,
            'body'          => $body_offer,
        ],$content_offer_en,$content_offer_ar,$title_en,$title_ar);
    }
}

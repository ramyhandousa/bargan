<?php

namespace App\Listeners;

use App\Events\phoneChange;
use App\Mail\codeActive;
use App\Models\VerifyUser;
use Illuminate\Support\Facades\Mail;

class phoneChangeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  phoneChange  $event
     * @return void
     */
    public function handle(phoneChange $event)
    {
        $data = [ 'email' => $event->request->email , 'action_code'  => $event->code ];

         VerifyUser::updateOrCreate(['user_id' => $event->user->id], $data);

        Mail::to($event->user->email)->send(new codeActive($event->user , $event->code));
    }
}

<?php

namespace App\Listeners;

use App\Events\uploadImage;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
class uploadImageListener
{

    public function __construct()
    {

    }


    public function handle(uploadImage $event)
    {
        $model = $event->model;

        $image = $event->request->file('image');

        $name = time().'.'.$image->getClientOriginalExtension();

//        $image->storeAs('images' ,$name);

        $event->request->image->move(public_path('/files'), $name);

//        if ($model->image){
//            unlink(public_path($model->image));
//        }
        $model->update(['image' => '/public/files/'.$name]);
    }

}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUsReply extends Mailable
{
    use Queueable, SerializesModels;

    public  $support ;
    public  $new_support ;

    public function __construct($support, $new_support)
    {
        $this->support = $support;
        $this->new_support = $new_support;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.reply')->with([
            'support' => $this->support,
            'new_support' => $this->new_support,
        ]);
    }
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class newInquerire extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $inquerie;
    public function __construct(  $user , $inquerie)
    {
        $this->user = $user;
        $this->inquerie = $inquerie;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.new_inquerie')->with([
            'user' => $this->user,
            'inquerie' => $this->inquerie,
        ]);
    }
}

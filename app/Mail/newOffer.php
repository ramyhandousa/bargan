<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class newOffer extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $offer;
    public function __construct(  $user , $offer)
    {
        $this->user = $user;
        $this->offer = $offer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.new_offer')->with([
            'user' => $this->user,
            'offer' => $this->offer,
        ]);
    }
}

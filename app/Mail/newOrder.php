<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class newOrder extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    public $order;
    public function __construct(  $user , $order)
    {
        $this->user = $user;
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('emails.new_order')->with([
            'user' => $this->user,
            'order' => $this->order,
        ]);
    }
}

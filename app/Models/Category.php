<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Translatable , HasFactory;

    protected $fillable = ['is_suspend'];
    public $translatedAttributes = ['name',"product_title","service_title",'description'];

    protected $hidden = [
        'translations'
    ];


    public function parent(){
        return $this->belongsTo(Category::class,'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id')->where('is_suspend','=',0);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function offers()
    {
        return $this->hasMany(Order::class)->whereHas('offers');
    }

    public function users(){
        return $this->belongsToMany(User::class,'category_users');
    }

}

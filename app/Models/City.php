<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use Translatable,HasFactory;

    public $translatedAttributes = ['name'];

    protected $fillable = ['is_suspend'];

    protected $hidden = [
        'translations'
    ];

    public function parent(){
        return $this->belongsTo(Category::class,'parent_id');
    }

    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id')->where('is_suspend','=',0);
    }


}

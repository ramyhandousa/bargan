<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryWay extends Model
{
    use Translatable , HasFactory;

    protected $fillable = ['is_suspend'];

    public $translatedAttributes = ['name'];

    protected $hidden = [
        'translations'
    ];
}

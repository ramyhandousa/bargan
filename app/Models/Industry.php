<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    use Translatable , HasFactory;

    public $translatedAttributes = ['name'];


    protected $fillable = ['is_suspend'];

    protected $hidden = [
        'translations'
    ];

}

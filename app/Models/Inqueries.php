<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inqueries extends Model
{
    use HasFactory;

    protected $fillable = ['order_id' , 'sender_id' , 'user_offer_id','message','is_accepted','is_read','created_at'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function sender()
    {
        return $this->belongsTo(User::class);
    }

    public function user_offer()
    {
        return $this->belongsTo(User::class,'user_offer_id');
    }
}

<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Label extends Model
{
    use Translatable,HasFactory;

    public $translatedAttributes = ['name'];

    protected $guarded = [];

    protected $hidden = [
        'translations'
    ];

    public function label_values(){
        return $this->hasMany(LabelValue::class);
    }
}

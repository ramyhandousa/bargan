<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    use HasFactory , CanBeScoped;

    protected $guarded = [];

    protected $dates = ['created_at' , 'updated_at'];
    public function user(){

        return $this->belongsTo(User::class);
    }

    public function order(){

        return $this->belongsTo(Order::class);
    }

    public function offer_details(){
        return $this->hasMany(OfferDetail::class);
    }

    public function industry(){

        return $this->belongsTo(Industry::class);
    }

    public function payment_method(){

        return $this->belongsTo(PaymentMethod::class);
    }

    public function warranty(){

        return $this->belongsTo(Warranty::class);
    }

    public function delivery_way(){

        return $this->belongsTo(DeliveryWay::class);
    }

    public function reason_rejection(){

        return $this->belongsTo(ReasonRejection::class);
    }

}

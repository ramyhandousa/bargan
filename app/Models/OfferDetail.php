<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfferDetail extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function label(){

        return $this->belongsTo(Label::class);
    }

    public function label_value(){
        return $this->belongsTo(LabelValue::class);
    }

}

<?php

namespace App\Models;

use App\Traits\CanBeScoped;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    use HasFactory , CanBeScoped;

    protected $guarded = [];

    protected $casts = [
        'images' => 'array'
    ];

    public function user(){

        return $this->belongsTo(User::class);
    }

    public function filter_user(){

        return $this->belongsTo(User::class,'user_id')->select('id');
    }


    public function interactive_forms(){
        return $this->hasMany(InteractiveForm::class);
    }


    public function order_details(){
        return $this->hasMany(OrderDetails::class);
    }


    public function my_offer(){

        return $this->belongsTo(Offer::class,'offer_id')->where('is_accepted','=',1);
    }
    public function offer_man(){

        return $this->belongsTo(User::class,'user_offer_id');
    }

    public function old_offer(){

        return $this->belongsTo(Offer::class,'old_offer_id')->where('is_accepted','=',1);
    }

    public function city(){

        return $this->belongsTo(City::class);
    }

    public function category(){

        return $this->belongsTo(Category::class);
    }

    public function industry(){

        return $this->belongsTo(Industry::class);
    }

    public function payment_method(){

        return $this->belongsTo(PaymentMethod::class);
    }

    public function warranty(){

        return $this->belongsTo(Warranty::class);
    }

    public function delivery_way(){

        return $this->belongsTo(DeliveryWay::class);
    }

    public function reason_rejection(){

        return $this->belongsTo(ReasonRejection::class);
    }

    public function offers_still_not_accepted(){
        return $this->hasMany(Offer::class,'order_id');
    }

    public function offers(){
        return $this->hasMany(Offer::class)->where('is_accepted','=',1);
    }

    public function user_offer($id){
        return $this->offers_still_not_accepted()->where('user_id','=',$id)->
            whereNotIn('status',['cancel','refuse'])->first();
    }

    public function user_pervious_offer($id){
        return $this->offers()->where('user_id','=',$id)
            ->where('status','=',"pending")->where('is_pay','=',1)->first();
    }
    public function user_current_offer($id,$status = 'pending'){

        return $this->offers()->where('user_id','=',$id)
            ->orderByRaw("case when status = '$status' then 0 else 1 end ")
            ->orderByRaw("case when is_pay = 0 then 0 else 1 end ")
//            ->orderByRaw(DB::raw("FIELD('status', '$status') DESC"))
            ->first();
    }

    public function accepted_offer($id){
//        return $this->offers()->where('user_id','=',$id)
//            ->where('is_pay','=',1)
//            ->orderByRaw("case when status = 'finish' then 0 else 1 end ")->first();
        return $this->offers()->where('id','=',$id)->first();
    }

    public function offers_pending(){
        return $this->offers()->where('status','=','pending');
    }

    public function golden_offer_status(){
        return $this->offers_pending()->where('golden_offer','=',1)->where('status','!=','cancel');
    }

    public function min_price_offers(){
        return  $this->offers()->count() > 0 ?
                        $this->order_type == 'tender' ?
                                $this->offers()->whereIn('status',['pending','finish'])->min('expected_price')
                            :   $this->offers()->whereIn('status',['pending','finish'])->max('expected_price')
                : null;
    }

    public function min_price_golden_offers(){
        $offer_golden =$this->offers()->where('golden_offer','=',1)->whereIn('status',['pending','finish']);

        return  $this->offers()->count() > 0 ?
                        $this->order_type == 'tender' ?
                            $offer_golden->min('expected_price')
                            :   $offer_golden->max('expected_price')
                : null;
    }


    public function rate_order(){
        return $this->hasOne(Rate::class);
    }

}

<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    use Translatable , HasFactory;

    protected $guarded = [];

    public $translatedAttributes = ['name'];

    protected $hidden = [
        'translations'
    ];


}

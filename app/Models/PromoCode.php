<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model
{
    protected $guarded = [];

    protected $dates = ['start_at' , 'end_at'];


    public function users_count_code(){
        return $this->hasMany(Transaction::class,'promo_code','code');
    }
}

<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StaticData extends Model
{
    use Translatable,HasFactory;

    public $translatedAttributes = ['name','title'];

    protected $hidden = [
        'translations'
    ];

}

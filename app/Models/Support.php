<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    use HasFactory;

    protected $fillable = [

        'is_read'
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sender()
    {
        return $this->belongsTo(User::class , 'sender_id');
    }


    public function children()
    {
       return  $this->hasMany(Support::class, 'parent_id');
    }


    public function parent(){
        return $this->belongsTo(Support::class,'parent_id');
    }


    public function reply_message()
    {
       return  $this->hasOne(Support::class, 'parent_id');
    }


    public function type()
    {
       return  $this->belongsTo(TypesSupport::class);
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $casts = [
        'message' => 'array'
    ];

    public function order(){
        return $this->belongsTo(Order::class);
    }

    public function offer(){
        return $this->belongsTo(Offer::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function reason_rejection(){
        return $this->belongsTo(ReasonRejection::class);
    }

}

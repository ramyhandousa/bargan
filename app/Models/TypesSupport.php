<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class TypesSupport extends Model
{
       use Translatable , HasFactory;
       public $translatedAttributes = ['name'];
       protected $fillable = ['name'];

    protected $hidden = [
        'created_at','updated_at' ,'translations'
    ];

       public function support()
       {
	    return $this->belongsTo(Support::class);
       }
}

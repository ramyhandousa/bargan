<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasFactory, Notifiable;


    protected $guarded = [];


    protected $casts = [
        "permission" => "array",
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

//    public function getDescCategoryAttribute()
//    {
//        if ($this->attributes['desc_category']){
//
////            return explode(',', $this->attributes['desc_category']);
//        }
//    }

    public function devices(){
        return $this->hasMany(Device::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

    public function offers(){
        return $this->hasMany(Offer::class);
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'category_users')->where('is_active','=',1);
    }


    public function rating(){
        return $this->hasMany(Rate::class);
    }

//    public function total_rate(){
//        return $this->rating()->sum('rate');
//    }

    public function total_rate($column = 'order_rate'){

        if ( $this->orders()->count() <= 0 && $this->orders()->sum($column) >= 0){
            return  "0";
        }

        return number_format($this->orders()->sum($column) / $this->orders()->count(),0);
    }

    public function transaction(){
        return $this->hasMany(Transaction::class);
    }

    public function transaction_deposit_by_promo_code(){
        return $this->transaction()
            ->where('progress','=','deposit_by_promo_code')
            ->sum('price');
    }

    public function transaction_deposit_by_admin(){
        return $this->transaction()
            ->where('progress','=','deposit_by_admin')
            ->sum('price');
    }

    public function transaction_deposit_by_user(){
        return $this->transaction()
            ->where('progress','=','deposit')
            ->sum('price');
    }

    public function transaction_pull(){
        return $this->transaction()
            ->where('progress','=','pull')
            ->sum('price');
    }

    public function my_wallet(){
        return ( $this->transaction_deposit_by_promo_code() +
            $this->transaction_deposit_by_admin() +
            $this->transaction_deposit_by_user()  ) -
            $this->transaction_pull()  ;
    }

    public function wallet(){
        return $this->transaction() ;
    }


}

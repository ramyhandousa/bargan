<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warranty extends Model
{
    use Translatable , HasFactory;


    public $translatedAttributes = ['name'];

    protected $hidden = [
        'translations'
    ];

    protected $fillable = ['is_suspend'];

}

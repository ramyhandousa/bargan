<?php

namespace App\Observers;

use App\Models\Offer;
use App\Models\Setting;
use Illuminate\Support\Str;

class OfferObserve
{

    public function creating(Offer $offer)
    {
        $offer->rand_user_id = substr(rand(), 0, 7);
        if ($offer->order->active_golden_offer){
            $offer->golden_offer = 1;
        }
    }

    /**
     * Handle the Offer "updated" event.
     *
     * @param  \App\Models\Offer  $offer
     * @return void
     */
    public function updated(Offer $offer)
    {
        //
    }

    /**
     * Handle the Offer "deleted" event.
     *
     * @param  \App\Models\Offer  $offer
     * @return void
     */
    public function deleted(Offer $offer)
    {
        //
    }

    /**
     * Handle the Offer "restored" event.
     *
     * @param  \App\Models\Offer  $offer
     * @return void
     */
    public function restored(Offer $offer)
    {
        //
    }

    /**
     * Handle the Offer "force deleted" event.
     *
     * @param  \App\Models\Offer  $offer
     * @return void
     */
    public function forceDeleted(Offer $offer)
    {
        //
    }
}

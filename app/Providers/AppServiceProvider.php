<?php

namespace App\Providers;

use App\Models\Offer;
use App\Models\Order;
use App\Models\User;
use App\Observers\OfferObserve;
use App\Observers\OrderObserve;
use App\Observers\UserObserver;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        URL::forceScheme('https');
        User::observe(UserObserver::class);
        Order::observe(OrderObserve::class);
        Offer::observe(OfferObserve::class);
    }
}

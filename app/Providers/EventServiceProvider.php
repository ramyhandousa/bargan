<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\PhoneOrEmailChange' => [
            'App\Listeners\PhoneOrEmailChangeListener',
        ],
        'App\Events\UserLogOut' => [
            'App\Listeners\UserLogOutListener',
        ],
        'App\Events\SendAcceptedOffer' => [
            'App\Listeners\SendAcceptedOfferListener',
        ],
        'App\Events\SendWaitingAcceptedOffer' => [
            'App\Listeners\SendWaitingAcceptedOfferListener',
        ],
        'App\Events\RefuseOtherOffersWithOutAcceptedOffer' => [
            'App\Listeners\RefuseOtherOffersWithOutAcceptedOfferListener',
        ],
        'App\Events\finishOrder' => [
            'App\Listeners\finishOrderListener',
        ],
        'App\Events\UpdateWallet' => [
            'App\Listeners\UpdateWalletListener',
        ],
        'App\Events\NewCategoryInOrder' => [
            'App\Listeners\NewCategoryInOrderListener',
        ],
        'App\Events\NewOfferOrEditInOrder' => [
            'App\Listeners\NewOfferOrEditInOrderListener',
        ],
        'App\Events\OfferManAcceptedOrder' => [
            'App\Listeners\OfferManAcceptedOrderListener',
        ],
        'App\Events\OrderGoldenOfferNotActive' => [
            'App\Listeners\OrderGoldenOfferNotActiveListener',
        ],
        'App\Events\OrderGoldenOfferActive' => [
            'App\Listeners\OrderGoldenOfferActiveListener',
        ],
        'App\Events\OrderUpdateGoldenOffer' => [
            'App\Listeners\OrderUpdateGoldenOfferListener',
        ],
        'App\Events\NewGoldenOffer' => [
            'App\Listeners\NewGoldenOfferListener',
        ],
        'App\Events\UpdateBiddingDuration' => [
            'App\Listeners\UpdateBiddingDurationListener',
        ],
        'App\Events\BestOfferAddOrRemove' => [
            'App\Listeners\BestOfferAddOrRemoveListener',
        ],
        'App\Events\RemoveOrderFromList' => [
            'App\Listeners\RemoveOrderFromListListener',
        ],
        'App\Events\GlobalPublicNotification' => [
            'App\Listeners\GlobalPublicNotificationListener',
        ],
        'App\Events\RateOrder' => [
            'App\Listeners\RateOrderListener',
        ],
        'App\Events\OrderKeysDiff' => [
            'App\Listeners\OrderKeysDiffListener',
        ],
        'App\Events\SendNotificationToOffers' => [
            'App\Listeners\SendNotificationToOffersListener',
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

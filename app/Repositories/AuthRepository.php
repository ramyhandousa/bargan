<?php


namespace App\Repositories;

use App\Events\phoneChange;
use App\Events\PhoneOrEmailChange;
use App\Events\UserLogOut;
use App\Http\Helpers\Sms;
use App\Http\Resources\User\UserResource;
use App\Mail\codeActivation;
use App\Mail\codeActive;
use App\Models\Category;
use App\Models\CategoryUser;
use App\Models\Device;
use App\Models\User;
use App\Models\VerifyUser;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use App\Traits\RespondsWithHttpStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthRepository implements AuthRepositoryInterface
{

    use RespondsWithHttpStatus;

    public $language;
    public function __construct( )
    {
        $this->language = request()->headers->get('Accept-Language') ?  : 'ar';
        app()->setLocale($this->language);
   }

    public function register($request)
    {
        $user = User::create($request->validated());

       $this->updateCategories($request,$user);

        $action_code = $this->random_code_active();

        $this->createVerfiy($request,$user,$action_code);

//        Mail::to($user->email)->send(new codeActive($user , $action_code));

        Sms::sendMessage('Activation code:' . $action_code, $request->phone);

        return ['code' => $action_code, 'api_token' => $user->api_token];
    }




    public function login($request)
    {
        $user =  User::whereEmail($request->provider)->orWhere('phone',$request->provider)->first();

        $user->update(['is_active_phone' => 0 ,'send_notification' => 0]);

        $action_code = $this->random_code_active();

        $this->createOrUpdateVerfiy($user,$action_code);

//        Mail::to($user->email)->send(new codeActive($user , $action_code));

        if ($request->provider != '0599999999' ){
            Sms::sendMessage('Activation code:' . $action_code, $user->phone);
        }


        $this->manageDevices($request, $user);

        return new UserResource($user);
    }


    public function user_info($user){

        return new UserResource($user);
    }

    public function forgetPassword($request)
    {
        $user = User::whereEmail($request->provider)->orWhere('phone', $request->provider)->first();

        $columnChange = filter_var($request->provider, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        $action_code = $this->random_code_active();

        event(new PhoneOrEmailChange($user,$action_code,$columnChange,$request));

        $message = $columnChange == 'email' ? trans('global.activation_code_sent_email')  :  trans('global.activation_code_sent');

        return ['code' => $action_code ,'message' => $message];
    }

    public function resetPassword($request)
    {
        $user = User::whereEmail($request->provider)->orWhere('phone', $request->provider)->first();

        $this->manageDevices($request, $user);

        if ($request->password){

            $user->update(['password' => $request->password]);
            $message = trans('global.password_was_edited_successfully');

        }else{
            $message = trans('global.password_not_edited');
        }

        return  [ 'data' => new UserResource($user) , 'message' => $message ];
    }

    public function checkCode($request)
    {
        $verifyUser = VerifyUser::whereEmail($request->provider)->orWhere('phone', $request->provider)->with('user')->first();

        $user = $verifyUser['user'];

        $phone = $verifyUser->phone ? : $user->phone;
        $email = $verifyUser->email ? : $user->email ;

        $verifyUser['user']->update(['phone' => $phone , 'email' => $email]);

//        $columnChange = filter_var($request->provider, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

//        if ($columnChange == 'phone'){
            $verifyUser['user']->update([ 'is_active_phone' => 1 ,'skip_user' => 0]);
//        }else{
//            $verifyUser['user']->update([ 'is_active_email' => 1 ]);
//        }

        $verifyUser->delete();

        $this->manageDevices($request, $verifyUser['user']);

        return new UserResource($verifyUser['user']);
    }

    public function resendCode($request)
    {
        $user = VerifyUser::whereEmail($request->provider)->orWhere('phone', $request->provider)->with('user')->first();

        $columnChange = filter_var($request->provider, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';

        $action_code = $this->random_code_active();

        event(new PhoneOrEmailChange($user['user'],$action_code,$columnChange,$request));

//        $message = $columnChange == 'email' ? trans('global.activation_code_sent_email')  :  trans('global.activation_code_sent');
        $message =  trans('global.activation_code_sent');

        return ['code' => $action_code ,'message' => $message];
    }

    public function changPassword($request)
    {
        $user = Auth::user();

        if ($request->newPassword){

            $user->update( [ 'password' => $request->newPassword] );

            $message =  trans('global.password_was_edited_successfully') ;

        }else{

            $message = trans('global.password_not_edited');
        }

        return  ['message' => $message] ;
    }

    public function editProfile($request)
    {
        $user = Auth::user();

        if ($request->name){
            $user->update(['name' => $request->name]);
        }

        $user->update(['disable_notifications' => $request->disable_notifications ]);

        if ($request->email){
            $user->update(['email' => $request->email]);
        }

        $this->updateCategories($request,$user);

        $user->load('categories');

        $action_code = $this->random_code_active();

        $columnChange = 'phone';
        $requestChange = $request->phone;

        $columnExists = $user->where($columnChange, $requestChange)->exists();

        if (!$columnExists && $requestChange){
            event(new PhoneOrEmailChange($user,$action_code,$columnChange,$request));
        }

        return new UserResource($user);
    }

    public function updateCategories($request, $user){
        if (count($request->categories) > 0){

            $data =  CategoryUser::whereUserId($user->id)->get();
            if (count($data) > 0){
                $data->each->delete();
            }
            $categories  = collect($request->categories)->map(function ($category) use ($user){
                return [
                    'user_id' => $user->id,
                    'category_id' => $category
                ];
            })->toArray();

            CategoryUser::insert($categories);
        }

        if (count($request->new_categories) > 0){

            foreach ($request->new_categories as $new_categories){
                $new = new Category();
                if ($this->language == 'ar'){

                    $new->{'name:ar'} = $new_categories;
                }else{

                    $new->{'name:en'} = $new_categories;
                }
                $new->save();
                CategoryUser::create(['user_id' => $user->id , 'category_id' => $new->id]);
            }
        }
    }

    public function logOut($request)
    {
        $user = Auth::user();

        event(new UserLogOut($user,$request));
    }

    public function manageDevices($request, $user)
    {
        if ($request->deviceToken) {

            Device::updateOrCreate(['device' => $request->deviceToken],['user_id' =>$user->id ,'device' => $request->deviceToken ]);
        }
    }

    public function createVerfiy($request , $user , $action_code){

        $data = [ 'phone' => $request->phone,'email' => $request->email , 'action_code'  => $action_code];

        VerifyUser::updateOrCreate(['user_id' => $user->id], $data);
    }

    public function createOrUpdateVerfiy( $user , $action_code){

        $data = [ 'phone' => $user->phone,'email' => $user->email , 'action_code'  => $action_code];

        VerifyUser::updateOrCreate(['user_id' => $user->id], $data);
    }

    public function refreshToken($request){

        $this->manageDevices($request,$request->user());

    }
}

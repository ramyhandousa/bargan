<?php


namespace App\Repositories;


use App\Events\BestOfferAddOrRemove;
use App\Events\finishOrder;
use App\Events\NewOfferOrEditInOrder;
use App\Events\OfferManAcceptedOrder;
use App\Events\OrderGoldenOfferActive;
use App\Events\OrderGoldenOfferNotActive;
use App\Events\OrderKeysDiff;
use App\Events\RefuseOtherOffersWithOutAcceptedOffer;
use App\Events\SendAcceptedOffer;
use App\Events\SendWaitingAcceptedOffer;
use App\Events\UpdateWallet;
use App\Http\Resources\Offer\OfferIndexResource;
use App\Http\Resources\Offer\OfferShowResource;
use App\Http\Resources\Offer\OrderOfferIndexResource;
use App\Http\Resources\Offer\OrderOfferResource;
use App\Http\Resources\Order\OrderIndex;
use App\Http\Resources\Order\OrderShowResource;
use App\Mail\newOffer;
use App\Models\OfferDetail;
use App\Models\Order;
use App\Models\Setting;
use App\Models\Transaction;
use App\Scoping\Scopes\OfferScope;
use App\Traits\paginationTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OfferRepository
{
    use paginationTrait ;

    public function list($request){

        $user = $request->user();

        $orders =  Order::whereHas('offers_still_not_accepted',function ($offer)use ($user){
            $offer->where('user_id' , $user->id)->withScopes($this->filterQuery());
        })->where('is_accepted',1)->where('is_suspend',0);

        $count_orders = $orders->count();

        $this->pagination_query($request, $orders);

        return ['total_count' => $count_orders , 'data' =>  OrderOfferIndexResource::collection($orders->with('offers_still_not_accepted')->get()) ] ;
    }

    protected function filterQuery(){
        return [
            'filter' => new OfferScope()
        ];
    }

    public function show_offer($offer){

        return new OrderOfferResource($offer->order);
    }

    public function showOrderOffer($order){

        return new OrderOfferResource($order);
    }

    public function make_offer($request){

        $offer = $request->user()->offers()->create($request->validated());

        if ($request->offer_details ){
            OfferDetail::insert($this->mappingOfferDetails($offer,$request));
        }

        $order = $offer->order;

//        event(new NewOfferOrEditInOrder($request->user(),$order,$offer,$request) );

        $email = Setting::getBody("email_bargain");

        if ($email){
            Mail::to($email)->send(new newOffer($request->user() , $offer));
        }

        if ($order->active_golden_offer == 1){
            return  new OrderOfferResource($order) ;
        }else{

            return new OrderShowResource($order);
        }
    }


    public function edit_offer($request,$offer){

        $offer->update(array_merge($request->validated(),['is_accepted' => 0, 'is_edit' => 1]));

        if ($request->offer_details ){
            $offer->offer_details->each->delete();
            OfferDetail::insert($this->mappingOfferDetails($offer,$request));
        }
        $order = $offer->order;

//        event(new NewOfferOrEditInOrder($request->user(),$order,$offer,$request,'edit') );

        return  new OrderOfferResource($order) ;
    }

    public function mappingOfferDetails($offer, $request){

        return collect($request->offer_details)->map(function ($details) use ($offer){
            return [
                'offer_id' => $offer->id,
                'label_id' => $details['label_id'],
                'label_value_id' => $details['label_value_id'],
            ];
        })->toArray();
    }

    public function cancel_offer($request,$offer){

        $my_order = $offer->order;

        event(new OrderKeysDiff($my_order->user,$request->user(),$my_order,$request,'orders','cancel_offer_with_user_name',$offer));

        $old_best_price = $my_order->min_price_offers();

        $offer->update(['time_out' => 1 ,'reason_rejection_id' => $request->reason_rejection_id ,'status' => 'cancel']);

        event(new BestOfferAddOrRemove("remove_offer", "remove_offer",$my_order,$request,'new_offer_or_edit_in_order',$old_best_price));

       return new OrderOfferResource($my_order);
    }


    public function pay_by_wallet($request,$offer){

        // 1 -  Update Order To be Payed
        // 2 -  Wallet de increment (pull)
        // 3 -  Send Notification To User Who make order Man offer Pay

        try {
            DB::beginTransaction();

            $user = $request->user();
            $order = $offer->order;

            if ($order->active_golden_offer == 0) {

                $commission = Setting::getBody('commission_offer');
                $offer->update(['commission' => $commission]);

            }else{

                $user_offer = $offer->where('user_id',$user->id)->where('is_pay',1)->first();

                $commission_golden_offer = Setting::getBody('commission_golden_offer');
                if ($user_offer){

                    $commission =   $commission_golden_offer - $user_offer->commission;
                }else{

                    $commission =  $commission_golden_offer;
                }

                $offer->update(['golden_commission' => $commission]);
                // Check If User Order Choose The Offer Man in First Time
                if ($order->old_offer_id != $offer->id){
//                    $last_offer = $order->old_offer;
//                    $last_user_pay = $order->old_offer->user;
//                    $last_golden_commission = $order->old_offer->commission;
//                    event(new UpdateWallet($last_user_pay,$last_golden_commission,$order,$last_offer,'deposit'));
                }
            }

            $offer->update(['is_pay' => 1 , 'status' => 'finish' ]);

            $time_control_panel = Setting::getBody('time_active_golden_offer');

            $time_active_golden_offer = Carbon::now()->addMinutes($time_control_panel);

            $order->update(['end_bidding_duration' => $time_active_golden_offer ,
                            'time_active_golden_offer' => $time_active_golden_offer ]);

            event(new UpdateWallet($user,$commission,$order,$offer));

//            event(new finishOrder($order));
            if ($order->golden_offer == 0 || $order->active_golden_offer == 1){

                if($order->active_golden_offer != 1){
                    event(new finishOrder($order));
                }else{

                    event(new OfferManAcceptedOrder($user,$order,$offer,$request));
                }
                event(new RefuseOtherOffersWithOutAcceptedOffer($user,$order,$offer,$request));
                event(new OrderGoldenOfferNotActive($user,$order,$offer,$request));
            }else{

                event(new OrderGoldenOfferActive($user,$order,$offer,$request));
            }

            DB::commit();

            return ['status' => 200 , 'message' => "تم دفع الطلب وتستطيع التواصل الان بنجاح" ,"data" => new OrderOfferResource($order) ];

        }catch (\Exception $exception){

            DB::rollBack();

            return ['status' => 400 , 'message' => $exception->getMessage() ];
        }

    }

}

<?php


namespace App\Repositories;
use App\Events\BestOfferAddOrRemove;
use App\Events\finishOrder;
use App\Events\GlobalPublicNotification;
use App\Events\NewCategoryInOrder;
use App\Events\NewGoldenOffer;
use App\Events\OfferManAcceptedOrder;
use App\Events\OrderGoldenOfferActive;
use App\Events\OrderGoldenOfferNotActive;
use App\Events\OrderKeysDiff;
use App\Events\OrderUpdateGoldenOffer;
use App\Events\RefuseOtherOffersWithOutAcceptedOffer;
use App\Events\RemoveOrderFromList;
use App\Events\SendAcceptedOffer;
use App\Events\SendNotificationToOffers;
use App\Events\SendWaitingAcceptedOffer;
use App\Events\UpdateBiddingDuration;
use App\Events\UpdateWallet;
use App\Http\Resources\Order\OrderIndex;
use App\Http\Resources\Order\OrderOfferShowResource;
use App\Http\Resources\Order\OrderShowResource;
use App\Mail\newOrder;
use App\Models\OrderDetails;
use App\Models\Setting;
use App\Repositories\Interfaces\OrderRepositoryInterface;
use App\Scoping\Scopes\OrderByScope;
use App\Traits\paginationTrait;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderRepository implements OrderRepositoryInterface
{
    use paginationTrait;

    public function list($request)
    {
        $orders =  $request->user()->orders()->where('is_suspend',0)->withScopes($this->filterQuery());

        $count_orders = $orders->count();

        $this->pagination_query($request, $orders);

        return ['total_count' => $count_orders , 'data' =>  OrderIndex::collection($orders->get()) ] ;
    }

    protected function filterQuery(){
        return [
            'filter' => new OrderByScope()
        ];
    }

    public function show($order)
    {
       return new OrderShowResource($order);
    }

    public function makeOrder($request)
    {
        $order =   $request->user()->orders()->create($request->validated());

        if ( $request->order_details ){
            OrderDetails::insert($this->mappingOrderDetails($order,$request));
        }

        $email = Setting::getBody("email_bargain");

        if ($email){
            Mail::to($email)->send(new newOrder($request->user() , $order));
        }

//        event(new NewCategoryInOrder( $request,$order));

        return new OrderShowResource($order);
    }

    public function updateOrder($order , $request)
    {
        $data = array_merge($request->validated() ,[ 'created_at' => now(), 'is_accepted' => 0]);
        $order->update($data);

        if ( $request->order_details ){
            $order->order_details->each->delete();
            OrderDetails::insert($this->mappingOrderDetails($order,$request));
        }

//        event(new NewCategoryInOrder( $request,$order));
        event(new UpdateBiddingDuration( $request,$order));

        return new OrderShowResource($order);
    }

    public function mappingOrderDetails($order, $request){

        return collect($request->order_details)->map(function ($details) use ($order){
            return [
              'order_id' => $order->id,
              'label_id' => $details['label_id'],
              'label_value_id' => $details['label_value_id'],
            ];
        })->toArray();
    }


    public function update_bidding_duration($order , $request)
    {
        $order->update([ 'time_out' => 0 , 'status' => 'pending' ,'created_at' => Carbon::now(),
                        'end_bidding_duration' =>  Carbon::now()->addMinutes($order->bidding_duration)
                         ]);

        event(new NewCategoryInOrder( $request,$order));
        event(new UpdateBiddingDuration( $request,$order));

        return new OrderShowResource($order);
    }


    public function update_bidding_duration_golden_offer($order , $request)
    {

        $offer = $order->offers->where('is_pay',1)->first();

        $time_active_golden = Setting::getBody('time_active_golden_offer');

        $end_bidding_duration = Carbon::now()->addMinutes($time_active_golden);

        // $end_bidding_duration = Carbon::parse(now())->addMinutes($order->bidding_duration)

        $order->update([ 'offer_id' => $offer->id, 'user_offer_id' => $offer->user_id,
                            'time_out' => 0 , 'end_bidding_duration' =>  $end_bidding_duration]);

//        $offer->update(['status' => 'pending']);

        event(new OrderUpdateGoldenOffer($request,$order));

        return new OrderShowResource($order);
    }

    public function cancel_order($order , $request){

        $order->update(['reason_rejection_id' => $request->reason_rejection_id,'is_accepted' => 1 ,'status' => 'refuse']);

        if ($order->active_golden_offer == 1){

            $offer = $order->offers->where('is_pay',1)->first();

            $order->update([ 'offer_id' => $offer->id, 'user_offer_id' => $offer->user_id]);

            event(new finishOrder($order));

//            event(new SendNotificationToOffers($order,'orders','cancel_order_with_user_name'));
        }

        $order->offers_pending->each->update(['status' => 'refuse']);
//        $order->load('offers_pending');

        event(new RemoveOrderFromList("orders","remove_order",$order,'new_offer_or_edit_in_order'));

        return new OrderShowResource($order);
    }

    public function cancel_offer($request,$offer){

        $my_order = $offer->order;

        event(new OrderKeysDiff($offer->user,$my_order->user,$my_order,$request,'orders','offer_refused',$offer));

        $old_best_price = $my_order->min_price_offers();

        $offer->update(['time_out' => 1 ,'reason_rejection_id' => $request->reason_rejection_id ,'status' => 'cancel']);

        event(new BestOfferAddOrRemove("remove_offer", "remove_offer",$my_order,$request,'new_offer_or_edit_in_order',$old_best_price));

        return new OrderShowResource($my_order);
    }


    public function pay_by_wallet($request,$offer){

        // 1 -  Update Order To be Payed and  (user_offer_id) => user who choose
        //      Update Time Offer End Bidding Duration
        // 2 -  Wallet de increment (pull)
        // 3 -  Send Notification To User Who make This Offer
        // 4 -  Send Notification To other people in this Offer Now We Review Order Offers

        try {
            DB::beginTransaction();

            $user = $request->user();
            $order = $offer->order;

            $commission =  $order->active_golden_offer == 0 ? Setting::getBody('commission_order') : Setting::getBody('commission_golden_order') ;

            if ($order->active_golden_offer == 0){
                $order->update([ 'commission' => $commission ]);
            }else{
                $order->update([ 'golden_commission' => $commission ]);

                if ($order->old_offer_id == null){
                    $order->update(['old_offer_id' => $order->offer_id]);
                }
            }

            $order->update(['offer_id' => $offer->id  ,'is_pay' => 1 , 'user_offer_id' => $offer->user_id  ]);

            $check_to_pay_commission =  $order->active_golden_offer == 0 ? Setting::getBody('commission_offer') : Setting::getBody('commission_golden_offer') ;

            if ($check_to_pay_commission == 0){ // that`s mean want admin not pay the offer man by system

                $offer->update(['is_pay' => 1 , 'status' => 'finish' ]);

                if ($order->golden_offer == 0){
                    $order->update(['status' => 'finish']);
                }

                if ($order->golden_offer == 0 || $order->active_golden_offer == 1){

                    if($order->active_golden_offer != 1) {
                        event(new finishOrder($order,true,false));
                    }

                    event(new RefuseOtherOffersWithOutAcceptedOffer($user,$order,$offer,$request));
                    event(new OrderGoldenOfferNotActive($user,$order,$offer,$request));
                }else{

                    event(new OrderGoldenOfferActive($user,$order,$offer,$request));
                }

            }else{
                $offer->update([ 'status' => 'pending',
                    'end_bidding_duration' =>  Carbon::now()->addMinutes(Setting::getBody('waitting_to_accept_offer_minutes')) ]);
            }

            event(new UpdateWallet($user,$commission,$order));

            event(new SendAcceptedOffer($user,$order,$offer,$request));

//            event(new SendWaitingAcceptedOffer($user,$order,$offer,$request));

            event(new GlobalPublicNotification("GlobalPublic", "GlobalPublic","new_offer_or_edit_in_order","offer_accepted",$order,$offer->user));
            DB::commit();

            return ['status' => 200 , 'message' =>$check_to_pay_commission == 0 ? trans('order.accepted_offer_with_payed_message') : trans('order.accepted_offer_message')  , 'data' => new OrderShowResource($order)];

        }catch (\Exception $exception){
            DB::rollBack();
            return ['status' => 400 , 'message' => $exception->getMessage() ];
        }
    }


    public function golden_offer($request , $order){

        $time_active_golden = Setting::getBody('time_active_golden_offer');

        $order->update([
            'time_active_golden_offer' =>  Carbon::now()->addMinutes($time_active_golden),
            'end_bidding_duration' =>  Carbon::now()->addMinutes($time_active_golden),
            'time_complete_order' => null,
            'time_out'  => 0 , 'is_pay'    => 0 , 'active_golden_offer' => 1
        ]);

        //        event(new finishOrder($order));

        // Send Notification To Users Have Offer With Out Price
            event(new NewGoldenOffer($request,$order));

        return new OrderShowResource($order);
    }


    public function list_previous_orders($order){

      $query =   $order->offers->where('created_at','>=',$order->created_at)->whereIn('status',['pending','refuse']);

      if ($order->active_golden_offer == 1){
          $query =  $query->where('golden_offer',1);
      }

      $data = $query;

      return OrderOfferShowResource::collection($data);
    }


    public function choose_another_offer($request,$offer){

        // Master User Order Choose Another Offer
        $user = $request->user();

        $order = $offer->order;

        $order->update(['offer_id' => $offer->id  , 'user_offer_id' => $offer->user_id ]);

        $offer->update([ 'status' => 'pending', 'time_out' => 0,
            'end_bidding_duration' =>  Carbon::parse(now())->addMinutes(Setting::getBody('waitting_to_accept_offer_minutes')) ]);

        event(new SendAcceptedOffer($user,$order,$offer,$request));

//        event(new SendWaitingAcceptedOffer($user,$order,$offer,$request));

        return  new OrderShowResource($order);
    }


    public function finish_order($order ,$request){

        $offer = $order->my_offer;

        // Send Notification To Tell Other Order Is Finish
            event(new RefuseOtherOffersWithOutAcceptedOffer($request->user(),$order,$offer,$request));

        if ($order->active_golden_offer == 0){

            event(new OrderGoldenOfferNotActive($request->user(),$order,$offer,$request));

        }else{

            if ($order->old_offer_id == null){

                event(new OrderGoldenOfferNotActive($request->user(),$order,$offer,$request));

            }else{
                $order->update(['offer_id' => $order->old_offer_id]);
                $order->load('my_offer');
                $order->update(['user_offer_id' => $offer->user_id]);
                event(new OrderGoldenOfferNotActive($request->user(),$order,$offer,$request));
            }
        }

        // Send Notification To User And Offer_man Order Is Completed
            event(new finishOrder($order));

        return  new OrderShowResource($order);
    }

}

<?php

namespace App\Rules;

use App\Models\Order;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Auth;

class checkOrderChatting implements Rule
{

    protected $message;

    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = Auth::user();

        $order = Order::whereId($value)->whereUserId($user->id)->orWhere('doctor_id',$user->id)->first();

        if (!$order){
            return  $this->fail('تأكد من أنك الشخص المستحق لإرسال رسالة');
        }

//        if ($order->status != 'accepted'){
//            return  $this->fail('تأكد من أن الطلب جاري للمحادثة وشكرا..');
//        }

        return true;
    }

    protected function fail($message){

        $this->message = $message;

        return false;
    }

    public function message()
    {
        return $this->message;
    }
}

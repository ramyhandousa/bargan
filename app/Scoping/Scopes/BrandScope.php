<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class BrandScope implements Scope
{

    public function apply(Builder $builder , $value){

        return $builder->where('brand_id','LIKE',$value);
    }
}

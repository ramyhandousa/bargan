<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class NameScope implements Scope
{

    public function apply(Builder $builder , $value){

        return $builder->where(function($query) use ($value) {
                    $query->where('title', 'LIKE', '%'.$value.'%')
                        ->orWhere('description', 'LIKE', '%'.$value.'%')
                        ->orWhereHas('category',function ($category) use ($value){
                        $category->whereTranslationLike('name' ,"%{$value}%");
                    });
                });
    }
}

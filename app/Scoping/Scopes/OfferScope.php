<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;

class OfferScope implements Scope
{

    public function apply(Builder $builder , $value){

        if($value == 'finish'){

            return $builder->where('status','=','finish');

        }elseif($value == 'refuse_system'){

            return $builder->where('status','=','refuse_system');

        }elseif($value == 'refuse'){

            return $builder->where('status','=','refuse' );

        }elseif($value == 'cancel'){

            return $builder->where('status','=','cancel' );

        }else{
            return $builder->where('status','=','pending');
        }
    }
}

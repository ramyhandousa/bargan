<?php


namespace App\Scoping\Scopes;


use App\Scoping\InterfaceScope\Scope;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class OrderByScope implements Scope
{

    public function apply(Builder $builder, $value)
    {
        if($value == 'finish'){

            return $builder->where('status','=','finish');

        }elseif($value == 'refuse'){

            return $builder->whereIn('status',['refuse_system','refuse' ]);

        }else{
             return $builder->where('status','=','pending');
        }
    }
}

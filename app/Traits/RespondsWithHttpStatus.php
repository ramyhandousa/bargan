<?php


namespace App\Traits;


trait  RespondsWithHttpStatus
{
    protected function success($message, $data = [], $status = 200)
    {
        return response([
            'status' => 200,
            'message' => $message,
            'data' => $data,
        ], $status);
    }

    protected function successWithPagination($message,$total_count = 0, $data = [], $status = 200)
    {
        return response([
            'status' => 200,
            'message' => $message,
            'total_count' => $total_count,
            'data' => $data,
        ], $status);
    }

    protected function failure($error = [], $status = 400)
    {
        return response([
            'status' => $status,
            'errors' => (array) $error,
        ], 200);
    }

    protected function random_code_active(){
//        return "1111";
        return substr(rand(), 0, 4);
    }

}

-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 25, 2021 at 09:01 AM
-- Server version: 5.7.34
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bargainc_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `is_suspend` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `is_suspend`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 0, 0, 1, '2021-01-26 09:52:16', '2021-05-20 10:15:05'),
(4, 0, 0, 1, '2021-01-28 15:38:28', '2021-01-28 15:38:28'),
(6, 0, 0, 1, NULL, '2021-04-14 12:58:29'),
(7, 0, 0, 1, NULL, NULL),
(9, 0, 0, 0, '2021-03-22 19:32:33', '2021-03-22 19:32:33'),
(10, 0, 0, 0, '2021-03-22 20:07:46', '2021-03-22 20:07:46'),
(11, 0, 0, 0, '2021-03-22 20:14:04', '2021-03-22 20:14:04'),
(12, 0, 0, 0, '2021-03-22 20:27:20', '2021-03-22 20:27:20'),
(13, 0, 0, 0, '2021-03-29 14:54:39', '2021-03-29 14:54:39'),
(14, 0, 0, 0, '2021-03-30 14:56:28', '2021-03-30 14:56:28'),
(15, 0, 0, 0, '2021-03-30 14:56:58', '2021-03-30 14:56:58'),
(16, 0, 0, 0, '2021-03-30 14:58:25', '2021-03-30 14:58:25'),
(17, 0, 0, 0, '2021-03-30 14:58:51', '2021-03-30 14:58:51'),
(18, 0, 0, 0, '2021-03-30 14:59:32', '2021-03-30 14:59:32'),
(19, 0, 0, 0, '2021-03-30 15:02:34', '2021-03-30 15:02:34'),
(20, 0, 0, 0, '2021-03-31 22:52:29', '2021-03-31 22:52:29'),
(21, 0, 0, 0, '2021-04-14 10:58:34', '2021-04-14 10:58:34'),
(22, 0, 0, 0, '2021-04-14 11:33:21', '2021-04-14 11:33:21'),
(23, 0, 0, 1, '2021-04-14 11:36:14', '2021-04-14 11:36:14'),
(24, 0, 0, 1, '2021-05-07 20:14:00', '2021-05-07 20:14:00'),
(25, 0, 0, 1, '2021-05-07 20:15:08', '2021-05-07 20:15:08'),
(26, 0, 0, 1, '2021-05-07 20:17:38', '2021-05-07 20:17:38'),
(27, 0, 0, 1, '2021-05-07 20:19:11', '2021-05-07 20:19:11'),
(28, 0, 0, 1, '2021-05-07 20:19:39', '2021-05-07 20:19:39'),
(29, 0, 0, 1, '2021-05-07 20:20:45', '2021-05-07 20:20:45'),
(30, 0, 0, 1, '2021-05-07 20:21:06', '2021-05-07 20:21:06'),
(31, 0, 0, 1, '2021-05-10 00:41:54', '2021-05-10 00:41:54'),
(32, 0, 0, 0, '2021-05-19 21:13:16', '2021-05-19 21:13:16'),
(33, 0, 0, 0, '2021-05-19 21:13:57', '2021-05-19 21:13:57');

-- --------------------------------------------------------

--
-- Table structure for table `category_translations`
--

CREATE TABLE `category_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_title` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_title` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_translations`
--

INSERT INTO `category_translations` (`id`, `category_id`, `locale`, `name`, `product_title`, `service_title`, `description`) VALUES
(1, 1, 'ar', 'سيارات', 'مرسيدس ‏إس ‏500 ‏موديل ‏2021', 'توصيل مدارس شهري', 'جديد مرسيدس ‏إس ‏500 ‏موديل ‏2021، ‏ ‏لون ‏أسود ‏والداخليه ‏أحمر ‏'),
(2, 1, 'en', 'Cars', 'Mercedes ‎S500 ‏2021', 'Monthly ‎School ‎Transportation ‎', 'Mercedes ‎S500 ‏2021'),
(7, 4, 'ar', 'الأثاث', 'مثال: ‏مطبج ‏للبيع', 'عنوان الخدمة للآثاث', 'أضف ‏وصف ‏نصي ‏للأثاث'),
(8, 4, 'en', 'Furniture', 'exm: ‏furniture ‎for ‎sale', 'service ‎title ‎for ‎furniture', 'add ‎description ‎for ‎furniture'),
(13, 6, 'ar', 'اجهزة منزلية', NULL, NULL, NULL),
(14, 6, 'en', 'Appliances', NULL, NULL, NULL),
(15, 7, 'ar', 'اجهزة الكترونية', NULL, NULL, NULL),
(16, 7, 'en', 'Electronics ‎', NULL, NULL, NULL),
(17, 9, 'ar', 'ملابس', NULL, '', NULL),
(18, 10, 'ar', 'ملابس رياضية', NULL, '', NULL),
(19, 11, 'ar', 'ملابس', NULL, '', NULL),
(20, 12, 'ar', 'ملابس شاري', NULL, '', NULL),
(21, 13, 'ar', 'اتتنات', NULL, '', 'نمتنتنم'),
(22, 13, 'en', 'ttttttttt', NULL, '', 'xsklxsjkljskj'),
(23, 14, 'ar', 'ملابس', NULL, '', NULL),
(24, 15, 'ar', 'ملابس', NULL, '', NULL),
(25, 16, 'ar', 'ملابس', NULL, '', NULL),
(26, 17, 'ar', 'ملابس', NULL, '', NULL),
(27, 18, 'ar', 'ا', NULL, '', NULL),
(28, 19, 'ar', 'كما', NULL, '', NULL),
(29, 20, 'ar', 'طيارات', NULL, '', NULL),
(30, 21, 'ar', 'لابتوب', NULL, '', NULL),
(31, 22, 'ar', 'اجهزة كهربائية', NULL, '', 'انظمة كاميرات عالية الوضوح عدد ٤ مع سعة تخزين كبيرة'),
(32, 22, 'en', 'Electrical ‎Systems ‎', NULL, '', 'CCTV with high resolution and larg storage capacity \n4 pcs'),
(33, 23, 'ar', 'اجهزة كهربائية', NULL, NULL, NULL),
(34, 23, 'en', 'Electrical ‎Systems ‎', NULL, NULL, NULL),
(35, 24, 'ar', 'عقارات', NULL, NULL, NULL),
(36, 24, 'en', 'Real ‎estates', NULL, NULL, NULL),
(37, 25, 'ar', 'تصاميم ديكورات', NULL, NULL, NULL),
(38, 25, 'en', 'Decorative ‎design ‎', NULL, NULL, NULL),
(39, 26, 'ar', 'تصاميم ‏الكترونية', NULL, NULL, NULL),
(40, 26, 'en', 'Electronic ‎design ‎', NULL, NULL, NULL),
(41, 27, 'ar', 'خدمات تسويقية', NULL, NULL, NULL),
(42, 27, 'en', 'Marketing ‎Services ‎', NULL, NULL, NULL),
(43, 28, 'ar', 'خدمات عامة', NULL, NULL, NULL),
(44, 28, 'en', 'General ‎services', NULL, NULL, NULL),
(45, 29, 'ar', 'مواصلات', NULL, NULL, NULL),
(46, 29, 'en', 'Transportation ‎', NULL, NULL, NULL),
(47, 30, 'ar', 'خدمات منزلية', NULL, NULL, NULL),
(48, 30, 'en', 'Home ‎Services', NULL, NULL, NULL),
(49, 31, 'ar', 'خدمات خاصة', NULL, NULL, NULL),
(50, 31, 'en', 'Special ‎services ‎', NULL, NULL, NULL),
(51, 32, 'ar', 'طيارات', NULL, NULL, NULL),
(52, 33, 'ar', 'طيارات', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_users`
--

CREATE TABLE `category_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_users`
--

INSERT INTO `category_users` (`id`, `user_id`, `category_id`) VALUES
(5, 12, 1),
(7, 12, 4),
(144, 9, 4),
(149, 21, 1),
(150, 21, 4),
(205, 46, 1),
(206, 46, 6),
(207, 48, 1),
(240, 45, 1),
(261, 44, 7),
(262, 44, 6),
(263, 44, 1),
(264, 44, 4),
(265, 51, 1),
(266, 51, 6),
(267, 52, 6),
(268, 52, 1),
(288, 50, 1),
(289, 50, 6),
(290, 50, 4),
(291, 50, 7),
(298, 33, 1),
(299, 33, 6),
(307, 55, 1),
(308, 57, 1),
(313, 32, 1),
(314, 32, 6),
(315, 17, 1),
(325, 20, 1),
(326, 58, 1),
(333, 34, 7),
(334, 34, 1),
(335, 34, 4),
(336, 59, 23),
(341, 53, 1),
(342, 53, 7),
(343, 15, 4),
(344, 15, 1),
(345, 15, 6),
(346, 15, 7),
(347, 30, 1),
(348, 30, 4),
(349, 60, 23),
(350, 60, 1),
(351, 60, 6),
(352, 60, 4),
(353, 61, 1),
(354, 62, 32),
(355, 63, 33);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_ways`
--

CREATE TABLE `delivery_ways` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `is_suspend` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_ways`
--

INSERT INTO `delivery_ways` (`id`, `is_suspend`, `created_at`, `updated_at`) VALUES
(1, 0, '2021-01-26 10:05:22', '2021-01-26 10:05:22'),
(2, 0, '2021-01-26 10:06:15', '2021-01-26 10:06:15'),
(3, 0, '2021-01-26 10:06:23', '2021-04-14 13:07:56'),
(4, 1, '2021-03-29 19:59:16', '2021-04-14 13:07:25'),
(5, 1, '2021-03-29 19:59:25', '2021-04-01 22:15:35'),
(6, 1, '2021-03-30 11:25:06', '2021-04-01 22:15:24');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_way_translations`
--

CREATE TABLE `delivery_way_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `delivery_way_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_way_translations`
--

INSERT INTO `delivery_way_translations` (`id`, `delivery_way_id`, `locale`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'ar', 'شركات الشحن', '2021-01-26 10:06:02', '2021-01-26 10:06:02'),
(2, 1, 'en', 'Shipping ‎Companies', '2021-01-26 10:06:02', '2021-05-05 13:19:59'),
(3, 2, 'ar', 'وسيط', '2021-01-26 10:08:06', '2021-05-05 13:18:53'),
(4, 2, 'en', 'Third ‎Party', '2021-01-26 10:08:06', '2021-05-05 13:18:53'),
(5, 3, 'ar', 'مباشر ‏', '2021-01-26 10:08:06', '2021-05-05 13:20:19'),
(6, 3, 'en', 'Direct ‎', '2021-01-26 10:08:06', '2021-05-05 13:20:19'),
(7, 4, 'ar', 'casvasvsad', '2021-03-29 19:59:16', '2021-03-30 11:25:14'),
(8, 4, 'en', 'asdgsdhfghkgjhlgrfrg', '2021-03-29 19:59:16', '2021-03-30 11:25:14'),
(9, 5, 'ar', 'يدا بيد', '2021-03-29 19:59:25', '2021-03-29 19:59:25'),
(10, 5, 'en', 'hand ‎by ‎hand', '2021-03-29 19:59:25', '2021-03-29 19:59:25'),
(11, 6, 'ar', 'تيست', '2021-03-30 11:25:06', '2021-03-30 11:25:06'),
(12, 6, 'en', 'bdsbsf', '2021-03-30 11:25:06', '2021-03-30 11:25:06');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE `devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `device` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_type` varchar(55) CHARACTER SET utf8 DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `user_id`, `device`, `device_type`, `created_at`, `updated_at`) VALUES
(452, 50, '60076485-01a3-4780-8988-251ecf64627f', '', '2021-05-12 00:00:55', '2021-05-12 00:00:55'),
(455, 59, '73ca7312-772d-43e7-b23f-f09d4d865660', '', '2021-05-12 02:38:13', '2021-05-12 02:38:13'),
(456, 50, 'a20f909f-b234-4550-9761-7007d87b4c3f', '', '2021-05-12 03:22:10', '2021-05-12 03:22:10'),
(460, 30, 'dfac6997-4c97-47ed-b489-7d7e1979c5a9', '', '2021-05-12 13:54:43', '2021-05-12 14:37:21'),
(498, 63, '2a871df9-43ce-4340-b25d-5a106fd90d9d', '', '2021-05-19 21:14:01', '2021-05-19 21:14:01');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

CREATE TABLE `industries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `is_suspend` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`id`, `is_suspend`, `created_at`, `updated_at`) VALUES
(1, 0, '2021-01-26 10:10:48', '2021-01-26 10:10:48'),
(2, 0, '2021-01-26 10:10:48', '2021-01-26 10:10:48'),
(3, 0, '2021-01-26 10:11:00', '2021-01-26 10:11:00'),
(4, 0, '2021-03-30 00:02:15', '2021-05-05 13:21:36'),
(5, 0, '2021-05-05 13:21:32', '2021-05-05 13:21:32');

-- --------------------------------------------------------

--
-- Table structure for table `industry_translations`
--

CREATE TABLE `industry_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `industry_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industry_translations`
--

INSERT INTO `industry_translations` (`id`, `industry_id`, `locale`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'ar', 'سعودي ', '2021-01-26 10:13:10', '2021-01-26 10:13:10'),
(2, 1, 'en', 'Saudi', '2021-01-26 10:13:10', '2021-01-26 10:13:10'),
(3, 2, 'ar', 'صيني ', '2021-01-26 10:13:10', '2021-01-26 10:13:10'),
(4, 2, 'en', 'Chinese', '2021-01-26 10:13:10', '2021-01-26 10:13:10'),
(5, 3, 'ar', 'أمريكي', '2021-01-26 10:13:10', '2021-05-05 13:21:57'),
(6, 3, 'en', 'American', '2021-01-26 10:13:10', '2021-01-26 10:13:10'),
(7, 4, 'ar', 'أوروبي', '2021-03-30 00:02:15', '2021-05-05 13:21:05'),
(8, 4, 'en', 'European ‎', '2021-03-30 00:02:15', '2021-05-05 13:21:05'),
(9, 5, 'ar', 'أخرى', '2021-05-05 13:21:32', '2021-05-05 13:21:32'),
(10, 5, 'en', 'Other', '2021-05-05 13:21:32', '2021-05-05 13:21:32');

-- --------------------------------------------------------

--
-- Table structure for table `interactive_forms`
--

CREATE TABLE `interactive_forms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `reason_rejection_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `interactive_forms`
--

INSERT INTO `interactive_forms` (`id`, `type`, `user_id`, `order_id`, `reason_rejection_id`, `message`, `created_at`, `updated_at`) VALUES
(9, 'Interact_order', 33, 129, 5, NULL, '2021-05-07 04:12:48', '2021-05-07 04:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `labels`
--

CREATE TABLE `labels` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `definition_type` enum('order','offer') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'order',
  `order_type` enum('auction','tender') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tender',
  `type` enum('checkbox','select') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'select',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `labels`
--

INSERT INTO `labels` (`id`, `definition_type`, `order_type`, `type`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'order', 'tender', 'select', 1, '2021-02-14 12:39:23', '2021-04-26 02:20:12'),
(2, 'order', 'tender', 'checkbox', 1, '2021-02-14 13:39:39', '2021-04-26 02:20:13'),
(3, 'offer', 'tender', 'select', 1, '2021-02-14 21:08:22', '2021-04-26 02:20:16'),
(4, 'offer', 'tender', 'checkbox', 1, '2021-02-14 21:08:22', '2021-02-14 21:08:22'),
(5, 'offer', 'tender', 'select', 1, '2021-04-06 15:34:21', '2021-04-06 15:48:15'),
(6, 'offer', 'tender', 'select', 1, '2021-04-06 15:45:39', '2021-04-06 15:56:04'),
(7, 'order', 'tender', 'select', 1, '2021-04-06 16:04:50', '2021-04-26 02:20:15'),
(8, 'order', 'tender', 'select', 1, '2021-04-14 13:39:17', '2021-04-14 13:39:22'),
(9, 'order', 'auction', 'select', 1, '2021-04-14 13:40:12', '2021-04-14 13:40:58'),
(10, 'offer', 'auction', 'checkbox', 1, '2021-04-15 09:28:50', '2021-04-15 09:32:07'),
(11, 'order', 'auction', 'select', 1, '2021-04-15 09:51:27', '2021-04-15 09:51:37');

-- --------------------------------------------------------

--
-- Table structure for table `label_translations`
--

CREATE TABLE `label_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `label_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `label_translations`
--

INSERT INTO `label_translations` (`id`, `label_id`, `locale`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'ar', 'فاكهة', '2021-02-14 12:39:59', '2021-02-14 12:39:59'),
(2, 1, 'en', 'fruits', '2021-02-14 12:39:59', '2021-02-14 12:39:59'),
(3, 2, 'ar', 'منتجات غذائية', '2021-02-14 13:40:14', '2021-02-14 13:40:14'),
(4, 2, 'en', 'food products\r\n', '2021-02-14 13:40:14', '2021-02-14 13:40:14'),
(5, 3, 'ar', 'جوميا', '2021-02-14 21:11:57', '2021-04-06 15:56:23'),
(6, 3, 'en', 'Jumia', '2021-02-14 21:11:57', '2021-04-06 15:56:23'),
(7, 4, 'ar', 'سوق', '2021-02-14 21:12:32', '2021-02-14 21:12:32'),
(8, 4, 'en', 'souq', '2021-02-14 21:12:32', '2021-02-14 21:12:32'),
(9, 5, 'ar', 'مراجعة', '2021-04-06 15:34:21', '2021-04-06 15:34:21'),
(10, 5, 'en', 'eeeee', '2021-04-06 15:34:21', '2021-04-06 15:34:21'),
(11, 6, 'ar', 'تست', '2021-04-06 15:45:39', '2021-04-06 15:45:39'),
(12, 6, 'en', 'Test', '2021-04-06 15:45:39', '2021-04-06 15:45:39'),
(13, 7, 'ar', 'عروض', '2021-04-06 16:04:50', '2021-04-06 16:04:50'),
(14, 7, 'en', 'Edit', '2021-04-06 16:04:50', '2021-04-06 16:04:50'),
(15, 8, 'ar', 'عروض', '2021-04-14 13:39:17', '2021-04-14 13:39:17'),
(16, 8, 'en', 'Offers', '2021-04-14 13:39:17', '2021-04-14 13:39:17'),
(17, 9, 'ar', 'عروض', '2021-04-14 13:40:12', '2021-04-14 13:40:12'),
(18, 9, 'en', 'offers', '2021-04-14 13:40:12', '2021-04-14 13:40:12'),
(19, 10, 'ar', 'وقت', '2021-04-15 09:28:50', '2021-04-15 09:32:03'),
(20, 10, 'en', 'order ‎time', '2021-04-15 09:28:50', '2021-04-15 09:28:50'),
(21, 11, 'ar', 'وقت', '2021-04-15 09:51:27', '2021-04-15 09:51:27'),
(22, 11, 'en', 'ti,e', '2021-04-15 09:51:27', '2021-04-15 09:51:27');

-- --------------------------------------------------------

--
-- Table structure for table `label_values`
--

CREATE TABLE `label_values` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `label_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `label_values`
--

INSERT INTO `label_values` (`id`, `label_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2021-02-14 12:40:47', '2021-02-14 12:40:47'),
(2, 1, '2021-02-14 12:42:15', '2021-02-14 12:42:15'),
(3, 2, '2021-02-14 13:40:39', '2021-02-14 13:40:39'),
(4, 3, '2021-02-14 13:40:39', '2021-04-06 15:56:23'),
(5, 3, '2021-02-14 21:13:00', '2021-02-14 21:13:00'),
(6, 3, '2021-02-14 21:13:00', '2021-02-14 21:13:00'),
(7, 4, '2021-02-14 21:13:13', '2021-02-14 21:13:13'),
(8, 4, '2021-02-14 21:13:13', '2021-02-14 21:13:13'),
(9, 5, '2021-04-06 15:34:21', '2021-04-06 15:34:21'),
(10, 5, '2021-04-06 15:34:21', '2021-04-06 15:34:21'),
(11, 6, '2021-04-06 15:45:39', '2021-04-06 15:45:39'),
(12, 6, '2021-04-06 15:45:39', '2021-04-06 15:45:39'),
(13, 3, '2021-04-06 15:56:23', '2021-04-06 15:56:23'),
(14, 3, '2021-04-06 15:57:54', '2021-04-06 15:57:54'),
(16, 7, '2021-04-06 16:04:50', '2021-04-06 16:04:50'),
(17, 7, '2021-04-06 16:08:33', '2021-04-06 16:08:33'),
(18, 8, '2021-04-14 13:39:17', '2021-04-14 13:39:17'),
(19, 9, '2021-04-14 13:40:12', '2021-04-14 13:40:12'),
(20, 10, '2021-04-15 09:28:50', '2021-04-15 09:28:50'),
(21, 11, '2021-04-15 09:51:27', '2021-04-15 09:51:27');

-- --------------------------------------------------------

--
-- Table structure for table `label_value_translations`
--

CREATE TABLE `label_value_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `label_value_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `label_value_translations`
--

INSERT INTO `label_value_translations` (`id`, `label_value_id`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(9, 1, 'ar', 'بطيخ', '2021-02-14 12:42:44', '2021-02-14 12:42:44'),
(10, 1, 'en', 'watermelon', '2021-02-14 12:42:44', '2021-02-14 12:42:44'),
(11, 2, 'ar', 'موز ', '2021-02-14 12:42:44', '2021-02-14 12:42:44'),
(12, 2, 'en', 'banana', '2021-02-14 12:42:44', '2021-02-14 12:42:44'),
(13, 3, 'ar', 'لبن ', NULL, NULL),
(14, 3, 'en', 'Milk', '2021-02-14 13:41:43', '2021-02-14 13:41:43'),
(15, 4, 'ar', 'رامي', '2021-02-14 13:42:33', '2021-04-06 15:56:23'),
(16, 4, 'en', 'ramy', '2021-02-14 13:42:33', '2021-04-06 15:56:23'),
(17, 5, 'ar', 'تكيف ', '2021-02-14 21:15:22', '2021-02-14 21:15:22'),
(18, 5, 'en', 'adaptation', '2021-02-14 21:15:22', '2021-02-14 21:15:22'),
(19, 6, 'ar', 'مروحة ', '2021-02-14 21:15:22', '2021-02-14 21:15:22'),
(20, 6, 'en', 'fan', '2021-02-14 21:15:22', '2021-02-14 21:15:22'),
(21, 7, 'ar', 'كمامات طبية', '2021-02-14 21:18:12', '2021-02-14 21:18:12'),
(22, 7, 'en', 'Medical respirators', '2021-02-14 21:18:12', '2021-02-14 21:18:12'),
(23, 8, 'ar', 'سرير ', '2021-02-14 21:18:12', '2021-02-14 21:18:12'),
(24, 8, 'en', 'bed', '2021-02-14 21:18:12', '2021-02-14 21:18:12'),
(25, 9, 'ar', 'رامي', '2021-04-06 15:34:21', '2021-04-06 15:34:21'),
(26, 9, 'en', 'ramy', '2021-04-06 15:34:21', '2021-04-06 15:34:21'),
(27, 10, 'ar', 'حمبولة', '2021-04-06 15:34:21', '2021-04-06 15:34:21'),
(28, 10, 'en', '7ampola', '2021-04-06 15:34:21', '2021-04-06 15:34:21'),
(29, 11, 'ar', 'رامي', '2021-04-06 15:45:39', '2021-04-06 15:45:39'),
(30, 11, 'en', 'ramy', '2021-04-06 15:45:39', '2021-04-06 15:45:39'),
(31, 12, 'ar', 'حمبولة', '2021-04-06 15:45:39', '2021-04-06 15:45:39'),
(32, 12, 'en', '7ampola', '2021-04-06 15:45:39', '2021-04-06 15:45:39'),
(33, 13, 'ar', 'حمبولة', '2021-04-06 15:56:23', '2021-04-06 15:56:23'),
(34, 13, 'en', '7ampola', '2021-04-06 15:56:23', '2021-04-06 15:56:23'),
(35, 14, 'ar', 'حمبولة', '2021-04-06 15:57:54', '2021-04-06 15:57:54'),
(36, 14, 'en', '7ampola', '2021-04-06 15:57:54', '2021-04-06 15:57:54'),
(39, 16, 'ar', 'تست ٢', '2021-04-06 16:04:50', '2021-04-06 16:07:51'),
(40, 16, 'en', 'test ‎2', '2021-04-06 16:04:50', '2021-04-06 16:07:51'),
(41, 17, 'ar', 'تست ‏٣', '2021-04-06 16:08:33', '2021-04-06 16:08:33'),
(42, 17, 'en', 'test ‎3', '2021-04-06 16:08:33', '2021-04-06 16:08:33'),
(43, 18, 'ar', 'عروض', '2021-04-14 13:39:17', '2021-04-14 13:39:17'),
(44, 18, 'en', 'offers', '2021-04-14 13:39:17', '2021-04-14 13:39:17'),
(45, 19, 'ar', 'عروض', '2021-04-14 13:40:12', '2021-04-14 13:40:12'),
(46, 19, 'en', 'offers', '2021-04-14 13:40:12', '2021-04-14 13:40:12'),
(47, 20, 'ar', 'الوقت', '2021-04-15 09:28:50', '2021-04-15 09:28:50'),
(48, 20, 'en', 'time', '2021-04-15 09:28:50', '2021-04-15 09:28:50'),
(49, 21, 'ar', 'ققققف', '2021-04-15 09:51:27', '2021-04-15 09:51:27'),
(50, 21, 'en', 'tttt', '2021-04-15 09:51:27', '2021-04-15 09:51:27');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(137, '2014_10_12_000000_create_users_table', 1),
(138, '2014_10_12_100000_create_password_resets_table', 1),
(139, '2019_08_19_000000_create_failed_jobs_table', 1),
(140, '2021_01_19_154045_create_categories_table', 1),
(141, '2021_01_19_154107_create_category_translations_table', 1),
(142, '2021_01_20_120424_create_industries_table', 1),
(143, '2021_01_20_120443_create_industry_translations_table', 1),
(144, '2021_01_20_120520_create_warranties_table', 1),
(145, '2021_01_20_120537_create_warranty_translations_table', 1),
(146, '2021_01_20_121014_create_delivery_ways_table', 1),
(147, '2021_01_20_121036_create_delivery_way_translations_table', 1),
(148, '2021_01_20_121440_create_payment_methods_table', 1),
(149, '2021_01_20_121459_create_payment_method_translations_table', 1),
(150, '2021_01_20_121649_create_labels_table', 1),
(151, '2021_01_20_121708_create_label_translations_table', 1),
(152, '2021_01_20_121722_create_label_values_table', 1),
(153, '2021_01_20_121734_create_label_value_translations_table', 1),
(154, '2021_01_20_122222_create_reason_rejections_table', 1),
(155, '2021_01_20_122223_create_reason_rejection_translations_table', 1),
(156, '2021_01_20_122227_create_orders_table', 1),
(157, '2021_01_20_122320_create_order_details_table', 1),
(158, '2021_01_20_122341_create_offers_table', 1),
(159, '2021_01_20_155129_create_transactions_table', 1),
(160, '2021_01_20_163704_create_category_users_table', 1),
(161, '2021_01_20_163734_create_rates_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `topic` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(11) UNSIGNED DEFAULT NULL,
  `sender_id` bigint(11) UNSIGNED DEFAULT NULL,
  `order_id` bigint(11) UNSIGNED DEFAULT NULL,
  `offer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `title` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `translation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci,
  `body_arguments` text COLLATE utf8mb4_unicode_ci,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `is_read` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `topic`, `user_id`, `sender_id`, `order_id`, `offer_id`, `title`, `translation`, `body`, `body_arguments`, `type`, `is_read`, `created_at`, `updated_at`) VALUES
(500, NULL, 30, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"\\u062a\\u0645\",\"message\":\"\\u0627\\u0633\\u062a\\u0641\\u0633\\u0627\\u0631\"}', 0, 1, '2021-05-07 02:55:49', '2021-05-07 02:55:56'),
(524, NULL, 30, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"hhh\",\"message\":\"hhh\"}', 0, 1, '2021-05-07 02:57:57', '2021-05-07 02:58:09'),
(580, NULL, 50, NULL, 127, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"\\u0639\\u0631\\u0628\\u064a\\u0629 4\\\"\\u2708\\ufe0f\"}', 0, 1, '2021-05-07 03:32:27', '2021-05-07 03:32:29'),
(581, NULL, 12, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(582, NULL, 15, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-07 03:32:27', '2021-05-07 03:37:00'),
(583, NULL, 17, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(584, NULL, 20, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(585, NULL, 21, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(586, NULL, 30, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-07 03:32:27', '2021-05-07 04:38:57'),
(587, NULL, 32, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(588, NULL, 33, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-07 03:32:27', '2021-05-07 03:33:41'),
(589, NULL, 34, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(590, NULL, 44, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(591, NULL, 45, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-07 03:32:27', '2021-05-10 20:22:20'),
(592, NULL, 46, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(593, NULL, 48, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(594, NULL, 51, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(595, NULL, 52, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(596, NULL, 53, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-07 03:32:27', '2021-05-19 20:42:52'),
(597, NULL, 55, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(598, NULL, 57, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(599, NULL, 58, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(600, NULL, 60, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:32:27', NULL),
(605, NULL, 50, NULL, 127, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"\\u0639\\u0631\\u0628\\u064a\\u0629 4\\\"\\u2708\\ufe0f\"}', 0, 1, '2021-05-07 03:37:10', '2021-05-07 03:37:39'),
(606, NULL, 12, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(607, NULL, 15, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-07 03:37:10', '2021-05-07 04:13:34'),
(608, NULL, 17, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(609, NULL, 20, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(610, NULL, 21, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(611, NULL, 30, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-07 03:37:10', '2021-05-07 04:38:57'),
(612, NULL, 32, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(613, NULL, 33, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-07 03:37:10', '2021-05-07 03:37:58'),
(614, NULL, 34, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(615, NULL, 44, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(616, NULL, 45, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-07 03:37:10', '2021-05-10 20:22:20'),
(617, NULL, 46, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(618, NULL, 48, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(619, NULL, 51, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(620, NULL, 52, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(621, NULL, 53, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-07 03:37:10', '2021-05-19 20:42:52'),
(622, NULL, 55, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(623, NULL, 57, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(624, NULL, 58, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(625, NULL, 60, 50, 127, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-07 03:37:10', NULL),
(629, NULL, 50, 15, 127, 213, 'offers', 'add_new_offer', 'محمد ‏داوود', '{\"user_name\":\"7445929\",\"order_name\":\"\\u0639\\u0631\\u0628\\u064a\\u0629 4\\\"\\u2708\\ufe0f\"}', 6, 1, '2021-05-07 03:43:04', '2021-05-07 03:43:05'),
(630, NULL, 50, 33, 127, 214, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"3361779\",\"order_name\":\"\\u0639\\u0631\\u0628\\u064a\\u0629 4\\\"\\u2708\\ufe0f\"}', 6, 1, '2021-05-07 03:43:12', '2021-05-07 03:43:55'),
(631, NULL, 15, 50, 127, 213, 'accepted_offer', 'owner_accepted_offer', 'ابو عبدالعزيز', '{\"order_name\":\"\\u0639\\u0631\\u0628\\u064a\\u0629 4\\\"\\u2708\\ufe0f\"}', 3, 1, '2021-05-07 03:43:53', '2021-05-07 04:13:34'),
(632, NULL, 50, 15, 127, 213, 'orders', 'offer_payed_and_waiting_golden_activation', 'محمد ‏داوود', '{\"order_name\":\"\\u0639\\u0631\\u0628\\u064a\\u0629 4\\\"\\u2708\\ufe0f\",\"user_name\":1945952}', 12, 1, '2021-05-07 03:44:13', '2021-05-07 03:45:20'),
(633, NULL, 15, NULL, 128, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"Tender\"}', 0, 1, '2021-05-07 03:45:08', '2021-05-07 04:13:34'),
(634, NULL, 9, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:45:08', NULL),
(635, NULL, 12, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:45:08', NULL),
(636, NULL, 21, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:45:08', NULL),
(637, NULL, 30, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 03:45:08', '2021-05-07 04:38:57'),
(638, NULL, 34, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:45:08', NULL),
(639, NULL, 44, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:45:08', NULL),
(640, NULL, 50, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 03:45:08', '2021-05-07 03:45:20'),
(641, NULL, 60, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:45:08', NULL),
(642, NULL, 15, 33, 128, 215, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"1990046\",\"order_name\":\"Tender\"}', 6, 1, '2021-05-07 03:45:19', '2021-05-07 04:13:34'),
(643, NULL, 15, 50, 128, 216, 'offers', 'add_new_offer', 'ابو عبدالعزيز', '{\"user_name\":\"9736662\",\"order_name\":\"Tender\"}', 6, 1, '2021-05-07 03:45:35', '2021-05-07 04:13:34'),
(644, NULL, 50, 15, 128, 216, 'accepted_offer', 'owner_accepted_offer', 'محمد ‏داوود', '{\"order_name\":\"Tender\"}', 3, 1, '2021-05-07 03:45:50', '2021-05-07 03:45:51'),
(645, NULL, 15, NULL, 128, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"Tender\"}', 0, 1, '2021-05-07 03:46:54', '2021-05-07 04:13:34'),
(646, NULL, 9, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:46:54', NULL),
(647, NULL, 12, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:46:54', NULL),
(648, NULL, 21, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:46:54', NULL),
(649, NULL, 30, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 03:46:54', '2021-05-07 04:38:57'),
(650, NULL, 34, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:46:54', NULL),
(651, NULL, 44, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:46:54', NULL),
(652, NULL, 50, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 03:46:54', '2021-05-07 03:47:44'),
(653, NULL, 60, 15, 128, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:46:54', NULL),
(654, NULL, 15, 33, 128, 217, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"1730538\",\"order_name\":\"Tender\"}', 6, 1, '2021-05-07 03:47:33', '2021-05-07 04:13:34'),
(655, NULL, 15, 50, 128, 218, 'offers', 'add_new_offer', 'ابو عبدالعزيز', '{\"user_name\":\"6064829\",\"order_name\":\"Tender\"}', 6, 1, '2021-05-07 03:47:55', '2021-05-07 04:13:34'),
(656, NULL, 15, 50, 128, 218, 'offers', 'edit_new_offer', 'ابو عبدالعزيز', '{\"user_name\":6064829,\"order_name\":\"Tender\"}', 7, 1, '2021-05-07 03:48:20', '2021-05-07 04:13:34'),
(657, NULL, 15, NULL, 129, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"Tender \\u200e2\"}', 0, 1, '2021-05-07 03:52:27', '2021-05-07 04:13:34'),
(658, NULL, 9, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:52:27', NULL),
(659, NULL, 12, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:52:27', NULL),
(660, NULL, 21, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:52:27', NULL),
(661, NULL, 30, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 03:52:27', '2021-05-07 04:38:57'),
(662, NULL, 34, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:52:27', NULL),
(663, NULL, 44, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:52:27', NULL),
(664, NULL, 50, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 03:52:27', '2021-05-07 03:53:37'),
(665, NULL, 60, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:52:27', NULL),
(666, NULL, 15, 33, 129, 219, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"2038349\",\"order_name\":\"Tender \\u200e2\"}', 6, 1, '2021-05-07 03:53:04', '2021-05-07 04:13:34'),
(667, NULL, 15, 50, 129, 220, 'offers', 'add_new_offer', 'ابو عبدالعزيز', '{\"user_name\":\"1441745\",\"order_name\":\"Tender \\u200e2\"}', 6, 1, '2021-05-07 03:53:50', '2021-05-07 04:13:34'),
(668, NULL, 50, 15, 129, 220, 'accepted_offer', 'owner_accepted_offer', 'محمد ‏داوود', '{\"order_name\":\"Tender \\u200e2\"}', 3, 1, '2021-05-07 03:54:04', '2021-05-07 03:54:05'),
(669, NULL, 15, NULL, 129, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"Tender \\u200e2\"}', 0, 1, '2021-05-07 03:54:33', '2021-05-07 04:13:34'),
(670, NULL, 9, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:54:33', NULL),
(671, NULL, 12, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:54:33', NULL),
(672, NULL, 21, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:54:33', NULL),
(673, NULL, 30, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 03:54:33', '2021-05-07 04:38:57'),
(674, NULL, 34, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:54:33', NULL),
(675, NULL, 44, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:54:33', NULL),
(676, NULL, 50, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 03:54:33', '2021-05-07 03:54:51'),
(677, NULL, 60, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 03:54:33', NULL),
(678, NULL, 15, 33, 129, 221, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"2123267\",\"order_name\":\"Tender \\u200e2\"}', 6, 1, '2021-05-07 03:54:50', '2021-05-07 04:13:34'),
(679, NULL, 15, 50, 129, 222, 'offers', 'add_new_offer', 'ابو عبدالعزيز', '{\"user_name\":\"1185679\",\"order_name\":\"Tender \\u200e2\"}', 6, 1, '2021-05-07 03:55:16', '2021-05-07 04:13:34'),
(680, NULL, 50, 15, 129, 222, 'accepted_offer', 'owner_accepted_offer', 'محمد ‏داوود', '{\"order_name\":\"Tender \\u200e2\"}', 3, 1, '2021-05-07 03:56:01', '2021-05-07 03:57:10'),
(681, NULL, 15, 50, 129, 222, 'orders', 'offer_payed_and_waiting_golden_activation', 'ابو عبدالعزيز', '{\"order_name\":\"Tender \\u200e2\",\"user_name\":8799824}', 12, 1, '2021-05-07 03:56:30', '2021-05-07 04:13:34'),
(682, NULL, 33, 15, 129, NULL, 'golden_offer', 'golden_offer_activated', 'golden_offer_activated', '{\"order_name\":\"Tender \\u200e2\",\"order_price\":\"7500.00\"}', 10, 1, '2021-05-07 03:56:42', '2021-05-07 03:57:04'),
(683, NULL, 50, 15, 129, NULL, 'golden_offer', 'golden_offer_activated', 'golden_offer_activated', '{\"order_name\":\"Tender \\u200e2\",\"order_price\":\"7500.00\"}', 10, 1, '2021-05-07 03:56:42', '2021-05-07 03:57:10'),
(684, NULL, 15, 33, 129, 223, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"1348240\",\"order_name\":\"Tender \\u200e2\"}', 6, 1, '2021-05-07 04:13:04', '2021-05-07 04:13:34'),
(685, NULL, 15, 50, 129, 224, 'offers', 'add_new_offer', 'ابو عبدالعزيز', '{\"user_name\":\"8797669\",\"order_name\":\"Tender \\u200e2\"}', 6, 1, '2021-05-07 04:14:03', '2021-05-07 04:33:08'),
(686, NULL, 33, 15, 129, 223, 'accepted_offer', 'owner_accepted_offer', 'محمد ‏داوود', '{\"order_name\":\"Tender \\u200e2\"}', 3, 1, '2021-05-07 04:14:51', '2021-05-07 04:16:53'),
(687, NULL, 9, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:15:56', NULL),
(688, NULL, 12, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:15:56', NULL),
(689, NULL, 21, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:15:56', NULL),
(690, NULL, 30, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 04:15:56', '2021-05-07 04:38:57'),
(691, NULL, 34, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:15:56', NULL),
(692, NULL, 44, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:15:56', NULL),
(693, NULL, 50, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 04:15:56', '2021-05-07 04:38:56'),
(694, NULL, 60, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:15:56', NULL),
(695, NULL, 33, 15, 129, NULL, 'golden_offer', 'golden_offer_updated', 'golden_offer_updated', '{\"order_name\":\"Tender \\u200e2\"}', 10, 1, '2021-05-07 04:15:56', '2021-05-07 04:16:53'),
(696, NULL, 50, 15, 129, NULL, 'golden_offer', 'golden_offer_updated', 'golden_offer_updated', '{\"order_name\":\"Tender \\u200e2\"}', 10, 1, '2021-05-07 04:15:56', '2021-05-07 04:38:56'),
(697, NULL, 50, 15, 129, 224, 'accepted_offer', 'owner_accepted_offer', 'محمد ‏داوود', '{\"order_name\":\"Tender \\u200e2\"}', 3, 1, '2021-05-07 04:37:12', '2021-05-07 04:38:56'),
(698, NULL, 15, 33, 129, 225, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"2094744\",\"order_name\":\"Tender \\u200e2\"}', 6, 1, '2021-05-07 04:45:43', '2021-05-07 04:47:23'),
(699, NULL, 15, 33, 129, 226, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"5541865\",\"order_name\":\"Tender \\u200e2\"}', 6, 1, '2021-05-07 04:46:15', '2021-05-07 04:47:23'),
(700, NULL, 15, 50, 129, 227, 'offers', 'add_new_offer', 'ابو عبدالعزيز', '{\"user_name\":\"1675677\",\"order_name\":\"Tender \\u200e2\"}', 6, 1, '2021-05-07 04:47:06', '2021-05-07 04:47:23'),
(701, NULL, 33, 15, 129, 226, 'accepted_offer', 'owner_accepted_offer', 'محمد ‏داوود', '{\"order_name\":\"Tender \\u200e2\"}', 3, 1, '2021-05-07 04:47:35', '2021-05-07 04:48:29'),
(702, NULL, 9, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:48:03', NULL),
(703, NULL, 12, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:48:03', NULL),
(704, NULL, 21, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:48:03', NULL),
(705, NULL, 30, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 04:48:03', '2021-05-07 21:37:12'),
(706, NULL, 34, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:48:03', NULL),
(707, NULL, 44, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:48:03', NULL),
(708, NULL, 50, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 04:48:03', '2021-05-07 04:52:48'),
(709, NULL, 60, 15, 129, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:48:03', NULL),
(710, NULL, 33, 15, 129, NULL, 'golden_offer', 'golden_offer_updated', 'golden_offer_updated', '{\"order_name\":\"Tender \\u200e2\"}', 10, 1, '2021-05-07 04:48:03', '2021-05-07 04:48:29'),
(711, NULL, 50, 15, 129, NULL, 'golden_offer', 'golden_offer_updated', 'golden_offer_updated', '{\"order_name\":\"Tender \\u200e2\"}', 10, 1, '2021-05-07 04:48:03', '2021-05-07 04:52:48'),
(712, NULL, 15, 33, 129, 228, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"7920855\",\"order_name\":\"Tender \\u200e2\"}', 6, 1, '2021-05-07 04:48:43', '2021-05-07 04:49:00'),
(713, NULL, 33, 15, 129, 228, 'accepted_offer', 'owner_accepted_offer', 'محمد ‏داوود', '{\"order_name\":\"Tender \\u200e2\"}', 3, 1, '2021-05-07 04:51:20', '2021-05-07 20:26:05'),
(714, NULL, 15, 33, 129, 228, 'orders', 'offer_payed_and_finish_order', 'talal ‎abdulaziz', '{\"order_name\":\"Tender \\u200e2\",\"user_name\":8799824}', 8, 1, '2021-05-07 04:51:57', '2021-05-07 04:54:01'),
(715, NULL, 50, 33, 129, 228, 'offers', 'offer_refused', 'talal ‎abdulaziz', '{\"order_name\":\"Tender \\u200e2\"}', 4, 1, '2021-05-07 04:51:57', '2021-05-07 04:52:48'),
(716, NULL, 33, 15, 129, 228, 'rating', 'rate_order_offer_man', 'rate_order_offer_man', '{\"order_name\":\"Tender \\u200e2\",\"user_name\":\"talal \\u200eabdulaziz\"}', 14, 1, '2021-05-07 04:54:05', '2021-05-07 20:26:05'),
(717, NULL, 15, 33, 129, 228, 'rating', 'rate_order_user', 'rate_order_user', '{\"order_name\":\"Tender \\u200e2\",\"user_name\":\"\\u0645\\u062d\\u0645\\u062f \\u200f\\u062f\\u0627\\u0648\\u0648\\u062f\"}', 15, 1, '2021-05-07 04:54:05', '2021-05-07 04:56:42'),
(718, NULL, 15, 33, 129, 228, 'rating', 'order_rate_by_offer_man', 'order_rate_by_offer_man', '{\"order_name\":\"Tender \\u200e2\",\"user_name\":8799824}', 16, 1, '2021-05-07 04:54:18', '2021-05-07 04:56:42'),
(719, NULL, 33, 15, 129, 228, 'rating', 'order_rate_by_user', 'order_rate_by_user', '{\"order_name\":\"Tender \\u200e2\",\"user_name\":8799824}', 17, 1, '2021-05-07 04:54:27', '2021-05-07 20:26:05'),
(720, NULL, 15, NULL, 130, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"test\"}', 0, 1, '2021-05-07 04:55:11', '2021-05-07 04:56:42'),
(721, NULL, 9, 15, 130, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:55:11', NULL),
(722, NULL, 12, 15, 130, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:55:11', NULL),
(723, NULL, 21, 15, 130, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:55:11', NULL),
(724, NULL, 30, 15, 130, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 04:55:11', '2021-05-07 21:37:12'),
(725, NULL, 34, 15, 130, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:55:11', NULL),
(726, NULL, 44, 15, 130, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:55:11', NULL),
(727, NULL, 50, 15, 130, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-07 04:55:11', '2021-05-07 04:55:34'),
(728, NULL, 60, 15, 130, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-07 04:55:11', NULL),
(729, NULL, 15, 33, 130, 229, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"1988387\",\"order_name\":\"test\"}', 6, 1, '2021-05-07 04:55:33', '2021-05-07 04:56:42'),
(730, NULL, 15, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"test\",\"message\":\"test\"}', 0, 1, '2021-05-07 05:02:03', '2021-05-10 00:11:06'),
(731, 'system', 15, NULL, 129, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"Tender \\u200e2\"}', 9, 1, '2021-05-07 05:49:04', '2021-05-10 00:11:06'),
(732, NULL, 33, NULL, 129, NULL, 'orders', 'order_refund_requested_refuse', 'order_refund_requested_refuse', '{\"order_name\":\"Tender \\u200e2\",\"message_ar\":\"\\u0628\",\"message_en\":\"g\"}', 0, 1, '2021-05-07 20:27:52', '2021-05-07 20:29:15'),
(733, NULL, 33, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"jkk\",\"message\":\"\\u200fbb\"}', 0, 1, '2021-05-07 20:28:38', '2021-05-07 20:29:15'),
(734, 'system', 50, NULL, 127, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"\\u0639\\u0631\\u0628\\u064a\\u0629 4\\\"\\u2708\\ufe0f\"}', 9, 1, '2021-05-08 03:38:04', '2021-05-10 00:04:30'),
(735, NULL, 15, 50, 130, 230, 'offers', 'add_new_offer', 'ابو عبدالعزيز', '{\"user_name\":\"7102371\",\"order_name\":\"test\"}', 6, 1, '2021-05-10 00:10:38', '2021-05-10 00:11:06'),
(736, NULL, 50, NULL, 127, NULL, 'orders', 'order_refuse', 'order_refuse', '{\"order_name\":\"\\u0639\\u0631\\u0628\\u064a\\u0629 4\\\"\\u2708\\ufe0f\",\"message_ar\":\"\\u062a\\u0633\\u062a\",\"message_en\":\"test\"}', 0, 1, '2021-05-10 03:27:50', '2021-05-10 04:00:58'),
(737, NULL, 15, NULL, 130, NULL, 'orders', 'order_refuse', 'order_refuse', '{\"order_name\":\"test\",\"message_ar\":\"\\u062a\\u0633\\u062a\",\"message_en\":\"test\"}', 0, 1, '2021-05-10 03:28:23', '2021-05-10 03:31:09'),
(738, NULL, 50, NULL, 131, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"testing \\u200e6 \\u200fAm \\u200e\"}', 0, 1, '2021-05-10 04:03:35', '2021-05-10 04:03:36'),
(739, NULL, 12, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(740, NULL, 15, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:03:35', '2021-05-10 04:04:02'),
(741, NULL, 17, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(742, NULL, 20, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(743, NULL, 21, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(744, NULL, 30, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:03:35', '2021-05-10 04:08:40'),
(745, NULL, 32, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(746, NULL, 33, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:03:35', '2021-05-10 12:09:04'),
(747, NULL, 34, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(748, NULL, 44, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(749, NULL, 45, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:03:35', '2021-05-10 20:22:20'),
(750, NULL, 46, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(751, NULL, 48, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(752, NULL, 51, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(753, NULL, 52, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(754, NULL, 53, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:03:35', '2021-05-19 20:42:52'),
(755, NULL, 55, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(756, NULL, 57, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(757, NULL, 58, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(758, NULL, 60, 50, 131, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:03:35', NULL),
(759, NULL, 50, 15, 131, 231, 'offers', 'add_new_offer', 'محمد ‏داوود', '{\"user_name\":\"8739091\",\"order_name\":\"testing \\u200e6 \\u200fAm \\u200e\"}', 6, 1, '2021-05-10 04:03:45', '2021-05-10 04:03:46'),
(760, NULL, 50, 15, 131, NULL, 'orders', 'cancel_offer_with_user_name', NULL, '{\"order_name\":\"testing \\u200e6 \\u200fAm \\u200e\",\"user_name\":\"\\u0645\\u062d\\u0645\\u062f \\u200f\\u062f\\u0627\\u0648\\u0648\\u062f\"}', 0, 1, '2021-05-10 04:08:38', '2021-05-10 04:08:40'),
(761, NULL, 50, 15, 131, 232, 'offers', 'add_new_offer', 'محمد ‏داوود', '{\"user_name\":\"1543382\",\"order_name\":\"testing \\u200e6 \\u200fAm \\u200e\"}', 6, 1, '2021-05-10 04:09:15', '2021-05-10 04:09:21'),
(762, NULL, 15, 50, 131, NULL, 'orders', 'cancel_order_with_user_name', NULL, '{\"order_name\":\"testing \\u200e6 \\u200fAm \\u200e\",\"user_name\":1549589}', 0, 1, '2021-05-10 04:18:32', '2021-05-10 04:18:45'),
(763, NULL, 15, 50, 131, NULL, 'orders', 'remove_order', NULL, '{\"order_name\":\"testing \\u200e6 \\u200fAm \\u200e\"}', 11, 1, '2021-05-10 04:18:32', '2021-05-10 04:18:45'),
(764, NULL, 50, NULL, 132, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"\\u062e\\u062f\\u0645\\u0629 \\u0645\\u062a\\u0627\\u062d\\u0629 \\u0644\\u0645\\u062f\\u0629 \\u0633\\u0627\\u0639\\u0629\"}', 0, 1, '2021-05-10 04:21:12', '2021-05-10 04:21:19'),
(765, NULL, 12, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(766, NULL, 15, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:21:12', '2021-05-10 04:24:15'),
(767, NULL, 17, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(768, NULL, 20, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(769, NULL, 21, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(770, NULL, 30, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:21:12', '2021-05-10 15:59:51'),
(771, NULL, 32, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(772, NULL, 33, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:21:12', '2021-05-10 12:09:04'),
(773, NULL, 34, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(774, NULL, 44, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(775, NULL, 45, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:21:12', '2021-05-10 20:22:20'),
(776, NULL, 46, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(777, NULL, 48, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(778, NULL, 51, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(779, NULL, 52, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(780, NULL, 53, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:21:12', '2021-05-19 20:42:52'),
(781, NULL, 55, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(782, NULL, 57, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(783, NULL, 58, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(784, NULL, 60, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:21:12', NULL),
(785, NULL, 50, 15, 132, 233, 'offers', 'add_new_offer', 'محمد ‏داوود', '{\"user_name\":\"1225521\",\"order_name\":\"\\u062e\\u062f\\u0645\\u0629 \\u0645\\u062a\\u0627\\u062d\\u0629 \\u0644\\u0645\\u062f\\u0629 \\u0633\\u0627\\u0639\\u0629\"}', 6, 1, '2021-05-10 04:21:35', '2021-05-10 04:21:42'),
(786, 'system', 50, NULL, 132, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"\\u062e\\u062f\\u0645\\u0629 \\u0645\\u062a\\u0627\\u062d\\u0629 \\u0644\\u0645\\u062f\\u0629 \\u0633\\u0627\\u0639\\u0629\"}', 9, 1, '2021-05-10 04:22:04', '2021-05-10 04:22:13'),
(787, NULL, 15, 50, 132, NULL, 'orders', 'update_bidding_duration', NULL, '{\"order_name\":\"\\u062e\\u062f\\u0645\\u0629 \\u0645\\u062a\\u0627\\u062d\\u0629 \\u0644\\u0645\\u062f\\u0629 \\u0633\\u0627\\u0639\\u0629\",\"user_name\":1869919}', 0, 1, '2021-05-10 04:23:52', '2021-05-10 04:24:15'),
(788, NULL, 50, NULL, 132, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"\\u062e\\u062f\\u0645\\u0629 \\u0645\\u062a\\u0627\\u062d\\u0629 \\u0644\\u0645\\u062f\\u0629 \\u0633\\u0627\\u0639\\u0629\"}', 0, 1, '2021-05-10 04:28:48', '2021-05-10 21:30:23'),
(789, NULL, 12, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(790, NULL, 15, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:28:48', '2021-05-10 21:28:51'),
(791, NULL, 17, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(792, NULL, 20, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(793, NULL, 21, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(794, NULL, 30, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:28:48', '2021-05-10 15:59:51'),
(795, NULL, 32, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(796, NULL, 33, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:28:48', '2021-05-10 12:09:04'),
(797, NULL, 34, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(798, NULL, 44, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL);
INSERT INTO `notifications` (`id`, `topic`, `user_id`, `sender_id`, `order_id`, `offer_id`, `title`, `translation`, `body`, `body_arguments`, `type`, `is_read`, `created_at`, `updated_at`) VALUES
(799, NULL, 45, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:28:48', '2021-05-10 20:22:20'),
(800, NULL, 46, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(801, NULL, 48, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(802, NULL, 51, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(803, NULL, 52, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(804, NULL, 53, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 04:28:48', '2021-05-19 20:42:52'),
(805, NULL, 55, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(806, NULL, 57, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(807, NULL, 58, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(808, NULL, 60, 50, 132, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 04:28:48', NULL),
(809, NULL, 33, NULL, 129, NULL, 'orders', 'order_refund_requested_accepted', 'order_refund_requested_accepted', '{\"order_name\":\"Tender \\u200e2\",\"message_ar\":null,\"message_en\":null}', 0, 1, '2021-05-10 16:06:57', '2021-05-10 16:53:12'),
(810, 'system', 15, NULL, 129, 228, 'waiting_duration', 'waiting_duration_ended', 'waiting_duration_ended', '{\"order_name\":\"Tender \\u200e2\",\"user_name\":8799824}', 13, 1, '2021-05-10 16:07:04', '2021-05-10 21:28:51'),
(811, NULL, 33, NULL, 133, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"test1\"}', 0, 1, '2021-05-10 20:20:01', '2021-05-10 20:25:38'),
(812, NULL, 12, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(813, NULL, 15, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 20:20:01', '2021-05-10 21:28:51'),
(814, NULL, 17, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(815, NULL, 20, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(816, NULL, 21, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(817, NULL, 30, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 20:20:01', '2021-05-10 21:28:35'),
(818, NULL, 32, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(819, NULL, 34, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(820, NULL, 44, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(821, NULL, 45, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 20:20:01', '2021-05-10 20:22:20'),
(822, NULL, 46, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(823, NULL, 48, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(824, NULL, 50, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 20:20:01', '2021-05-10 21:30:23'),
(825, NULL, 51, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(826, NULL, 52, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(827, NULL, 53, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 20:20:01', '2021-05-19 20:42:52'),
(828, NULL, 55, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(829, NULL, 57, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(830, NULL, 58, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(831, NULL, 60, 33, 133, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 20:20:01', NULL),
(832, NULL, 33, 59, 133, 234, 'offers', 'add_new_offer', 'abu ‎aseel', '{\"user_name\":\"1033304\",\"order_name\":\"test1\"}', 6, 1, '2021-05-10 20:21:41', '2021-05-10 20:25:38'),
(833, NULL, 33, 45, 133, 235, 'offers', 'add_new_offer', 'talal123', '{\"user_name\":\"1753255\",\"order_name\":\"test1\"}', 6, 1, '2021-05-10 20:22:37', '2021-05-10 20:25:38'),
(834, NULL, 33, 59, 133, 234, 'offers', 'edit_new_offer', 'abu ‎aseel', '{\"user_name\":1033304,\"order_name\":\"test1\"}', 7, 1, '2021-05-10 20:23:56', '2021-05-10 20:25:38'),
(835, NULL, 33, 45, 133, 235, 'offers', 'edit_new_offer', 'talal123', '{\"user_name\":1753255,\"order_name\":\"test1\"}', 7, 1, '2021-05-10 20:24:54', '2021-05-10 20:25:38'),
(836, NULL, 45, 33, 133, 235, 'accepted_offer', 'owner_accepted_offer', 'talal ‎abdulaziz', '{\"order_name\":\"test1\"}', 3, 1, '2021-05-10 20:27:20', '2021-05-10 20:28:32'),
(837, NULL, 33, 45, 133, 235, 'orders', 'offer_payed_and_waiting_golden_activation', 'talal123', '{\"order_name\":\"test1\",\"user_name\":4158487}', 12, 1, '2021-05-10 20:31:41', '2021-05-10 20:35:38'),
(838, NULL, 59, 33, 133, NULL, 'golden_offer', 'golden_offer_activated', 'golden_offer_activated', '{\"order_name\":\"test1\",\"order_price\":\"10000.00\"}', 10, 1, '2021-05-10 20:36:28', '2021-05-10 20:37:48'),
(839, NULL, 45, 33, 133, NULL, 'golden_offer', 'golden_offer_activated', 'golden_offer_activated', '{\"order_name\":\"test1\",\"order_price\":\"10000.00\"}', 10, 1, '2021-05-10 20:36:28', '2021-05-10 20:37:08'),
(840, NULL, 33, 45, 133, 236, 'offers', 'add_new_offer', 'talal123', '{\"user_name\":\"1344723\",\"order_name\":\"test1\"}', 6, 1, '2021-05-10 20:37:28', '2021-05-10 20:39:16'),
(841, NULL, 33, 59, 133, 237, 'offers', 'add_new_offer', 'abu ‎aseel', '{\"user_name\":\"1143733\",\"order_name\":\"test1\"}', 6, 1, '2021-05-10 20:38:04', '2021-05-10 20:39:16'),
(842, NULL, 59, 33, 133, 237, 'accepted_offer', 'owner_accepted_offer', 'talal ‎abdulaziz', '{\"order_name\":\"test1\"}', 3, 1, '2021-05-10 20:39:57', '2021-05-10 20:41:34'),
(843, NULL, 33, 59, 133, 237, 'orders', 'offer_payed_and_finish_order', 'abu ‎aseel', '{\"order_name\":\"test1\",\"user_name\":4158487}', 8, 1, '2021-05-10 20:41:52', '2021-05-10 20:43:36'),
(844, NULL, 45, 59, 133, 237, 'offers', 'offer_refused', 'abu ‎aseel', '{\"order_name\":\"test1\"}', 4, 1, '2021-05-10 20:41:52', '2021-05-10 20:42:35'),
(845, NULL, 59, 33, 133, 237, 'rating', 'rate_order_offer_man', 'rate_order_offer_man', '{\"order_name\":\"test1\",\"user_name\":\"abu \\u200easeel\"}', 14, 1, '2021-05-10 20:46:05', '2021-05-10 22:10:05'),
(846, NULL, 33, 59, 133, 237, 'rating', 'rate_order_user', 'rate_order_user', '{\"order_name\":\"test1\",\"user_name\":\"talal \\u200eabdulaziz\"}', 15, 1, '2021-05-10 20:46:05', '2021-05-10 22:07:45'),
(847, NULL, 50, 15, 132, 238, 'offers', 'add_new_offer', 'محمد ‏داوود', '{\"user_name\":\"7347125\",\"order_name\":\"\\u062e\\u062f\\u0645\\u0629 \\u0645\\u062a\\u0627\\u062d\\u0629 \\u0644\\u0645\\u062f\\u0629 \\u0633\\u0627\\u0639\\u0629\"}', 6, 1, '2021-05-10 21:32:44', '2021-05-10 21:33:00'),
(848, NULL, 15, NULL, 134, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"uuuuuu\"}', 0, 1, '2021-05-10 21:34:19', '2021-05-11 22:10:28'),
(849, NULL, 34, 15, 134, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a\\u0629\",\"category_name_en\":\"Electronics \\u200e\"}', 5, 0, '2021-05-10 21:34:19', NULL),
(850, NULL, 44, 15, 134, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a\\u0629\",\"category_name_en\":\"Electronics \\u200e\"}', 5, 0, '2021-05-10 21:34:19', NULL),
(851, NULL, 50, 15, 134, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a\\u0629\",\"category_name_en\":\"Electronics \\u200e\"}', 5, 1, '2021-05-10 21:34:19', '2021-05-10 21:40:30'),
(852, NULL, 53, 15, 134, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a\\u0629\",\"category_name_en\":\"Electronics \\u200e\"}', 5, 1, '2021-05-10 21:34:19', '2021-05-19 20:42:52'),
(853, NULL, 15, NULL, 129, NULL, 'orders', 'order_refund_requested_accepted', 'order_refund_requested_accepted', '{\"order_name\":\"Tender \\u200e2\",\"message_ar\":null,\"message_en\":null}', 0, 1, '2021-05-10 21:36:33', '2021-05-11 22:10:28'),
(854, 'system', 33, NULL, 133, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"test1\"}', 9, 1, '2021-05-10 21:37:04', '2021-05-10 22:07:45'),
(855, NULL, 50, NULL, 129, NULL, 'orders', 'order_refund_requested_accepted', 'order_refund_requested_accepted', '{\"order_name\":\"Tender \\u200e2\",\"message_ar\":null,\"message_en\":null}', 0, 1, '2021-05-10 21:37:05', '2021-05-10 21:40:30'),
(856, NULL, 50, NULL, 129, NULL, 'orders', 'order_refund_requested_accepted', 'order_refund_requested_accepted', '{\"order_name\":\"Tender \\u200e2\",\"message_ar\":null,\"message_en\":null}', 0, 1, '2021-05-10 21:37:39', '2021-05-10 21:40:30'),
(857, 'system', 15, NULL, 129, 222, 'waiting_duration', 'waiting_duration_ended', 'waiting_duration_ended', '{\"order_name\":\"Tender \\u200e2\",\"user_name\":8799824}', 13, 1, '2021-05-10 21:38:04', '2021-05-11 22:10:28'),
(858, NULL, 50, NULL, 127, NULL, 'orders', 'order_refund_requested_accepted', 'order_refund_requested_accepted', '{\"order_name\":\"\\u0639\\u0631\\u0628\\u064a\\u0629 4\\\"\\u2708\\ufe0f\",\"message_ar\":null,\"message_en\":null}', 0, 1, '2021-05-10 21:44:40', '2021-05-10 21:46:32'),
(859, NULL, 50, NULL, 127, NULL, 'orders', 'order_refund_requested_refuse', 'order_refund_requested_refuse', '{\"order_name\":\"\\u0639\\u0631\\u0628\\u064a\\u0629 4\\\"\\u2708\\ufe0f\",\"message_ar\":\"\\u0631\\u0641\\u0636\",\"message_en\":\"refuse\"}', 0, 1, '2021-05-10 21:45:11', '2021-05-10 21:46:32'),
(860, NULL, 33, NULL, 136, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"test2\"}', 0, 1, '2021-05-10 22:08:57', '2021-05-10 22:13:20'),
(861, NULL, 15, 33, 136, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0645\\u0646\\u0632\\u0644\\u064a\\u0629\",\"category_name_en\":\"Appliances\"}', 5, 1, '2021-05-10 22:08:57', '2021-05-11 22:10:28'),
(862, NULL, 32, 33, 136, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0645\\u0646\\u0632\\u0644\\u064a\\u0629\",\"category_name_en\":\"Appliances\"}', 5, 0, '2021-05-10 22:08:57', NULL),
(863, NULL, 44, 33, 136, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0645\\u0646\\u0632\\u0644\\u064a\\u0629\",\"category_name_en\":\"Appliances\"}', 5, 0, '2021-05-10 22:08:57', NULL),
(864, NULL, 46, 33, 136, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0645\\u0646\\u0632\\u0644\\u064a\\u0629\",\"category_name_en\":\"Appliances\"}', 5, 0, '2021-05-10 22:08:57', NULL),
(865, NULL, 50, 33, 136, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0645\\u0646\\u0632\\u0644\\u064a\\u0629\",\"category_name_en\":\"Appliances\"}', 5, 1, '2021-05-10 22:08:57', '2021-05-10 22:14:05'),
(866, NULL, 51, 33, 136, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0645\\u0646\\u0632\\u0644\\u064a\\u0629\",\"category_name_en\":\"Appliances\"}', 5, 0, '2021-05-10 22:08:57', NULL),
(867, NULL, 52, 33, 136, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0645\\u0646\\u0632\\u0644\\u064a\\u0629\",\"category_name_en\":\"Appliances\"}', 5, 0, '2021-05-10 22:08:57', NULL),
(868, NULL, 60, 33, 136, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0645\\u0646\\u0632\\u0644\\u064a\\u0629\",\"category_name_en\":\"Appliances\"}', 5, 0, '2021-05-10 22:08:57', NULL),
(869, NULL, 33, 59, 136, 239, 'offers', 'add_new_offer', 'abu ‎aseel', '{\"user_name\":\"1794920\",\"order_name\":\"test2\"}', 6, 1, '2021-05-10 22:10:43', '2021-05-10 22:13:20'),
(870, NULL, 33, 59, 133, 237, 'rating', 'order_rate_by_offer_man', 'order_rate_by_offer_man', '{\"order_name\":\"test1\",\"user_name\":4158487}', 16, 1, '2021-05-10 22:11:04', '2021-05-10 22:13:20'),
(871, NULL, 33, 45, 136, 240, 'offers', 'add_new_offer', 'talal123', '{\"user_name\":\"3867136\",\"order_name\":\"test2\"}', 6, 1, '2021-05-10 22:12:45', '2021-05-10 22:13:20'),
(872, NULL, 50, NULL, 135, NULL, 'orders', 'order_updated_by_admin', 'order_updated_by_admin', '{\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\"}', 0, 1, '2021-05-10 22:13:27', '2021-05-10 22:14:05'),
(873, NULL, 12, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(874, NULL, 15, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 22:13:27', '2021-05-11 22:10:28'),
(875, NULL, 17, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(876, NULL, 20, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(877, NULL, 21, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(878, NULL, 30, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 22:13:27', '2021-05-10 22:30:15'),
(879, NULL, 32, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(880, NULL, 33, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 22:13:27', '2021-05-10 22:14:48'),
(881, NULL, 34, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(882, NULL, 44, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(883, NULL, 45, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 22:13:27', '2021-05-10 22:16:57'),
(884, NULL, 46, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(885, NULL, 48, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(886, NULL, 51, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(887, NULL, 52, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(888, NULL, 53, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-10 22:13:27', '2021-05-19 20:42:52'),
(889, NULL, 55, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(890, NULL, 57, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(891, NULL, 58, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(892, NULL, 60, 50, 135, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-10 22:13:27', NULL),
(893, NULL, 45, 33, 136, 240, 'accepted_offer', 'owner_accepted_offer', 'talal ‎abdulaziz', '{\"order_name\":\"test2\"}', 3, 1, '2021-05-10 22:13:33', '2021-05-10 22:16:57'),
(894, NULL, 33, 59, 136, NULL, 'orders', 'cancel_offer_with_user_name', NULL, '{\"order_name\":\"test2\",\"user_name\":\"abu \\u200easeel\"}', 0, 1, '2021-05-10 22:14:25', '2021-05-10 22:14:48'),
(895, NULL, 33, 45, 136, 240, 'orders', 'offer_payed_and_waiting_golden_activation', 'talal123', '{\"order_name\":\"test2\",\"user_name\":2092729}', 12, 1, '2021-05-10 22:17:20', '2021-05-10 22:51:58'),
(896, NULL, 50, NULL, 129, NULL, 'orders', 'order_refund_requested_accepted', 'order_refund_requested_accepted', '{\"order_name\":\"Tender \\u200e2\",\"message_ar\":null,\"message_en\":null}', 0, 1, '2021-05-10 22:21:28', '2021-05-10 22:29:52'),
(897, NULL, 50, NULL, 127, NULL, 'orders', 'order_refund_requested_accepted', 'order_refund_requested_accepted', '{\"order_name\":\"\\u0639\\u0631\\u0628\\u064a\\u0629 4\\\"\\u2708\\ufe0f\",\"message_ar\":null,\"message_en\":null}', 0, 1, '2021-05-10 22:21:30', '2021-05-10 22:29:52'),
(898, NULL, 50, NULL, 127, NULL, 'orders', 'order_refund_requested_refuse', 'order_refund_requested_refuse', '{\"order_name\":\"\\u0639\\u0631\\u0628\\u064a\\u0629 4\\\"\\u2708\\ufe0f\",\"message_ar\":\"\\u0631\\u0641\\u0636\",\"message_en\":\"ccc\"}', 0, 1, '2021-05-10 22:21:48', '2021-05-10 22:29:52'),
(899, NULL, 50, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"kkk\",\"message\":\"hhh\"}', 0, 1, '2021-05-10 22:29:44', '2021-05-10 22:29:52'),
(900, NULL, 30, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"jjj\",\"message\":\"jjj\"}', 0, 1, '2021-05-10 22:30:41', '2021-05-11 23:07:36'),
(901, NULL, 30, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"bvnbvn\",\"message\":\"bhh\"}', 0, 1, '2021-05-10 22:38:17', '2021-05-11 23:07:36'),
(902, NULL, 30, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"gjghfjfg\",\"message\":\"mmkj\"}', 0, 1, '2021-05-10 22:46:46', '2021-05-11 23:07:36'),
(903, NULL, 33, 45, 136, 240, 'orders', 'complete_order_user', 'complete_order_user', '{\"order_name\":\"test2\"}', 0, 1, '2021-05-10 22:52:09', '2021-05-10 23:05:00'),
(904, NULL, 45, 33, 136, 240, 'orders', 'complete_order_offer', 'complete_order_offer', '{\"order_name\":\"test2\"}', 0, 1, '2021-05-10 22:52:09', '2021-05-10 22:53:37'),
(905, NULL, 30, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"fdfg\",\"message\":\"nbb\"}', 0, 1, '2021-05-10 22:53:59', '2021-05-11 23:07:36'),
(906, NULL, 30, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"\\u0633\\u0630\\u0633\\u0634\",\"message\":\"\\u200fnfc\"}', 0, 1, '2021-05-10 22:55:41', '2021-05-11 23:07:36'),
(907, NULL, 45, 33, 136, 240, 'rating', 'rate_order_offer_man', 'rate_order_offer_man', '{\"order_name\":\"test2\",\"user_name\":\"talal123\"}', 14, 1, '2021-05-10 22:57:05', '2021-05-10 23:00:39'),
(908, NULL, 33, 45, 136, 240, 'rating', 'rate_order_user', 'rate_order_user', '{\"order_name\":\"test2\",\"user_name\":\"talal \\u200eabdulaziz\"}', 15, 1, '2021-05-10 22:57:05', '2021-05-10 23:05:00'),
(909, NULL, 45, NULL, 137, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"test3\"}', 0, 1, '2021-05-10 22:58:53', '2021-05-10 23:00:39'),
(910, NULL, 45, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"ok\",\"message\":\"bBsnzn\"}', 0, 1, '2021-05-10 22:59:41', '2021-05-10 23:00:39'),
(911, NULL, 45, NULL, 133, NULL, 'orders', 'order_refund_requested_refuse', 'order_refund_requested_refuse', '{\"order_name\":\"test1\",\"message_ar\":\"\\u0646\\u0648\",\"message_en\":\"no\"}', 0, 1, '2021-05-10 23:00:22', '2021-05-10 23:00:39'),
(912, NULL, 33, 45, 136, 240, 'rating', 'order_rate_by_offer_man', 'order_rate_by_offer_man', '{\"order_name\":\"test2\",\"user_name\":2092729}', 16, 1, '2021-05-10 23:00:56', '2021-05-10 23:05:00'),
(913, NULL, 45, 33, 137, 241, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"8565441\",\"order_name\":\"test3\"}', 6, 1, '2021-05-10 23:05:38', '2021-05-10 23:06:23'),
(914, NULL, 45, 33, 136, 240, 'rating', 'order_rate_by_user', 'order_rate_by_user', '{\"order_name\":\"test2\",\"user_name\":2092729}', 17, 1, '2021-05-10 23:06:03', '2021-05-10 23:06:23'),
(915, NULL, 45, 59, 137, 242, 'offers', 'add_new_offer', 'abu ‎aseel', '{\"user_name\":\"6344628\",\"order_name\":\"test3\"}', 6, 1, '2021-05-10 23:07:43', '2021-05-10 23:08:10'),
(916, NULL, 33, 45, 137, 241, 'accepted_offer', 'owner_accepted_offer', 'talal123', '{\"order_name\":\"test3\"}', 3, 1, '2021-05-10 23:08:26', '2021-05-10 23:09:26'),
(917, NULL, 45, 33, 137, 241, 'orders', 'offer_payed_and_waiting_golden_activation', 'talal ‎abdulaziz', '{\"order_name\":\"test3\",\"user_name\":5459975}', 12, 1, '2021-05-10 23:14:00', '2021-05-10 23:14:52'),
(918, NULL, 33, 45, 137, NULL, 'golden_offer', 'golden_offer_activated', 'golden_offer_activated', '{\"order_name\":\"test3\",\"order_price\":\"40000.00\"}', 10, 1, '2021-05-10 23:15:10', '2021-05-10 23:15:49'),
(919, NULL, 59, 45, 137, NULL, 'golden_offer', 'golden_offer_activated', 'golden_offer_activated', '{\"order_name\":\"test3\",\"order_price\":\"40000.00\"}', 10, 1, '2021-05-10 23:15:10', '2021-05-10 23:17:05'),
(920, NULL, 45, 33, 137, 243, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"1436850\",\"order_name\":\"test3\"}', 6, 1, '2021-05-10 23:16:36', '2021-05-10 23:20:46'),
(921, NULL, 45, 59, 137, 244, 'offers', 'add_new_offer', 'abu ‎aseel', '{\"user_name\":\"1149381\",\"order_name\":\"test3\"}', 6, 1, '2021-05-10 23:18:44', '2021-05-10 23:20:46'),
(922, NULL, 59, 45, 137, 244, 'accepted_offer', 'owner_accepted_offer', 'talal123', '{\"order_name\":\"test3\"}', 3, 1, '2021-05-10 23:21:35', '2021-05-10 23:22:40'),
(923, NULL, 45, 59, 137, 244, 'orders', 'offer_payed_and_finish_order', 'abu ‎aseel', '{\"order_name\":\"test3\",\"user_name\":5459975}', 8, 1, '2021-05-10 23:23:25', '2021-05-10 23:24:11'),
(924, NULL, 33, 59, 137, 244, 'offers', 'offer_refused', 'abu ‎aseel', '{\"order_name\":\"test3\"}', 4, 1, '2021-05-10 23:23:25', '2021-05-10 23:23:48'),
(925, NULL, 59, 45, 137, 244, 'rating', 'rate_order_offer_man', 'rate_order_offer_man', '{\"order_name\":\"test3\",\"user_name\":\"abu \\u200easeel\"}', 14, 1, '2021-05-10 23:28:04', '2021-05-11 00:47:38'),
(926, NULL, 45, 59, 137, 244, 'rating', 'rate_order_user', 'rate_order_user', '{\"order_name\":\"test3\",\"user_name\":\"talal123\"}', 15, 1, '2021-05-10 23:28:04', '2021-05-12 14:21:41'),
(927, NULL, 59, 45, 137, 244, 'rating', 'order_rate_by_user', 'order_rate_by_user', '{\"order_name\":\"test3\",\"user_name\":5459975}', 17, 1, '2021-05-10 23:28:12', '2021-05-11 00:47:38'),
(928, NULL, 45, NULL, 138, NULL, 'orders', 'order_updated_by_admin', 'order_updated_by_admin', '{\"order_name\":\"test4\"}', 0, 1, '2021-05-10 23:48:15', '2021-05-12 14:21:41'),
(929, NULL, 15, 45, 138, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a\\u0629\",\"category_name_en\":\"Electronics \\u200e\"}', 5, 1, '2021-05-10 23:48:15', '2021-05-11 22:10:27'),
(930, NULL, 34, 45, 138, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a\\u0629\",\"category_name_en\":\"Electronics \\u200e\"}', 5, 0, '2021-05-10 23:48:15', NULL),
(931, NULL, 44, 45, 138, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a\\u0629\",\"category_name_en\":\"Electronics \\u200e\"}', 5, 0, '2021-05-10 23:48:15', NULL),
(932, NULL, 50, 45, 138, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a\\u0629\",\"category_name_en\":\"Electronics \\u200e\"}', 5, 1, '2021-05-10 23:48:15', '2021-05-11 22:49:56'),
(933, NULL, 53, 45, 138, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u062c\\u0647\\u0632\\u0629 \\u0627\\u0644\\u0643\\u062a\\u0631\\u0648\\u0646\\u064a\\u0629\",\"category_name_en\":\"Electronics \\u200e\"}', 5, 1, '2021-05-10 23:48:15', '2021-05-19 20:42:52'),
(934, NULL, 45, NULL, 136, NULL, 'orders', 'order_refund_requested_accepted', 'order_refund_requested_accepted', '{\"order_name\":\"test2\",\"message_ar\":null,\"message_en\":null}', 0, 1, '2021-05-10 23:49:14', '2021-05-12 14:21:41'),
(935, NULL, 45, NULL, 137, NULL, 'orders', 'order_refund_requested_accepted', 'order_refund_requested_accepted', '{\"order_name\":\"test3\",\"message_ar\":null,\"message_en\":null}', 0, 1, '2021-05-10 23:49:55', '2021-05-12 14:21:41'),
(936, 'system', 33, NULL, 136, 240, 'waiting_duration', 'waiting_duration_ended', 'waiting_duration_ended', '{\"order_name\":\"test2\",\"user_name\":2092729}', 13, 1, '2021-05-10 23:50:05', '2021-05-11 00:19:46'),
(937, 'system', 45, NULL, 137, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"test3\"}', 9, 1, '2021-05-11 00:16:04', '2021-05-12 14:21:41'),
(938, NULL, 45, 33, 138, 245, 'offers', 'add_new_offer', 'talal ‎abdulaziz', '{\"user_name\":\"1650906\",\"order_name\":\"test4\"}', 6, 1, '2021-05-11 00:47:10', '2021-05-12 14:21:41'),
(939, NULL, 45, 59, 138, 246, 'offers', 'add_new_offer', 'abu ‎aseel', '{\"user_name\":\"1854112\",\"order_name\":\"test4\"}', 6, 1, '2021-05-11 00:47:53', '2021-05-12 14:21:41'),
(940, NULL, 45, 59, 137, 244, 'rating', 'order_rate_by_offer_man', 'order_rate_by_offer_man', '{\"order_name\":\"test3\",\"user_name\":5459975}', 16, 1, '2021-05-11 00:48:11', '2021-05-12 14:21:41'),
(941, 'system', 50, NULL, 131, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"testing \\u200e6 \\u200fAm \\u200e\"}', 9, 1, '2021-05-11 04:04:04', '2021-05-11 22:49:56'),
(942, 'system', 50, NULL, 132, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"\\u062e\\u062f\\u0645\\u0629 \\u0645\\u062a\\u0627\\u062d\\u0629 \\u0644\\u0645\\u062f\\u0629 \\u0633\\u0627\\u0639\\u0629\"}', 9, 1, '2021-05-11 04:29:03', '2021-05-11 22:49:56'),
(943, NULL, 50, 15, 135, 247, 'offers', 'add_new_offer', 'محمد ‏داوود', '{\"user_name\":\"1410158\",\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\"}', 6, 1, '2021-05-11 22:50:25', '2021-05-11 22:50:27'),
(944, NULL, 15, 50, 135, 247, 'accepted_offer', 'owner_accepted_offer', 'ابو عبدالعزيز', '{\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\"}', 3, 1, '2021-05-11 22:51:04', '2021-05-11 23:58:12'),
(945, NULL, 50, 15, 135, 247, 'orders', 'offer_payed_and_waiting_golden_activation', 'محمد ‏داوود', '{\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\",\"user_name\":1453513}', 12, 1, '2021-05-11 22:55:05', '2021-05-11 22:55:07'),
(946, NULL, 15, 50, 135, NULL, 'golden_offer', 'golden_offer_activated', 'golden_offer_activated', '{\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\",\"order_price\":\"30000.00\"}', 10, 1, '2021-05-11 22:56:57', '2021-05-11 23:58:12'),
(947, NULL, 50, 15, 135, 248, 'offers', 'add_new_offer', 'محمد ‏داوود', '{\"user_name\":\"1668702\",\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\"}', 6, 1, '2021-05-11 22:57:29', '2021-05-11 22:57:31'),
(948, NULL, 50, 15, 135, 248, 'offers', 'edit_new_offer', 'محمد ‏داوود', '{\"user_name\":1668702,\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\"}', 7, 1, '2021-05-11 23:07:34', '2021-05-11 23:46:43'),
(949, NULL, 15, 50, 135, 248, 'accepted_offer', 'owner_accepted_offer', 'ابو عبدالعزيز', '{\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\"}', 3, 1, '2021-05-11 23:47:04', '2021-05-11 23:58:12'),
(950, NULL, 50, 15, 135, 248, 'orders', 'offer_payed_and_finish_order', 'محمد ‏داوود', '{\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\",\"user_name\":1453513}', 8, 1, '2021-05-11 23:47:17', '2021-05-11 23:47:20'),
(951, NULL, 15, 50, 135, 248, 'rating', 'rate_order_offer_man', 'rate_order_offer_man', '{\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\",\"user_name\":\"\\u0645\\u062d\\u0645\\u062f \\u200f\\u062f\\u0627\\u0648\\u0648\\u062f\"}', 14, 1, '2021-05-11 23:50:05', '2021-05-11 23:58:12'),
(952, NULL, 50, 15, 135, 248, 'rating', 'rate_order_user', 'rate_order_user', '{\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\",\"user_name\":\"\\u0627\\u0628\\u0648 \\u0639\\u0628\\u062f\\u0627\\u0644\\u0639\\u0632\\u064a\\u0632\"}', 15, 1, '2021-05-11 23:50:05', '2021-05-11 23:50:11'),
(953, NULL, 50, 15, 135, 248, 'rating', 'order_rate_by_offer_man', 'order_rate_by_offer_man', '{\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\",\"user_name\":1453513}', 16, 1, '2021-05-11 23:50:13', '2021-05-11 23:50:14'),
(954, NULL, 15, 50, 135, 248, 'rating', 'order_rate_by_user', 'order_rate_by_user', '{\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\",\"user_name\":1453513}', 17, 1, '2021-05-11 23:50:32', '2021-05-11 23:58:12'),
(955, 'system', 50, NULL, 135, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"\\u0645\\u0637\\u0628\\u062e\"}', 9, 1, '2021-05-11 23:57:03', '2021-05-11 23:57:15'),
(956, NULL, 59, NULL, 139, NULL, 'orders', 'order_refuse', 'order_refuse', '{\"order_name\":\"hdh\",\"message_ar\":\"\\u0637\\u0644\\u0628\\u0643 \\u063a\\u064a\\u0631 \\u0645\\u062a\\u0648\\u0641\\u0631\",\"message_en\":\"your request is not avilable\"}', 0, 1, '2021-05-12 02:42:49', '2021-05-12 14:18:01'),
(957, NULL, 33, 45, 138, 245, 'accepted_offer', 'owner_accepted_offer', 'talal123', '{\"order_name\":\"test4\"}', 3, 1, '2021-05-12 14:22:19', '2021-05-12 14:22:35'),
(958, NULL, 45, 59, 138, NULL, 'orders', 'cancel_offer_with_user_name', NULL, '{\"order_name\":\"test4\",\"user_name\":1854112,\"price\":\"3000.00\"}', 0, 1, '2021-05-12 14:24:59', '2021-05-12 14:26:29'),
(959, NULL, 45, 33, 138, 245, 'orders', 'offer_payed_and_waiting_golden_activation', 'talal ‎abdulaziz', '{\"order_name\":\"test4\",\"user_name\":7108026}', 12, 1, '2021-05-12 14:27:48', '2021-05-12 14:29:22'),
(960, NULL, 45, 33, 138, 245, 'orders', 'complete_order_user', 'complete_order_user', '{\"order_name\":\"test4\"}', 0, 1, '2021-05-12 14:29:59', '2021-05-12 14:35:37'),
(961, NULL, 33, 45, 138, 245, 'orders', 'complete_order_offer', 'complete_order_offer', '{\"order_name\":\"test4\"}', 0, 1, '2021-05-12 14:29:59', '2021-05-12 14:30:29'),
(962, NULL, 33, NULL, 140, NULL, 'orders', 'order_updated_by_admin', 'order_updated_by_admin', '{\"order_name\":\"test5\"}', 0, 1, '2021-05-12 14:33:49', '2021-05-12 14:37:32'),
(963, NULL, 33, 45, 138, 245, 'rating', 'rate_order_offer_man', 'rate_order_offer_man', '{\"order_name\":\"test4\",\"user_name\":\"talal \\u200eabdulaziz\"}', 14, 1, '2021-05-12 14:34:04', '2021-05-12 14:37:32'),
(964, NULL, 45, 33, 138, 245, 'rating', 'rate_order_user', 'rate_order_user', '{\"order_name\":\"test4\",\"user_name\":\"talal123\"}', 15, 1, '2021-05-12 14:34:05', '2021-05-12 14:35:37'),
(965, NULL, 45, 33, 138, 245, 'rating', 'order_rate_by_offer_man', 'order_rate_by_offer_man', '{\"order_name\":\"test4\",\"user_name\":7108026}', 16, 1, '2021-05-12 14:34:18', '2021-05-12 14:35:37'),
(966, NULL, 33, 59, 140, 249, 'offers', 'add_new_offer', 'abu ‎aseel', '{\"user_name\":\"1298052\",\"order_name\":\"test5\"}', 6, 1, '2021-05-12 14:35:14', '2021-05-12 14:37:32'),
(967, NULL, 33, 45, 138, 245, 'rating', 'order_rate_by_user', 'order_rate_by_user', '{\"order_name\":\"test4\",\"user_name\":7108026}', 17, 1, '2021-05-12 14:35:52', '2021-05-12 14:37:32'),
(968, NULL, 33, 45, 140, 250, 'offers', 'add_new_offer', 'talal123', '{\"user_name\":\"2131679\",\"order_name\":\"test5\"}', 6, 1, '2021-05-12 14:36:58', '2021-05-12 14:37:32'),
(969, NULL, 45, 33, 140, 250, 'accepted_offer', 'owner_accepted_offer', 'talal ‎abdulaziz', '{\"order_name\":\"test5\"}', 3, 1, '2021-05-12 14:37:50', '2021-05-12 14:38:05'),
(970, NULL, 33, 45, 140, 250, 'orders', 'offer_payed_and_waiting_golden_activation', 'talal123', '{\"order_name\":\"test5\",\"user_name\":1250868}', 12, 1, '2021-05-12 14:38:15', '2021-05-12 14:38:44'),
(971, NULL, 59, 33, 140, NULL, 'golden_offer', 'golden_offer_activated', 'golden_offer_activated', '{\"order_name\":\"test5\",\"order_price\":\"3000.00\"}', 10, 1, '2021-05-12 14:39:13', '2021-05-12 14:42:05'),
(972, NULL, 45, 33, 140, NULL, 'golden_offer', 'golden_offer_activated', 'golden_offer_activated', '{\"order_name\":\"test5\",\"order_price\":\"3000.00\"}', 10, 1, '2021-05-12 14:39:13', '2021-05-12 14:40:18'),
(973, NULL, 33, 45, 140, 251, 'offers', 'add_new_offer', 'talal123', '{\"user_name\":\"5745062\",\"order_name\":\"test5\"}', 6, 1, '2021-05-12 14:41:31', '2021-05-12 14:42:59'),
(974, NULL, 33, 59, 140, 252, 'offers', 'add_new_offer', 'abu ‎aseel', '{\"user_name\":\"4372145\",\"order_name\":\"test5\"}', 6, 1, '2021-05-12 14:42:21', '2021-05-12 14:42:59'),
(975, NULL, 59, 33, 140, 252, 'accepted_offer', 'owner_accepted_offer', 'talal ‎abdulaziz', '{\"order_name\":\"test5\"}', 3, 1, '2021-05-12 14:43:12', '2021-05-12 14:44:57'),
(976, NULL, 33, 59, 140, 252, 'orders', 'offer_payed_and_finish_order', 'abu ‎aseel', '{\"order_name\":\"test5\",\"user_name\":1250868}', 8, 1, '2021-05-12 14:46:33', '2021-05-12 14:48:04'),
(977, NULL, 45, 59, 140, 252, 'offers', 'offer_refused', 'abu ‎aseel', '{\"order_name\":\"test5\"}', 4, 1, '2021-05-12 14:46:33', '2021-05-12 14:47:24'),
(978, NULL, 59, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"bKallslskskz\",\"message\":\"\\u0627\\u064a\\u0647\\u064a\\u0646\\u064a\\u0648\\u0635\\u0646\\u064a\\u0647\\u0630\\u0646\\u062f\"}', 0, 1, '2021-05-12 14:50:36', '2021-05-19 20:44:37'),
(979, NULL, 59, 33, 140, 252, 'rating', 'rate_order_offer_man', 'rate_order_offer_man', '{\"order_name\":\"test5\",\"user_name\":\"abu \\u200easeel\"}', 14, 1, '2021-05-12 14:51:05', '2021-05-19 20:44:37'),
(980, NULL, 33, 59, 140, 252, 'rating', 'rate_order_user', 'rate_order_user', '{\"order_name\":\"test5\",\"user_name\":\"talal \\u200eabdulaziz\"}', 15, 1, '2021-05-12 14:51:05', '2021-05-12 15:41:21'),
(981, NULL, 59, 33, 140, 252, 'rating', 'order_rate_by_user', 'order_rate_by_user', '{\"order_name\":\"test5\",\"user_name\":1250868}', 17, 1, '2021-05-12 14:51:13', '2021-05-19 20:44:37'),
(982, NULL, 33, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"lglglgpvooglg\",\"message\":\"bznznznznz\"}', 0, 1, '2021-05-12 14:51:36', '2021-05-12 15:41:21'),
(983, 'system', 33, NULL, 140, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"test5\"}', 9, 1, '2021-05-12 15:40:04', '2021-05-12 15:41:21'),
(984, 'system', 45, NULL, 138, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"test4\"}', 9, 1, '2021-05-13 23:49:04', '2021-05-19 20:45:19'),
(985, 'system', 15, NULL, 128, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"Tender\"}', 9, 0, '2021-05-14 03:47:04', '2021-05-14 03:47:04'),
(986, 'system', 15, NULL, 134, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"uuuuuu\"}', 9, 0, '2021-05-17 21:35:04', '2021-05-17 21:35:04'),
(987, 'system', 33, NULL, 136, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"test2\"}', 9, 1, '2021-05-17 22:09:05', '2021-05-17 22:19:57'),
(988, NULL, 33, NULL, 141, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\"}', 0, 1, '2021-05-19 20:42:18', '2021-05-19 20:46:45'),
(989, NULL, 12, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(990, NULL, 15, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(991, NULL, 17, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(992, NULL, 20, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(993, NULL, 21, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(994, NULL, 30, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-19 20:42:18', '2021-05-19 22:27:58'),
(995, NULL, 32, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(996, NULL, 34, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(997, NULL, 44, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(998, NULL, 45, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-19 20:42:18', '2021-05-19 20:45:19'),
(999, NULL, 46, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(1000, NULL, 48, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(1001, NULL, 50, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(1002, NULL, 51, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(1003, NULL, 52, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(1004, NULL, 53, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-19 20:42:18', '2021-05-19 20:42:52'),
(1005, NULL, 55, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(1006, NULL, 57, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(1007, NULL, 58, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(1008, NULL, 60, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(1009, NULL, 61, 33, 141, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 20:42:18', NULL),
(1010, NULL, 33, 53, 141, 253, 'offers', 'add_new_offer', 'talal ‎abdul123', '{\"user_name\":\"3734335\",\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\"}', 6, 1, '2021-05-19 20:43:29', '2021-05-19 20:46:45'),
(1011, NULL, 33, 59, 141, 254, 'offers', 'add_new_offer', 'abu ‎aseel', '{\"user_name\":\"4648328\",\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\"}', 6, 1, '2021-05-19 20:44:50', '2021-05-19 20:46:45'),
(1012, NULL, 33, 45, 141, 255, 'offers', 'add_new_offer', 'talal123', '{\"user_name\":\"1675594\",\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\"}', 6, 1, '2021-05-19 20:45:28', '2021-05-19 20:46:45'),
(1013, NULL, 33, 45, 141, 255, 'offers', 'edit_new_offer', 'talal123', '{\"user_name\":1675594,\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\"}', 7, 1, '2021-05-19 20:46:29', '2021-05-19 20:46:45'),
(1014, NULL, 45, 33, 141, 255, 'accepted_offer', 'owner_accepted_offer', 'talal ‎abdulaziz', '{\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\"}', 3, 1, '2021-05-19 20:47:42', '2021-05-19 20:48:19'),
(1015, NULL, 33, 45, 141, 255, 'orders', 'offer_payed_and_waiting_golden_activation', 'talal123', '{\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\",\"user_name\":9995306}', 12, 1, '2021-05-19 20:49:15', '2021-05-19 20:50:08'),
(1016, NULL, 53, 33, 141, NULL, 'golden_offer', 'golden_offer_activated', 'golden_offer_activated', '{\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\",\"order_price\":\"35000.00\"}', 10, 0, '2021-05-19 20:50:37', NULL),
(1017, NULL, 59, 33, 141, NULL, 'golden_offer', 'golden_offer_activated', 'golden_offer_activated', '{\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\",\"order_price\":\"35000.00\"}', 10, 1, '2021-05-19 20:50:37', '2021-05-19 20:51:13'),
(1018, NULL, 45, 33, 141, NULL, 'golden_offer', 'golden_offer_activated', 'golden_offer_activated', '{\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\",\"order_price\":\"35000.00\"}', 10, 0, '2021-05-19 20:50:37', NULL),
(1019, NULL, 33, 59, 141, 256, 'offers', 'add_new_offer', 'abu ‎aseel', '{\"user_name\":\"5641244\",\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\"}', 6, 1, '2021-05-19 20:51:56', '2021-05-19 20:52:15'),
(1020, NULL, 59, 33, 141, 256, 'accepted_offer', 'owner_accepted_offer', 'talal ‎abdulaziz', '{\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\"}', 3, 0, '2021-05-19 20:52:37', '2021-05-19 20:52:37');
INSERT INTO `notifications` (`id`, `topic`, `user_id`, `sender_id`, `order_id`, `offer_id`, `title`, `translation`, `body`, `body_arguments`, `type`, `is_read`, `created_at`, `updated_at`) VALUES
(1021, NULL, 33, 47, NULL, NULL, 'contact_us', 'contact_us_body', 'contact_us_body', '{\"reply\":\"bshsbsh\",\"message\":\"gsgskdjdn\"}', 0, 1, '2021-05-19 20:54:06', '2021-05-19 20:54:15'),
(1022, NULL, 33, NULL, 141, NULL, 'orders', 'order_refund_requested_refuse', 'order_refund_requested_refuse', '{\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\",\"message_ar\":\"\\u0646\\u0637\\u0646\\u0637\\u0631\",\"message_en\":\"hdbsb\"}', 0, 1, '2021-05-19 20:57:01', '2021-05-19 21:04:04'),
(1023, 'system', 33, NULL, 141, 256, 'waiting_duration', 'waiting_duration_ended', 'waiting_duration_ended', '{\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\",\"user_name\":9995306}', 13, 1, '2021-05-19 20:58:04', '2021-05-19 21:04:04'),
(1024, NULL, 33, NULL, 143, NULL, 'orders', 'order_accepted', 'order_accepted', '{\"order_name\":\"corola\"}', 0, 1, '2021-05-19 21:04:59', '2021-05-19 21:36:22'),
(1025, NULL, 12, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1026, NULL, 15, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1027, NULL, 17, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1028, NULL, 20, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1029, NULL, 21, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1030, NULL, 30, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 1, '2021-05-19 21:04:59', '2021-05-19 22:27:58'),
(1031, NULL, 32, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1032, NULL, 34, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1033, NULL, 44, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1034, NULL, 45, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1035, NULL, 46, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1036, NULL, 48, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1037, NULL, 50, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1038, NULL, 51, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1039, NULL, 52, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1040, NULL, 53, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1041, NULL, 55, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1042, NULL, 57, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1043, NULL, 58, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1044, NULL, 60, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1045, NULL, 61, 33, 143, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0633\\u064a\\u0627\\u0631\\u0627\\u062a\",\"category_name_en\":\"Cars\"}', 5, 0, '2021-05-19 21:04:59', NULL),
(1046, NULL, 33, NULL, 142, NULL, 'orders', 'order_updated_by_admin', 'order_updated_by_admin', '{\"order_name\":\"\\u0631\\u0637\\u0631\\u0637\"}', 0, 1, '2021-05-19 21:05:09', '2021-05-19 21:36:22'),
(1047, NULL, 9, 33, 142, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-19 21:05:09', NULL),
(1048, NULL, 12, 33, 142, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-19 21:05:09', NULL),
(1049, NULL, 15, 33, 142, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-19 21:05:09', NULL),
(1050, NULL, 21, 33, 142, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-19 21:05:09', NULL),
(1051, NULL, 30, 33, 142, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 1, '2021-05-19 21:05:09', '2021-05-19 22:27:58'),
(1052, NULL, 34, 33, 142, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-19 21:05:09', NULL),
(1053, NULL, 44, 33, 142, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-19 21:05:09', NULL),
(1054, NULL, 50, 33, 142, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-19 21:05:09', NULL),
(1055, NULL, 60, 33, 142, NULL, 'follow_categories', 'new_category_of_order', 'new_category_of_order', '{\"category_name_ar\":\"\\u0627\\u0644\\u0623\\u062b\\u0627\\u062b\",\"category_name_en\":\"Furniture\"}', 5, 0, '2021-05-19 21:05:09', NULL),
(1056, NULL, 33, 63, 142, 257, 'offers', 'add_new_offer', 'test123456', '{\"user_name\":\"1851020\",\"order_name\":\"\\u0631\\u0637\\u0631\\u0637\"}', 6, 1, '2021-05-19 21:15:03', '2021-05-19 21:36:22'),
(1057, 'system', 33, NULL, 141, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"\\u0645\\u0631\\u0633\\u064a\\u062f\\u0633 \\u200fS500\"}', 9, 0, '2021-05-19 21:51:04', '2021-05-19 21:51:04'),
(1058, 'system', 33, NULL, 142, NULL, 'bidding_duration', 'end_bidding_duration', 'end_bidding_duration', '{\"order_name\":\"\\u0631\\u0637\\u0631\\u0637\"}', 9, 0, '2021-05-22 21:06:04', '2021-05-22 21:06:04');

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `rand_user_id` int(11) DEFAULT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_identity` enum('personal','commercial') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'personal',
  `description` text COLLATE utf8mb4_unicode_ci,
  `expected_price` decimal(15,2) NOT NULL,
  `commission` double DEFAULT NULL,
  `golden_commission` double DEFAULT NULL,
  `delivery_duration` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bidding_duration` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delivery_way_id` bigint(20) UNSIGNED DEFAULT NULL,
  `industry_id` bigint(20) UNSIGNED DEFAULT NULL,
  `warranty_id` bigint(20) UNSIGNED DEFAULT NULL,
  `payment_method_id` bigint(20) UNSIGNED DEFAULT NULL,
  `golden_offer` tinyint(1) NOT NULL DEFAULT '0',
  `is_pay` tinyint(1) NOT NULL DEFAULT '0',
  `end_bidding_duration` datetime DEFAULT NULL,
  `time_out` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('pending','finish','refuse_system','refuse','cancel') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `reason_rejection_id` bigint(20) UNSIGNED DEFAULT NULL,
  `message` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `rand_user_id`, `order_id`, `user_id`, `order_identity`, `description`, `expected_price`, `commission`, `golden_commission`, `delivery_duration`, `bidding_duration`, `delivery_way_id`, `industry_id`, `warranty_id`, `payment_method_id`, `golden_offer`, `is_pay`, `end_bidding_duration`, `time_out`, `status`, `reason_rejection_id`, `message`, `created_at`, `updated_at`) VALUES
(213, 7445929, 127, 15, 'personal', NULL, 5000.00, 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2021-05-07 03:48:53', 0, 'pending', NULL, NULL, '2021-05-07 03:43:04', '2021-05-07 03:44:13'),
(214, 3361779, 127, 33, 'personal', NULL, 4000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'pending', NULL, NULL, '2021-05-07 03:43:12', '2021-05-07 03:43:12'),
(215, 1990046, 128, 33, 'personal', NULL, 900.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-07 03:45:19', '2021-05-07 03:46:15'),
(216, 9736662, 128, 50, 'commercial', NULL, 8000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2021-05-07 03:50:50', 1, 'refuse', 11, NULL, '2021-05-07 03:45:35', '2021-05-07 03:46:15'),
(217, 1730538, 128, 33, 'personal', NULL, 600.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-07 03:47:33', '2021-05-07 03:48:57'),
(218, 6064829, 128, 50, 'commercial', NULL, 5000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-07 03:47:55', '2021-05-07 03:48:57'),
(219, 2038349, 129, 33, 'personal', NULL, 3000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-07 03:53:04', '2021-05-07 03:54:27'),
(220, 1441745, 129, 50, 'commercial', NULL, 9500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2021-05-07 03:59:04', 1, 'refuse', 14, NULL, '2021-05-07 03:53:50', '2021-05-07 03:54:27'),
(221, 2123267, 129, 33, 'personal', NULL, 2700.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-07 03:54:50', '2021-05-07 04:51:57'),
(222, 1185679, 129, 50, 'commercial', NULL, 7500.00, 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, '2021-05-07 04:01:01', 1, 'refuse', NULL, '{\"message_ar\":\"\\u062a\\u0645 \\u0631\\u0641\\u0636 \\u0627\\u0644\\u0637\\u0644\\u0628 \\u0628\\u0633\\u0628\\u0628 \\u0625\\u0633\\u062a\\u0631\\u062f\\u0627\\u062f \\u0627\\u0644\\u0645\\u0628\\u0644\\u063a \\u0627\\u0644\\u0645\\u062f\\u0641\\u0648\\u0639\",\"message_en\":\"The request was rejected due to the refund of the amount paid\"}', '2021-05-07 03:55:16', '2021-05-10 21:38:04'),
(223, 1348240, 129, 33, 'personal', NULL, 3000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-05-07 04:19:51', 1, 'cancel', 12, NULL, '2021-05-07 04:13:04', '2021-05-07 04:15:30'),
(224, 8797669, 129, 50, 'commercial', NULL, 500.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-05-07 04:42:12', 1, 'cancel', 10, NULL, '2021-05-07 04:14:03', '2021-05-07 04:38:54'),
(225, 2094744, 129, 33, 'personal', NULL, 5000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 1, 'cancel', 10, NULL, '2021-05-07 04:45:43', '2021-05-07 04:46:01'),
(226, 5541865, 129, 33, 'personal', NULL, 8000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-05-07 04:52:35', 1, 'cancel', 14, NULL, '2021-05-07 04:46:15', '2021-05-07 04:47:47'),
(227, 1675677, 129, 50, 'personal', NULL, 60000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-07 04:47:06', '2021-05-07 04:51:57'),
(228, 7920855, 129, 33, 'personal', NULL, 8888.00, NULL, 98, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-05-07 04:56:20', 1, 'refuse', NULL, '{\"message_ar\":\"\\u062a\\u0645 \\u0631\\u0641\\u0636 \\u0627\\u0644\\u0637\\u0644\\u0628 \\u0628\\u0633\\u0628\\u0628 \\u0625\\u0633\\u062a\\u0631\\u062f\\u0627\\u062f \\u0627\\u0644\\u0645\\u0628\\u0644\\u063a \\u0627\\u0644\\u0645\\u062f\\u0641\\u0648\\u0639\",\"message_en\":\"The request was rejected due to the refund of the amount paid\"}', '2021-05-07 04:48:43', '2021-05-10 16:07:04'),
(229, 1988387, 130, 33, 'personal', NULL, 200.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'pending', NULL, NULL, '2021-05-07 04:55:33', '2021-05-07 04:55:33'),
(230, 7102371, 130, 50, 'personal', NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'pending', NULL, NULL, '2021-05-10 00:10:38', '2021-05-10 00:10:38'),
(231, 8739091, 131, 15, 'personal', NULL, 1000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 1, 'cancel', 11, NULL, '2021-05-10 04:03:45', '2021-05-10 04:08:38'),
(232, 1543382, 131, 15, 'personal', NULL, 6666.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-10 04:09:15', '2021-05-10 04:18:32'),
(233, 1225521, 132, 15, 'personal', NULL, 444.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-10 04:21:35', '2021-05-10 04:23:52'),
(234, 1033304, 133, 59, 'personal', NULL, 20000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-10 20:21:41', '2021-05-10 20:41:52'),
(235, 1753255, 133, 45, 'commercial', NULL, 10000.00, 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2021-05-10 20:32:19', 0, 'refuse', NULL, NULL, '2021-05-10 20:22:37', '2021-05-10 20:41:52'),
(236, 1344723, 133, 45, 'personal', NULL, 8000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-10 20:37:28', '2021-05-10 20:41:52'),
(237, 1143733, 133, 59, 'personal', NULL, 5000.00, NULL, 98, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2021-05-10 20:44:57', 0, 'finish', NULL, NULL, '2021-05-10 20:38:04', '2021-05-10 20:41:52'),
(238, 7347125, 132, 15, 'personal', NULL, 33333.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'pending', NULL, NULL, '2021-05-10 21:32:44', '2021-05-10 21:32:44'),
(239, 1794920, 136, 59, 'personal', NULL, 40000.00, NULL, NULL, '4320', NULL, 1, 1, 2, 4, 0, 0, NULL, 1, 'cancel', 12, NULL, '2021-05-10 22:10:43', '2021-05-10 22:14:25'),
(240, 3867136, 136, 45, 'personal', NULL, 30000.00, 49, NULL, '20160', NULL, 2, 4, 1, 3, 0, 0, '2021-05-10 22:18:33', 1, 'refuse', NULL, '{\"message_ar\":\"\\u062a\\u0645 \\u0631\\u0641\\u0636 \\u0627\\u0644\\u0637\\u0644\\u0628 \\u0628\\u0633\\u0628\\u0628 \\u0625\\u0633\\u062a\\u0631\\u062f\\u0627\\u062f \\u0627\\u0644\\u0645\\u0628\\u0644\\u063a \\u0627\\u0644\\u0645\\u062f\\u0641\\u0648\\u0639\",\"message_en\":\"The request was rejected due to the refund of the amount paid\"}', '2021-05-10 22:12:45', '2021-05-10 23:50:05'),
(241, 8565441, 137, 33, 'personal', NULL, 40000.00, 49, NULL, '20160', NULL, 1, 3, 3, 4, 0, 1, '2021-05-10 23:13:26', 0, 'refuse', NULL, NULL, '2021-05-10 23:05:38', '2021-05-10 23:23:25'),
(242, 6344628, 137, 59, 'commercial', NULL, 45000.00, NULL, NULL, '43200', NULL, 1, 4, 3, 4, 0, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-10 23:07:43', '2021-05-10 23:23:25'),
(243, 1436850, 137, 33, 'commercial', NULL, 300000.00, NULL, NULL, '20160', NULL, 2, 4, 5, 3, 1, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-10 23:16:36', '2021-05-10 23:23:25'),
(244, 1149381, 137, 59, 'personal', NULL, 20000.00, NULL, 98, '43200', NULL, 2, 4, 4, 3, 1, 1, '2021-05-10 23:26:35', 0, 'finish', NULL, NULL, '2021-05-10 23:18:44', '2021-05-10 23:23:25'),
(245, 1650906, 138, 33, 'personal', NULL, 1000.00, 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2021-05-12 14:27:19', 0, 'finish', NULL, NULL, '2021-05-11 00:47:10', '2021-05-12 14:29:59'),
(246, 1854112, 138, 59, 'personal', NULL, 3000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 1, 'cancel', 10, NULL, '2021-05-11 00:47:53', '2021-05-12 14:24:59'),
(247, 1410158, 135, 15, 'personal', NULL, 30000.00, 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2021-05-11 22:56:04', 0, 'refuse', NULL, NULL, '2021-05-11 22:50:25', '2021-05-11 23:47:18'),
(248, 1668702, 135, 15, 'personal', NULL, 50000.00, NULL, 49, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2021-05-11 23:52:04', 0, 'finish', NULL, NULL, '2021-05-11 22:57:29', '2021-05-11 23:47:18'),
(249, 1298052, 140, 59, 'commercial', NULL, 4000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-12 14:35:14', '2021-05-12 14:46:33'),
(250, 2131679, 140, 45, 'personal', NULL, 3000.00, 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2021-05-12 14:42:50', 0, 'refuse', NULL, NULL, '2021-05-12 14:36:58', '2021-05-12 14:46:33'),
(251, 5745062, 140, 45, 'personal', NULL, 2000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, 0, 'refuse', NULL, NULL, '2021-05-12 14:41:30', '2021-05-12 14:46:33'),
(252, 4372145, 140, 59, 'personal', NULL, 1000.00, NULL, 98, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2021-05-12 14:48:12', 0, 'finish', NULL, NULL, '2021-05-12 14:42:21', '2021-05-12 14:46:33'),
(253, 3734335, 141, 53, 'personal', NULL, 40000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'pending', NULL, NULL, '2021-05-19 20:43:29', '2021-05-19 20:43:29'),
(254, 4648328, 141, 59, 'commercial', NULL, 35000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, 0, 'pending', NULL, NULL, '2021-05-19 20:44:50', '2021-05-19 20:44:50'),
(255, 1675594, 141, 45, 'personal', NULL, 35000.00, 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, '2021-05-19 20:52:42', 0, 'pending', NULL, NULL, '2021-05-19 20:45:28', '2021-05-19 20:49:15'),
(256, 5641244, 141, 59, 'personal', NULL, 34000.00, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2021-05-19 20:57:37', 1, 'pending', NULL, NULL, '2021-05-19 20:51:56', '2021-05-19 20:58:04'),
(257, 1851020, 142, 63, 'personal', NULL, 20000.00, NULL, NULL, '20160', NULL, 1, 2, 1, 3, 0, 0, NULL, 0, 'pending', NULL, NULL, '2021-05-19 21:15:03', '2021-05-19 21:15:03');

-- --------------------------------------------------------

--
-- Table structure for table `offer_details`
--

CREATE TABLE `offer_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `offer_id` bigint(20) UNSIGNED NOT NULL,
  `label_id` bigint(20) UNSIGNED NOT NULL,
  `label_value_id` bigint(20) UNSIGNED DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` int(11) DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_offer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `offer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `old_offer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `account_type` enum('personal','commercial') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'personal',
  `order_type` enum('auction','tender') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tender',
  `service_type` enum('product','service') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'product',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `images` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commission` double DEFAULT NULL,
  `golden_commission` double DEFAULT NULL,
  `expected_price` decimal(15,2) NOT NULL,
  `delivery_duration` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bidding_duration` varchar(2000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_way_id` bigint(20) UNSIGNED DEFAULT NULL,
  `industry_id` bigint(20) UNSIGNED DEFAULT NULL,
  `warranty_id` bigint(20) UNSIGNED DEFAULT NULL,
  `payment_method_id` bigint(20) UNSIGNED DEFAULT NULL,
  `golden_offer` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'check if he choose or no',
  `active_golden_offer` tinyint(1) NOT NULL DEFAULT '0',
  `is_pay` tinyint(1) NOT NULL DEFAULT '0',
  `end_bidding_duration` datetime DEFAULT NULL,
  `time_out` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('pending','accepted','finish','refuse_system','refuse') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `message` text COLLATE utf8mb4_unicode_ci,
  `reason_rejection_id` bigint(20) UNSIGNED DEFAULT NULL,
  `is_accepted` tinyint(1) NOT NULL DEFAULT '0',
  `is_suspend` tinyint(1) NOT NULL DEFAULT '0',
  `order_rate` tinyint(4) DEFAULT NULL,
  `offer_rate` tinyint(4) DEFAULT NULL,
  `can_rate` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `uuid`, `user_id`, `user_offer_id`, `offer_id`, `old_offer_id`, `account_type`, `order_type`, `service_type`, `title`, `category_id`, `description`, `images`, `commission`, `golden_commission`, `expected_price`, `delivery_duration`, `bidding_duration`, `delivery_way_id`, `industry_id`, `warranty_id`, `payment_method_id`, `golden_offer`, `active_golden_offer`, `is_pay`, `end_bidding_duration`, `time_out`, `status`, `message`, `reason_rejection_id`, `is_accepted`, `is_suspend`, `order_rate`, `offer_rate`, `can_rate`, `created_at`, `updated_at`) VALUES
(127, 1945952, 50, 15, 213, NULL, 'commercial', 'auction', 'product', 'عربية 4\"✈️', 1, '✈️ ‎good ‎working ‎', '[\"files\\/GhdP0jcUf3oT2OplI7Seimage_picker2574175739778979507.jpg\"]', 49, NULL, 2500.00, NULL, '1440', NULL, NULL, NULL, NULL, 1, 0, 0, '2021-05-08 03:37:10', 1, 'refuse', '{\"message_ar\":\"\\u062a\\u0645 \\u0631\\u0641\\u0636 \\u0627\\u0644\\u0637\\u0644\\u0628 \\u0628\\u0633\\u0628\\u0628 \\u0625\\u0633\\u062a\\u0631\\u062f\\u0627\\u062f \\u0627\\u0644\\u0645\\u0628\\u0644\\u063a \\u0627\\u0644\\u0645\\u062f\\u0641\\u0648\\u0639\",\"message_en\":\"The request was rejected due to the refund of the amount paid\"}', NULL, -1, 0, NULL, NULL, 0, '2021-05-07 03:36:27', '2021-05-10 21:44:40'),
(128, 1768075, 15, NULL, NULL, NULL, 'personal', 'tender', 'product', 'Tender', 4, 'Tenfder', '[]', 49, NULL, 1000.00, NULL, '10080', NULL, NULL, NULL, NULL, 1, 0, 1, '2021-05-14 03:46:54', 1, 'refuse', NULL, 2, 1, 0, 2, NULL, 0, '2021-05-07 03:46:15', '2021-05-14 03:47:04'),
(129, 8799824, 15, NULL, NULL, 222, 'personal', 'tender', 'product', 'Tender ‎2', 4, 'tender ‎2', '[]', 49, 98, 1000.00, NULL, '20160', NULL, NULL, NULL, NULL, 1, 1, 0, '2021-05-07 05:48:03', 1, 'refuse', '{\"message_ar\":\"\\u062a\\u0645 \\u0631\\u0641\\u0636 \\u0627\\u0644\\u0637\\u0644\\u0628 \\u0628\\u0633\\u0628\\u0628 \\u0625\\u0633\\u062a\\u0631\\u062f\\u0627\\u062f \\u0627\\u0644\\u0645\\u0628\\u0644\\u063a \\u0627\\u0644\\u0645\\u062f\\u0641\\u0648\\u0639\",\"message_en\":\"The request was rejected due to the refund of the amount paid\"}', NULL, 1, 0, 3, 4, 1, '2021-05-07 03:54:27', '2021-05-10 21:36:33'),
(130, 9689680, 15, NULL, NULL, NULL, 'personal', 'tender', 'service', 'test', 4, 'test', '[]', NULL, NULL, 1000.00, NULL, '10080', NULL, NULL, NULL, NULL, 1, 0, 0, '2021-05-14 04:55:11', 1, 'refuse_system', '{\"message_ar\":\"\\u062a\\u0633\\u062a\",\"message_en\":\"test\"}', NULL, -1, 0, NULL, NULL, 0, '2021-05-07 04:55:01', '2021-05-10 03:28:23'),
(131, 1549589, 50, NULL, NULL, NULL, 'commercial', 'auction', 'product', 'testing ‎6 ‏Am ‎', 1, 'Ramy% ‎& ‏', '[\"files\\/3dFNRL8wL3FAq47pGpCaimage_picker4164795956500079857.jpg\"]', NULL, NULL, 369852.00, NULL, '1440', NULL, NULL, NULL, NULL, 0, 0, 0, '2021-05-11 04:03:35', 1, 'refuse', NULL, 1, 1, 0, NULL, NULL, 0, '2021-05-10 04:03:17', '2021-05-11 04:04:04'),
(132, 1869919, 50, NULL, NULL, NULL, 'commercial', 'tender', 'service', 'خدمة متاحة لمدة ساعة', 1, 'جديدة وحضري لمدة بسيطة', '[]', NULL, NULL, 450680.00, NULL, '1440', NULL, NULL, NULL, NULL, 0, 0, 0, '2021-05-11 04:28:48', 1, 'pending', NULL, NULL, 1, 0, NULL, NULL, 0, '2021-05-10 04:23:52', '2021-05-11 04:29:03'),
(133, 4158487, 33, 59, 237, 235, 'personal', 'tender', 'product', 'test1', 1, 'نبنينينسمسمسممنبننبنبنب بنبنيه نبنينينمذمذننب نبنينينمذم تطنينينني', '[]', 49, 98, 50000.00, NULL, '1440', NULL, NULL, NULL, NULL, 1, 1, 1, '2021-05-10 21:36:28', 1, 'finish', NULL, NULL, 1, 0, NULL, 1, 1, '2021-05-10 20:19:07', '2021-05-10 22:11:04'),
(134, 2667949, 15, NULL, NULL, NULL, 'personal', 'auction', 'product', 'uuuuuu', 7, 'uuuu', '[\"files\\/Gg9H9XsBdFmeu9Au9WSeimage_picker_604B82A3-70B5-47F9-B335-57C7A61FCC2C-76993-00002A10407BB7FD.jpg\"]', NULL, NULL, 10000.00, NULL, '10080', NULL, NULL, NULL, NULL, 1, 0, 0, '2021-05-17 21:34:19', 1, 'pending', NULL, NULL, 1, 0, NULL, NULL, 0, '2021-05-10 21:34:05', '2021-05-17 21:35:04'),
(135, 1453513, 50, 15, 248, 247, 'commercial', 'auction', 'product', 'مطبخ', 1, 'وصل', '[\"files\\/3xEfPcZ6NK2ChyDoul1Timage_picker_119D3549-1F15-44A1-B0E2-A39D1BD15BDC-1456-000001481CB70672.jpg\"]', 49, 98, 5000.00, NULL, '20160', NULL, NULL, NULL, NULL, 1, 1, 1, '2021-05-11 23:56:57', 1, 'finish', NULL, NULL, 1, 0, 1, 3, 1, '2021-05-10 22:13:27', '2021-05-11 23:57:03'),
(136, 2092729, 33, NULL, NULL, NULL, 'commercial', 'tender', 'product', 'test2', 6, '$,!:!:!:@,’', '[]', 49, NULL, 50000.00, '2880', '10080', 2, 2, 2, 4, 1, 0, 1, '2021-05-17 22:08:57', 1, 'finish', NULL, NULL, 1, 0, 3, 5, 1, '2021-05-10 22:08:22', '2021-05-17 22:09:05'),
(137, 5459975, 45, 59, 244, 241, 'commercial', 'tender', 'product', 'test3', 27, '(:&?:77gjbvuu hi kbvo', '[]', 49, 98, 50000.00, '4320', '2880', 1, 2, 3, 3, 1, 1, 0, '2021-05-11 00:15:10', 1, 'refuse', '{\"message_ar\":\"\\u062a\\u0645 \\u0631\\u0641\\u0636 \\u0627\\u0644\\u0637\\u0644\\u0628 \\u0628\\u0633\\u0628\\u0628 \\u0625\\u0633\\u062a\\u0631\\u062f\\u0627\\u062f \\u0627\\u0644\\u0645\\u0628\\u0644\\u063a \\u0627\\u0644\\u0645\\u062f\\u0641\\u0648\\u0639\",\"message_en\":\"The request was rejected due to the refund of the amount paid\"}', NULL, 1, 0, 5, 5, 1, '2021-05-10 22:56:56', '2021-05-11 00:48:11'),
(138, 7108026, 45, 33, 245, NULL, 'personal', 'auction', 'product', 'test4', 7, '$:$:', '[\"files\\/hHW1Itc6gPIg0xEWbfENimage_picker_2350306E-B3F6-445C-B656-A9A4183D87CC-13543-00000A3D1AA636F5.jpg\"]', 49, NULL, 2000.00, NULL, '4320', NULL, NULL, NULL, NULL, 1, 0, 1, '2021-05-13 23:48:15', 1, 'finish', NULL, NULL, 1, 0, 4, 1, 1, '2021-05-10 23:48:15', '2021-05-13 23:49:04'),
(139, 2508588, 59, NULL, NULL, NULL, 'personal', 'tender', 'product', 'hdh', 24, 'hah', '[]', NULL, NULL, 50000.00, NULL, '4320', NULL, NULL, NULL, NULL, 1, 0, 0, '2021-05-15 02:38:41', 1, 'refuse_system', '{\"message_ar\":\"\\u0637\\u0644\\u0628\\u0643 \\u063a\\u064a\\u0631 \\u0645\\u062a\\u0648\\u0641\\u0631\",\"message_en\":\"your request is not avilable\"}', NULL, -1, 0, NULL, NULL, 0, '2021-05-12 02:38:41', '2021-05-12 02:42:49'),
(140, 1250868, 33, 59, 252, 250, 'personal', 'tender', 'product', 'test5', 26, 'jdjdndbd', '[]', 49, 98, 5000.00, NULL, '10080', NULL, NULL, NULL, NULL, 1, 1, 1, '2021-05-12 15:39:13', 1, 'finish', NULL, NULL, 1, 0, 2, NULL, 1, '2021-05-12 14:33:49', '2021-05-12 15:40:04'),
(141, 9995306, 33, 59, 256, 255, 'personal', 'tender', 'product', 'مرسيدس ‏S500', 1, 'مستعمل لون احمر 2010', '[]', 49, 98, 50000.00, NULL, '1440', NULL, NULL, NULL, NULL, 1, 1, 1, '2021-05-19 21:50:37', 1, 'pending', NULL, NULL, 1, 0, NULL, NULL, 0, '2021-05-19 20:41:25', '2021-05-19 21:51:04'),
(142, 6139806, 33, NULL, NULL, NULL, 'commercial', 'tender', 'product', 'رطرط', 4, 'تطويرير', '[]', NULL, NULL, 500000.00, '1440', '4320', 2, 2, 4, 3, 0, 0, 0, '2021-05-22 21:05:08', 1, 'pending', NULL, NULL, 1, 0, NULL, NULL, 0, '2021-05-19 21:05:08', '2021-05-22 21:06:04'),
(143, 1091581, 33, NULL, NULL, NULL, 'personal', 'auction', 'product', 'corola', 1, 'bzbsbs', '[\"files\\/akGxcze6NE69uhI8edqTimage_picker_2EED87F8-A64C-4A39-9C52-2095BD77A2CB-2383-000001744F267700.jpg\"]', NULL, NULL, 50000.00, NULL, '20160', NULL, NULL, NULL, NULL, 0, 0, 0, '2021-06-02 21:04:59', 0, 'pending', NULL, NULL, 1, 0, NULL, NULL, 0, '2021-05-19 21:02:40', '2021-05-19 21:04:59');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `label_id` bigint(20) UNSIGNED NOT NULL,
  `label_value_id` bigint(20) UNSIGNED DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

CREATE TABLE `payment_methods` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `is_suspend` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `is_suspend`, `created_at`, `updated_at`) VALUES
(1, 0, '2021-01-26 10:22:13', '2021-01-26 10:22:13'),
(2, 1, '2021-01-26 10:22:13', '2021-05-07 00:31:00'),
(3, 0, '2021-01-26 10:22:25', '2021-01-26 10:22:25'),
(4, 0, '2021-01-26 10:22:25', '2021-01-26 10:22:25'),
(5, 0, '2021-01-26 10:25:41', '2021-05-06 23:30:35');

-- --------------------------------------------------------

--
-- Table structure for table `payment_method_translations`
--

CREATE TABLE `payment_method_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payment_method_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_method_translations`
--

INSERT INTO `payment_method_translations` (`id`, `payment_method_id`, `locale`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'ar', 'دفعات', '2021-01-26 10:23:20', '2021-05-07 00:28:55'),
(2, 1, 'en', 'Installment ‎', '2021-01-26 10:23:20', '2021-05-07 00:28:55'),
(3, 2, 'ar', 'تقسيط', '2021-01-26 10:23:47', '2021-05-07 00:29:46'),
(4, 2, 'en', 'Installment ‎', '2021-01-26 10:23:47', '2021-05-07 00:29:46'),
(5, 3, 'ar', 'بالاجل', '2021-01-26 10:23:20', '2021-05-07 00:27:36'),
(6, 3, 'en', 'Credit ‎', '2021-01-26 10:23:20', '2021-05-07 00:27:36'),
(7, 4, 'ar', 'حوالة', '2021-01-26 10:23:47', '2021-05-07 00:28:10'),
(8, 4, 'en', 'Transfer ‎', '2021-01-26 10:23:47', '2021-05-07 00:28:10'),
(9, 5, 'ar', 'كاش', '2021-01-26 10:26:06', '2021-05-07 00:27:08'),
(10, 5, 'en', 'Cash', '2021-01-26 10:26:06', '2021-05-07 00:27:08');

-- --------------------------------------------------------

--
-- Table structure for table `promo_codes`
--

CREATE TABLE `promo_codes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_at` timestamp NULL DEFAULT NULL,
  `end_at` timestamp NULL DEFAULT NULL,
  `times` int(10) UNSIGNED DEFAULT NULL,
  `percent` decimal(8,2) DEFAULT NULL,
  `money` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_suspend` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `promo_codes`
--

INSERT INTO `promo_codes` (`id`, `code`, `start_at`, `end_at`, `times`, `percent`, `money`, `is_suspend`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'Day1', '2021-04-18 22:00:00', '2021-04-23 22:00:00', NULL, NULL, '2500', 0, 0, '2021-04-13 09:44:02', '2021-04-14 20:32:26'),
(2, '30500', '2021-04-13 09:30:00', '2021-04-30 09:30:00', NULL, NULL, '1500', 0, 0, '2021-04-13 09:56:06', '2021-04-13 09:56:06'),
(3, 'ffghffgh', '2021-04-21 22:00:00', '2021-04-28 22:00:00', NULL, NULL, '500', 0, 0, '2021-04-13 19:50:29', '2021-04-13 19:50:29'),
(4, 'test ‎', '2021-04-29 21:00:00', '2021-05-02 21:00:00', NULL, NULL, '1000', 0, 0, '2021-04-26 12:49:47', '2021-04-30 16:59:57');

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `rate` decimal(5,2) DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reason_rejections`
--

CREATE TABLE `reason_rejections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` enum('offer','cancel_order','complaint','Interact_order') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'cancel_order',
  `is_suspend` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reason_rejections`
--

INSERT INTO `reason_rejections` (`id`, `type`, `is_suspend`, `created_at`, `updated_at`) VALUES
(1, 'cancel_order', 0, '2021-01-26 11:41:45', '2021-01-26 11:41:45'),
(2, 'cancel_order', 0, '2021-01-26 11:41:45', '2021-01-26 11:41:45'),
(3, 'cancel_order', 0, '2021-01-26 11:46:25', '2021-01-26 11:46:28'),
(4, 'Interact_order', 0, '2021-01-26 11:50:36', NULL),
(5, 'Interact_order', 0, '2021-01-26 11:50:38', NULL),
(6, 'Interact_order', 0, '2021-01-26 11:50:41', '2021-04-20 20:48:13'),
(7, 'complaint', 0, '2021-01-26 11:51:09', '2021-01-26 11:51:09'),
(8, 'complaint', 0, '2021-01-26 11:51:09', '2021-01-26 11:51:09'),
(10, 'offer', 0, '2021-01-26 11:59:30', '2021-01-26 11:59:30'),
(11, 'offer', 0, '2021-01-26 11:59:30', '2021-04-26 12:38:10'),
(12, 'offer', 0, '2021-04-23 23:18:44', '2021-05-05 13:41:50'),
(13, 'cancel_order', 0, '2021-04-23 23:19:18', '2021-04-23 23:19:18'),
(14, 'offer', 0, '2021-05-05 13:45:38', '2021-05-05 13:45:38'),
(15, 'complaint', 0, '2021-05-05 13:53:42', '2021-05-05 13:53:42'),
(16, 'complaint', 0, '2021-05-05 13:54:53', '2021-05-05 13:54:53');

-- --------------------------------------------------------

--
-- Table structure for table `reason_rejection_translations`
--

CREATE TABLE `reason_rejection_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reason_rejection_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reason_rejection_translations`
--

INSERT INTO `reason_rejection_translations` (`id`, `reason_rejection_id`, `locale`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'ar', 'لم تجد عرض مناسب حتي الأن', NULL, NULL),
(2, 1, 'en', ' You haven\'t found a suitable offer yet', '2021-01-26 11:46:05', '2021-01-26 11:46:05'),
(3, 2, 'ar', 'لا يوجد سعر مناسب', '2021-01-26 11:46:05', '2021-01-26 11:46:05'),
(4, 2, 'en', 'There is no suitable price', '2021-01-26 11:46:05', '2021-01-26 11:46:05'),
(5, 3, 'ar', 'عدم الرغبة في متابعة الطلب', '2021-01-26 11:47:05', '2021-05-05 13:47:41'),
(6, 3, 'en', 'No interest to continue   ‎', '2021-01-26 11:47:05', '2021-05-05 13:48:44'),
(7, 4, 'ar', 'الطلب غير واضح ', NULL, NULL),
(8, 4, 'en', 'The request is unclear', '2021-01-26 11:46:05', '2021-01-26 11:46:05'),
(9, 5, 'ar', 'الطلب غير مقبول', '2021-01-26 11:46:05', '2021-01-26 11:46:05'),
(10, 5, 'en', 'The request is not acceptable', '2021-01-26 11:46:05', '2021-01-26 11:46:05'),
(11, 6, 'ar', 'الطلب غير تنافسي', '2021-01-26 11:47:05', '2021-01-26 11:47:05'),
(12, 6, 'en', 'The request is not competitive', '2021-01-26 11:47:05', '2021-05-05 13:55:37'),
(13, 7, 'ar', 'حدث ‏خطأ ‏في ‏العملية', NULL, '2021-05-05 13:52:06'),
(14, 7, 'en', 'An error occurred in the process', '2021-01-26 11:46:05', '2021-05-05 13:52:06'),
(15, 8, 'ar', 'عدم ‏إلتزام ‏الطرف ‏الاخر ‏', '2021-01-26 11:46:05', '2021-05-05 13:53:06'),
(16, 8, 'en', 'Non-commitment of the other party', '2021-01-26 11:46:05', '2021-05-05 13:53:06'),
(19, 10, 'ar', 'عدم جدية صاحب الطلب', '2021-01-26 12:02:27', '2021-01-26 12:02:27'),
(20, 10, 'en', 'The lack of seriousness of the applican', '2021-01-26 12:02:27', '2021-01-26 12:02:27'),
(21, 11, 'ar', 'متطلبات ‏الطلب ‏غير ‏قابلة ‏للتنافس', '2021-01-26 12:02:27', '2021-05-05 13:43:59'),
(22, 11, 'en', 'The ‎request ‎requirements ‎not ‎possible ‎to ‎compete ‎', '2021-01-26 12:02:27', '2021-05-05 13:43:59'),
(23, 12, 'ar', 'عدم الرغبة في المتابعة', '2021-04-23 23:18:44', '2021-05-05 13:48:33'),
(24, 12, 'en', 'No interest to continue   ‎', '2021-04-23 23:18:44', '2021-05-05 13:48:33'),
(25, 13, 'ar', 'مدة ‏تقديم ‏العروض', '2021-04-23 23:19:18', '2021-05-05 13:50:35'),
(26, 13, 'en', 'Duration ‎of ‎the ‎offer ‎submitting ‎', '2021-04-23 23:19:18', '2021-05-05 13:50:35'),
(27, 14, 'ar', 'مدة ‏الطرح ‏المنافسة ‏', '2021-05-05 13:45:38', '2021-05-07 20:10:16'),
(28, 14, 'en', 'Duration of the competition offering', '2021-05-05 13:45:38', '2021-05-05 13:45:38'),
(29, 15, 'ar', 'بيانات الاتصال غير صحيحه', '2021-05-05 13:53:42', '2021-05-05 13:53:42'),
(30, 15, 'en', 'Contact information is incorrect', '2021-05-05 13:53:42', '2021-05-05 13:53:42'),
(31, 16, 'ar', 'لم تتم العملية ينجاح', '2021-05-05 13:54:53', '2021-05-07 05:14:18'),
(32, 16, 'en', 'The ‎ ‎operation ‎didn\'t ‎complete ‎ ‎successfully ‎', '2021-05-05 13:54:53', '2021-05-07 05:14:18');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `body`, `created_at`, `updated_at`) VALUES
(1, 'commission_order', '49', '2021-02-01 13:47:47', '2021-05-05 10:26:16'),
(2, 'commission_offer', '49', '2021-02-01 13:47:47', '2021-05-05 10:26:22'),
(3, 'waitting_to_accept_offer_minutes', '5', '2021-02-01 13:47:47', '2021-05-07 02:40:52'),
(4, 'commission_golden_order', '98', '2021-02-01 13:47:47', '2021-05-05 10:26:52'),
(5, 'commission_golden_offer', '98', '2021-02-01 13:47:47', '2021-05-05 10:26:59'),
(6, 'time_active_golden_offer', '60', '2021-02-08 11:18:14', '2021-05-05 12:11:47'),
(7, 'about_us_ar', '<p style=\"text-align:right\"><span style=\"font-size:14px\"><strong>بارجين</strong></span></p>\n\n<p style=\"text-align:right\">هي أول وأضخم منصة صفقات تنافسية سعودية تأسست في سنة 2020 خلال أزمة جائحة كورونا، لتسهل على المشتري والبائع في عملية طرح المناقصات والمزايدات على المنتجات والخدمات الخاصة او المشاريع، والوصول لافضل الفرص على الطلبات والعروض المتاحة في السوق بشكل احترافي ودقيق ومن دون الحاجة الى بذل الجهد والوقت والمال من خلال الطرق الاعتيادية</p>', '2021-02-14 15:56:05', '2021-04-09 04:21:45'),
(8, 'about_us_en', '<p><b>Bargain </b></p><p>is the first and largest Saudi competitive deals platform, established in 2020 during the Corona pandemic crisis, to facilitate the buyer and seller in the process of bidding and auctioning on products and services for privet or project’s needs, and accessing the best opportunities for requests and offers available in the market professionally, accurately and without the need to spend effort, time and money through regular methods</p>', NULL, '2021-04-14 09:43:03'),
(9, 'terms_user_ar', '<p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">بارجين هي منصة تسهل على المشتري والبائع في عملية طرح المناقصات والمزايدات على المنتجاتأوالخدمات الخاصة او المشاريع، والوصول لافضل الفرص على الطلبات والعروض المتاحةفي السوق بشكل احترافي ودقيق ومن دون الحاجة الى بذل الجهد والوقت والمال من خلالالطرق الاعتيادية. <o:p></o:p></span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">وعلىذلك نرجو من المستخدمين الالتزام بالشروط والاحكام أدناه، حيث أن قبولك كمستخدم فيتطبيق منصة بارجين هو إقرارك على فهمك وموافقتك الكاملة لتلك الشروط والاحكام والتزامكبها، مما يمنح&nbsp;لنا<b> </b>الحق الكامل في حال مخالفتك لاحدى تلك الشروط أوالاحكام بحجب عضويتك كمستخدم أومنعك من الوصول للتطبيق دون الحاجة لإخطارك بذلك أوتضعك تحت المسائلة القانونية وطلب التعويض المناسب بحسب حجم الضرر أو تبعات لتلكالمخالفة</span><span dir=\"LTR\"></span><span dir=\"LTR\"></span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span>.<br><!--[if !supportLineBreakNewLine]--><br><!--[endif]--><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpFirst\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">1.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">يضمن المستخدم أن جميع المعلومات والبيانات التي يقوم بإضافتها فيحسابه في بارجين صحيحة تماما، ويتحمل المستخدم كامل المسؤولية عن أي معلومات خاطئةيقوم بإضافتها.<o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">2.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">يلتزم المستخدم بعدم إنشاء أكثر من حساب واحد في المنصة، وفي حالواجه مشكلة في حسابه الأول فعليه المتابعة مع فريق الدعم لحل المشكلة.<o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">3.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">يتعهد المستخدم المسجل في بارجين أنه الوحيد الذي يستخدم الحسابويكون مسؤولا عن كل ما يتم تدواله عبر حسابه، ولا يسمح له أن يقوم أكثر من شخصباستخدام حسابه.<o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">4.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">يلتزم المستخدم المسجل في منصة بارجين أن عمره أكبر من 18 سنة، وقديطلب بارجين وثائق تثبت ذلك في حال دعت الحاجة.<o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">5.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">عدم التسبب بإلحاق الضرر المباشر أو الغير مباشر سواء للمنصة أومستخدميه بشكل نهائي.<o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">6.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span dir=\"RTL\"></span><span dir=\"LTR\"></span><span dir=\"LTR\"></span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span>&nbsp;</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">يتماقتطاع رسوم منصة بارجين من المستخدمين سواءً كان مشتري او بائع خلال المرحلةالنهائية لقبول العرض او الطلب على العملية.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">7.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">بارحين هي منصة عملية لتقديم أفضل عرض منافس من خلال مستخدميها ولاتتحمل اي مسؤولية في حالة عدم قبول الطلب او العرض بعد الانتهاء العملية خارجالمنصة.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">8.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">بارجين لاتتحمل الامور التعاقدية أو القانوية والتزامتها المنصوصةبين المستخدمين سواء كانت داخل او خارج المنصة. </span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">9.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp; </span></span><!--[endif]--><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">في حال وجود مخالفة صريحة في عدم التزام المستخدم&nbsp; بشروط الطلب أو العرض او وجود سبب يتعلق بفشلعملية المنافسة داخل منصة بارجين فقط، فإنه يمكن إعادة الرصيد في محفظة المستخدم عبرقيامه بفتح تذكرة دعم فني</span><span dir=\"LTR\"></span><span dir=\"LTR\"></span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span>.</span><span dir=\"RTL\"></span><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span dir=\"RTL\"></span><span dir=\"RTL\"></span>&nbsp;</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">10.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>عدمتداول عمليات مشبوهة بشكل عام أو خاص من خلال الدول الممنوع التعامل معا حسبالقوانين والانظمة السعودية.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">11.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>عدماتمام عملية أوسيلة دفع تتم من حسابات مسروقة أو غير مملوكة من قبل المستخدم.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">12.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>عدمالتلاعب بأسعار السلع أو الخدمات سواء في البيع او الشراء وإلحاق الضرر بالمستخدمينأو بأسعار السوق بشكل عام</span><span dir=\"LTR\"></span><span dir=\"LTR\"></span><span lang=\"AR-SA\" dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span> </span><span dir=\"RTL\"></span><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span dir=\"RTL\"></span><span dir=\"RTL\"></span>&nbsp;أوخاص.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">13.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>عدماستخدام منصة بارجين كوسيلة لجمع البيانات والمعلومات والتحاليل السوقية، أوكادواتتسويقية او تجارية.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">14.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>عدمانتحال منصة بارجين كصفة رسمية او غير رسمية في اعمال تجارية او تسويقية او بنيابةعنها.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">15.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>عدمسرقة الافكار والمعلومات والبيانات وحقوق الملكية الفكرية لها بشكل عام أو خاص.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">16.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>عدمتكرار طلبات او عروض من دون انهائها أودفع رسومها للمنصة.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">17.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>عدماستغلال أو الاستفادة من معلومات او بيانات لعمليات المنصة والعمل عليها خارجالمنصة.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">18.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>عدممشاركة بيانات او معلومات التواصل الشخصي للمستخدم في الطلب نفسه.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">19.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>عدمالطلب والمنافسه على سلع او خدمات منافية لقوانين المملكة العربية السعوديةوشريعتها الاسلامية.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">20.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>بارجينغير مسؤولة عن اي اضرار او تبعات الناتجة حال عدم التزام احد المستخدمين بالطلب اوالعرض المنافس عليه بشكل عام أو خاص.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">21.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\"><span style=\"font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;</span></span><!--[endif]--><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">بارجينغير مسؤولة عن مشاركتك لأي بيانات او معلومات شخصية خاصة أو بنكية أو غيرها معالمستخدمين والتي تكون على مسؤوليتك الشخصية.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">22.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\"><span style=\"font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;</span></span><!--[endif]--><span dir=\"RTL\"></span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">يقر المستخدم بأنه المسؤول الوحيد عن طبيعة الاستخدامالذي تحدد المنصة، وتخلي إدارة المنصة طرفها، إلى أقصى مدى يجيزه القانون، من كاملالمسؤولية عن أية خسائر أو أضرار أو نفقات أو مصاريف يتكبدها المستخدم أو يتعرضلها هو أو أي طرف آخر من جراء استخدام المنصة أو العجز عن استخدامه.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">23.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>يختار المستخدم كلمة سر / مرور لحسابه، وسيدخل عنوانابريديا خاصا به لمراسلته عليه، وتكون مسؤولية حماية كلمة السر هذه وعدم مشاركتهاأو نشرها، وفي حال حدوث أي معاملات باستخدام كلمة السر هذه فسيتحمل المستخدم كافةالمسؤوليات المترتبة على ذلك، دون أدنى مسؤولية على منصة بارجين.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">24.</span><span lang=\"AR-SA\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;&nbsp;</span>يتحمل المستخدم كامل المسؤولية عن جميع الطلبات والعروضوالمحتويات الخاصة به، التي يرفعها وينشرها عبر المنصة.</span><span dir=\"LTR\" style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><o:p></o:p></span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">25.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;</span></span><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;</span><span style=\"font-family: &quot;Times New Roman&quot;, serif; font-size: 16pt; text-indent: -0.25in;\">لا يمكن المستخدم حذف حسابه من المنصة بأي وسيلة ولاتعديل اسم المستخدم لحسابه، بسبب تعلق الحساب بأمور مالية وحقوق مستخدمين آخرينيمكن الرجوع لها في أي وقت.</span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">26.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;</span></span><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;</span><span style=\"font-family: &quot;Times New Roman&quot;, serif; font-size: 16pt; text-indent: -0.25in;\">يتعهد المستخدم بمشاركة جميع البيانات والمعلومات الشخصيةوالاتصال الصحيحة ، حيث ستعمل المنصة على توثيق تلك البيانات والمعلومات لمشاركتهامع المستخدمين في حالة قبول العرض او الطلب ولاتمام عملية الطرح على المنافسةالمرجوة.</span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">27.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;</span></span><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp;&nbsp;&nbsp;</span><span style=\"font-family: &quot;Times New Roman&quot;, serif; font-size: 16pt; text-indent: -0.25in;\">يتم التعامل مع جميع المعلومات وبيانات المستخدمين فيحالات التطوير الخدمات التي تقدمها المنصة وفي حال التعاقد مع أطرف أخرى بهدف التطويروتحسين الخدمات التي نقدمها، أو في الدراسات البحثية والاعمال التسويقية، أو فيحال كنا ملزمين قانونيا بأمر قضائي بالكشف عن أي من تلك معلومات والبيانات.</span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">28.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;</span></span><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp; &nbsp;</span><span style=\"font-family: &quot;Times New Roman&quot;, serif; font-size: 16pt; text-indent: -0.25in;\">تحتفظ إدارة المنصة بالحق في مراقبة أي محتوى يدخله المستخدم،دون أن يكون ذلك لزاما عليها، لذا تحتفظ بالحق (من دون التزام) في حذف أو إزالة أوتحرير أي مواد مدخلة من شأنها انتهاك شروط وأحكام المنصة دون الرجوع للمستخدم.</span></p><p class=\"MsoListParagraphCxSpMiddle\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">29.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;</span></span><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp; &nbsp;</span><span lang=\"AR-SA\" style=\"text-indent: -0.25in; font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">جميعالبنود والشروط والأحكام والمنازعات القانونية للقوانين والتشريعات والأنظمةالمعمول بها في المملكة العربية السعودية</span><span dir=\"LTR\" style=\"text-indent: -0.25in;\"></span><span dir=\"LTR\" style=\"text-indent: -0.25in;\"></span><span dir=\"LTR\" style=\"text-indent: -0.25in; font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span>.</span></p><p class=\"MsoNormal\" dir=\"RTL\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"></p><p class=\"MsoListParagraphCxSpLast\" dir=\"RTL\" style=\"margin: 0in 0.5in 7.5pt 0in; text-indent: -0.25in; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; direction: rtl; unicode-bidi: embed;\"><!--[if !supportLists]--><span style=\"font-size: 13.5pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">30.<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;</span></span><span style=\"font-family: &quot;Times New Roman&quot;; font-size: 9.33333px;\">&nbsp; &nbsp;</span><span lang=\"AR-SA\" style=\"text-indent: -0.25in; font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">يحقلمنصة بارجين التعديل على هذه الاتفاقية في أي وقت وتعتبر ملزمة بعد الإعلان عنالتحديث للتطبيق</span><span dir=\"LTR\" style=\"text-indent: -0.25in;\"></span><span dir=\"LTR\" style=\"text-indent: -0.25in;\"></span><span dir=\"LTR\" style=\"text-indent: -0.25in; font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\"><span dir=\"LTR\"></span><span dir=\"LTR\"></span>.</span></p>', NULL, '2021-05-06 22:26:04');
INSERT INTO `settings` (`id`, `key`, `body`, `created_at`, `updated_at`) VALUES
(10, 'terms_user_en', '<p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">Bargainis a platform that facilitate the buyer and seller in the process of biddingand auctioning on products and services for privet or project’s needs, andaccessing the best opportunities for requests and offers available in themarket professionally, accurately and without the need to spend effort, timeand money through regular methods<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">Accordingly,we ask users to abide by the terms and conditions below, as your acceptance asa user in the application of the Bargain platform is your acknowledgment ofyour understanding and full acceptance of these terms and conditions and yourcommitment to them, which gives us the full right in case you violate one ofthese terms or conditions to withhold your membership as a user or prevent you fromaccessing the application and without the need to notify you of that or put youunder legal liability and request appropriate compensation according to thesize of the damage or the consequences of that violation.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">&nbsp;</span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">1.The user guarantees that all the information and data that been added to his/heraccount in Bargain are completely correct, and the user bears fullresponsibility for any wrong information been added.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">2.The user is obligated not to create more than one account in the platform.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">3.The registered user in Bargain undertakes that he/she is the only one who usesthe account and is responsible for everything that is circulated through his/heraccount, and he/she is not allowed to have more than one person to use theaccount.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">4.The registered user on the Bargain platform is obligated to be over the age of18 years, and Bargain may request documents to prove this if the need arises.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">5.Never cause harm, direct or indirect, to either the platform or its users.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">6.The bargain platform fees deducted from the users, whether buyer or sellerduring the final stage to accept the offer or request for the process.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">7.Bargain is a practical platform to present the best competitive offer throughits users and it does not bear any responsibility in the event that the requestor offer is not accepted after the process outside the platform is completed.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">8.Bargain does not assume contractual or legal matters and its obligationsstipulated between users, whether inside or outside the platform.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">9.In the event that there is an explicit violation in the user\'s failure tocomply with the terms of the request or offer, or there is a reason related tothe failure of the competition process within the Bargain platform only, thenthe balance in the user\'s wallet can be returned by opening a technical supportticket.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">10.Not to circulate suspicious operations, in general or in particular, throughcountries. It is prohibited to deal with each other according to Saudi laws andregulations.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">11.Failure to complete a payment method that takes place from accounts that arestolen or not owned by the user.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">12.Not to tamper with the prices of goods or services, whether in buying orselling, and harm users or market prices in general or in particular.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">13.Not to use the Bargain platform as a means of gathering data, information,market analysis, marketing or commercial tools.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">14.Not to impersonate the Bargain platform as an official or unofficial characterin a commercial or marketing business or on its behalf.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">15.Not to steal ideas, information, data, and intellectual property rights, ingeneral or in particular.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">16.Not to repeat requests and offers without terminating them or paying fees forthe platform.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">17.Not to exploit or benefit from information or data for the platform\'soperations and to work on it outside the platform.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">18.Not to share the user\'s personal contact data or information in the applicationitself.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">19.Not to demand and compete for goods or services that are contrary to the lawsof the Kingdom of Saudi Arabia and its Islamic Sharia.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">20.Bargain is not responsible for any damages or consequences resulting fromfailure of one of the users to comply with the demand or the competing offer,in general or in particular.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">21.Bargain is not responsible for your sharing of any private, bank or otherpersonal data or information with users that is at your own risk.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">22.The user acknowledges that he/she is solely responsible for the nature of theuse that defines the platform, and the platform’s management releases itsparty, to the maximum extent permitted by law, from all responsibility for anylosses, damages, expenses or expenses incurred by the user or incurred by him/heror any other party as a result of using The platform or the inability to useit.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">23.The user chooses a password / password for his/her account, and he will enterhis own mailing address to correspond with him/her, and it is the responsibilityto protect this password and not to share or publish it, and in the event thatany transactions occur using this password, the user will bear all theresponsibilities arising from that, without any responsibility for bargainplatform.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">24.The user bears full responsibility for all his/her requests, offers andcontent, which he uploads and publishes through the platform.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">25.The user cannot delete his account from the platform by any means or modify theusername of his/her account, due to the account’s attachment to financialmatters and the rights of other users that can be referred to at any time.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">26.The user undertakes to share all data, personal information and correctcontact, as the platform will document that data and information to share withusers in the event of acceptance of the offer or request and to complete theoffering process to the desired competition.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">27.All information and user data are dealt with in cases of developing theservices provided by the platform and in the event of contracting with otherparties in order to develop and improve the services we provide, or in researchstudies and marketing works, or if we are legally obligated by a judicial orderto disclose any of that information. And data.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">28.The management of the platform reserves the right to monitor any contententered by the user, without being obligated to do so, so it reserves the right(without obligation) to delete, remove or edit any entered materials thatviolate the terms and conditions of the platform without referring to the user.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">29.All terms, conditions, provisions and legal disputes of the laws, legislationsand regulations in force in the Kingdom of Saudi Arabia.<o:p></o:p></span></p><p class=\"MsoNormal\" style=\"margin-bottom: 7.5pt; line-height: 150%; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\"><span style=\"font-size: 16pt; line-height: 150%; font-family: &quot;Times New Roman&quot;, serif;\">30.The Bargain platform has the right to amend this agreement at any time and itis considered binding after announcing the update of the application.<font color=\"#333333\"><o:p></o:p></font></span></p>', NULL, '2021-05-06 22:24:51'),
(11, 'linkedin', 'https://www.linkedin.com/company/bargainsa', '2021-03-30 13:42:42', '2021-03-30 13:42:42'),
(12, 'twitter', 'https://twitter.com/bargain1sa', '2021-03-30 13:42:42', '2021-03-30 13:42:42'),
(13, 'facebook', 'https://www.facebook.com/Bargain1sa/', '2021-03-30 13:43:13', '2021-03-30 13:43:13'),
(14, 'instagram', 'https://www.instagram.com/Bargain1sa/', '2021-03-30 13:43:13', '2021-03-30 13:43:13'),
(15, 'snapchat', 'https://www.snapchat.com/add/Bargain1sa', '2021-03-30 13:43:13', '2021-05-06 22:20:29'),
(16, 'youtube', 'https://youtube.com/channel/UCl8iZfx3gL5Z16wzmssG9HA', '2021-03-30 15:07:59', '2021-03-30 15:07:59'),
(17, 'time_order_rate', '5', '2021-04-13 17:42:50', '2021-05-11 23:50:24'),
(18, 'resend_rate_time', '1441', '2021-04-13 19:46:46', '2021-05-06 22:21:29'),
(19, 'email_bargain', 'bargain@bargain.com.sa', '2021-04-14 19:01:48', '2021-04-14 19:01:48'),
(20, 'system_alert_minutes', '60', '2021-04-20 21:35:27', '2021-05-07 20:09:25');

-- --------------------------------------------------------

--
-- Table structure for table `static_data`
--

CREATE TABLE `static_data` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` enum('intro_page','golden_offer') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'intro_page',
  `image` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `static_data`
--

INSERT INTO `static_data` (`id`, `type`, `image`, `color`, `created_at`, `updated_at`) VALUES
(1, 'intro_page', NULL, '#ff1e90ff', '2021-02-14 16:34:50', '2021-05-05 10:31:44'),
(2, 'intro_page', NULL, '#fffbbc05', '2021-02-14 16:34:54', '2021-05-05 10:31:23'),
(4, 'golden_offer', NULL, NULL, '2021-02-17 17:26:04', '2021-02-17 17:26:04'),
(5, 'golden_offer', NULL, NULL, '2021-02-17 17:26:04', '2021-02-17 17:26:04'),
(7, 'intro_page', NULL, '#ffea4335', '2021-03-18 09:08:59', '2021-05-05 10:30:46');

-- --------------------------------------------------------

--
-- Table structure for table `static_data_translations`
--

CREATE TABLE `static_data_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `static_data_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `static_data_translations`
--

INSERT INTO `static_data_translations` (`id`, `static_data_id`, `locale`, `title`, `image`, `name`, `color`, `created_at`, `updated_at`) VALUES
(1, 1, 'ar', 'ارفع طلبك', 'order.png', 'لافضل العروض التنافسية على السلع والخدمات', '#1E90FF', '2021-02-14 16:35:43', '2021-05-05 10:31:44'),
(2, 1, 'en', 'Upload your request', 'order.png', 'For the most competitive offers on products and services', '#1E90FF', '2021-02-14 16:35:43', '2021-05-05 10:31:44'),
(3, 2, 'ar', 'قدم عرضك', 'offer.png', 'جميع طلبات السوق على  السلع والخدمات في مكان واحد', '#FBBC05', NULL, '2021-05-05 10:31:23'),
(4, 2, 'en', 'Submit your bid', 'offer.png', 'All market demands on products and services in one place', '#FBBC05', '2021-02-14 16:36:30', '2021-05-05 10:31:23'),
(9, 4, 'ar', NULL, NULL, 'بعد قبولك للعرض المناسب لك، سوف نعمل لتقديم لك عرض تنافسي أفضل من المقبول لديك', NULL, '2021-02-17 17:28:32', '2021-05-05 13:30:53'),
(10, 4, 'en', NULL, NULL, 'After accepting the suitable offer for you, ‏we will work to propose you the most competitive offer vs. ‏what you had already.', NULL, '2021-02-17 17:27:54', '2021-05-05 13:30:53'),
(11, 5, 'ar', NULL, NULL, 'ويمكنك حينها قبول احد تلك العروض التنافسية بمقابل قيمة 89 ريال سعودي', NULL, NULL, '2021-05-05 13:36:48'),
(12, 5, 'en', NULL, NULL, 'And you can accept one of these competitive offers with ‎89 ‏SAR', NULL, '2021-02-17 17:27:54', '2021-05-05 13:36:48'),
(15, 7, 'ar', 'المزاد', 'auction.png', 'ارفع طلب على مزادك, وزايد على سلع تنافسية', '#EA4335', NULL, '2021-05-05 10:30:46'),
(16, 7, 'en', 'Auction', 'auction.png', 'Upload your auction order and bid on competitive items', '#EA4335', '2021-02-14 16:36:30', '2021-02-14 16:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `supports`
--

CREATE TABLE `supports` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `sender_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `skip_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'phone_or_email',
  `parent_id` tinyint(4) NOT NULL DEFAULT '0',
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci,
  `is_read` tinyint(4) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  `replied_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supports`
--

INSERT INTO `supports` (`id`, `user_id`, `sender_id`, `name`, `phone`, `email`, `skip_user`, `parent_id`, `type_id`, `message`, `is_read`, `is_deleted`, `replied_at`, `created_at`, `updated_at`) VALUES
(2, 47, 50, NULL, NULL, NULL, NULL, 0, 1, 'hhh', 0, 0, NULL, '2021-05-10 22:29:27', '2021-05-10 22:29:27'),
(3, 50, 47, NULL, NULL, NULL, NULL, 2, 1, 'kkk', 0, 0, '2021-05-10 22:29:40', '2021-05-10 22:29:40', '2021-05-10 22:29:40'),
(4, 47, 30, NULL, NULL, NULL, NULL, 0, 1, 'jjj', 0, 0, NULL, '2021-05-10 22:30:21', '2021-05-10 22:30:21'),
(5, 30, 47, NULL, NULL, NULL, NULL, 4, 1, 'jjj', 0, 0, '2021-05-10 22:30:39', '2021-05-10 22:30:39', '2021-05-10 22:30:39'),
(6, 47, NULL, 'RAMY AHMED', '0500000001', 'probulidapp@gmail.com', NULL, 0, 1, 'Hello from Contact Us', 0, 0, NULL, '2021-05-10 22:37:28', '2021-05-10 22:37:28'),
(7, 47, 30, NULL, NULL, NULL, NULL, 0, 1, 'bhh', 0, 0, NULL, '2021-05-10 22:37:58', '2021-05-10 22:37:58'),
(8, 30, 47, NULL, NULL, NULL, NULL, 7, 1, 'bvnbvn', 0, 0, '2021-05-10 22:38:16', '2021-05-10 22:38:16', '2021-05-10 22:38:16'),
(9, 47, NULL, 'hesham ashraf', '0583847762', 'hesham.cr2013@gmail.com', NULL, 0, 1, 'hehsha', 0, 0, NULL, '2021-05-10 22:40:30', '2021-05-10 22:40:30'),
(10, 47, 30, NULL, NULL, NULL, NULL, 0, 2, 'mmkj', 0, 0, NULL, '2021-05-10 22:46:34', '2021-05-10 22:46:34'),
(11, 30, 47, NULL, NULL, NULL, NULL, 10, 2, 'gjghfjfg', 0, 0, '2021-05-10 22:46:43', '2021-05-10 22:46:43', '2021-05-10 22:46:43'),
(12, 47, 30, NULL, NULL, NULL, NULL, 0, 1, 'nbb', 0, 0, NULL, '2021-05-10 22:53:34', '2021-05-10 22:53:34'),
(13, 30, 47, NULL, NULL, NULL, NULL, 12, 1, 'fdfg', 0, 0, '2021-05-10 22:53:57', '2021-05-10 22:53:57', '2021-05-10 22:53:57'),
(14, 47, 30, NULL, NULL, NULL, NULL, 0, 17, '‏nfc', 0, 0, NULL, '2021-05-10 22:55:31', '2021-05-10 22:55:31'),
(15, 30, 47, NULL, NULL, NULL, NULL, 14, 17, 'سذسش', 0, 0, '2021-05-10 22:55:39', '2021-05-10 22:55:39', '2021-05-10 22:55:39'),
(16, 47, 45, NULL, NULL, NULL, NULL, 0, 1, 'bBsnzn', 0, 0, NULL, '2021-05-10 22:57:43', '2021-05-10 22:57:43'),
(17, 45, 47, NULL, NULL, NULL, NULL, 16, 1, 'ok', 0, 0, '2021-05-10 22:59:39', '2021-05-10 22:59:39', '2021-05-10 22:59:39'),
(18, 47, 59, NULL, NULL, NULL, NULL, 0, 17, 'ايهينيوصنيهذند', 0, 0, NULL, '2021-05-12 02:43:23', '2021-05-12 02:43:23'),
(19, 59, 47, NULL, NULL, NULL, NULL, 18, 17, 'bKallslskskz', 0, 0, '2021-05-12 14:50:34', '2021-05-12 14:50:34', '2021-05-12 14:50:34'),
(20, 47, 33, NULL, NULL, NULL, NULL, 0, 1, 'bznznznznz', 0, 0, NULL, '2021-05-12 14:51:25', '2021-05-12 14:51:25'),
(21, 33, 47, NULL, NULL, NULL, NULL, 20, 1, 'lglglgpvooglg', 0, 0, '2021-05-12 14:51:33', '2021-05-12 14:51:33', '2021-05-12 14:51:33'),
(22, 47, NULL, 'Test', '05555555555', 'talal-alsehli@hotmail.com', NULL, 0, 1, 'Test test', 0, 0, NULL, '2021-05-17 17:13:31', '2021-05-17 17:13:31'),
(23, 47, 33, NULL, NULL, NULL, NULL, 0, 17, 'gsgskdjdn', 0, 0, NULL, '2021-05-19 20:53:44', '2021-05-19 20:53:44'),
(24, 33, 47, NULL, NULL, NULL, NULL, 23, 17, 'bshsbsh', 0, 0, '2021-05-19 20:54:04', '2021-05-19 20:54:04', '2021-05-19 20:54:04');

-- --------------------------------------------------------

--
-- Table structure for table `terms`
--

CREATE TABLE `terms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `term_translations`
--

CREATE TABLE `term_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED DEFAULT NULL,
  `offer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `progress` enum('deposit','deposit_by_admin','pull','deposit_by_promo_code') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pull',
  `payment` enum('wallet','online') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'wallet',
  `price` decimal(10,2) NOT NULL,
  `paymentId` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `promo_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refund_requested` enum('pending','request','accepted','refuse') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `reason_rejection_id` bigint(20) DEFAULT NULL,
  `message` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `order_id`, `offer_id`, `progress`, `payment`, `price`, `paymentId`, `promo_code`, `refund_requested`, `reason_rejection_id`, `message`, `created_at`, `updated_at`) VALUES
(1, 20, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1500.00, NULL, '30500', 'pending', NULL, NULL, '2021-04-21 23:47:35', '2021-04-21 23:47:35'),
(6, 50, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1500.00, NULL, '30500', 'pending', NULL, NULL, '2021-04-21 23:47:35', '2021-04-21 23:47:35'),
(24, 51, NULL, NULL, 'deposit_by_promo_code', 'wallet', 2500.00, NULL, 'Day1', 'pending', NULL, NULL, '2021-04-22 22:28:55', '2021-04-22 22:28:55'),
(37, 17, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1500.00, NULL, '30500', 'pending', NULL, NULL, '2021-04-24 00:28:04', '2021-04-24 00:28:04'),
(39, 32, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1500.00, NULL, '30500', 'pending', NULL, NULL, '2021-04-24 00:38:48', '2021-04-24 00:38:48'),
(40, 55, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1500.00, NULL, '30500', 'pending', NULL, NULL, '2021-04-24 00:59:21', '2021-04-24 00:59:21'),
(62, 32, NULL, NULL, 'deposit_by_admin', 'wallet', 10000.00, NULL, NULL, 'pending', NULL, NULL, '2021-04-24 03:18:25', '2021-04-24 03:18:25'),
(63, 17, NULL, NULL, 'deposit_by_admin', 'wallet', 10000.00, NULL, NULL, 'pending', NULL, NULL, '2021-04-24 03:18:25', '2021-04-24 03:18:25'),
(64, 55, NULL, NULL, 'deposit_by_admin', 'wallet', 10000.00, NULL, NULL, 'pending', NULL, NULL, '2021-04-24 03:19:19', '2021-04-24 03:19:19'),
(98, 33, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1000.00, NULL, 'teat', 'pending', NULL, NULL, '2021-04-26 13:07:11', '2021-04-26 13:07:11'),
(99, 34, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1000.00, NULL, 'teat', 'pending', NULL, NULL, '2021-04-26 13:43:36', '2021-04-26 13:43:36'),
(103, 53, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1000.00, NULL, 'teat', 'pending', NULL, NULL, '2021-04-26 15:56:11', '2021-04-26 15:56:11'),
(105, 59, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1000.00, NULL, 'teat', 'pending', NULL, NULL, '2021-04-26 16:05:57', '2021-04-26 16:05:57'),
(113, 45, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1000.00, NULL, 'teat', 'pending', NULL, NULL, '2021-04-26 17:06:36', '2021-04-26 17:06:36'),
(122, 20, NULL, NULL, 'deposit_by_admin', 'wallet', 35.00, NULL, NULL, 'pending', NULL, NULL, '2021-04-27 00:03:24', '2021-04-27 00:03:24'),
(123, 33, NULL, NULL, 'deposit_by_admin', 'wallet', 35.00, NULL, NULL, 'pending', NULL, NULL, '2021-04-27 00:04:45', '2021-04-27 00:04:45'),
(124, 33, NULL, NULL, 'deposit_by_admin', 'wallet', 100.00, NULL, NULL, 'pending', NULL, NULL, '2021-04-27 00:05:48', '2021-04-27 00:05:48'),
(125, 15, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1500.00, NULL, '30500', 'pending', NULL, NULL, '2021-04-27 20:37:30', '2021-04-27 20:37:30'),
(126, 15, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1000.00, NULL, 'teat', 'pending', NULL, NULL, '2021-04-27 20:38:51', '2021-04-27 20:38:51'),
(128, 30, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1500.00, NULL, '30500', 'pending', NULL, NULL, '2021-04-27 20:40:31', '2021-04-27 20:40:31'),
(130, 15, NULL, NULL, 'deposit_by_admin', 'wallet', 35.00, NULL, NULL, 'pending', NULL, NULL, '2021-04-27 20:50:39', '2021-04-27 20:50:39'),
(160, 33, NULL, NULL, 'deposit_by_admin', 'wallet', 35.00, NULL, NULL, 'pending', NULL, NULL, '2021-04-30 17:52:52', '2021-04-30 17:52:52'),
(163, 60, NULL, NULL, 'deposit_by_promo_code', 'wallet', 1000.00, NULL, 'test', 'pending', NULL, NULL, '2021-04-30 18:26:56', '2021-04-30 18:26:56'),
(169, 15, NULL, NULL, 'deposit_by_admin', 'wallet', 35.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-05 00:09:55', '2021-05-05 00:09:55'),
(209, 50, 127, NULL, 'pull', 'wallet', -49.00, NULL, NULL, 'refuse', 7, '\"{\\\"message_ar\\\":\\\"\\\\u0631\\\\u0641\\\\u0636\\\",\\\"message_en\\\":\\\"ccc\\\"}\"', '2021-05-07 03:35:03', '2021-05-10 22:21:48'),
(210, 50, 127, NULL, 'pull', 'wallet', -49.00, NULL, NULL, 'accepted', 8, NULL, '2021-05-07 03:43:53', '2021-05-10 22:21:30'),
(211, 15, 127, 213, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-07 03:44:13', '2021-05-07 03:44:13'),
(212, 15, 128, NULL, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-07 03:45:50', '2021-05-07 03:45:50'),
(213, 15, 129, NULL, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-07 03:54:04', '2021-05-07 03:54:04'),
(214, 50, 129, 222, 'pull', 'wallet', -49.00, NULL, NULL, 'accepted', 16, NULL, '2021-05-07 03:56:30', '2021-05-10 22:21:28'),
(215, 15, 129, NULL, 'pull', 'wallet', -98.00, NULL, NULL, 'accepted', 15, NULL, '2021-05-07 04:14:51', '2021-05-10 21:36:33'),
(216, 33, 129, 228, 'pull', 'wallet', -98.00, NULL, NULL, 'accepted', NULL, '\"{\\\"message_ar\\\":\\\"\\\\u0628\\\",\\\"message_en\\\":\\\"g\\\"}\"', '2021-05-07 04:51:57', '2021-05-10 16:06:57'),
(217, 33, NULL, NULL, 'deposit_by_admin', 'wallet', 98.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 16:06:57', '2021-05-10 16:06:57'),
(218, 33, 133, NULL, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 20:27:19', '2021-05-10 20:27:19'),
(219, 45, 133, 235, 'pull', 'wallet', -49.00, NULL, NULL, 'refuse', 15, '\"{\\\"message_ar\\\":\\\"\\\\u0646\\\\u0648\\\",\\\"message_en\\\":\\\"no\\\"}\"', '2021-05-10 20:31:41', '2021-05-10 23:00:22'),
(220, 33, 133, NULL, 'pull', 'wallet', -98.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 20:39:57', '2021-05-10 20:39:57'),
(221, 59, 133, 237, 'pull', 'wallet', -98.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 20:41:52', '2021-05-10 20:41:52'),
(222, 15, NULL, NULL, 'deposit_by_admin', 'wallet', 98.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 21:36:33', '2021-05-10 21:36:33'),
(226, 33, 136, NULL, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 22:13:33', '2021-05-10 22:13:33'),
(227, 45, 136, 240, 'pull', 'wallet', -49.00, NULL, NULL, 'accepted', 7, NULL, '2021-05-10 22:17:20', '2021-05-10 23:49:14'),
(228, 50, NULL, NULL, 'deposit_by_admin', 'wallet', 49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 22:21:28', '2021-05-10 22:21:28'),
(229, 50, NULL, NULL, 'deposit_by_admin', 'wallet', 49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 22:21:30', '2021-05-10 22:21:30'),
(230, 45, 137, NULL, 'pull', 'wallet', -49.00, NULL, NULL, 'accepted', 16, NULL, '2021-05-10 23:08:26', '2021-05-10 23:49:55'),
(231, 33, 137, 241, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 23:14:00', '2021-05-10 23:14:00'),
(232, 45, 137, NULL, 'pull', 'wallet', -98.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 23:21:35', '2021-05-10 23:21:35'),
(233, 59, 137, 244, 'pull', 'wallet', -98.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 23:23:25', '2021-05-10 23:23:25'),
(234, 45, NULL, NULL, 'deposit_by_admin', 'wallet', 49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 23:49:14', '2021-05-10 23:49:14'),
(235, 45, NULL, NULL, 'deposit_by_admin', 'wallet', 49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-10 23:49:55', '2021-05-10 23:49:55'),
(236, 50, 135, NULL, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-11 22:51:04', '2021-05-11 22:51:04'),
(237, 15, 135, 247, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-11 22:55:05', '2021-05-11 22:55:05'),
(238, 50, 135, NULL, 'pull', 'wallet', -98.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-11 23:47:04', '2021-05-11 23:47:04'),
(239, 15, 135, 248, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-11 23:47:17', '2021-05-11 23:47:17'),
(240, 50, NULL, NULL, 'deposit', 'wallet', 0.50, '07072360058239115672', NULL, 'pending', NULL, NULL, '2021-05-12 03:42:38', '2021-05-12 03:42:38'),
(241, 50, NULL, NULL, 'deposit', 'wallet', 1.00, '07072360117239123771', NULL, 'pending', NULL, NULL, '2021-05-12 03:50:08', '2021-05-12 03:50:08'),
(242, 45, 138, NULL, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-12 14:22:19', '2021-05-12 14:22:19'),
(243, 33, 138, 245, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-12 14:27:48', '2021-05-12 14:27:48'),
(244, 33, 140, NULL, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-12 14:37:50', '2021-05-12 14:37:50'),
(245, 45, 140, 250, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-12 14:38:15', '2021-05-12 14:38:15'),
(246, 33, 140, NULL, 'pull', 'wallet', -98.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-12 14:43:12', '2021-05-12 14:43:12'),
(247, 59, 140, 252, 'pull', 'wallet', -98.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-12 14:46:33', '2021-05-12 14:46:33'),
(248, 33, 141, NULL, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-19 20:47:42', '2021-05-19 20:47:42'),
(249, 45, 141, 255, 'pull', 'wallet', -49.00, NULL, NULL, 'pending', NULL, NULL, '2021-05-19 20:49:15', '2021-05-19 20:49:15'),
(250, 33, 141, NULL, 'pull', 'wallet', -98.00, NULL, NULL, 'refuse', 16, '\"{\\\"message_ar\\\":\\\"\\\\u0646\\\\u0637\\\\u0646\\\\u0637\\\\u0631\\\",\\\"message_en\\\":\\\"hdbsb\\\"}\"', '2021-05-19 20:52:37', '2021-05-19 20:57:01');

-- --------------------------------------------------------

--
-- Table structure for table `types_supports`
--

CREATE TABLE `types_supports` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_suspend` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types_supports`
--

INSERT INTO `types_supports` (`id`, `is_suspend`, `created_at`, `updated_at`) VALUES
(1, 0, '2018-12-22 14:25:45', '2018-12-22 14:25:45'),
(2, 0, '2018-12-22 20:10:31', '2018-12-22 20:10:31'),
(17, 0, '2018-12-22 20:10:31', '2018-12-22 20:10:31');

-- --------------------------------------------------------

--
-- Table structure for table `types_support_translations`
--

CREATE TABLE `types_support_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `types_support_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `types_support_translations`
--

INSERT INTO `types_support_translations` (`id`, `types_support_id`, `locale`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'ar', 'اقتراح', NULL, NULL),
(2, 1, 'en', 'order', NULL, NULL),
(33, 17, 'ar', 'شكاوي', NULL, NULL),
(34, 17, 'en', 'problems', NULL, NULL),
(35, 2, 'ar', 'إستفسار', NULL, NULL),
(36, 2, 'en', 'Enquiry', '2021-03-30 13:47:49', '2021-03-30 13:47:49');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `defined_user` enum('admin','helper_admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` enum('personal','commercial') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'personal',
  `is_active_email` tinyint(1) NOT NULL DEFAULT '0',
  `is_active_phone` tinyint(1) NOT NULL DEFAULT '0',
  `send_notification` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_suspend` tinyint(1) NOT NULL DEFAULT '0',
  `message` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ar',
  `desc_category` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(2000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `defined_user`, `name`, `phone`, `email`, `category`, `is_active_email`, `is_active_phone`, `send_notification`, `is_active`, `is_suspend`, `message`, `api_token`, `password`, `lang`, `desc_category`, `permission`, `remember_token`, `created_at`, `updated_at`) VALUES
(9, 'user', 'ramy Ahmed', '0100059881505', 'ramyahmedhandousa2020@gmail.com', 'commercial', 1, 1, 0, 1, 0, NULL, 'jJpG1cbumugHJZww7mMxjQD3KglOkYnovHDnUX6o0b52ug2juX68TBmaaYDM', '$2y$10$3sn4H468/nLPzn9r6oJta.ee8UYYbP7UYOOcgCt2Wcbv.Y62FXR9W', 'ar', NULL, NULL, NULL, '2021-01-24 14:00:23', '2021-04-07 22:38:28'),
(10, 'user', 'bola', '0503698741', 'bola@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, 'TNYfxRdXzTNQNxk3ZGP7gk6IWRHZUCwbvkPp0MPxp5CmtxkSyHDcdiHeOz5e', NULL, 'ar', NULL, NULL, NULL, '2021-01-27 12:57:11', '2021-01-27 12:57:11'),
(11, 'user', 'mona', '05236541789', 'mona@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, '3aHLdprtxbjXlqPYXpvLI1ahu3n7OBuAQrt8e9XwwrJXvLarIESoeHZlT96q', NULL, 'ar', NULL, NULL, NULL, '2021-01-27 13:21:51', '2021-01-27 13:21:51'),
(12, 'user', 'aya', '6547893210', 'aya@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, 'Vt1NgYiacUanBSz5faNitKe81z6ZKsCGIFpIGHKlhdTXmJdf4hmfuT1AGLLD', NULL, 'ar', NULL, NULL, NULL, '2021-01-27 13:22:07', '2021-01-27 13:22:07'),
(13, 'user', 'ali', '0588698788', 'ali@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, 'NghQkcegPIEhyrvIt3f3BkMwVFxsXAfyiNYtH9SX3CP2WY2KUNes7DxP4PnS', NULL, 'ar', NULL, NULL, NULL, '2021-01-28 13:43:52', '2021-01-28 13:43:52'),
(14, 'user', 'salma', '0582757545', 'salmatizkx55@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, 'igUf0OYoJk0p09RcsE1lHrXwLAH7jnwKJJEhxwJzdNLjAQb66eoncYBY66UI', NULL, 'ar', NULL, NULL, NULL, '2021-01-31 11:25:04', '2021-02-01 12:48:28'),
(15, 'user', 'محمد ‏داوود', '0599999999', 'mo.ah.dawood@gmail.com', 'commercial', 1, 1, 0, 0, 0, NULL, 'zgwM3TwkRS0RPZVCKGLYlr075xTEBXTYvOs9XpYIB7wfDHfRSuwLXtzvrhDq', NULL, 'ar', NULL, NULL, NULL, '2021-01-31 11:25:08', '2021-05-12 02:39:55'),
(16, 'user', 'ahmedd', '0503698745', 'ahmed@gmail.com', 'commercial', 1, 1, 1, 0, 0, NULL, 'X7Jg1Gd16OG3U556KYKFaiFMRGX7fGpwmnrR13CLn3xkTSB2hOJErB87yrEL', NULL, 'ar', NULL, NULL, NULL, '2021-01-31 14:42:46', '2021-02-22 21:57:04'),
(17, 'user', 'salma', '0502540855', 'salmarizkx55@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, 'rb0yCeQpabvxtYzZr2OcqgG2F9ceIsvVCqI80faTfPBf38xqTBwbrrDs9M7v', NULL, 'ar', NULL, NULL, NULL, '2021-02-01 08:01:09', '2021-04-25 15:41:36'),
(19, 'user', 'Reem ahmed', '050254088', 'reem@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, '57OWZxhldtO3Ju6kDIjbX4EW8oxc4gjJPkXjZiE2jdco7YYoe5pDxxPeYpHN', NULL, 'ar', NULL, NULL, NULL, '2021-02-01 13:33:24', '2021-02-03 09:09:14'),
(20, 'user', 'محمد ‏داوود', '0588899999', 'ss@ss.com', 'personal', 1, 1, 0, 0, 0, NULL, 'czSGSC6QSrCNKU09e8YtbQ18sxebJLc15gycTSMkCrxKsqsGUSFihzosDdfr', NULL, 'ar', NULL, NULL, NULL, '2021-02-08 14:22:21', '2021-04-30 16:55:14'),
(21, 'user', 'Zoba ‎', '0501234567', 'zoba@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, 'svxpNVOOhdoPaLLl01pFjrk5Fj0WKVyg56dhn7Ti9evYmTdadqRx14d3dSGH', NULL, 'ar', NULL, NULL, NULL, '2021-02-16 12:37:07', '2021-02-18 02:23:12'),
(30, 'user', 'hesham', '0583847762', 'hesham.cr2013@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, '84b9RcRTsN85xEVSckKhlafGPKv8oUINJZbl0VDYk2gJrf78bemhtxBms2K8', NULL, 'ar', NULL, NULL, NULL, '2021-02-21 15:03:17', '2021-05-12 14:37:25'),
(31, 'user', 'omar ‎mahmoud', '0502540888', 'omar@gmail.com', 'commercial', 1, 1, 0, 0, 0, NULL, 'dNrWuGywHL3uZQR4ATEMAGA5KpbnvBGm5eOt7IcTElWcRggU342A6VUW5HTp', NULL, 'ar', NULL, NULL, NULL, '2021-02-22 08:25:39', '2021-02-26 11:22:37'),
(32, 'user', 'salma ‎rizk', '0505050505', 'salma@gmail.com', 'commercial', 1, 1, 0, 0, 0, NULL, '6vW4Zfhl7wdmbQIm6F0svQoBHqKIIQ2kzAShNtbzbdOnHbUKRhueeO7ePwuV', NULL, 'ar', NULL, NULL, NULL, '2021-02-22 08:59:34', '2021-04-25 15:36:24'),
(33, 'user', 'talal ‎abdulaziz', '0555992006', 'talal-alsehli@hotmail.com', 'personal', 1, 1, 0, 0, 0, NULL, '3Zne93RyoVnDVe4FgoaWirbcn3dQmFn09Pb3I5ikBzjJLwZXwZQRLGeCLN61', NULL, 'ar', NULL, NULL, NULL, '2021-02-22 12:49:39', '2021-05-19 21:36:21'),
(34, 'user', 'Abdulmajeed', '0566766212', 'e.majood@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, 'Ard0vjldUtqBlVHnsBYntyluD0mixDAxSeSlyrFEIUdOBeqXkollgmJgpdrO', NULL, 'ar', NULL, NULL, NULL, '2021-02-22 15:26:30', '2021-04-26 13:14:56'),
(35, 'user', 'Mohamed ‎dawood', '0565556666', 'mail@email.com', 'personal', 1, 1, 0, 0, 0, NULL, 'Bm7jdTCdbU0rNW8TJ4a4IaaEQkCH9LUL1kr86p6N6gPu18vqq2ORl5FMlWoX', NULL, 'ar', NULL, NULL, NULL, '2021-02-22 17:04:07', '2021-02-22 17:32:39'),
(36, 'user', 'menna', '05044444444', 'menna@gmail.com', 'commercial', 1, 1, 0, 0, 0, NULL, 'jl9WgvgTR8e6BJcUkIEWjRVRLtTpkpuEF5lbOr9C5tIlpPAZYUNlxWvzKz7k', NULL, 'ar', NULL, NULL, NULL, '2021-02-23 09:32:36', '2021-04-15 10:56:46'),
(37, 'user', 'qujddh', '0555555555', 'talal@hotmail.com', 'personal', 1, 1, 0, 0, 0, NULL, 'IaDDhjhSajYgqHI4uPV203PZeILNJ4Zxxro5jB84UXzsB15a3pKlTPekdQ2f', NULL, 'ar', NULL, NULL, NULL, '2021-02-23 13:27:46', '2021-02-23 13:27:51'),
(44, 'user', 'داوود', '0588778888', 'saned.inc@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, 'Ct7aTG89iMuaI5ROSRZwxrBPBRAYLSe1Pw7eT3ObykJ4MkrwFxD4yIf80hKF', NULL, 'ar', NULL, NULL, NULL, '2021-03-22 20:27:20', '2021-04-07 22:37:51'),
(45, 'user', 'talal123', '0500000000', 'emin3m_2006@hotmail.com', 'personal', 1, 1, 0, 0, 0, NULL, 'wWiS126JKSE33TTRtvSJtrCLgqPLmDRrZh9wrdJvKlORlt1o22urF6UYXCJJ', NULL, 'ar', NULL, NULL, NULL, '2021-03-24 13:08:58', '2021-05-19 20:48:18'),
(46, 'user', 'طلال', '0505555555', 'talal@hshd.com', 'personal', 0, 1, 0, 0, 0, NULL, 'pfMxwNVm3ycjHQZXji1ZNbzVBT0wfvZ8UysJKpJyxuTcJTNATpd8amVq0QtZ', NULL, 'ar', NULL, NULL, NULL, '2021-03-25 16:35:07', '2021-04-04 14:05:43'),
(47, 'admin', 'Ramy', '05022558896', 'admin@admin.com', 'personal', 1, 1, 0, 1, 0, NULL, 'csdfdhkjh5greftghyjukilokhjmnhfgbdfsdcfgethytujyijhmggfdffrgtrhyujmj', '$2y$10$0/81RCRbpwtWkplAHHr4cuAE6sUAOIr8hlL3dq4RmC0sLCSEzAJ6e', 'ar', NULL, NULL, NULL, '2021-03-29 11:37:21', '2021-04-26 12:09:12'),
(48, 'user', 'tala', '0502560896', 'salma.sanedaamalak@gmail.com', 'personal', 1, 0, 0, 0, 0, NULL, 'lHzeEzqUJPLzLOljuVLEZDVpzIvufUzEJMzCiRmlTQWbOe4FVmhVWYANeTuk', NULL, 'ar', NULL, NULL, NULL, '2021-03-30 13:04:55', '2021-03-30 13:16:15'),
(49, 'user', 'menna', '0502540851', 'salmarizkx556@gmail.com', 'personal', 0, 1, 0, 0, 0, NULL, 'DRqRHUr7P4XmddvO8PMgq6mykLCFOapZJ9FRTpvcXonhN2pFGz3higCGw7nc', NULL, 'ar', NULL, NULL, NULL, '2021-03-30 13:07:19', '2021-03-30 13:07:26'),
(50, 'user', 'ابو عبدالعزيز', '0533333333', 'a@a.com', 'personal', 0, 1, 0, 0, 0, NULL, 'zyPRe7BPs9mLFoHxOfaIGEu9bccQSbvSqA1gSGKOXqiGHJxm3Lj8ZtlvQ0yV', NULL, 'ar', NULL, NULL, NULL, '2021-04-04 14:00:18', '2021-05-12 03:22:12'),
(51, 'user', 'حساب ‏جديد', '0567777777', 'email@ggg.com', 'personal', 0, 1, 0, 0, 0, NULL, 'TqVfhPfutkXgylEGlKVUzOPetVQqlrSQrA7ZZEv0qXwTgTSgWGiqV4KVxVeW', NULL, 'ar', NULL, NULL, NULL, '2021-04-08 14:11:24', '2021-04-30 03:22:38'),
(52, 'user', 'saad', '0512345622', 'sa3dsalem01@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, 'hygs6c9t1R5YdGcmUJ7PrSmb043T1Nz3NdFL4FKk2Lg3GeKdbzOiQ79fAvBD', NULL, 'ar', NULL, NULL, NULL, '2021-04-09 22:55:52', '2021-04-09 22:57:23'),
(53, 'user', 'talal ‎abdul123', '0555992000', 'gh@hh.com', 'personal', 0, 1, 0, 0, 0, NULL, 'x35bYs9g3mNSSmNzdZjok4oxZUzRYBIJHAN0qX4HiiJXXcYWBMjNAddwSOky', NULL, 'ar', NULL, NULL, NULL, '2021-04-14 10:58:34', '2021-05-19 20:42:51'),
(54, 'helper_admin', 'mm@mm.com222', NULL, 'mm@mm.com', 'personal', 1, 1, 0, 0, 0, NULL, 'bAoZTuZoKGqMOpdIfc3vZmCRcf1y2rwGKC3AQx24GzQKRD1piCrG4XLyAGq1', '$2y$10$MXt.pGqDeHL1l18obcpOqeXFuF/o0GUz/ObFCsK7l5RgXhr75hbIu', 'ar', NULL, '[\"dashboard\",\"users\",\"order_lists\",\"categories\",\"intro\",\"order_settings\"]', NULL, '2021-04-20 23:46:40', '2021-04-21 00:25:51'),
(55, 'user', 'mahmoud', '0504322747', 'mahmoud@gmail.com', 'personal', 0, 1, 0, 0, 0, NULL, 'FcyRFqvRVkEIpo3Uuchl8rCBi90MDe3cxnlZkYvA3ifBxdsHjxFHbcn51hQB', NULL, 'ar', NULL, NULL, NULL, '2021-04-23 21:46:11', '2021-04-23 21:46:18'),
(56, 'helper_admin', 'salma', NULL, 'salma.rizk148@gmail.com', 'personal', 1, 1, 0, 0, 0, NULL, 'jdDZwocy3TsghtdfQOcHpXZKttAXQQUCdkNn6cKNkpfcbvrzePeo1MX9Iqfy', '$2y$10$WM7ZIvHGdDuUTp5jhbLJceGxGMpSTq6tWMzj6ZzAvq8QjJz4GX.RO', 'ar', NULL, '[\"dashboard\",\"order_lists\"]', NULL, '2021-04-23 23:00:41', '2021-04-26 12:39:31'),
(57, 'user', 'ويوي', '0504372784', 'salmad@gmail.com', 'personal', 0, 0, 0, 0, 0, NULL, 'VivgLhMOqg7bJm9qBGOobkMUP5knNrJoZyJbwDg2s8DhVOOwbpyPBJCRONHP', NULL, 'ar', NULL, NULL, NULL, '2021-04-24 04:01:18', '2021-04-24 04:01:18'),
(58, 'user', 'Bola', '0566767676', 'gg@hjfgg.com', 'personal', 0, 1, 0, 0, 0, NULL, 'XPcLEGxHQEDWKfSSm5OMmFDeRGWFXfXiqxjjQ2TzIVVLBFpSHeR0JKqBPqDq', NULL, 'ar', NULL, NULL, NULL, '2021-04-25 22:21:50', '2021-04-25 22:21:53'),
(59, 'user', 'abu ‎aseel', '0555992021', 't@t.com', 'personal', 0, 1, 0, 0, 0, NULL, 'S5N1rX5M99ChoD2RzU5lrYI6s8eScQTtbbKDsx3THXQuMdsId8jhqmV8lreg', NULL, 'ar', NULL, NULL, NULL, '2021-04-26 16:05:15', '2021-05-19 20:51:12'),
(60, 'user', 'talal new', '0511111111', 'talal.Alsehli@bargain.com.sa', 'personal', 0, 1, 0, 0, 0, NULL, 'QMMb1XLKNf7xdU51rpm9tSsKoXLUXjABOd0jsuB3MMSAoq16aFV33gnoCHvI', NULL, 'ar', NULL, NULL, NULL, '2021-04-30 16:50:54', '2021-05-04 02:54:01'),
(61, 'user', 'محمد الشربيني', '0546650898', 'm.elsherbiny@saned.sa', 'personal', 0, 1, 0, 0, 0, NULL, 'nNIQrJPjpHJopmbX2GAKhcvm3DF3x9SVMDHoJFmWS0ndKD2bepEtnls6lTeF', NULL, 'ar', NULL, NULL, NULL, '2021-05-12 13:49:38', '2021-05-12 13:50:42'),
(62, 'user', 'test', '0500000001', 'talal1@hotmail.com', 'personal', 0, 0, 0, 0, 0, NULL, '0YOim5c4yvCaHzOeJSqoi6rbWQUapUaWAVozxB5rUu46ugE406NzW8LCIDPR', NULL, 'ar', NULL, NULL, NULL, '2021-05-19 21:13:16', '2021-05-19 21:13:16'),
(63, 'user', 'test123456', '0500000002', 'talal12@hotmail.com', 'personal', 0, 1, 0, 0, 0, NULL, '9h2PmIRyhA1hZgIBhheo64BDXNueSxtTEP9HJwu8FCy8OvUkmAf5O66Pq25W', NULL, 'ar', NULL, NULL, NULL, '2021-05-19 21:13:57', '2021-05-19 21:14:01');

-- --------------------------------------------------------

--
-- Table structure for table `verify_users`
--

CREATE TABLE `verify_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `verify_users`
--

INSERT INTO `verify_users` (`id`, `user_id`, `phone`, `email`, `action_code`, `created_at`, `updated_at`) VALUES
(31, 10, '0503698741', 'bola@gmail.com', '1111', '2021-01-27 12:57:11', '2021-01-27 12:57:11'),
(32, 11, '05236541789', 'mona@gmail.com', '1111', '2021-01-27 13:21:51', '2021-01-27 13:21:51'),
(33, 12, '6547893210', 'aya@gmail.com', '1111', '2021-01-27 13:22:07', '2021-01-27 13:22:07'),
(34, 13, '0588698788', 'ali@gmail.com', '1111', '2021-01-28 13:43:52', '2021-01-28 13:43:52'),
(37, 16, '0503698745', 'ahmed@gmail.com', '1111', '2021-01-31 14:42:46', '2021-01-31 14:42:46'),
(46, 19, '050254088', 'reem@gmail.com', '1111', '2021-02-02 12:29:56', '2021-02-02 12:29:56'),
(382, 48, '0502560896', 'salma.sanedaamalak@gmail.com', '1111', '2021-03-30 13:16:15', '2021-03-30 13:16:15'),
(429, 57, '0504372784', 'salmad@gmail.com', '1111', '2021-04-24 04:01:18', '2021-04-24 04:01:18'),
(439, 47, NULL, 'ramyahmedhandousa@gmail.com', '1111', '2021-04-25 22:23:17', '2021-04-25 22:23:17'),
(671, 62, NULL, 'talal1@hotmail.com', '1111', '2021-05-19 21:13:16', '2021-05-19 21:13:20');

-- --------------------------------------------------------

--
-- Table structure for table `warranties`
--

CREATE TABLE `warranties` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `is_suspend` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warranties`
--

INSERT INTO `warranties` (`id`, `is_suspend`, `created_at`, `updated_at`) VALUES
(1, 0, '2021-01-26 10:16:54', '2021-03-30 00:39:08'),
(2, 0, '2021-01-26 10:16:54', '2021-03-30 00:39:07'),
(3, 0, '2021-01-26 10:16:54', '2021-03-30 00:39:06'),
(4, 0, '2021-01-26 10:16:54', '2021-03-30 00:39:05'),
(5, 0, '2021-03-30 00:03:25', '2021-04-14 13:24:21'),
(6, 0, '2021-04-14 13:24:50', '2021-05-05 13:23:31');

-- --------------------------------------------------------

--
-- Table structure for table `warranty_translations`
--

CREATE TABLE `warranty_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `warranty_id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warranty_translations`
--

INSERT INTO `warranty_translations` (`id`, `warranty_id`, `locale`, `name`, `created_at`, `updated_at`) VALUES
(1, 1, 'ar', 'شهر', '2021-01-26 10:19:42', '2021-04-14 13:24:29'),
(2, 1, 'en', 'One ‎month', '2021-01-26 10:19:42', '2021-03-29 20:22:53'),
(3, 2, 'ar', '3 شهور', NULL, NULL),
(4, 2, 'en', '3 months', NULL, NULL),
(5, 3, 'ar', '6 شهور', NULL, NULL),
(6, 3, 'en', '6 months', NULL, NULL),
(7, 4, 'ar', 'سنه', NULL, NULL),
(8, 4, 'en', 'Year', NULL, NULL),
(9, 5, 'ar', 'سنتين', '2021-03-30 00:03:25', '2021-03-30 00:03:25'),
(10, 5, 'en', '2 ‏years', '2021-03-30 00:03:25', '2021-03-30 00:03:25'),
(11, 6, 'ar', 'أخرى', '2021-04-14 13:24:50', '2021-05-05 13:23:26'),
(12, 6, 'en', 'Other', '2021-04-14 13:24:50', '2021-05-05 13:23:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_translations`
--
ALTER TABLE `category_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_translations_category_id_locale_unique` (`category_id`,`locale`),
  ADD KEY `category_translations_locale_index` (`locale`);

--
-- Indexes for table `category_users`
--
ALTER TABLE `category_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_users_user_id_foreign` (`user_id`),
  ADD KEY `category_users_category_id_foreign` (`category_id`);

--
-- Indexes for table `delivery_ways`
--
ALTER TABLE `delivery_ways`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_way_translations`
--
ALTER TABLE `delivery_way_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `delivery_way_translations_delivery_way_id_locale_unique` (`delivery_way_id`,`locale`),
  ADD KEY `delivery_way_translations_locale_index` (`locale`);

--
-- Indexes for table `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `devices_user_id_foreign` (`user_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `industry_translations`
--
ALTER TABLE `industry_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `industry_translations_industry_id_locale_unique` (`industry_id`,`locale`),
  ADD KEY `industry_translations_locale_index` (`locale`);

--
-- Indexes for table `interactive_forms`
--
ALTER TABLE `interactive_forms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `interactive_forms_user_id_foreign` (`user_id`),
  ADD KEY `interactive_forms_reason_rejection_id_foreign` (`reason_rejection_id`),
  ADD KEY `interactive_forms_order_id_foreign` (`order_id`);

--
-- Indexes for table `labels`
--
ALTER TABLE `labels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `label_translations`
--
ALTER TABLE `label_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `label_translations_label_id_locale_unique` (`label_id`,`locale`),
  ADD KEY `label_translations_locale_index` (`locale`);

--
-- Indexes for table `label_values`
--
ALTER TABLE `label_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `label_values_label_id_foreign` (`label_id`);

--
-- Indexes for table `label_value_translations`
--
ALTER TABLE `label_value_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `label_value_translations_label_value_id_locale_unique` (`label_value_id`,`locale`),
  ADD KEY `label_value_translations_locale_index` (`locale`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_user_id_foreign` (`user_id`),
  ADD KEY `notifications_sender_id_foreign` (`sender_id`),
  ADD KEY `notifications_order_id_foreign` (`order_id`),
  ADD KEY `notifications_offer_id_foreign` (`offer_id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offers_user_id_foreign` (`user_id`),
  ADD KEY `offers_delivery_way_id_foreign` (`delivery_way_id`),
  ADD KEY `offers_industry_id_foreign` (`industry_id`),
  ADD KEY `offers_warranty_id_foreign` (`warranty_id`),
  ADD KEY `offers_payment_method_id_foreign` (`payment_method_id`),
  ADD KEY `offers_reason_rejection_id_foreign` (`reason_rejection_id`),
  ADD KEY `offers_order_id_foreign` (`order_id`);

--
-- Indexes for table `offer_details`
--
ALTER TABLE `offer_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `offer_details_offer_id_foreign` (`offer_id`),
  ADD KEY `offer_details_label_id_foreign` (`label_id`),
  ADD KEY `offer_details_label_value_id_foreign` (`label_value_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_user_offer_id_foreign` (`user_offer_id`),
  ADD KEY `orders_category_id_foreign` (`category_id`),
  ADD KEY `orders_delivery_way_id_foreign` (`delivery_way_id`),
  ADD KEY `orders_industry_id_foreign` (`industry_id`),
  ADD KEY `orders_warranty_id_foreign` (`warranty_id`),
  ADD KEY `orders_payment_method_id_foreign` (`payment_method_id`),
  ADD KEY `orders_reason_rejection_id_foreign` (`reason_rejection_id`),
  ADD KEY `orders_offer_id_foreign` (`offer_id`),
  ADD KEY `orders_old_offer_id_foreign` (`old_offer_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_details_order_id_foreign` (`order_id`),
  ADD KEY `order_details_label_id_foreign` (`label_id`),
  ADD KEY `order_details_label_value_id_foreign` (`label_value_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_methods`
--
ALTER TABLE `payment_methods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_method_translations`
--
ALTER TABLE `payment_method_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `payment_method_translations_payment_method_id_locale_unique` (`payment_method_id`,`locale`),
  ADD KEY `payment_method_translations_locale_index` (`locale`);

--
-- Indexes for table `promo_codes`
--
ALTER TABLE `promo_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rates_user_id_foreign` (`user_id`),
  ADD KEY `rates_order_id_foreign` (`order_id`);

--
-- Indexes for table `reason_rejections`
--
ALTER TABLE `reason_rejections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reason_rejection_translations`
--
ALTER TABLE `reason_rejection_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reason_rejection_translations_reason_rejection_id_locale_unique` (`reason_rejection_id`,`locale`),
  ADD KEY `reason_rejection_translations_locale_index` (`locale`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_data`
--
ALTER TABLE `static_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_data_translations`
--
ALTER TABLE `static_data_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `static_data_translations_static_data_id_foreign` (`static_data_id`);

--
-- Indexes for table `supports`
--
ALTER TABLE `supports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `supports_type_id_foreign` (`type_id`),
  ADD KEY `supports_user_id_foreign` (`user_id`),
  ADD KEY `supports_sender_id_foreign` (`sender_id`);

--
-- Indexes for table `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `term_translations`
--
ALTER TABLE `term_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `term_translations_term_id_locale_unique` (`term_id`,`locale`),
  ADD KEY `term_translations_locale_index` (`locale`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_user_id_foreign` (`user_id`),
  ADD KEY `transactions_order_id_foreign` (`order_id`),
  ADD KEY `transactions_offer_id_foreign` (`offer_id`);

--
-- Indexes for table `types_supports`
--
ALTER TABLE `types_supports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types_support_translations`
--
ALTER TABLE `types_support_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `types_support_translations_type_support_id_locale_unique` (`types_support_id`,`locale`),
  ADD KEY `types_support_translations_locale_index` (`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- Indexes for table `verify_users`
--
ALTER TABLE `verify_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `verify_users_user_id_foreign` (`user_id`);

--
-- Indexes for table `warranties`
--
ALTER TABLE `warranties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `warranty_translations`
--
ALTER TABLE `warranty_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `warranty_translations_warranty_id_locale_unique` (`warranty_id`,`locale`),
  ADD KEY `warranty_translations_locale_index` (`locale`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `category_translations`
--
ALTER TABLE `category_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `category_users`
--
ALTER TABLE `category_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=356;

--
-- AUTO_INCREMENT for table `delivery_ways`
--
ALTER TABLE `delivery_ways`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `delivery_way_translations`
--
ALTER TABLE `delivery_way_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=499;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `industries`
--
ALTER TABLE `industries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `industry_translations`
--
ALTER TABLE `industry_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `interactive_forms`
--
ALTER TABLE `interactive_forms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `labels`
--
ALTER TABLE `labels`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `label_translations`
--
ALTER TABLE `label_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `label_values`
--
ALTER TABLE `label_values`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `label_value_translations`
--
ALTER TABLE `label_value_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=162;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1059;

--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=258;

--
-- AUTO_INCREMENT for table `offer_details`
--
ALTER TABLE `offer_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_methods`
--
ALTER TABLE `payment_methods`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payment_method_translations`
--
ALTER TABLE `payment_method_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `promo_codes`
--
ALTER TABLE `promo_codes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reason_rejections`
--
ALTER TABLE `reason_rejections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `reason_rejection_translations`
--
ALTER TABLE `reason_rejection_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `static_data`
--
ALTER TABLE `static_data`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `static_data_translations`
--
ALTER TABLE `static_data_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `supports`
--
ALTER TABLE `supports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `terms`
--
ALTER TABLE `terms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `term_translations`
--
ALTER TABLE `term_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=251;

--
-- AUTO_INCREMENT for table `types_supports`
--
ALTER TABLE `types_supports`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `types_support_translations`
--
ALTER TABLE `types_support_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `verify_users`
--
ALTER TABLE `verify_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=672;

--
-- AUTO_INCREMENT for table `warranties`
--
ALTER TABLE `warranties`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `warranty_translations`
--
ALTER TABLE `warranty_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category_translations`
--
ALTER TABLE `category_translations`
  ADD CONSTRAINT `category_translations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `category_users`
--
ALTER TABLE `category_users`
  ADD CONSTRAINT `category_users_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `delivery_way_translations`
--
ALTER TABLE `delivery_way_translations`
  ADD CONSTRAINT `delivery_way_translations_delivery_way_id_foreign` FOREIGN KEY (`delivery_way_id`) REFERENCES `delivery_ways` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `devices`
--
ALTER TABLE `devices`
  ADD CONSTRAINT `devices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `industry_translations`
--
ALTER TABLE `industry_translations`
  ADD CONSTRAINT `industry_translations_industry_id_foreign` FOREIGN KEY (`industry_id`) REFERENCES `industries` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `interactive_forms`
--
ALTER TABLE `interactive_forms`
  ADD CONSTRAINT `interactive_forms_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `interactive_forms_reason_rejection_id_foreign` FOREIGN KEY (`reason_rejection_id`) REFERENCES `reason_rejections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `interactive_forms_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `label_translations`
--
ALTER TABLE `label_translations`
  ADD CONSTRAINT `label_translations_label_id_foreign` FOREIGN KEY (`label_id`) REFERENCES `labels` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `label_values`
--
ALTER TABLE `label_values`
  ADD CONSTRAINT `label_values_label_id_foreign` FOREIGN KEY (`label_id`) REFERENCES `labels` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `label_value_translations`
--
ALTER TABLE `label_value_translations`
  ADD CONSTRAINT `label_value_translations_label_value_id_foreign` FOREIGN KEY (`label_value_id`) REFERENCES `label_values` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `notifications_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notifications_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `offers`
--
ALTER TABLE `offers`
  ADD CONSTRAINT `offers_delivery_way_id_foreign` FOREIGN KEY (`delivery_way_id`) REFERENCES `delivery_ways` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `offers_industry_id_foreign` FOREIGN KEY (`industry_id`) REFERENCES `industries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `offers_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `offers_payment_method_id_foreign` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `offers_reason_rejection_id_foreign` FOREIGN KEY (`reason_rejection_id`) REFERENCES `reason_rejections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `offers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `offers_warranty_id_foreign` FOREIGN KEY (`warranty_id`) REFERENCES `warranties` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `offer_details`
--
ALTER TABLE `offer_details`
  ADD CONSTRAINT `offer_details_label_id_foreign` FOREIGN KEY (`label_id`) REFERENCES `labels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `offer_details_label_value_id_foreign` FOREIGN KEY (`label_value_id`) REFERENCES `label_values` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `offer_details_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_delivery_way_id_foreign` FOREIGN KEY (`delivery_way_id`) REFERENCES `delivery_ways` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_industry_id_foreign` FOREIGN KEY (`industry_id`) REFERENCES `industries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_old_offer_id_foreign` FOREIGN KEY (`old_offer_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_payment_method_id_foreign` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_reason_rejection_id_foreign` FOREIGN KEY (`reason_rejection_id`) REFERENCES `reason_rejections` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_user_offer_id_foreign` FOREIGN KEY (`user_offer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_warranty_id_foreign` FOREIGN KEY (`warranty_id`) REFERENCES `warranties` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_label_id_foreign` FOREIGN KEY (`label_id`) REFERENCES `labels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_details_label_value_id_foreign` FOREIGN KEY (`label_value_id`) REFERENCES `label_values` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `payment_method_translations`
--
ALTER TABLE `payment_method_translations`
  ADD CONSTRAINT `payment_method_translations_payment_method_id_foreign` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rates`
--
ALTER TABLE `rates`
  ADD CONSTRAINT `rates_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `rates_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reason_rejection_translations`
--
ALTER TABLE `reason_rejection_translations`
  ADD CONSTRAINT `reason_rejection_translations_reason_rejection_id_foreign` FOREIGN KEY (`reason_rejection_id`) REFERENCES `reason_rejections` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `static_data_translations`
--
ALTER TABLE `static_data_translations`
  ADD CONSTRAINT `static_data_translations_static_data_id_foreign` FOREIGN KEY (`static_data_id`) REFERENCES `static_data` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `supports`
--
ALTER TABLE `supports`
  ADD CONSTRAINT `supports_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `supports_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `term_translations`
--
ALTER TABLE `term_translations`
  ADD CONSTRAINT `term_translations_term_id_foreign` FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_offer_id_foreign` FOREIGN KEY (`offer_id`) REFERENCES `offers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `verify_users`
--
ALTER TABLE `verify_users`
  ADD CONSTRAINT `verify_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `warranty_translations`
--
ALTER TABLE `warranty_translations`
  ADD CONSTRAINT `warranty_translations_warranty_id_foreign` FOREIGN KEY (`warranty_id`) REFERENCES `warranties` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

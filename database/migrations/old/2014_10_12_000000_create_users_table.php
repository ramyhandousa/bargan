<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->enum('defined_user',['admin','user'])->default('user');
            $table->string('name');
            $table->string('phone')->unique();
            $table->string('email')->unique();
            $table->enum("category",["personal","commercial"])->default('personal');
            $table->boolean('is_active_email')->default(0);
            $table->boolean('is_active')->default(0);
            $table->boolean('is_suspend')->default(0);
            $table->string('message')->nullable();
            $table->string('api_token');
            $table->string('password')->nullable();
            $table->string('lang')->default('ar');
            $table->string("desc_category",500)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

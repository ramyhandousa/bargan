<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->cascadeOnDelete();
            $table->foreignId('user_offer_id')->nullable()->constrained('users')->cascadeOnDelete();
            $table->enum('account_type',['personal','commercial'])->default('personal');
            $table->enum('order_type',['auction','tender'])->default('tender');
            $table->enum('service_type',['product','service'])->default('product');
            $table->string('title');
            $table->foreignId('category_id')->constrained()->cascadeOnDelete();
            $table->text('description');
            $table->decimal('expected_price',5,2);
            $table->string('delivery_duration');
            $table->string('bidding_duration');
            $table->foreignId('delivery_way_id')->constrained()->cascadeOnDelete();
            $table->foreignId('industry_id')->constrained()->cascadeOnDelete();
            $table->foreignId('warranty_id')->constrained()->cascadeOnDelete();
            $table->foreignId('payment_method_id')->constrained()->cascadeOnDelete();
            $table->boolean('golden_offer')->default(0);
            $table->boolean('is_pay')->default(0);
            $table->dateTime('end_bidding_duration')->nullable();
            $table->boolean('time_ot')->default(0);
            $table->enum('status', ['pending', 'accepted','active_golden_offer','finish','refuse_system','refuse'])->default('pending');
            $table->text('message')->nullable();
            $table->foreignId('reason_rejection_id')->nullable()->constrained()->cascadeOnDelete();
            $table->timestamps();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->cascadeOnDelete();
            $table->enum('order_identity',['personal','commercial'])->default('personal');
            $table->text('description')->nullable();
            $table->decimal('expected_price',5,2);
            $table->string('delivery_duration')->nullable();
            $table->string('bidding_duration')->nullable();
            $table->foreignId('delivery_way_id')->nullable()->constrained()->cascadeOnDelete();
            $table->foreignId('industry_id')->nullable()->constrained()->cascadeOnDelete();
            $table->foreignId('warranty_id')->nullable()->constrained()->cascadeOnDelete();
            $table->foreignId('payment_method_id')->nullable()->constrained()->cascadeOnDelete();
            $table->boolean('is_pay')->default(0);
            $table->boolean('is_accepted')->default(0);
            $table->foreignId('reason_rejection_id')->nullable()->constrained()->cascadeOnDelete();
            $table->timestamps();
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}

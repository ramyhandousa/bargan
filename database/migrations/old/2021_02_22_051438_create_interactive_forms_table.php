<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInteractiveFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interactive_forms', function (Blueprint $table) {
            $table->id();
            $table->string("type");
            $table->foreignId('user_id')->constrained()->cascadeOnDelete();
            $table->foreignId('reason_rejection_id')->constrained()->cascadeOnDelete();
            $table->text("message");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interactive_forms');
    }
}

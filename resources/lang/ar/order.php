<?php

return [
    "email_activation"=> "تفعيل البريد",
    "your_email_has_been_activated"=> "تم تفعيل تآكيد بريدك الإلكتروني",
    "offers"=> "العروض",
    "orders"=> "الطلبات",
    "account"=> "الحساب",
    "suspended"=> "تم إيقاف حسابك من قبل الادارة بسبب (reason_ar) ",
    "accepted_offer"=> "قبول العرض",
    "accepted_offer_message"=> "تم قبول العرض ودفعه بنجاح\nالرجاء الإنتظار حتي يتم قبول طلب من مقدم العرض",
    "accepted_offer_with_payed_message"=> "تم قبول العرض ودفعه بنجاح.",
    "owner_accepted_offer"=>
        "تم الموافقة على العرض المقدم من قبلك على طلب :order_name",
    "remove_order"=> "تم سحب الطلب :order_name",
    "follow_categories"=> "أقسامي",
    "new_category_of_order"=> "تم إضافة طلب :order_name في قسم :category_name_ar ",
    "add_new_offer"=>
        "تم إضافة عرض جديد على طلبك :order_name من قبل المستخدم # :user_name ",
    "edit_new_offer"=>
        "تم تعديل العرض المقدم  على طلبك :order_name من قبل المستخدم # :user_name ",
    "offer_payed_and_waiting_golden_activation"=>
        "قام المستخدم # :user_name  بتاكيد العرض المقدم على :order_name يمكنك الان اتمام الطلب أو تفعيل العرض الذهبي بالدخول علي تفاصيل الطلب \nأو مراجعة بيانات صاحب العرض بالدخول على السلة ",
    "offer_payed_and_finish_order"=>
        "قام المستخدم # :user_name  بتاكيد العرض المقدم على :order_name يمكنك الان الذهاب للسلة لعرض معلوماته",
    "end_bidding_duration_golden_offer"=> "تم انتهاء مدة الطرح للعرض الذهبي بالطلب :order_name",
    "end_bidding_duration"=> "تم انتهاء مدة الطرح الخاصة بالطلب :order_name",
    "offer_end_bidding_duration"=>
        "تم انتهاء مدة الطرح الخاصة بالطلب :order_name",
    "bidding_duration"=> "مدة الطرح",
    "waiting_duration"=> "مدة الانتظار",
    "waiting_duration_ended"=>
        "إنتهي الوقت ولم يقم  المستخدم # :user_name  بتاكيد عرضه على طلبك :order_name يمكنك اختيار عرض آخر ",
    "golden_offer"=> "فرصة ذهبية",
    "add_golden_offer" => "تم إضافة العرض الذهبي",
    "reintroduced_golden_offer" => "تم إعادة طرح العرض الذهبي بنجاح",
    "golden_offer_activated"=>
        "تم تفعيل العرض الذهبي على طلب :order_name الرجاء تقديم عرض أفضل من :order_price  ﷼",
    "golden_offer_updated"=> "تم تمديد العرض الذهبي على طلب :order_name ",
    "order_accepted"=> "تم قبول طلبك  :order_name",
    "offer_accepted"=> "تم قبول عرضك علي الطلب  :order_name",
    "order_refuse"=> " تم رفض طلبك  :order_name بسبب :message_ar",
    "order_updated_by_admin"=> "تم تعديل وقبول طلبك  :order_name",
    "order_refuse_by_admin"=> " تم رفض طلبك  :order_name ",
    "order_refund_requested_accepted"=> "  تم قبول طلب إسترداد المبلغ على طلبك  :order_name ",
    "order_refund_requested_refuse"=> "   تم رفض  إسترداد المبلغ على طلبك  :order_name بسبب :message_ar",

    "update_bidding_duration" => " تم إعادة طرح الطلب :order_name من المستخدم :user_name",
    "cancel_order_with_user_name" => " تم سحب الطلب :order_name من المستخدم :user_name",
    "cancel_offer_with_user_name" => " تم سحب العرض من المستخدم :user_name على الطلب :order_name بسعر :price ",
    "cancel_offer"  =>"تم إلغاء العرض  بنجاح",
    'suspend_order' => 'حظر الطلبات',
    'suspend_order_message' => ' تم حظر الطلب   order_name:  الرجاء التواصل مع الإدارة ',

    'promo_code_not_exists' => "هذا الكود غير موجود لدينا",
    'promo_code_used' => "تم إستخدام الكود مسبقا ",
    'promo_code_suspend' => "تم إيقاف صلاحية الكود للإستخدام مؤقتا",
    'promo_code_finished' => "تم إنتهاء صلاحية الكود للإستخدام ",

    "rating" => "التقيمات",
    "order_rate_by_user" => " تم تقييم عرضك على الطلب :order_name من المستخدم # :user_name",
    "order_rate_by_offer_man" => "تم تقييم طلبك :order_name  من مقدم العرض",
    'rate_order_user' => "شكرا لك  يا :user_name  الرجاء تقيم الطلب :order_name ",
    'rate_order_offer_man' => "شكرا لك  :user_name  الرجاء تقيم الطلب :order_name ",
    'order_rating' => 'تم تقييم الطلب بنجاح',

    'contact_us' => "تواصل معنا" ,
//    "contact_us_body" => "  تم الرد على رسالتك من قبل الإدارة \n "
    "contact_us_body" => "تم الرد على رسالتك من قبل الإدارة\n الرد: \n :reply \n الرسالة الأصلية: \n :message ",
    "offer_refused" => "تم رفض العرض المقدم على الطلب  :order_name  ",
    "complete_order_user" => "تم إكتمال الطلب  :order_name",
    "complete_order_offer" => "تم إكتمال الطلب  :order_name",

    'offer' => 'العرض',
    'cancel_order' => 'إلغاء الطلب',
    'complaint' => 'شكوى',
    'Interact_order' => 'التفاعل مع الطلب',


    "session_user_time_out" => "عذرا! تم إنتهاء صلاحية الجلسة برجاء تسجيل الدخول مرة اخري" ,

    "owner_order_notify_before_about_offers" => "برجاء النظر في العروض المقدمة لديكم لان الوقت المتبقي هو :time_system ",
    "notify_users_about_interesting_categories" => "مدة تقيم العروض علي الطلب   :order_name على قرب الإنتهاء  ",

    "inqueries_offers" => "تم إرسال إستفسار على طلب :order_name "
];

<?php

return [
    "offers"=> "العروض",
    "orders"=> "الطلبات",
    "account"=> "الحساب",
    "suspended"=> "تم إيقاف حسابك من قبل الادارة بسبب (reason_ar) ",
    "accepted_offer"=> "قبول العرض",
    "owner_accepted_offer"=>
        "تم الموافقة علي العرض المقدم من قبلك علي طلب (order_name)",
    "remove_order"=> "تم سحب الطلب (order_name)",
    "follow_categories"=> "أقسامي",
    "new_category_of_order"=> "تم إضافة طلب جديد في قسم (category_name_ar)",
    "add_new_offer"=>
        "تم إضافة عرض جديد علي طلبك (order_name) من قبل (user_name)",
    "edit_new_offer"=>
        "تم تعديل العرض المقدم  علي طلبك (order_name) من قبل المستخدم (user_name)",
    "offer_payed_and_waiting_golden_activation"=>
        "قام المستخدم (user_name) بتاكيد العرض المقدم علي (order_name) يمكنك الان اتمام الطلب أو تفعيل العرض الذهبي",
    "offer_payed_and_finish_order"=>
        "قام المستخدم (user_name) بتاكيد العرض المقدم علي (order_name) يمكنك الان الذهاب للسلة لعرض معلوماته",
    "end_bidding_duration"=> "تم انتهاء مدة الطرح الخاصة بالطلب (order_name)",
    "offer_end_bidding_duration"=>
        "تم انتهاء مدة الطرح الخاصة بالطلب (order_name)",
    "bidding_duration"=> "مدة الطرح",
    "waiting_duration"=> "مدة الانتظار",
    "waiting_duration_ended"=>
        "إنتهي الوقت ولم يقم (user_name) بتاكيد عرضه على طلبك (order_name) يمكنك اختيار عرض آخر ",
    "golden_offer"=> "فرصة ذهبية",
    "golden_offer_activated"=>
        "تم تفعيل العرض الذهبي علي طلب (order_name) برجاء تقديم عرض أفضل من (order_price) ﷼",
    "order_accepted"=> "تم قبول طلبك  (order_name)",
    "order_updated_by_admin"=> "تم تعديل وقبول طلبك  (order_name)",
];

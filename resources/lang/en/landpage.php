<?php

return [

    'navbar' => [
        'about_us'   =>  ' About Us  ',
        'services'   =>  'Services  ',
        'download'   =>  'DownloadApp',
        'contact'    =>  'Contact Us ',
        'lang'       =>  'AR ',
    ],

    'about_content' => [
        'title' => 'About the application of Bargin',
        'body' => 'Bargin is the first and largest Saudi competitive deals platform established in 2020 during the Corona pandemic crisis,
                    to facilitate the buyer and seller in the process of bidding and bidding on special products and services or projects,
                    and accessing the best opportunities for requests and offers available in the market professionally,
                    accurately and without the need To spend effort, time and money through regular methods'
    ],

    'services_content' => [
        'show_orders' => 'See all market demands',
        'show_offers' => 'Submit your offer',
        'show_details' => 'Earn order and get data',
    ],

    'download_content' => [
        'download_app_now' => 'Download the app now! Get many of the services, including tenders and auctions',
        'download_app_store' => 'Download the application now on the Apple Store and Google Play and enjoy many of our available services',
    ],

    'contact_us_content' =>[
        'contact_us' => 'Contact Us',
        'contact_us_send' => 'Message Send Success',
        'reason' => 'Reason for communication',
        'name' => 'Name  ',
        'phone' => 'Phone  ',
        'valid_is_saudi_phone' => 'make sure Formula phone pls',
        'email' => 'Email',
        'message' => 'Message',
        'submit' => 'Send',
    ],

    'footer' => [
        'copy_right' => 'All rights reserved Bargain'
    ]



];

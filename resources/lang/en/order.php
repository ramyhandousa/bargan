<?php

return [
    "offers"=> "Offers",
    "orders"=> "Orders",
    "email_activation"=> "Email activation",
    "your_email_has_been_activated"=> "Your email has been confirmed",
    "account"=> "Account",
    "suspended"=> "You are suspended because of (reason_ar)",
    "accepted_offer"=> "Accept offer",
    'accepted_offer_message' => "Bid has been accepted and successfully paid \n Please wait while the bidder's request is accepted",
    'accepted_offer_with_payed_message' => "Bid has been accepted and successfully paid .",
    "owner_accepted_offer"=> "Your offer on :order_name was accepted",
    "remove_order"=> "Order :order_name was removed",
    "follow_categories"=> "My categories",
    "new_category_of_order"=> " :order_name order was added on :category_name_en ",
    "add_new_offer"=> "A new offer was added on :order_name by User # :user_name",
    "edit_new_offer"=> "An offer on :order_name was edited by User # :user_name",
    "offer_payed_and_waiting_golden_activation"=>
        "User # :user_name confirmed his offer on :order_name , you can now finish order or activate golden offer  By entering the order details \nor reviewing the bidder's data by entering the basket",
    "offer_payed_and_finish_order"=>
        "User # :user_name confirmed his offer on  :order_name, you can now view his information in cart",
    "end_bidding_duration_golden_offer"=> "GoldenOffer in :order_name bidding duration has ended",

    "end_bidding_duration"=> "Order :order_name bidding duration has ended",
    "offer_end_bidding_duration"=>
        "Order :order_name bidding duration has ended",
    "bidding_duration"=> "Bidding duration",
    "waiting_duration"=> "Waiting duration",
    "waiting_duration_ended"=>
        "Waiting duration has ended but User # :user_name didn't confirm his offer on :order_name you can choose another one",
    "golden_offer"=> "Golden offer",
    "add_golden_offer" => "Golden offer add Successful",
    "reintroduced_golden_offer" => "The golden offer has been successfully reintroduced",
    "golden_offer_activated"=>
        "Order :order_name owner activated golden offer , please submit an offer better than  :order_price SAR",
    "golden_offer_updated"=> "The golden offer has been extended upon request :order_name",
    "order_accepted"=> "Your order :order_name been accepted",
    "offer_accepted"=> "Your offer on :order_name been accepted",

    "order_refuse"=> "Your order :order_name been rejected because of :message_en",
    "order_updated_by_admin"=>
        "Your order :order_name been edited and accepted",

    "order_refuse_by_admin"=> "Your order :order_name been rejected ",
    "order_refund_requested_accepted"=> " The refund request on your request has been accepted  :order_name ",
    "order_refund_requested_refuse"=> "The refund on your request has been rejected  :order_name because of  :message_en",


    "update_bidding_duration" => "Order :order_name has been re-launched by user :user_name",
    "cancel_order_with_user_name" => "Your Order :order_name Has Been Cancel by user :user_name",
    "cancel_offer_with_user_name" => " Offer Has Been Cancel by user #:user_name On Order :order_name with price :price",
    "cancel_offer" => "Offer canceled successfully",

    'suspend_order' => 'Suspend Order',
    'suspend_order_message' => '   Your order :order_name Has Been Suspend Please contact the administration',
    'promo_code_not_exists' => "We do not have this code",
    'promo_code_used' => "The code has already been used ",
    'promo_code_finished' => "The code has expired for use ",
    'promo_code_suspend' => "The validity of the code has been temporarily suspended",


    "rating" => "rating",
    "order_rate_by_user" => "Your bid has been evaluated on demand :order_name Mr :user_name",
    "order_rate_by_offer_man" => "Your request has been evaluated :order_name from the bidder",

    'rate_order_user' => "Thank you, please rate the order :order_name",
    'rate_order_offer_man' => "Thank you, please rate the order :order_name",
    'order_rating' => 'Order Rating Successful',

    'contact_us' => "Contact Us" ,
    "contact_us_body" => " Your message has been answered by admin \n  :reply \n original message \n :message",

    "offer_refused" => "Your offer were rejected on the order :order_name  ",
    "complete_order_user" => "Order :order_name has been completed ",
    "complete_order_offer" => "Order :order_name has been completed ",


    "session_user_time_out" => "Excuse me! The session has expired, please log in again" ,
    "owner_order_notify_before_about_offers" => "Please consider your offers because the remaining time is :time_system ",

    "notify_users_about_interesting_categories" => "The period for evaluating offers on demand near expiration ",

    "inqueries_offers" => "An inquiry has been sent on a request :order_name "
];

<?php



return [


    'register_details' => [
        'hello' => 'hello ....',
        "coming" => "Register with us the product or service, whether it is a personal need for you or your business, which you wish to buy or sell at the best price.",
        'name' => 'name',
        'first_name' => 'الاسم الأول',
        'last_name' => 'الاسم الأخير',
        'email'=> ' email',
        'phone'=> ' phone',
        "hoppies" => "What is the good or service you want to buy?  ",
        'dreams' => "What is the good or service you would like to sell? ",
        'password'=> 'الباسورد',
        'confirm_password'=> ' تاكيد الباسورد ',
        'day'=> 'اليوم',
        'month'=> 'الشهر',
        'year'=> ' السنة',
        'gender'=> 'النوع ',
        'gender_male'=> 'ذكر',
        'gender_female'=> 'أنثي ',
        'contact_details'=> 'بيانات الاتصال',
        'company'=> 'الشركة',
        'company_phone'=> 'رقم الشركة',
        'telephone'=> 'رقم الهاتف',
        'fax'=> 'فاكس ',
        'address_details'=> 'تفاصيل العنوان',
        'address'=> 'العنوان',
        'city'=> 'الدولة',
        'region'=> 'المدينة',
        'zip'=> 'الرمز البريدي',
        'text_static' => 'تسري على عمليات الشراء والدفع جميع الانظمه المعمول بها في المملكة العربية السعودية',
        'text_terms' => 'أوافق علي الشروط و الأحكام',
        'back' => 'رجوع',
        'submit' => 'تسجيل',
        'send' => 'Send',
    ],
    'footer' => [
        'copyright' => 'كل الحقوق محفوظة. حقوق النشر',
        'footer_text' => 'الاشتراك في تلقي النشرة الإخبارية لدينا',
    ],
];

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
      xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn't be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting"> <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title>bargain</title> <!-- The title tag shows in email notifications, like Android 4.4. -->

    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

    <!-- CSS Reset : BEGIN -->
    <style>
        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
            background: #fff;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin: 0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
        a {
            text-decoration: none;
        }

        /* What it does: A work-around for email clients meddling in triggered links. */
        *[x-apple-data-detectors],
            /* iOS */
        .unstyle-auto-detected-links *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }

        /* What it does: Prevents Gmail from changing the text color in conversation threads. */
        .im {
            color: inherit !important;
        }

        /* If the above doesn't work, add a .g-img class to any image in question. */
        img.g-img+div {
            display: none !important;
        }

        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you'd like to fix */

        /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
        @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
            u~div .email-container {
                min-width: 320px !important;
            }
        }

        /* iPhone 6, 6S, 7, 8, and X */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
            u~div .email-container {
                min-width: 375px !important;
            }
        }

        /* iPhone 6+, 7+, and 8+ */
        @media only screen and (min-device-width: 414px) {
            u~div .email-container {
                min-width: 414px !important;
            }
        }
    </style>

    <!-- CSS Reset : END -->

    <!-- Progressive Enhancements : BEGIN -->
    <style>
        .primary {
            background: #30e3ca;
        }

        .bg_white {
            background: #ffffff;
        }

        .bg_light {
            background: #fafafa;
        }

        .bg_black {
            background: #000000;
        }

        .bg_dark {
            background: rgba(0, 0, 0, .8);
        }

        .email-section {
            padding: 2.5em;
        }

        /*BUTTON*/
        .btn {
            padding: 10px 15px;
            display: inline-block;
        }

        .btn.btn-primary {
            border-radius: 5px;
            background: #30e3ca;
            color: #ffffff;
        }

        .btn.btn-white {
            border-radius: 5px;
            background: #ffffff;
            color: #000000;
        }

        .btn.btn-white-outline {
            border-radius: 5px;
            background: transparent;
            border: 1px solid #fff;
            color: #fff;
        }

        .btn.btn-black-outline {
            border-radius: 0px;
            background: transparent;
            border: 2px solid #000;
            color: #000;
            font-weight: 700;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            font-family: 'Lato', sans-serif;
            color: #000000;
            margin-top: 0;
            font-weight: 400;
        }

        body {
            font-family: 'Lato', sans-serif;
            font-weight: 400;
            font-size: 15px;
            line-height: 1.8;
            color: rgba(0, 0, 0, .4);
        }

        a {
            color: #30e3ca;
        }

        table {}

        /*LOGO*/

        .logo h1 {
            margin: 0;
        }

        .logo h1 a {
            color: #30e3ca;
            font-size: 24px;
            font-weight: 700;
            font-family: 'Lato', sans-serif;
        }

        /*HERO*/
        .hero {
            position: relative;
            z-index: 0;
        }

        .hero .text {
            color: rgba(0, 0, 0, .3);
        }

        .hero .text h2 {
            color: #000;
            font-size: 40px;
            margin-bottom: 0;
            font-weight: 400;
            line-height: 1.4;
        }

        .hero .text h3 {
            font-size: 24px;
            font-weight: 300;
        }

        .hero .text h2 span {
            font-weight: 600;
            color: #30e3ca;
        }


        /*HEADING SECTION*/
        .heading-section {}

        .heading-section h2 {
            color: #000000;
            font-size: 28px;
            margin-top: 0;
            line-height: 1.4;
            font-weight: 400;
        }

        .heading-section .subheading {
            margin-bottom: 20px !important;
            display: inline-block;
            font-size: 13px;
            text-transform: uppercase;
            letter-spacing: 2px;
            color: rgba(0, 0, 0, .4);
            position: relative;
        }

        .heading-section .subheading::after {
            position: absolute;
            left: 0;
            right: 0;
            bottom: -10px;
            content: '';
            width: 100%;
            height: 2px;
            background: #30e3ca;
            margin: 0 auto;
        }

        .heading-section-white {
            color: rgba(255, 255, 255, .8);
        }

        .heading-section-white h2 {
            font-family:
            line-height: 1;
            padding-bottom: 0;
        }

        .heading-section-white h2 {
            color: #ffffff;
        }

        .heading-section-white .subheading {
            margin-bottom: 0;
            display: inline-block;
            font-size: 13px;
            text-transform: uppercase;
            letter-spacing: 2px;
            color: rgba(255, 255, 255, .4);
        }


        ul.social {
            padding: 0;
        }

        ul.social li {
            display: inline-block;
            margin-right: 10px;
        }

        /*FOOTER*/

        .footer {
            border-top: 1px solid rgba(0, 0, 0, .05);
            color: rgba(0, 0, 0, .5);
        }

        .footer .heading {
            color: #000;
            font-size: 20px;
        }

        .footer ul {
            margin: 0;
            padding: 0;
        }

        .footer ul li {
            list-style: none;
            margin-bottom: 10px;
        }

        .footer ul li a {
            color: rgba(0, 0, 0, 1);
        }


        @media screen and (max-width: 500px) {}
    </style>


</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #fff;">
<center style="width: 100%; background-color: #fff;">
    <div
        style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
        &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
    </div>
    <div style="max-width: 600px; margin: 0 auto;" class="email-container">
        <!-- BEGIN BODY -->
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%"
               style="margin: auto;">
            <tr>
                <td valign="top" class="bg_white"
                    style="padding: 35px 35px 20px 35px; background-color: #ffffff; border-radius: 50px 50px 0 0;"
                    bgcolor="#ffffff">
                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td align="center" style="font-size: 16px;  line-height: 24px; padding-top: 25px;">
                                <img src="https://bargain.com.sa/public/BargainLanding/images/bg.png" width="75"
                                     style="display: block; border: 0px;" /><br>
                                <h2
                                    style="font-size: 30px; font-weight: 800; line-height: 36px; color: #333333; margin: 1em 0 0 0;">
                                    بيانات التواصل </h2>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr><!-- end tr -->


            <!-- 1 Column Text + Button : END -->
        </table>
        <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%"
               style="margin: auto;">
            <tr>
                <td valign="middle" class="bg_light footer email-section"
                    style="background-color: #ffffff; border-radius: 0 0 50px 50px;" bgcolor="#ffffff">
                    <table>
                        <tr>
                        <!-- {{--                            <td valign="top" width="33.333%" style="padding-top: 20px;">--}}
                        {{--                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">--}}
                        {{--                                    <tr>--}}
                        {{--                                        <td style="text-align: left; padding-right: 10px;">--}}
                        {{--                                            <h3 class="heading">About</h3>--}}
                        {{--                                            <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>--}}
                        {{--                                        </td>--}}
                        {{--                                    </tr>--}}
                        {{--                                </table>--}}
                        {{--                            </td>--}} -->
                            <td valign="top" width="100%">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td
                                            style="text-align: right; direction: rtl; padding-left: 5px; padding-right: 5px; font-size: 20px">
                                            <h3 class="heading" style="font-weight: bold;">بيانات الطلب</h3>
                                            <p>
                                                رقم الطلب :
                                            {{$order->id}}
                                            <p>
                                            <p>
                                                رقم المستخدم :
                                            {{$order->uuid}}
                                            <p>
                                            <p>
                                                عنوان الطلب :
                                            {{$order->title}}
                                            <p>
                                            <p>
                                                السعر المتوقع للطلب :
                                            {{$order->expected_price}}
                                            <p>
                                            <p>
                                                لديه فرصه ذهبية :
                                            {{$order->golden_offer ? "نعم" : "لا " }}
                                            <p>
                                            <p>
                                                إسم القسم :
                                            {{$order->category->name}}
                                            <p>
                                            <p>
                                                السعر المقبول للطلب :
                                            {{ optional($order->my_offer)->expected_price }}
                                            <p>

                                            <p> نوع المنافسة :
                                                @if($order->account_type == 'personal')
                                                    شخصي
                                                @else
                                                    تجاري
                                                @endif
                                                -
                                                @if($order->order_type == 'auction')
                                                    مزاد
                                                @else
                                                    مناقصة
                                                @endif

                                                @if($order->service_type == 'product')
                                                    - منتج
                                                @else
                                                    - خدمة
                                                @endif
                                            <p>

                                            <p>
                                                وصف :
                                            {{$order->description }}
                                            <p>

                                            <p>
                                                مده الطرح   :
                                            @if( ($order->bidding_duration /60 ) < 24)
                                                    {{number_format($order->bidding_duration /60 ),0}}  ساعة
                                            @elseif(($order->bidding_duration /60) == 24  )
                                                يوم
                                            @elseif(($order->bidding_duration /60) > 24 && ($order->bidding_duration /60) < 48)
                                                يومين
                                            @elseif(($order->bidding_duration /60) > 48 && ($order->bidding_duration /60) < 72)
                                                3 أيام
                                            @elseif(($order->bidding_duration /60) > 72 && ($order->bidding_duration /60) < 10080)
                                                اسبوع
                                            @elseif(($order->bidding_duration /60) > 10080 && ($order->bidding_duration /60) < 20160)
                                                أسبوعين
                                            @elseif(($order->bidding_duration /60) > 20160 && ($order->bidding_duration /60) < 43800)
                                                شهر
                                            @else
                                                    شهرين
                                            @endif

                                            <p>
                                            <p>
                                                @if($order->delivery_duration)
                                                مده التوصيل   :
                                                @if( ($order->delivery_duration /60 ) < 24)
                                                        {{number_format($order->delivery_duration /60 ),0}}  ساعة
                                                @elseif(($order->delivery_duration /60) == 24  )
                                                    يوم
                                                @elseif(($order->delivery_duration /60) > 24 && ($order->delivery_duration /60) < 48)
                                                    يومين
                                                @elseif(($order->delivery_duration /60) > 48 && ($order->delivery_duration /60) < 72)
                                                    3 أيام
                                                @elseif(($order->delivery_duration /60) > 72 && ($order->delivery_duration /60) < 10080)
                                                    اسبوع
                                                @elseif(($order->delivery_duration /60) > 10080 && ($order->delivery_duration /60) < 20160)
                                                    أسبوعين
                                                @elseif(($order->delivery_duration /60) > 20160 && ($order->delivery_duration /60) < 43800)
                                                    شهر
                                                @else
                                                        شهرين
                                                @endif

                                            @endif

                                            <p>
                                            @if($order->warranty)
                                            <p>
                                                مدة الضمان:
                                                {{ optional($order->warranty)->name}}
                                            </p>
                                            @endif
{{--                                            <p>Minutes : {{\Carbon\Carbon::now()->subMinutes($order->bidding_duration /60)->diffForHumans()}}</p>--}}

                                            @if($order->account_type == 'commercial' && $order->order_type == "tender")

                                                <p>
                                                    مده التسليم :

                                                    @if( ($order->delivery_duration /60 ) < 24)
                                                        {{number_format($order->delivery_duration /60 ),0}}  ساعة
                                                    @elseif(($order->delivery_duration /60) == 24  )
                                                        يوم
                                                    @elseif(($order->delivery_duration /60) > 24 && ($order->delivery_duration /60) < 48)
                                                        يومين
                                                    @elseif(($order->delivery_duration /60) > 48 && ($order->delivery_duration /60) <= 72)
                                                        3 أيام
                                                    @elseif(($order->delivery_duration /60) > 72 && ($order->delivery_duration /60) < 10080)
                                                        اسبوع
                                                    @elseif(($order->delivery_duration /60) > 10080 && ($order->delivery_duration /60) < 20160)
                                                        أسبوعين
                                                    @elseif(($order->delivery_duration /60) > 20160 && ($order->delivery_duration /60) < 43800)
                                                        شهر
                                                    @else
                                                        شهرين
                                                    @endif
                                                <p>

                                                @if($order->delivery_way)
                                                    <p>
                                                        طريقة الاستلام   :
                                                    {{ optional($order->delivery_way)->name }}
                                                    <p>
                                                @endif


                                                @if($order->industry)
                                                    <p>
                                                        الصناعه     :
                                                    {{ optional($order->industry)->name }}
                                                    <p>
                                                @endif

                                                @if($order->payment_method)
                                                    <p>
                                                        طريقة الدفع     :
                                                    {{ optional($order->payment_method)->name }}
                                                    <p>
                                                @endif

                                            @endif

                                            @if($order->images)
                                                <p>صور الطلب    :<p>
                                                    @foreach($order->images as $image)
                                                        <img style="width: 200px;height: 150px;border-radius: 10px;margin-left: 10px;object-fit: cover;" src="{{URL('/').'/'.$image}}" alt="image">
                                                @endforeach
                                            @endif

                                        </td>
                                    </tr>
                                </table>
                            </td>

                        </tr>
                        <tr>
                            <td valign="top" width="100%" style="padding-top: 20px;">
                                <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                    <tr>
                                        <td style="text-align: right;  direction: rtl; padding-left: 10px; font-size: 20px">
                                            <h3 class="heading" style="font-weight: bold;">البيانات الشخصية </h3>
                                            <p>
                                                الاسم :
                                            {{$user->name}}
                                            <p>
                                            <p>
                                                الهاتف :
                                            {{$user->phone}}
                                            <p>
                                            <p>
                                                الإيميل الشخصي :
                                            {{$user->email}}
                                            <p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr><!-- end: tr -->


        </table>

        <br>
        <a href="https://www.facebook.com/Bargain1sa/">
            <img src="https://i.imgur.com/eciK6eU.png" width="30" style="margin: 5px;">
        </a>
        <a href="https://twitter.com/bargain1sa">
            <img src="https://i.imgur.com/mhIWnHB.png" width="30" style="margin: 5px;">
        </a>
        <a href="https://www.snapchat.com/add/Bargain1sa/">
            <img src="https://i.imgur.com/9nJ2epy.png" width="30" style="margin: 5px;">
        </a>
        <a href="https://www.instagram.com/Bargain1sa/">
            <img src="https://i.imgur.com/Mga3N2C.jpg" width="30" style="margin: 5px;">
        </a>
        <a href="https://www.linkedin.com/company/bargainsa/">
            <img src="https://i.imgur.com/LdUCwc6.png" width="30" style="margin: 5px;">
        </a>
        <a href="https://youtube.com/channel/UCl8iZfx3gL5Z16wzmssG9HA">
            <img src="https://i.imgur.com/tSuXN8P.png" width="30" style="margin: 5px;">
        </a>
    </div>
</center>
</body>

<script>


    function format( duration) {

        var maxParts = 2,
            secondsToShowNow = 0;
        var delta = duration;
        if (delta <= (secondsToShowNow)) return formatter.getNow();
        var years = (delta / (2592000 * 12)).floor();
        delta -= years * (2592000 * 12);
        var months = (delta / 2592000).floor();
        delta -= months * 2592000;
        var weeks = (delta / 86400).floor() ~/ 7;
        delta -= weeks * 7 * 86400;
        var days = (delta / 86400).floor();
        delta -= days * 86400;
        var hours = (delta / 3600).floor() % 24;
        delta -= hours * 3600;
        var minutes = (delta / 60).floor() % 60;
        delta -= minutes * 60;
        var seconds = delta % 60;
        var text;
        var parts = 0;
        if (years > 0) {
            if (text == null) {
                text = formatter.getYears(years);
            } else {
                text += formatter.getSeparator() + formatter.getYears(years);
            }
            parts++;
            if (parts >= maxParts) return text;
        }
        if (months > 0) {
            if (text == null) {
                text = formatter.getMonths(months);
            } else {
                text += formatter.getSeparator() + formatter.getMonths(months);
            }
            parts++;
            if (parts >= maxParts) return text;
        }
        if (weeks > 0) {
            if (text == null) {
                text = formatter.getWeeks(weeks);
            } else {
                text += formatter.getSeparator() + formatter.getWeeks(days);
            }
            parts++;
            if (parts >= maxParts) return text;
        }
        if (days > 0) {
            if (text == null) {
                text = formatter.getDays(days);
            } else {
                text += formatter.getSeparator() + formatter.getDays(days);
            }
            parts++;
            if (parts >= maxParts) return text;
        }
        if (hours > 0) {
            if (text == null) {
                text = formatter.getHours(hours);
            } else {
                text += formatter.getSeparator() + formatter.getHours(hours);
            }
            parts++;
            if (parts >= maxParts) return text;
        }
        if (minutes > 0) {
            if (text == null) {
                text = formatter.getMinutes(minutes);
            } else {
                text += formatter.getSeparator() + formatter.getMinutes(minutes);
            }
            parts++;
            if (parts >= maxParts) return text;
        }
        if (seconds > 0) {
            if (text == null) {
                text = formatter.getSeconds(seconds);
            } else {
                text += formatter.getSeparator() + formatter.getSeconds(seconds);
            }
            parts++;
            if (parts >= maxParts) return text;
        }
        return text ?? '';
    }
</script>

</html>

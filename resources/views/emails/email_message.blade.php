<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <style type="text/css">
        @media screen {
            @font-face {
                font-family: 'Lato';
                font-style: normal;

                src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: normal;
                font-weight: 700;
                src: local('Lato Bold'), local('Lato-Bold'), url(https://fonts.gstatic.com/s/lato/v11/qdgUG4U09HnJwhYI-uK18wLUuEpTyoUstqEm5AMlJo4.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: italic;

                src: local('Lato Italic'), local('Lato-Italic'), url(https://fonts.gstatic.com/s/lato/v11/RYyZNoeFgb0l7W3Vu1aSWOvvDin1pK8aKteLpeZ5c0A.woff) format('woff');
            }

            @font-face {
                font-family: 'Lato';
                font-style: italic;
                font-weight: 700;
                src: local('Lato Bold Italic'), local('Lato-BoldItalic'), url(https://fonts.gstatic.com/s/lato/v11/HkF_qI1x_noxlxhrhMQYELO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
            }
        }

        /* CLIENT-SPECIFIC STYLES */
        body,
        table,
        td,
        a {
            font-family: 'Tajawal', sans-serif;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            font-weight: 800;
            text-decoration: none;
        }

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /* MOBILE STYLES */
        @media screen and (max-width:600px) {
            h1 {
                font-size: 32px !important;
                line-height: 32px !important;
            }
        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style>
</head>

<body style="background-color: #fff; margin: 0 !important; padding: 0 !important;">
<!-- HIDDEN PREHEADER TEXT -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#4284f3" align="center">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td bgcolor="#4284f3" align="center" style="padding: 0px 10px 0px 10px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; direction:rtl; border-radius: 4px 4px 0px 0px; color: #111111;  font-size: 48px;  line-height: 48px;">
                        <img src="https://bargain.com.sa/public/BargainLanding/images/bg.png" width="75" style="display: block; border: 0px;" />
                        <p style="font-size: 20px;  margin: 2px;">بخصوص
                                  {{$data['title'] ?? ' '}}
                            <br>
                            {{$data['type'] ?? ' '}}
                            <br>
                            {{$data['user'] ?? ' '}}
                            <br>

                            {{$data['message'] ?? ' '}}
                            <br>
                        </p>
                            @if(isset($data['order']))
                                <p >بيانات الطلب</p>
                                <p>
                                    رقم الطلب :
                                {{$data['order']->id}}
                                <p>
                                <p>
                                    عنوان الطلب :
                                {{$data['order']->title}}
                                <p>
                                <p>
                                    السعر المتوقع للطلب :
                                {{$data['order']->expected_price}}
                                <p>
                                <p>
                                    لديه فرصه ذهبية :
                                {{$data['order']->golden_offer ? "نعم" : "لا " }}
                                <p>
                                <p>
                                    إسم القسم :
                                {{$data['order']->category->name}}
                                <p>
                                <p>
                                    السعر المقبول للطلب :
                                {{ optional($data['order']->my_offer)->expected_price }}
                                <p>

                                <p> نوع المنافسة :
                                    @if($data['order']->account_type == 'personal')
                                        شخصي
                                    @else
                                        تجاري
                                    @endif
                                    -
                                    @if($data['order']->order_type == 'auction')
                                        مزاد
                                    @else
                                        مناقصة
                                    @endif

                                    @if($data['order']->service_type == 'product')
                                        - منتج
                                    @else
                                        - خدمة
                                    @endif
                                    <p>

                                    <p>
                                        وصف :
                                    {{$data['order']->description }}
                                <p>

                            @endif
                        <br>
                        <a href="https://www.facebook.com/Bargain1sa/">
                            <img src="https://i.imgur.com/eciK6eU.png" width="20">
                        </a>
                        <a href="https://twitter.com/bargain1sa">
                            <img src="https://i.imgur.com/mhIWnHB.png" width="20">
                        </a>
                        <a href="https://www.snapchat.com/add/Bargain1sa/">
                            <img src="https://i.imgur.com/9nJ2epy.png" width="20">
                        </a>
                        <a href="https://www.instagram.com/Bargain1sa/">
                            <img src="https://i.imgur.com/Mga3N2C.jpg" width="20">
                        </a>
                        <a href="https://www.linkedin.com/company/bargainsa/">
                            <img src="https://i.imgur.com/LdUCwc6.png" width="20">
                        </a>
                        <a href="https://youtube.com/channel/UCl8iZfx3gL5Z16wzmssG9HA">
                            <img src="https://i.imgur.com/tSuXN8P.png" width="20">
                        </a>

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>

</html>

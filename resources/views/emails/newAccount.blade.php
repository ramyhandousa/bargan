<!DOCTYPE html>
<html>

<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link href="https://fonts.googleapis.com/css2?family=Tajawal:wght@300&display=swap" rel="stylesheet">
    <style type="text/css">

        /* CLIENT-SPECIFIC STYLES */
        body,
        table,
        td,
        a {
            font-family: 'Tajawal', sans-serif;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            font-weight: 800;
        }

        table,
        td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            line-height: inherit !important;
        }

        /* MOBILE STYLES */
        @media screen and (max-width:600px) {
            h1 {
                font-size: 32px !important;
                line-height: 32px !important;
            }
        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }
    </style>
</head>

<body style="background-color: #f4f4f4; margin: 0 !important; padding: 0 !important;">
<!-- HIDDEN PREHEADER TEXT -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!-- LOGO -->
    <tr>
        <td bgcolor="#4284f3" align="center">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 10px 40px 10px;"> </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td bgcolor="#4284f3" align="center" style="padding: 0px 10px 0px 10px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#ffffff" align="center" valign="top" style="padding: 40px 20px 20px 20px; border-radius: 50px 50px 0px 0px; color: #111111;  font-size: 48px;  line-height: 48px;">
                        <img src="https://bargain.com.sa/public/BargainLanding/images/bg.png" width="75" style="display: block; border: 0px;" />
                        <h1 style="font-size: 38px;  margin: 2;"> تهانينا ... {{$user->name}}    </h1>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td bgcolor="#f4f4f4" align="center" style="padding: 0px 10px 0px 10px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">

                {{--                <tr>--}}
                {{--                    <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px; color: ;  font-size: 18px;  line-height: 25px;">--}}
                {{--                        <p style="margin: 0;"> {{$user->email}}</p>--}}
                {{--                    </td>--}}
                {{--                    <td bgcolor="#ffffff" align="right" style="padding: 20px 30px 40px 30px; color: ;  font-size: 18px;  line-height: 25px; ">--}}
                {{--                        <p style="margin: 0;">إيميلك الشخصي لدينا   </p>--}}
                {{--                    </td>--}}
                {{--                </tr>--}}
                <tr>
                    <td bgcolor="#ffffff" align="left" style="padding: 20px 30px 40px 30px;  font-size: 18px;  line-height: 25px; border-radius: 0 0 0 50px">
                        <p style="margin: 0;"><a href="{{url('/active/account')}}?_token={{\Illuminate\Support\Str::random(40)}}{{$user->api_token}}" target="_blank">إضغط هنا</a> </p>
                    </td>
                    <td bgcolor="#ffffff" align="right" style="padding: 20px 30px 40px 30px; font-size: 18px;  line-height: 25px; border-radius: 0 0 50px 0">
                        <p style="margin: 0;">لتفعيل الأكونت الخاص بك   </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <!-- <td bgcolor="#f4f4f4" align="center" style="padding: 30px 10px 0px 10px;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                <tr>
                    <td bgcolor="#FABD09" align="center" style="padding: 30px 30px 30px 30px; border-radius: 4px 4px 4px 4px; color: ;  font-size: 18px;  line-height: 25px;">
                        <h2 style="font-size: 20px;  color: #111111; margin: 0;">إذا كان لديك اي مشاكل يمكنك التواصل معنا  </h2>
                        <p style="margin: 0;"><a href="#" target="_blank" style="color: #4284f3;">We&rsquo;re here to help you out</a></p>
                    </td>
                </tr>
            </table>
        </td> -->
    </tr>
    <tr>
    </tr>
</table>
</body>

</html>

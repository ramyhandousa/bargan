
<div class=" section pt-5 pb-5 pr-3 pl-3" id="about">
    <div class="container">
        <div class="row ltr">
            <div class="col-md-6 slide text-center">
                <img src="{{asset('BargainLanding/images/about.png')}}" class="img-fluid py-5">
            </div>
            <div class="col-md-6 slide tips text-right">
                <h3 class="w-700">
                    {{trans('landpage.about_content.title')}}
                </h3>
                <p class="pt-3 pr-3 pr-md-0">
                    {{trans('landpage.about_content.body')}}
                 </p>

            </div>

        </div>
    </div>
</div>

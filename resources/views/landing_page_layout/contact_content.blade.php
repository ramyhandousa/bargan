
<div class=" section bg-red pt-5 pb-5 pr-3 pl-3">
    <div class="container">
        <div class="row" id="contact">
            <div class="col-md-8 col-md-6 m-auto  p-5  contact ">
                <h5 class="w-700 text-center pb-4">
                    {{trans('landpage.contact_us_content.contact_us')}}
                </h5>
                <form id="contact-form">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label class="w-700" for="name"> {{trans('landpage.contact_us_content.name')}}
                        </label>
                        <input type="text" id="name" name="name" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label class="w-700" for="phone">  {{trans('landpage.contact_us_content.phone')}}
                        </label>
                        <input type="text" id="phone" name="phone" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label class="w-700" for="email"> {{trans('landpage.contact_us_content.email')}}
                        </label>
                        <input type="email" id="email" name="email" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label class="w-700" for="phone">{{trans('landpage.contact_us_content.reason')}}</label>
                        <select class="form-control" name="type_id" required style="    height: 50px;
    border: none;
    border-radius: 10px;">
                            @foreach($types as $type)
                                <option value="{{$type->id}}">{{$type->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="w-700" for="msg">{{trans('landpage.contact_us_content.message')}}</label>
                        <textarea class="form-control" name="message" rows="5" id="msg" required></textarea>
                    </div>
                    <button type="submit" id="submit_form" class="btn w-100 bg-dark text-white w-700 border-0 ">
                        {{trans('landpage.contact_us_content.submit')}}
                    </button>
                </form>
            </div>
        </div>
        <div class="d-md-flex justify-content-between text-center pt-md-5 ">
            <div>
                <ul class="p-0 m-0">
                    <li class="d-inline-block mx-2">
                        <a href="{{\App\Models\Setting::getBody('facebook')}}" target="_blank" class="text-dark">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li class="d-inline-block mx-2">
                        <a href="{{\App\Models\Setting::getBody('snapchat')}}" target="_blank" class="text-dark">
                            <i class="fab fa-snapchat-ghost"></i>
                        </a>
                    </li>
                    <li class="d-inline-block mx-2">
                        <a href="{{\App\Models\Setting::getBody('twitter')}}" target="_blank" class="text-dark">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>

                    <li class="d-inline-block mx-2">
                        <a href="{{\App\Models\Setting::getBody('instagram')}}" target="_blank" class="text-dark">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li class="d-inline-block mx-2">
                        <a href="{{\App\Models\Setting::getBody('linkedin')}}" target="_blank" class="text-dark">
                            <i class="fab fa-linkedin"></i>
                        </a>
                    </li>
                    <li class="d-inline-block mx-2">
                        <a href="{{\App\Models\Setting::getBody('youtube')}}" target="_blank" class="text-dark">
                            <i class="fab fa-youtube"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div>
                <p>
                    {{trans('landpage.footer.copy_right')}}
                </p>
            </div>
        </div>
    </div>

</div>


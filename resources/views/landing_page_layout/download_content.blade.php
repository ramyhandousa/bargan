
<div class=" section bg-blue pt-5 pb-5 pr-3 pl-3">
    <div class="container">
        <div class="row ltr" id="download">
            <div class="col-md-6 slide text-center">
                @if($session == "ar")
                    <img src="{{asset('BargainLanding/images/1.png')}}" class="img-fluid header-img">
                @else
                    <img src="{{asset('intro_en.jpg')}}" class="img-fluid header-img">
                @endif

            </div>
            <div class="col-md-6 slide my-auto tips text-right">
                <h3 class="w-700">
                    {{trans('landpage.download_content.download_app_now')}}
                </h3>
                <p class="pt-3">

                    {{trans('landpage.download_content.download_app_store')}}
                </p>
                <div class="my-5">
                    <a href="">
                        <img src="{{asset('BargainLanding/images/google_play.png')}}" class="store" alt="">
                    </a>
                    <a href="">
                        <img src="{{asset('BargainLanding/images/app-store.png')}}" class="store" alt="">
                    </a>
                </div>
            </div>

        </div>
    </div>

</div>


<header>
    <nav class="navbar navbar-expand-lg fixed-top">
        <div class="container ">
            <a href="#" class="d-none d-xl-block d-md-block bg-white" style="padding:10px; border-radius:10px">
                <img src="{{asset('BargainLanding/images/logo1.png')}}" class="logo" style="width:80px">
            </a>
            <a href="#" class="d-xl-none d-md-none bg-white" style="padding:10px; border-radius:10px">
                <img src="{{asset('BargainLanding/images/logo1.png')}}" class="logo" style="width:80px">
            </a>
            <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">

                <ul class="navbar-nav mr-auto">
                    <li class="nav-item mx-3 ">
                        <a class="nav-link " href="#about">
                            {{trans('landpage.navbar.about_us')}}
                        </a>
                    </li>
                    <li class="nav-item mx-3 ">
                        <a class="nav-link " href="#services">
                            {{trans('landpage.navbar.services')}}
                        </a>
                    </li>
                    <li class="nav-item mx-3 active">
                        <a class="nav-link " href="#download">
                            {{trans('landpage.navbar.download')}}
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link " href="#contact">
                            {{trans('landpage.navbar.contact')}}
                        </a>
                    </li>
                    <li class="nav-item mx-3">
                        <a class="nav-link " href="#"
                       onclick="return change_lang();"
                        >
                            {{trans('landpage.navbar.lang')}}
                        </a>
                    </li>
                </ul>

            </div>
        </div>

    </nav>
</header>

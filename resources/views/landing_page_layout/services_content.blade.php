
<div class=" section bg-yellow pt-5 pb-5 pr-3 pl-3 d-flex">
    <div class="container">
        <div class="row h-100" id="services">

            <div class="col-md-4 serv slide fp-auto-height ">
                <div class="servCard">
                    <img src="{{asset('BargainLanding/images/serv1.png')}}" class="img-fluid">
                </div>
                <h4 class="w-700 text-center mt-5">
                     {{trans('landpage.services_content.show_orders')}}
                </h4>
            </div>
            <div class="col-md-4 serv slide fp-auto-height ">
                <div class="servCard">
                    <img src="{{asset('BargainLanding/images/serv2.png')}}" class="img-fluid">
                </div>
                <h4 class="w-700 text-center mt-5">
                    {{trans('landpage.services_content.show_offers')}}
                </h4>
            </div>
            <div class="col-md-4 serv slide fp-auto-height ">
                <div class="servCard">
                    <img src="{{asset('BargainLanding/images/serv3.png')}}" class="img-fluid">
                </div>
                <h4 class="w-700 text-center mt-5">
                    {{trans('landpage.services_content.show_details')}}
                </h4>
            </div>


        </div>
    </div>
</div>

<!DOCTYPE html>
<html lang="ar">

<head>
    <!-- Meta-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title-->
    <title>Bargain</title>
    <link rel="shortcut icon" type="image/png" href="{{asset('BargainLanding/images/fav.ico')}}" />
    <!--Fonts -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('BargainLanding/css/bootstrap.min.css')}}">
    @if($session != "en")
        <link rel="stylesheet" href="{{asset('BargainLanding/css/bootstrap-rtl.min.css')}}">
    @endif
    <link rel="stylesheet" href="{{asset('BargainLanding/fonts/fontawesome/css/all.css')}}">
    <link rel="stylesheet" href='{{asset('BargainLanding/css/animate.css')}}'>
    <link rel="stylesheet" href="{{asset('BargainLanding/css/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">


@if($session == "en")
            <link rel="stylesheet" href="{{asset('BargainLanding/css/style-en.css')}}">
            <style>
                html, body, h1,h2,h3,h4,h5,h6,p{
    text-align:left !important;
    direction: ltr;
}
            </style>
    @endif
    @yield('styles')
</head>

<body>

@include('landing_page_layout.header')
<!--  -->
<div class="p-0" id="fullpage">
    <div class=" text-dark">

        @include('landing_page_layout.about_content')
        @include('landing_page_layout.services_content')
        @include('landing_page_layout.download_content')
        @include('landing_page_layout.contact_content')
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
</div>

@yield('scripts')
<!--scripts -->

<script type="text/javascript" src="{{asset('BargainLanding/js/jquery-3.2.1.min.js')}}"></script>
<script type="text/javascript" src="{{asset('BargainLanding/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('BargainLanding/js/wow.min.js')}}"></script>
<script src="{{asset('BargainLanding/js/easings.min.js')}}"></script>
<script type="text/javascript" src="{{asset('BargainLanding/js/scrolloverflow.min.js')}}"></script>

<script type="text/javascript" src="{{asset('BargainLanding/js/scripts.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    function change_lang(){
        var   lang = '{{$session == "ar" ?"en": "ar"}}' ,
         url = '{{ route("change_language", ":locale") }}';
        url = url.replace(':locale', lang);
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            success: function (data) {

            },complete:function (){

                setTimeout(function() {
                    location.reload();
                }, 200);
            }
        });

    };
</script>


<script>
    new WOW().init();
</script>
<script>
    $(document).ready(function(){
        // Add smooth scrolling to all links
        $("a").on('click', function(event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function(){

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });
</script>


<script>
    $('form').on('submit', function (e) {
        e.preventDefault();

        var regex = new RegExp(/^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/),
            phone_number = $("#phone").val();

        if (!regex.test(phone_number)){
            $title = '{{trans('landpage.contact_us_content.phone')}}';
            $body = '{{trans('landpage.contact_us_content.valid_is_saudi_phone')}}';
            toastr.options.ltr = true;
            toastr.options = {
                "preventDuplicates": true,
                "preventOpenDuplicates": true
            };
            toastr.error($body,$title)
            return false;
        }

        var formData = new FormData(this);
        $.ajax({
            type: 'POST',
            url: '{{route('submit_contact_us')}}',
            beforeSend:function () {
                $('#submit_form').prop( "disabled", true )
            },
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {

                $title = '{{trans('landpage.contact_us_content.contact_us')}}';
                $body = '{{trans('landpage.contact_us_content.contact_us_send')}}';
                toastr.options.ltr = true;
                toastr.options.progressBar = true;
                toastr.success($body,$title)
                setTimeout(function() {

                    $('#submit_form').prop( "disabled", false )
                }, 5000);
            },
            error: function (data) {
                $('#submit_form').prop( "disabled", false )
            }
        });
    })
</script>
</body>

</html>

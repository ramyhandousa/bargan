@extends('social_media.master')

@section('styles')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">

@endsection
@section('content')


    <!-- table -->
    <div class="table-responsive">
        <table class="table table-bordered text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>إسم صاحب الطلب</th>
                <th>الوصف</th>
                <th>مزاد / مناقصة</th>
                <th> عنوان الطلب</th>
                <th>إسم القسم </th>
                <th>السعر المتوقع</th>
                <th> قبول الطلب</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
                <tr>

                    <td>{{$order->id}}</td>
                    <td>{{optional($order->user)->name}}</td>
                    <td>{{ $order->description }}</td>
                    <td>{{$order->order_type == "auction" ? "مزاد" : "مناقصة"}}</td>
                    <td>{{$order->title}}</td>
                    <td>{{optional($order->category)->name}}</td>
                    <td>{{$order->expected_price}}</td>
                    <td>

                        <a href="javascript:;" data-url="{{ route('order.accepted') }}" id="elementRow{{ $order->id }}" data-id="{{ $order->id }}"
                           class="removeElement btn btn-icon btn-trans btn-xs waves-effect waves-light btn-success m-b-5">
                            <i class="fas fa-check" ></i>
                        </a>


                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>


@endsection


@section('scripts')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>
    <script>
        $('body').on('click', '.removeElement', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            var $tr = $(this).closest($('#elementRow' + id).parent().parent());

            swal({
                title: "قبول الطلب ؟",
                text: "سوف يتم ظهور الطلب في التطبيق .",
                type: "success",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "موافق",
                cancelButtonText: "إلغاء",
                confirmButtonClass: 'btn-success waves-effect  ',
                closeOnConfirm: true,
                closeOnCancel: true,
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: {id: id , "_token": "{{ csrf_token() }}",},
                        dataType: 'json',
                        success: function (data) {

                            if (data.status === true) {
                                var shortCutFunction = 'success';
                                var msg = 'لقد تمت قبول الطلب   بنجاح.';
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

                                $tr.find('td').fadeOut(1000, function () {
                                    $tr.remove();
                                });
                            }

                            if (data.status === false) {
                                var shortCutFunction = 'warning';
                                var msg =data.message;
                                var title = data.title;
                                toastr.options = {
                                    positionClass: 'toast-top-left',
                                    onclick: null
                                };
                                var $toast = toastr[shortCutFunction](msg, title); // Wire up an event handler to a button in the toast, if it exists
                                $toastlast = $toast;

                            }
                        }
                    });
                }
            });
        });


    </script>


{{--    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>--}}
{{--    <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>--}}

@endsection

@extends('social_media.master')

@section("styles")


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    @if($session == "en")
    <link rel="stylesheet " href="{{ asset("social_media/css/style-en.css")}}">
    @endif
@endsection

@section('content')


    <button id="change_lang" class="form-control btn btn-send m-auto d-block" value="{{$session == "ar" ?"ar": "en"}}">
        <span id="my_lang" style=" text-transform: capitalize;">{{$session == "ar" ?"English": "اللغة العربية"}}</span>
    </button>
    <!-- form -->
    @if(session('success'))
        <div class="alert alert-success text-center" style=" text-transform: capitalize;">    {!! session('success') !!} </div>
    @endif
    <div class="row ">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form id="my_form" class="col-md-7 mx-auto" method="POST" action="{{route("register_data")}}" >
            <h3 style=" text-transform: capitalize;">{{trans("site.register_details.hello")}}</h3>
            <p>{{trans('site.register_details.coming')}}</p>
            @csrf
            <div class="form-group wow fadeInDown">
                                <label for="userName" class="input-label">{{trans('site.register_details.name')}}</label>

                <input type="text" max="10" class="form-control @error('name') is-invalid @enderror" name="name" id="userName" required>
                @error('name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group wow fadeInUp">
                                <label for="phone" class="input-label">{{trans("site.register_details.phone")}}</label>

                <input type="tel" class="form-control  @error('phone') is-invalid @enderror" name="phone" id="phone" required>
                @error('phone')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <div class="form-group wow fadeInDown">
                                <label for="email" class="input-label">{{trans('site.register_details.email')}}</label>

                <input type="email" class="form-control  @error('email') is-invalid @enderror" name="email" id="email" required>
                @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>
            <!--<div class="form-group wow fadeInUp">-->
        <!--    <select class="form-control  @error('category') is-invalid @enderror" id="select" name="category">-->
            <!--        <option value="personal">-->
            <!--            شخصي-->
            <!--        </option>-->
            <!--        <option value="commercial">-->
            <!--            تجاري-->
            <!--        </option>-->
            <!--    </select>-->
            <!--    <label for="select" class="input-label">ما هو نوع نشاطك</label>-->
        <!--    @error('category')-->
        <!--    <div class="alert alert-danger">{{ $message }}</div>-->
            <!--    @enderror-->
            <!--</div>-->

            <div class="form-group wow fadeInDown">
                                <label for="desc_category" class="input-label">{{trans('site.register_details.hoppies')}} </label>

                <input class="form-control  @error('desc_category') is-invalid @enderror"  type="text" name="desc_category"
                       data-role="tagsinput" id="category" required  />
                @error('desc_category')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group wow fadeInDown">

                <label for="category" class="input-label">{{trans("site.register_details.dreams")}}  </label>
                <textarea  type="text" name="desc_dream" class="form-control  @error('desc_dream') is-invalid @enderror"  cols="30"
                           data-role="tagsinput" rows="5" required></textarea>
                @error('desc_dream')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group text-center wow fadeInUp">
                <button type="submit" class="btn btn-send">
                    {{trans('site.register_details.send')}}
                </button>
            </div>
        </form>
    </div>


@endsection

@section("scripts")

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>

    <script>

        function checkPhone(e){

            var regex = new RegExp(/^(05)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/),
                phone_number = $("#phone").val();

            if (!regex.test(phone_number)){
                e.preventDefault();
                $title = '{{trans('landpage.contact_us_content.phone')}}';
                $body = '{{trans('landpage.contact_us_content.valid_is_saudi_phone')}}';
                toastr.options.ltr = true;
                toastr.options = {
                    "preventDuplicates": true,
                    "preventOpenDuplicates": true
                };
                toastr.error($body,$title)
                return false;
            }

            return  true;
        }


        // your form
        var form = document.getElementById("my_form");

        // attach event listener
        form.addEventListener("submit", checkPhone, true);


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $("#change_lang").click(function (){
            var input_value = this.value;

            if (input_value === "ar"){
                $("#change_lang").val('en')
                $("#my_lang").html("English")
            }else {
                $("#change_lang").val('ar')
                $("#my_lang").html("اللغة العربية")
            }
            var url = '{{ route("change_language", ":locale") }}';
            url = url.replace(':locale', $("#change_lang").val());

            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'json',
                success: function (data) {

                },complete:function (){

                    setTimeout(function() {
                        location.reload();
                    }, 200);
                }
            });

        });
    </script>

@endsection

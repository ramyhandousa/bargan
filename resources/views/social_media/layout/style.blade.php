<!--Fonts -->
<link href="https://fonts.googleapis.com/css?family=Tajawal" rel="stylesheet">
<!-- Bootstrap -->
<link rel="stylesheet" href="{{ asset("social_media/css/bootstrap.min.css")}}">
@if(isset($session))
    @if($session == "ar")
        <link rel="stylesheet" href="{{ asset("social_media/css/bootstrap-rtl.min.css")}}">
    @endif
@endif
<link rel="stylesheet " href="{{ asset("social_media/css/animate.css")}} ">
<link rel="stylesheet " href="{{ asset("social_media/css/bootstrap-tagsinput.css")}}">
<link rel="stylesheet " href="{{ asset("social_media/css/style.css")}}">
<link rel="shortcut icon" type="image/ico" href="{{ asset("social_media/images/fav.ico")}}">

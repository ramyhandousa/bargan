<!DOCTYPE html>
<html lang="ar">

<head>
    <!-- Meta-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Title-->
    <title>Bargain</title>
    @include(".social_media.layout.style")
    @yield('styles')
</head>

<body>

<div class="container">
    <!-- logo -->
    <div class="text-center">
        <img src="{{ asset("social_media/images/logo2.png")}}" class="img-fluid my-5 logo" style="visibility: hidden !important;">
    </div>

    @yield('content')

</div>
<!--scripts -->

@include(".social_media.layout.scripts")

@yield('scripts')
</body>

</html>

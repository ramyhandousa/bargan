@extends('social_media.master')

@section('content')

    <!-- table -->
    <div class="table-responsive">
        <table class="table table-bordered text-center">
            <thead>
            <tr>
                <th>#</th>
                <th>الاسم</th>
                <th>رقم الجوال</th>
                <th>البريد الالكتروني</th>
                <th>تصنيف</th>
                <th>الاقسام</th>
                <th>إيش تحلم</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr>

                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->phone}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->category == "personal" ? " شخصي " : "تجاري"}}</td>
                    <td>
                        @foreach(explode(',', $user->desc_category) as $str)
                         <span class="badge "> {{$str }} </span>
                        @endforeach
                    </td>

                    <td>{{$user->desc_dream ? : "لا يوجد"}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>

@endsection

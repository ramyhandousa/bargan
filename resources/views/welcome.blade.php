<!DOCTYPE html>
<html>

<head>
    <!--
    If you are serving your web app in a path other than the root, change the
    href value below to reflect the base path you are serving from.

    The path provided below has to start and end with a slash "/" in order for
    it to work correctly.

    For more details:
    * https://developer.mozilla.org/en-US/docs/Web/HTML/Element/base
  -->
    <base href="/">

    <meta charset="UTF-8">
    <meta content="IE=Edge" http-equiv="X-UA-Compatible">
    <meta name="description" content="A new Flutter project.">

    <!-- iOS meta tags & icons -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="bargain_front_end">
    <link rel="apple-touch-icon" href="icons/Icon-192.png">


    <title>Bargain</title>
    <link rel="manifest" href="manifest.json">

</head>

<body>
<link rel="stylesheet" href="/loading.css">
<div class="loader">
    <div class="box box0">
        <div></div>
    </div>
    <div class="box box1">
        <div></div>
    </div>
    <div class="box box2">
        <div></div>
    </div>
    <div class="box box3">
        <div></div>
    </div>
    <div class="box box4">
        <div></div>
    </div>
    <div class="box box5">
        <div></div>
    </div>
    <div class="box box6">
        <div></div>
    </div>
    <div class="box box7">
        <div></div>
    </div>
    <div class="ground">
        <div></div>
    </div>
</div>
<script src="https://www.gstatic.com/firebasejs/7.20.0/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.20.0/firebase-messaging.js"></script>

<script>
    var firebaseConfig = {
        apiKey: "AIzaSyD549M3BLzGa-_2CTpGyoFgLuOi399S4lU",
        authDomain: "bargain-c8e9e.firebaseapp.com",
        projectId: "bargain-c8e9e",
        storageBucket: "bargain-c8e9e.appspot.com",
        messagingSenderId: "1078129119742",
        appId: "1:1078129119742:web:c7f495a828a21f31fe30af",
        measurementId: "G-WSS0MDYY9B"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
</script>
<script>
    var serviceWorkerVersion = '3663094699';
    var scriptLoaded = false;

    function loadMainDartJs() {
        if (scriptLoaded) {
            return;
        }
        scriptLoaded = true;
        var scriptTag = document.createElement('script');
        scriptTag.src = 'main.dart.v2.js';
        scriptTag.type = 'application/javascript';

        document.body.append(scriptTag);
    }

    if ('serviceWorker' in navigator) {
        // Service workers are supported. Use them.
        window.addEventListener('load', function () {
            // Wait for registration to finish before dropping the <script> tag.
            // Otherwise, the browser will load the script multiple times,
            // potentially different versions.
            var serviceWorkerUrl = 'flutter_service_worker.js?v=' + serviceWorkerVersion;
            navigator.serviceWorker.register(serviceWorkerUrl)
                .then((reg) => {
                    function waitForActivation(serviceWorker) {
                        serviceWorker.addEventListener('statechange', () => {
                            if (serviceWorker.state == 'activated') {
                                console.log('Installed new service worker.');
                                loadMainDartJs();
                            }
                        });
                    }
                    if (!reg.active && (reg.installing || reg.waiting)) {
                        // No active web worker and we have installed or are installing
                        // one for the first time. Simply wait for it to activate.
                        waitForActivation(reg.installing ?? reg.waiting);
                    } else if (!reg.active.scriptURL.endsWith(serviceWorkerVersion)) {
                        // When the app updates the serviceWorkerVersion changes, so we
                        // need to ask the service worker to update.
                        console.log('New service worker available.');
                        reg.update();
                        waitForActivation(reg.installing);
                    } else {
                        // Existing service worker is still good.
                        console.log('Loading app from service worker.');
                        loadMainDartJs();
                    }
                });

            // If service worker doesn't succeed in a reasonable amount of time,
            // fallback to plaint <script> tag.
            setTimeout(() => {
                if (!scriptLoaded) {
                    console.warn(
                        'Failed to load app from service worker. Falling back to plain <script> tag.',
                    );
                    loadMainDartJs();
                }
            }, 4000);
        });
    } else {
        // Service workers not supported. Just drop the <script> tag.
        loadMainDartJs();
    }
</script>
</body>

</html>

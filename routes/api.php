<?php

use App\Mail\emailMessage;
use App\Models\Inqueries;
use App\Models\ReasonRejection;
use App\Models\User;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\TestController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\OrderController;
use App\Http\Controllers\Api\ListController;
use App\Http\Controllers\Api\OfferController;
use App\Http\Controllers\Api\InqueriesOfferController;
use App\Http\Controllers\Api\WalletController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\DynamicController;
use App\Http\Controllers\Api\SettingController;
use App\Http\Controllers\Api\PaymentController;
use App\Http\Controllers\Admin\AuthController as AdminAuthController ;
use App\Http\Controllers\Admin\CategoriesController as AdminCategoriesController ;
use App\Http\Controllers\Admin\CityController as AdminCityController ;
use App\Http\Controllers\Admin\DeliveryWayController as AdminDeliveryWayController ;
use App\Http\Controllers\Admin\IndustryController as AdminIndustryController ;
use App\Http\Controllers\Admin\WarrantyController as AdminWarrantyController ;
use App\Http\Controllers\Admin\OfferController as AdminOfferController ;
use App\Http\Controllers\Admin\OrderController as AdminOrderController ;
use App\Http\Controllers\Admin\InquerieController as AdminInquerieController ;
use App\Http\Controllers\Admin\ReasonRejectionController as AdminReasonRejectionController ;
use App\Http\Controllers\Admin\DynamicController as AdminDynamicController ;
use App\Http\Controllers\Admin\StaticDataController as AdminStaticDataController ;
use App\Http\Controllers\Admin\TermController as AdminTermController ;
use App\Http\Controllers\Admin\SettingController as AdminSettingController ;
use App\Http\Controllers\Admin\UserController as AdminUserController ;
use App\Http\Controllers\Admin\PromoCodesController as AdminPromoCodesController ;
use App\Http\Controllers\Admin\HomeController as AdminHomeController ;
use App\Http\Controllers\Admin\ContactUsController as AdminContactUsController ;
use App\Http\Controllers\Admin\InteractiveFormController as AdminInteractiveFormController ;
use App\Http\Controllers\Admin\HelperAdminController as AdminHelperAdminController ;
use App\Http\Controllers\Admin\PaymentController as AdminPaymentController ;
use App\Http\Controllers\Admin\TransactionController as AdminTransactionController;

Route::group([
    'prefix' => 'v1'
], function () {

    Route::post("test_notification",[TestController::class,'test_notification']);

    Route::group(['prefix' => 'Auth'], function () {
        Route::post('register',[AuthController::class, 'registerUser']);
        Route::post('login',[AuthController::class, 'login']);
        Route::get('get_test_code',[AuthController::class, 'get_test_code']);
        Route::get('user_info',[AuthController::class, 'user_info']);
        Route::post('forgetPassword',[AuthController::class, 'forgetPassword']);
        Route::post('resetPassword', [AuthController::class, 'resetPassword']);
        Route::post('checkCode', [AuthController::class, 'checkCodeActivation']);
        Route::post('checkCodeCorrect', [AuthController::class, 'checkCodeCorrect']);
        Route::post('resendCode', [AuthController::class, 'resendCode']);
        Route::post('changPassword', [AuthController::class, 'changPassword']);
        Route::post('editProfile', [AuthController::class, 'editProfile']);
        Route::post('logOut',[AuthController::class, 'logOut']);
        Route::post('refreshToken',[AuthController::class, 'refreshToken']);
    });

    Route::group(['prefix' => 'Home'],function (){
        Route::get('suggest_for_you',[HomeController::class,'suggest_for_you']);
        Route::get('all_orders',[HomeController::class,'all_orders']);
    });

    Route::resource('orders',OrderController::class);
    Route::post('orders/update_bidding_duration/{order}',[OrderController::class,'update_bidding_duration']);
    Route::post('orders/bidding_duration_golden_offer/{order}',[OrderController::class,'bidding_duration_golden_offer']);
    Route::post('orders/cancel_order/{order}',[OrderController::class,'cancel_order']);
    Route::post('orders/cancel_offer/{offer}',[OrderController::class,'cancel_offer']);
    Route::post('orders/finish/{order}',[OrderController::class,'finish']);
    Route::post('orders/accepted_offer/{offer}',[OrderController::class,'accepted_offer']);
    Route::post('orders/payment_wallet/{offer}',[OrderController::class,'accepted_offer_by_wallet']);
    Route::post('orders/choose_another_offer/{offer}',[OrderController::class,'choose_another_offer']);
    Route::post('orders/active_golden_offer/{order}',[OrderController::class,'active_golden_offer']);
    Route::post('orders/accepted_order/{order}',[OrderController::class,'accepted_order']);
    Route::post('orders/rate/{order}',[OrderController::class,'rate_order']);
    Route::post('orders/upload',[OrderController::class,'upload']);
    Route::post('orders/removeImage',[OrderController::class,'removeImage']);
    Route::get('list_previous_orders/{order}',[OrderController::class,'list_previous_orders']);


    Route::resource('offers',OfferController::class);
    Route::get('show_order_offer/{order}',[OfferController::class,'showOrderOffer']);
    Route::post('offers/cancel_offer/{offer}',[OfferController::class,'cancel_offer']);
    Route::post('offers/payment_wallet/{offer}',[OfferController::class,'accepted_offer_by_wallet']);
    Route::post('offers/rate/{order}',[OfferController::class,'rate_order']);

    Route::apiResource("inqueries",InqueriesOfferController::class);


    Route::group(['prefix' => 'lists'], function () {
        Route::get('categories',[ ListController::class, 'categories']);
        Route::get('cities',[ ListController::class, 'cities']);
        Route::get('search/categories',[ ListController::class, 'search_categories']);
        Route::get('delivery_ways',[ ListController::class, 'delivery_ways']);
        Route::get('industries',[ ListController::class, 'industries']);
        Route::get('industries',[ ListController::class, 'industries']);
        Route::get('warranties',[ ListController::class, 'warranties']);
        Route::get('payment_methods',[ ListController::class, 'payment_methods']);
        Route::get('reason_rejections',[ ListController::class, 'reason_rejections']);
    });

    Route::group(['prefix' => 'dynamic'], function () {
        Route::get('orders',[ DynamicController::class, 'orders']);
        Route::get('offers',[ DynamicController::class, 'offers']);
    });

    Route::post('category_follow/{category}',[UserController::class,'category_follow']);
    Route::get('my_wallet',[ WalletController::class ,'my_wallet']);
    Route::post('my_wallet/refund/{transaction}',[ WalletController::class ,'refund_requested']);
    Route::get('my_cart',[ WalletController::class ,'my_cart']);
    Route::post('send_email_by_cart',[ WalletController::class ,'send_email_by_cart']);
    Route::post('wallet/promo_code',[ WalletController::class ,'promo_code_wallet']);
    Route::get('notifications',[ UserController::class ,'list_notifications']);

    Route::group(['prefix' => 'setting'], function () {
        Route::get('intro_static_data',[SettingController::class,'intro_static_data']);
        Route::get('intro_golden_offer',[SettingController::class,'intro_golden_offer']);
        Route::get('common_questions',[SettingController::class,'common_questions']);
        Route::get('aboutUs',[SettingController::class,'aboutUs']);
        Route::get('terms',[SettingController::class,'terms_user']);
        Route::get('typesSupport',[SettingController::class,'getTypesSupport'] );
        Route::post('contact_us',[SettingController::class,'contact_us'] );
        Route::post('interactive_form',[SettingController::class,'interactive_form'] );
    });

    Route::group(['prefix' => 'payment'], function () {

        Route::get('orders/success/{offer}',[PaymentController::class,'orders_success']);
        Route::get('orders/error',[PaymentController::class,'orders_error']);
        Route::get('offers/success/{offer}',[PaymentController::class,'offers_success']);
        Route::get('offers/error',[PaymentController::class,'offers_error']);
        Route::get('wallet/charge/{user}',[PaymentController::class,'wallet_charge']);
        Route::get('wallet/error',[PaymentController::class,'wallet_error']);
    });


    Route::post("active_email",function (\Illuminate\Http\Request $request){

        $user = \App\Models\User::wherePhone($request->phone)->firstOrFail();
        $user->update(['is_active_email' => 1]);

        return new \App\Http\Resources\User\UserResource($user);
    });

    Route::post("active_my_email",function (\Illuminate\Http\Request $request){

        $user =  \App\Models\User::whereEmail($request->provider)->orWhere('phone',$request->provider)->firstOrFail();

        $user->update(['is_active_email' => 1]);

        return 'تم تفعيل الأكونت بنجاح';
    });

    Route::group(['prefix' => 'administrator' ,'middleware' => 'cors'], function () {

        Route::group(['prefix' => 'Auth'], function () {
            Route::post('login',[AdminAuthController::class, 'login']);
            Route::post('forgetPassword',[AdminAuthController::class, 'forgetPassword']);
            Route::post('check_code',[AdminAuthController::class, 'check_code']);
            Route::post('resendCode', [AdminAuthController::class, 'resendCode']);
            Route::post('changPassword', [AdminAuthController::class, 'changPassword']);
            Route::post('editProfile', [AdminAuthController::class, 'editProfile']);
            Route::post('logOut',[AdminAuthController::class, 'logOut']);
        });

        Route::resource('categories',AdminCategoriesController::class);
        Route::post("categories/{category}/suspend",[AdminCategoriesController::class,'suspendBoolean']);
        Route::post("categories/{category}/active",[AdminCategoriesController::class,'activeBoolean']);

        Route::resource('cities',AdminCityController::class);
        Route::post("cities/{city}/suspend",[AdminCityController::class,'suspendBoolean']);

        Route::resource('delivery_ways',AdminDeliveryWayController::class);
        Route::post("delivery_ways/{deliveryWay}/suspend",[AdminDeliveryWayController::class,'suspendBoolean']);

        Route::resource('industries',AdminIndustryController::class);
        Route::post("industries/{industry}/suspend",[AdminIndustryController::class,'suspendBoolean']);

        Route::resource('warranties',AdminWarrantyController::class);
        Route::post("warranties/{warranty}/suspend",[AdminWarrantyController::class,'suspendBoolean']);

        Route::resource('reason_rejections',AdminReasonRejectionController::class);
        Route::post("reason_rejections/{reasonRejection}/suspend",[AdminReasonRejectionController::class,'suspendBoolean']);

        Route::resource('payment_methods',AdminPaymentController::class);
        Route::post("payment_methods/{paymentMethod}/suspend",[AdminPaymentController::class,'suspendBoolean']);

        Route::resource('orders',AdminOrderController::class);
        Route::post("orders/{order}/accepted",[AdminOrderController::class,'acceptedOrder']);
        Route::post("orders/{order}/suspendOrder",[AdminOrderController::class,'suspendOrder']);
        Route::post("orders/{order}/refuse",[AdminOrderController::class,'refuseOrder']);
        Route::get("orders/{order}/details",[AdminOrderController::class,'details']);
        Route::post('orders/upload',[AdminOrderController::class,'upload']);

        Route::apiResource('offers',AdminOfferController::class);
        Route::post("offers/{offer}/accepted",[AdminOfferController::class,'acceptedOffer']);
        Route::post("offers/{offer}/refuse",[AdminOfferController::class,'refuseOffer']);

        Route::apiResource('inqueries',AdminInquerieController::class);
        Route::post("inqueries/{Inquerie}/accepted",[AdminInquerieController::class,'acceptedInquerie']);
        Route::post("inqueries/{Inquerie}/refuse",[AdminInquerieController::class,'refuseInquerie']);

        Route::resource("dynamic_options",AdminDynamicController::class);
        Route::resource("static_data",AdminStaticDataController::class);
        Route::resource("terms",AdminTermController::class);
        Route::resource("setting",AdminSettingController::class);
        Route::resource("users",AdminUserController::class);
        Route::get('search/users',[AdminUserController::class,'search_users']);
        Route::post("users/{user}/suspend",[AdminUserController::class,'suspendBoolean']);

        Route::get('list/wallet/refund_requested',[AdminUserController::class,'refund_requested']);
        Route::post('list/wallet/refund_requested/{transaction}/accepted',[AdminUserController::class,'accepted_refund_requested']);
        Route::post('list/wallet/refund_requested/{transaction}/refuse',[AdminUserController::class,'refuse_refund_requested']);

        Route::resource("promo_codes",AdminPromoCodesController::class);
        Route::get("list/users/promo_codes",[AdminPromoCodesController::class,'users_code']);
        Route::post("promo_codes/{promoCode}/suspend",[AdminPromoCodesController::class,'suspend']);

        Route::get("home",[AdminHomeController::class ,'index']);

        Route::resource("messages",AdminContactUsController::class);
        Route::post('messages/reply/{id}',[AdminContactUsController::class,'reply_to_email']);

        Route::get("interactive_forms",[AdminInteractiveFormController::class,"index"]);

        Route::resource("helper_admin",AdminHelperAdminController::class);
        Route::post("helper_admin/my_permission",[AdminHelperAdminController::class,'my_permission']);
        Route::post("helper_admin/{user}/suspend",[AdminHelperAdminController::class,'suspendBoolean']);

        Route::post('public_notification',[AdminSettingController::class ,'send_public_notification']);

        Route::get('transaction_process',AdminTransactionController::class);

        Route::get("order-export", [AdminUserController::class,'ExportOrders']);
        Route::get("user-export", [AdminUserController::class,'ExportUsers']);
        Route::get("get-code", [AdminUserController::class,'getActivationCode']);

    });


});

Route::get('testing_now',function (){

    $order = \App\Models\Order::with('user')->find(181);
    $user = $order->user;
    return view('emails.cart',compact('order','user'));
});

Route::post('testing_mail',function (\Illuminate\Http\Request $request){
    $reason_rejection = ReasonRejection::find($request->reason_rejection_id);
    $user = \App\Models\User::find(9);
    $order = \App\Models\Order::find(3);
    $message = [
        'message' =>     $reason_rejection->name . ' من المستخدم ' . $user->name . ' علي الطلب ' . $order->id
    ] ;
    Mail::to("ramyahmedhandousa@gmail.com")->send(new emailMessage($message));
    return $request->all();
});





Route::get("notify-order-user",function (){




//    return  \Carbon\Carbon::now()->toDateTimeString();
//    return  \Carbon\Carbon::now()->subHours(6)->toDateTimeString();

//    return \App\Models\Order::whereHas('user',function ($user){
//                            $user->where('disable_notifications', 0);
//                })->whereNull('offer_id')
//                ->whereHas('offers')->whereStatus('pending')
//               ->where('end_bidding_duration', '>',
//                    \Carbon\Carbon::now()->subHours(6)->toDateTimeString()
//               )->get(['id','time_out','is_pay','status','end_bidding_duration','created_at']);

//    $time_now = \Carbon\Carbon::now()->toDateTimeString();
//    $end_time_order = \Carbon\Carbon::now()->subHours(6)->toDateTimeString();
//
//   $orders =  \App\Models\Order::whereHas('user',function ($user){
//                            $user->where('disable_notifications', 0);
//                })->whereNull('offer_id')->whereNotifyBefore(0)
//                ->whereHas('offers')->whereStatus('pending')
//                ->whereBetween('end_bidding_duration',[$end_time_order , $time_now ])
//                ->with(['filter_user.devices'])
//                ->select(['id','user_id','notify_before'])
//                ->chunk(1, function ($orders) {
//                    foreach ($orders as $order){
//                        // update notify_before to make sure not send again
//
//                        $order->update(['notify_before' => 1 ]);
//                        $devices = $order->filter_user->devices?->pluck('device');
//
//                        \Illuminate\Support\Facades\Log::info('order_id ' . $order->id);
//                        \Illuminate\Support\Facades\Log::info($devices);
//                    }
//                });
//
//   return $orders;
});

Route::get("delete_all_users",function (){
//
    $users = User::whereNotIn('id',[47,77])->chunk(500, function ($user) {
        $user->each->delete();
    });



    return $users;

});




<?php

use App\Mail\emailMessage;
use App\Models\Setting;
use App\Models\TypesSupport;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers;
use Illuminate\Support\Str;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $language = \Illuminate\Support\Facades\Session::get('language');

    $session = $language ? : 'ar';

    \Illuminate\Support\Facades\App::setlocale($session);

//    return view('social_media.index' ,compact("session"));

    $types = TypesSupport::select('id')->where('is_suspend',0)->get();

    return view('landingpage' ,compact("session",'types'));
});

Route::get('/login/admin', function () {

    return view('welcome');
});


//Route::get('/orders', function () {
//    $orders = \App\Models\Order::where('is_accepted',0)->latest()->get();
//
//    return view('orders',compact('orders'));
//});


//Route::post("/order/accepted",[Controllers\IndexController::class , "accepted_order"])->name("order.accepted");

Route::post("/register_data",[Controllers\IndexController::class , "register_data"])->name("register_data");

//Route::get('/show/users/details', [Controllers\IndexController::class , "data_users"]);



//Route::get('/emails/newAccount',function (){
//    $user = \App\Models\User::find(8);
//    return view('emails.newAccount',compact('user'));
//});


//Route::get('/active/account',function (\Illuminate\Http\Request $request){
//
//    $token = substr($request->_token,40);
//
//    $user = \App\Models\User::whereApiToken($token)->with('devices')->firstOrFail();
//
//    $user->update(['is_active_email' => 1]);
//
//    return redirect()->route('wasActiveted');
//});

Route::get('/active/account',[Controllers\Api\UserController::class,'active_mail_user']);


Route::get('emails/wasActiveted',function (){
    return view('emails.wasActiveted');
})->name('wasActiveted');


Route::get("my_cart_email",function (){

    $user = \App\Models\User::find(9);
    $order = \App\Models\Order::find(23);

    return view("emails.cart",compact('user','order'));
});


Route::get("/update_trans",function (){

    $users = \App\Models\User::all();

    $price = 100000;
    foreach ($users as $user){
        \App\Models\Transaction::create(['user_id' => $user->id ,
            'progress' => "deposit_by_admin" ,'price' => $price ,'payment' => "wallet"]);
    }
});


Route::get('/change_language/{locale}', function ($locale) {

    \Illuminate\Support\Facades\Session::put('language', $locale);

    \Illuminate\Support\Facades\App::setLocale($locale);

    return app()->getLocale();
})->name("change_language");

Route::get('/landing_page', function () {

    $language = \Illuminate\Support\Facades\Session::get('language');

    $session = $language ? : 'ar';

    \Illuminate\Support\Facades\App::setlocale($session);

    $types = TypesSupport::select('id')->where('is_suspend',0)->get();

    return view('landingpage' ,compact("session",'types'));
});


Route::post('submit_contact_us',function (\Illuminate\Http\Request $request){

    $support = new \App\Models\Support();
    $support->user_id = 47;
    $support->name = $request->name;
    $support->phone = $request->phone;
    $support->email = $request->email;
    $support->type_id = $request->type_id;
    $support->message = $request->message;
    $support->save();

    $message = [
        'title'     => 'تواصل معنا',
        'type'      => TypesSupport::find( $request->type_id)->name,
        'user'      =>  '   من المستخدم  ' . $request->name,
        'message'   =>       $request->message
    ] ;

    try {
        Mail::to(Setting::getBody('email_bargain'))->send(new emailMessage($message));
    }catch (\Exception $exception){
        \Illuminate\Support\Facades\Log::info($exception);
    }

})->name('submit_contact_us');


Route::view('/chatting_room','testing');

Route::get("testing_cart_email",function (){

    $order = \App\Models\Order::find(178);

    $user = $order->user;

    return view('emails.cart',compact('order','user'));
});


Route::get('my_phones_ramy',function (){

    $users_phones = [];
    $users = \App\Models\User::select('id','phone')->get();

    foreach ($users as $key => $user){
        if (Str::startsWith($user->phone, ['009665','+966','966','5'])){

//        if (Str::startsWith($user->phone, ['009665'])){

//        if (Str::startsWith($user->phone, ['+966','966'])){

//        if (Str::startsWith($user->phone, ['5'])){

            $users_phones[$key] =  $user;
//            $users_phones[$key]['phone'] = Str::replaceFirst('009665', '05', $user->phone);

//            $users_phones[$key]['phone'] = Str::replaceFirst('+9665', '05', $user->phone);
//            $users_phones[$key]['phone'] = Str::replaceFirst('9665', '05', $user->phone);

//            $users_phones[$key]['phone'] = Str::replaceFirst('5', '05', $user->phone);
        }
    }

    $data = collect($users_phones)->values();

    return $data;

    foreach ($data as $datum){
        $user = \App\Models\User::find($datum->id);
        try {

            $user->update(['phone' => $datum->phone]);
        }catch (Exception $exception){

        }
    }
    return $data ;
});


Route::get('filter_my_email',function (\Illuminate\Http\Request $request){

    $user = \App\Models\User::whereEmail($request->email)->get(['id','phone','email']);

    return $user;
});


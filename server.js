const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require("socket.io")(server,{
    cors: {
        origin: "*",
        methods: ["GET", "POST"]
    }
});

// app.get('/', (req, res) => {
//     // res.sendFile( __dirname+'/views/chat.blade.php');
//     // console.log(__dirname+'/views/chat.blade.php')
//     // res.send("<h1>Ramy Handousa</h1>");
// });

io.on('connection', (socket) => {
    console.log('a user connected');

    socket.on('disconnect',(socket) => {
        console.log('Disconnected')
    });
});



server.listen(3000, () => {
    console.log('listening on *:3000');
});
